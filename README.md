# tenpo
## コンポーネント説明
    Yahoo!ロコの店舗詳細、プラン詳細のコンポーネントです。飲食店のみならず、式場やイベント情報なども取り扱います。

## エンドポイント一覧
| Name                 | Path                                  | IF設計書                             | エンドポイント仕様書                               | ResponseTime (msec) |
| :------------------- | :------------------------------------ | :----------------------------------- | :------------------------------------------------- | :------------------ |
| 店舗詳細ページ         | /place/g-\<gid\>                      | https://cptl.corp.yahoo.co.jp/x/dKzHVg | [店舗詳細ページ仕様書](doc/view/tenpoDetail.md)    | -                   |
| 店舗口コミページ       | /place/g-\<gid\>/review               | https://hogehoge                       | [口コミページ仕様書](doc/view/reviewList.md)     | -                   |
| 店舗口コミ詳細ページ   | /place/g-\<gid\>/review/\<reviewId\> | https://hogehoge                       | [口コミ詳細ページ仕様書](doc/view/reviewDetail.md)     | -                   |
| 店舗地図ページ       | /place/g-\<gid\>/map                  | https://cptl.corp.yahoo.co.jp/x/5z83Vg | [地図ページ仕様書](doc/view/map.md)                | -                   |
| 店舗写真ページ       | /place/g-\<gid\>/photo                | https://cptl.corp.yahoo.co.jp/x/EpxpVg | [写真ページ仕様書](doc/view/photo.md)              | -                   |
| 式場情報ページ       | /place/g-\<gid\>/marriage/            | https://cptl.corp.yahoo.co.jp/x/L7leVQ | [式場ページ仕様書](doc/view/wedding.md)           | -                   |
| 挙式プランページ      | /place/g-\<gid\>/wedding/            | https://cptl.corp.yahoo.co.jp/x/L7leVQ | [式場ページ仕様書](doc/view/wedding.md)           | -                   |
| ブライダルフェアページ | /place/g-\<gid\>/bridal/             | https://cptl.corp.yahoo.co.jp/x/L7leVQ | [式場ページ仕様書](doc/view/wedding.md)           | -                   |
| コース一覧ページ     | /place/g-\<gid\>/course/              | https://cptl.corp.yahoo.co.jp/x/0cXPV  | [コース一覧ページ仕様書](doc/view/course.md)       | -                   |
| コース詳細ページ     | /place/g-\<gid\>/course/P\<courseId\> | https://cptl.corp.yahoo.co.jp/x/cqfHVg | [コース詳細ページ仕様書](doc/view/courseDetail.md) | -                   |
| プラン一覧ページ     | /place/g-\<gid\>/plan/                | https://cptl.corp.yahoo.co.jp/x/hVNSVg | [プラン一覧ページ仕様書](doc/view/plan.md)         | -                   |
| クーポンページ       | /place/g-\<gid\>/coupon/              | https://cptl.corp.yahoo.co.jp/x/pCnOVg | [クーポンページ仕様書](doc/view/coupon.md)         | -                   |

## 設計思想
レイヤードアーキテクチャー<br>
アプリケーション層（UI層）、ドメイン層、インフラストラクチャ層の3層構造で、それぞれを疎結合にする

| レイヤー | 役割 |
| :-- | :-- |
| アプリケーション層（UI層） | リクエストの受付、ドメイン層を使ったデータの取得 |
| ドメイン層 | 業務ロジック<br>アプリケーション層にインフラストラクチャ層を使ったメソッドを提供する |
| インフラストラクチャ層 | 業務データの操作<br>IF(repository/domain)はドメイン層に定義する（この層のロジックが変わっても影響しない） |

参考：https://terasolunaorg.github.io/guideline/public_review/Overview/ApplicationLayering.html#id9

    ・なるべく「はじめてのSpringBoot」を意識した設計にしています。(リッチなモデルにするのではなく、Serviceクラスに業務ロジックを書く)
    ・本システムから参照する外部サービスのテスト用のスタブはOSSを利用するのではなく、自前で用意しています。「spring.profile.active」が「dev」もしくは「test」の時に動作します。
    ・単体テストは基本的にSpockで書きます。Spockで実装できないテストがある場合のみ、JUnitを利用してください。
    ・画面を返すエンドポイントでは、インフラ側で下記を実施しています。適宜、Thymeleaf側で利用してください。
      ・マストヘッドのHTMLを取得して、modelに追加(th:untextで利用してください)
      ・店舗情報を取得して、modelに追加
      ・eappIdを取得して、modelに追加
      ・ユーザーのプレミアム会員情報を取得して、modelに追加
      ・ULT用のページパラメータを取得して、modelに追加
    ・サニタイズはThymeleafの機構を利用します。
    ・テストレポートはSpockとGebそれぞれで出力しています。
    ・RequestHeader情報はLocoRequestHeaderクラス(リクエストスコープのオブジェクト)に入れています。適宜、Autowiredして利用してください。

## コーディング方針
    ・システム障害や共通系のExceptionは全て上位レイヤーにスローしてください。ExceptionHandlerで一括で処理します。
    ・Error系の例外はアプリでキャッチしてはいけません。こちらもExceptionHandlerで一括で処理します。
    ・単純な入力値チェックはBeanValidationを利用してください。
    ・ServiceクラスではRepositoryインターフェースをインジェクトして利用してください。実装はinfrastructureパッケージ配下で行ってください。
    ・Spockは単体テスト、内部結合テストで、GebはE2Eテストで利用してください。
    ・MiffyはMiffyUtilクラスを利用してBucketIdを取得し、ThymeleafでThymeleafコンポーネントの出し分けを行ってください。
    ・UIへ複数のEntityを返す際は、ControllerでEntityの数だけServiceの呼び出しを実施してください。
    ・domain層直下には、メインとなるEntityのみ定義してください。
    ・Stubファイルは基本 「src/main/resources/json/**」に入れます。
    ・groovyテストコードの「jp.co.yahoo.dining.frontend.e2e.**」はE2E用のテストコードを書く場所なので単体テストコードと混ぜないように気をつけてください。
    ・RequestMappingにPathVariableでgidを入れる時に必ず変数名をg-{gid}に設定してください。

## パッケージ構成
| 1st | 2nd  | 3rd       | 4th            | 5th        | description                                       |
| :-- | :--- | :-------- | :------------- | :--------- | :------------------------------                   |
| doc |      |           |                |            | 設計書フォルダ                                    |
|     | api  |           |                |            | JSONを返すAPIの設計書                             |
|     | view |           |                |            | UIを返すAPIの設計書                               |
| src |      |           |                |            |                                                   |
|     | main |           |                |            |                                                   |
|     |      | java      |                |            |                                                   |
|     |      |           | controller     |            | アプリケーション層（UI層） |
|     |      |           |                | api        | JSONを返すエンドポイント                  |
|     |      |           |                | view       | Viewを返すエンドポイント                    |
|     |      |           |                | form       | パラメータを保持する                  |
|     |      |           |                | constraint | Custom Bean Validation          |
|     |      |           | service        |            | ドメイン層<br>repositoryを使った業務ロジックを実装し、<br>アプリケーション層に提供する |
|     |      |           | repository     |            | ドメイン層<br>業務データを操作するメソッドのinterface<br>Serviceクラスに提供する |
|     |      |           | domain         |            | ドメイン層<br>アプリケーション層が操作するデータを保持する |
|     |      |           | infrastructure |            | インフラ層<br>ドメイン層の実装を提供する  |
|     |      |           |                | repositoryImpl | repositoryの実装、レスポンスはdomain |
|     |      |           |                | rest       | RestTemplate周り                |
|     |      |           |                | cassandra  | cassandraへのアクセス基盤                         |
|     |      |           |                | eappid     | eappid取得                                        |
|     |      |           |                | masthead   | masthead取得                                      |
|     |      |           |                | miffy      | miffy取得                                         |
|     |      |           |                | rest       | RestTemplate周り                                  |
|     |      |           |                | udb        | udb取得                                           |
|     |      |           |                | ups        | ups取得                                           |
|     |      | resources |                |            |                                                   |
|     |      |           | webapp         |            | [Frontend](./src/main/resources/webapp/README.md) |
|     | test |           |                |            |                                                   |
|     |      | java      |                |            | JUnitでしか担保出来ないテスト群                   |
|     |      | groovy    |                |            |                                                   |
|     |      |           | contract       |            | 外部APIテスト用のBaseClass                        |
|     |      |           | controller     |            | Controllerのテスト                                |
|     |      |           | usecase        |            | 結合テスト                                        |
|     |      |           | interfaces     |            | 単体テスト                                        |
|     |      |           | infrastructure |            |                                                   |
|     |      |           |                | rest       | RestTemplate周りのテスト                          |
|     |      | resources |                |            |                                                   |
|     |      |           | contract       |            | 外部APIのContructDSL                              |

参考：<br>
https://github.com/making/hajiboot-samples/tree/master/chapter03/3.2.3_hajiboot-rest
https://terasolunaorg.github.io/guideline/public_review/Overview/ApplicationLayering.html#id9

## 利用技術
| library                                   | description                                                                            |
|:------------------------------------------|:---------------------------------------------------------------------------------------|
| spring-boot-devtools                      | 開発補助ライブラリ                                                                     |
| spring-boot-starter-actuator              | 状態監視用ライブラリ                                                                   |
| spring-boot-starter-aop                   | 差し込み処理用ライブラリ。DIコンテナに登録されているクラスに対して差し込み処理が可能。 |
| spring-boot-starter-cache                 | キャッシュライブラリ                                                                   |
| spring-boot-starter-test                  | Springテスト用ライブラリ                                                               |
| spring-boot-starter-thymeleaf             | Web ページ向けテンプレートエンジン                                                        |
| spring-boot-starter-web                   | SpringWebMVC                                                                           |
| spring-boot-starter-data-cassandra        | cassandraを利用する為のライブラリ                                                          |
| lombok                                    | getter, setter等の自動生成ライブラリ                                                   |
| groovy-all                                | Groovyを利用するためのライブラリ                                                       |
| spock-core                                | Spockを利用するためのライブラリ                                                        |
| spock-spring                              | SpringでSpockを利用するためのライブラリ                                                |

## インフラ層説明
|                       Task                       |                                                                                                       Class名                                                                                                       |
|--------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| エラーハンドリング                               | [RestErrorHandler.class](./src/main/java/jp/co/yahoo/dining/frontend/controller/api/RestErrorHandler.java) <br> [ErrorHandler.class](./src/main/java/jp/co/yahoo/dining/frontend/controller/view/ErrorHandler.java) |
| リクエストヘッダー情報(セッション、デバイス情報) | [LocoRequestHeader](./src/main/java/jp/co/yahoo/dining/frontend/controller/LocoRequestHeader.java)                                                                                                                  |
| 入力値チェック                                   | 標準のValidationクラス + [constraintパッケージ](./src/main/java/jp/co/yahoo/dining/frontend/controller/constraint)                                                                                            |
| コントローラー差し込み処理                       | [GlobalControllerAdvice.java](./src/main/java/jp/co/yahoo/dining/frontend/controller/GlobalControllerAdvice.java)                                                                                                   |
| ORM                                              | 今回はなし                                                                                                                                                                                                          |
| トランザクション                                 | 今回はなし                                                                                                                                                                                                          |

## 横断的な保持情報

## ログ設計
* [ログレベル設計書](https://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1473116415)


## リリース方法
### DEV
    cf push -f manifest-dev.yml
### STG
    cf push -f manifest-stg.yml
### PROD
    cf push -f manifest-prod.yml

## ローカル起動方法
### デザインのテンプレートの確認のための起動方法
    $ cd src/main/resources/webapp/
    $ npm install （初回のみ）
    $ npm run dev
    # http://localhost:8090/place/index 他にアクセス
    
### スタブデータを使用したアプリケーションの動作確認のための起動方法
    $ cd src/main/resources/webapp/
    $ npm install （初回のみ）
    $ npm run dev
    # IDE で DiningLocoTenpoApplication を実行
    # http://localhost:8080/place/g-CGBtPVWV6EE 他にアクセス
### 本番データでの挙動確認のための起動方法
    $ cd src/main/resources/webapp/
    $ npm install （初回のみ）
    # 全データ更新(更新ファイル数が多い場合)
    $ npm run build local-prod
    # 部分更新(更新ファイル数が少ない場合)
    # 部分更新 or 全データ更新のどちらかを実施。npm run dev はしない。
    $ npm run build-local-prod --files=src/pc/pages/place/index.html
    # SpringBoot の run config の active profiles を local-prod で起動
    # IDE で DiningLocoTenpoApplication を実行
    # http://localhost:8080/place/g-CGBtPVWV6EE 他にアクセス

## 環境ごとの本番との差分
| Env   | description                   |
| :---- | :----------------------       |
| local | ・ ログレベルがDebugになる       |
|       | ・ ExternalAPIはモックを利用     |
|       | ・ Spring-Actuatorフルで有効    |
| Dev   |                               |
|       |                               |
| STG   |                               |
|       |                               |
| Prod  |                               |
|       |                               |

## ローカル開発時のサンプルURL
| View              | Url                                                      | 備考 |
| :---------------- | :---------------------------------                       | ---|
| 店舗詳細        | http://localhost:8080/place/g-CGBtPVWV6EE/ | |
| 店舗詳細        | http://localhost:8080/place/g-CQgD-fVDZ_M/ | CPクーポンあり |
| 店舗詳細        | http://localhost:8080/place/g-PaOkPxkI4Q6/ | Yクーポンあり |
| コース一覧        | http://localhost:8080/place/g-1cT7Evr4n5Y/ ||
| コース詳細        | http://localhost:8080/place/g-bRvYxu7yzo6/course/P837104 ||
※ src/main/resources/json/tenpo 参照

## Stubデータ一覧
    同期で呼び出すAPIのStubデータのEndpoint一覧
| EndPoint   | description                           |
| :---------------- | :---------------------------------   |
| /mock/tenpo       | 店舗の情報取得API (be.lsbe.yahoo.co.jp)|
| /mock/cpcoupon | LocalSearchAPIからCPCoupon取得API (search.olp.yahooapis.jp) |
| /mock/coupon   | YCoupon+CpCouponをMashUpしたAPI  |
| /mock/plan/list   | プラン一覧取得API（reservation-restaurant.yahooapis.jp） |
| /mock/plan/detail | プラン詳細取得API（reservation-restaurant.yahooapis.jp） |
| /mock/video       | ビデオ情報取得API (feapi-yvpub.yahooapis.jp)            |

### Stubテスト用Key一覧
    ・店舗情報 ＋ クーポン情報 （GID）
      ・クーポンデータない店舗 ： CGBtPVWV6EE
      ・CPクーポン済み店舗 ： CQgD-fVDZ_M, aRQ5tISjHjA
      ・CP+Yクーポン済み店舗 ： PaOkPxkI4Q6
    ・プラン一覧 （GID）
      ・1cT7Evr4n5Y
    ・プラン詳細 （店舗ID / プランID）
      ・s210000602 / 39781
    ・ビデオ情報 （UID）
      ・d27a0167a68e6bf27bfee3a45a68d5266b98ce6b

## 開発の流れ(Zuul導入後)：

1. 先に npm run dev する
2. SpringBoot を起動する
3. View 側の修正を行う
    - Thymeleaf のバイディング
    - Vuejs の修正
4. SpringBoot を再起動する
    - Thymeleaf の修正があった時のみ
5. http://localhost:8080/place/g-GID にアクセスして動作確認する
6. 3 ~ 5 を繰り返す

[参考](http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1459086406)

---

### 基本リリース手順
##### サーバ構成 (　全社 PaaS　)
|  環境種別   |  管理ページ 　  |    インスタンス数     |    PCF Route     |　備考 |
|-----------|----------------|--------------------|------------| ---- |
| MiniY | https://apps.system.dev02.ssk.cfm.yahoo.co.jp | 1 | https://tenpo-dev.dev02.ssk.cfm.yahoo.co.jp | アプリケーション |
|       | |  |             https://tenpo-coverage.dev02.ssk.cfm.yahoo.co.jp | UT カバレッジ |
|       | |  |            https://tenpo-coverage.dev02.ssk.cfm.yahoo.co.jp | IT カバレッジ |
| Stage | https://apps.system.odin.ssk.cfp.yahoo.co.jp  | 1 | https://tenpo-dark.odin.ssk.cfp.yahoo.co.jp | アプリケーション |
| 本番   |       |           ? |  https://tenpo.odin.ssk.cfp.yahoo.co.jp | アプリケーション |


##### CI / CD ツール
* Concourse : http://concourse.corp.yahoo.co.jp/teams/dining-loco/pipelines/tenpo
* ConcourseのCredential情報 : https://partner.git.corp.yahoo.co.jp/dining-loco-admin/tenpo-concourse-credential

### リリース手順
#### リリース手順コンフル
* Concourseを使ってリリースする手順書 : https://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1512409949

##### Pull Request チェック
* Partner Git で Pull Requestを登録します。**（手動）**
* Concourseがそのブランチにリスクチェック、UTを実行して異常があるか検査します。
* もし異常が見つかったら、Concourseのパイプラインが赤色になって、Pull Requestにも警告通知してくれます。
* 異常が見つかれなかったら、Concourseのパイプラインが青色になって、Pull Requestにも正常通知してくれます。

##### パッケージビルド
* メインブランチ(Master)でブランチマージまたはプッシュ等、コミットします。**（手動）**
* Concourseが自動でビルドを始まります。
* リスクチェック、UTを実行して異常が見つかったらConcourseのパイプラインが赤色になって、ビルドを中止します。
* 異常が見つかれなかったらUTカバレッジを更新します。(https://tenpo-coverage.dev02.ssk.cfm.yahoo.co.jp)
* そしてビルドした結果パッケージをDragonでアップロードするし、Partner Gitに新しいバージョンのタグが生成できます。

##### MiniY + Staging リリース
* パッケージビルドが成功したら、自動でMiniY + Stagingにリリースが実行されます。
* Dragonからパッケージを取ってきて、そのパッケージをPCF MiniY + Stagingにプッシュします。
* 異常が発生したら、Concourseのパイプラインが赤色になって、PCF管理ページでAppがCrashと表示になります。
* 無事にプッシュが完了になったら、Concourseのパイプラインが青色になって、PCF管理ページでAppがRunと表示になります。

##### 本番リリース
* Partner Gitでビルドの時に生成になったタグをそのままリリースパッケージに登録したら、本番リリースが実行されます。**（手動）**
* Dragonからパッケージを取ってきて、そのパッケージをPCF Odinにプッシュします。
* 異常が発生したら、Concourseのパイプラインが赤色になって、PCF管理ページでAppがCrashと表示になります。
* 無事にプッシュが完了になったら、Concourseのパイプラインが青色になって、PCF管理ページでAppがRunと表示になります。

備考：**（手動）** が付いてないところは基本自動で行われます。


#### 監視
* 監視は KANSEITO を使います
    - ルール: http://kanseito.corp.yahoo.co.jp/#/2.0/settings/rules/7497
    - グループ: http://kanseito.corp.yahoo.co.jp/#/2.0/contact/7548/hosts
        - stg: dining-loco-dark-prod.tenpo.dining-loco.odin.ssk.cfp.yahoo.co.jp
        - 本番: dining-loco-prod.tenpo.dining-loco.odin.ssk.cfp.yahoo.co.jp
    - 連絡先: http://sumatsu.corp.yahoo.co.jp/groups/1936