'use strict'
const chalk = require('chalk')
const express = require('express')
const glob = require('glob')
const path = require('path')
const request = require('request')
const app = express()
const port = process.env.PORT ? process.env.PORT : 8090
const isDev = process.env.NODE_ENV !== 'production'

function resolve (dir) {
  return path.resolve(__dirname, dir)
}

function middlewareLoader () {
  const webpack = require('webpack')
  const webpackConfig = require('./build/webpack.config.dev')

  const compiler = webpack(webpackConfig)
  const devMiddleware = require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    quiet: true,
    dynamicPublicPath: true
  })
  app.use(devMiddleware)

  const hotMiddleware = require('webpack-hot-middleware')(compiler, {
    log: false,
    heartbeat: 5000
  })

  app.use(hotMiddleware)

  const watcher = require('chokidar').watch(
    glob.sync(path.join(__dirname, 'src', '@(pc|sp)', '@(pages|styles)', '**/*.@(html|scss)'))
  )
  watcher.on('ready', () => {
    console.log('  ホットリロード...')
  })
  watcher.on('change', (path) => {
    console.log(`${path} に変更がありました`)
    hotMiddleware.publish({ action: 'reload' })
  })
}

function customRouter () {
  let device = 'pc'
  let userAgent = ''
  app.use(require('express-useragent').express())
  app.use('/', (req, res, next) => {
    device = (req.useragent.isMobile) ? 'sp' : 'pc'
    userAgent = req.headers['user-agent']
    next()
  })
  let urlForwarding = ''
  app.get('*', (req, res) => {
    const pathArr = req.path.split('/')
    pathArr.shift()
    let dir = 'html'
    let filePath = ''

    if (['pc', 'sp'].includes(pathArr[0])) {
      pathArr.shift()
    }

    // img assets 対応
    if (['images'].includes(pathArr[0])) {
      const mainFolder = isDev ? 'src' : 'public'
      filePath = pathArr.filter((v) => v.length > 0).join('/')
      const finalPath = `${mainFolder}/${device}/${filePath}`
      console.log(finalPath)
      res.sendFile(resolve(`${finalPath}`))
      return
    }

    if (['js', 'css', 'html'].includes(pathArr[0])) {
      dir = pathArr[0]
      pathArr.shift()
    }

    filePath = pathArr.filter((v) => v.length > 0).join('/')
    const ext = filePath.split('.').length > 1 ? '' : `.${dir}`
    filePath = (filePath.length === 0 && ext === '.html') ? 'index' : filePath
    if (isDev) {
      // 開発router
      let finalPath = ''

      if (ext === '.html') {
        // htmlWebpackPlugins alwaysWriteToDisk 対応
        const finalPath = `public/${device}/${dir}/${filePath}${ext}`
        res.sendFile(resolve(`${finalPath}`))
        return
      }

      if (filePath.split('.').includes('hot-update')) {
        finalPath = `${device}/${filePath}${ext}`
      } else {
        finalPath = `${device}/${dir}/${filePath}${ext}`
      }

      const fullUrlPath = `${req.protocol}://${req.get('host')}/${finalPath}`
      if (urlForwarding === fullUrlPath) {
        urlForwarding = ''
        return true
      }

      urlForwarding = fullUrlPath
      const promiseRequest = new Promise((resolve, reject) => {
        request({
          url: fullUrlPath,
          headers: {
            'user-agent': userAgent
          },
          timeout: 1500,
          forever: false
        }, (error, response, body) => {
          if (error) {
            reject(error)
          } else {
            resolve({ response, body })
          }
        })
      })
      promiseRequest
        .then((data) => {
          const { response, body } = data
          res.status(response.statusCode).send(body)
        })
        .catch((error) => {
          res.status(404).send('ERROR: ' + JSON.stringify(error))
        })
        .then(() => {
          urlForwarding = '' // reset
        })
    } else {
      // 本番router
      const finalPath = `public/${device}/${dir}/${filePath}${ext}`
      res.sendFile(resolve(`${finalPath}`))
    }
  })
}

function prepareRouter () {
  if (isDev) {
    middlewareLoader()
  }
  customRouter()
}

app.use('/localapi', express.static(resolve('../json'), {
  extensions: ['json']
}))
app.use('/dist', express.static(resolve('public/dist'), {
  extensions: ['json'],
  setHeaders: (res, path, stat) => {
    if (path.indexOf('.svgz') !== -1) {
      res.setHeader({ 'Access-Control-Allow-Origin': '*' })
      res.setHeader({ 'Content-Type': 'image/svg+xml' })
      res.setHeader({ 'Content-Encoding': 'gzip' })
    }
  }
}))

try {
  prepareRouter()
  app.listen(port)
  if (!isDev) {
    console.log(chalk.black.bgYellow.bold(' http://localhost:8090 '), chalk.yellow(' にアクセスしてください。'))
  } else {
    console.log(chalk.white.bold('  ビルド中...'))
  }
} catch (error) {
  console.log(chalk.red.bold(' サーバー起動にエラーが発生しました！'))
}
