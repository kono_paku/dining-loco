'use strict'
const bootstrap = require('./bootstrap')
const cheerio = require('cheerio')
const fs = require('fs')

function CustomHtmlWebpack () {}

CustomHtmlWebpack.prototype.apply = function (compiler) {
  compiler.plugin('compilation', function (compilation) {
    compilation.plugin('html-webpack-plugin-before-html-processing', function (htmlPluginData, callback) {
      if (process.env.NODE_ENV === 'production') {
        const device = htmlPluginData.outputName.split('/').slice(0, 1)
        htmlPluginData.assets.css.unshift(`${htmlPluginData.assets.publicPath}${bootstrap.imgCenterVer}/${device}/css/common.css`)
        htmlPluginData.assets.js.unshift(`${htmlPluginData.assets.publicPath}${bootstrap.imgCenterVer}/${device}/js/vendor.bundle.js`)
        htmlPluginData.assets.js.unshift(`${htmlPluginData.assets.publicPath}${bootstrap.imgCenterVer}/${device}/js/commonchunk.bundle.js`)
      } else {
        htmlPluginData.assets.css.unshift(`${htmlPluginData.assets.publicPath}css/common.css`)
        htmlPluginData.assets.js.unshift(`${htmlPluginData.assets.publicPath}js/vendor.bundle.js`)
        htmlPluginData.assets.js.unshift(`${htmlPluginData.assets.publicPath}js/commonchunk.bundle.js`)
      }
      callback(null, htmlPluginData)
    })
    compilation.plugin('html-webpack-plugin-after-html-processing', function (htmlPluginData, callback) {
      if (process.env.NODE_ENV === 'production') {
        // 広告スクリプトをbody最後に移動
        const $ = cheerio.load(htmlPluginData.html)
        const yjAdsJsWrapper = $('.yjAdsJsWrapper')
        if (yjAdsJsWrapper !== null) {
          $('body').append(yjAdsJsWrapper)
        }

        // PVTagスクリプトをbody最後に追加
        const device = htmlPluginData.outputName.split('/').slice(0, 1)
        const pvTag = fs.readFileSync(`src/${device}/pages/partial/common/pvTag.html`, 'utf8')
        $('body').append(pvTag)

        htmlPluginData.html = $.html()
      }
      callback(null, htmlPluginData)
    })
  })
}

module.exports = CustomHtmlWebpack
