'use strict'
const baseArray = require('./webpack.config.base')
const chalk = require('chalk')
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
const webpack = require('webpack')
const merge = require('webpack-merge')

// const deviceOrder = ['pc', 'sp']

let webpackModules = []
baseArray.forEach((base, index) => {
  Object.keys(base.entry).forEach(function (name) {
    let hotMiddlewareDevice = `./hotreload/client-${base.name}`
    base.entry[name] = [hotMiddlewareDevice].concat(base.entry[name])
  })
  webpackModules.push(
    merge(base, {
      devtool: '#cheap-module-source-map',
      plugins: [
        new webpack.DefinePlugin({
          'process.env.NODE_ENV': '"development"'
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new FriendlyErrorsPlugin({
          compilationSuccessInfo: {
            messages: [`${chalk.black.bgYellow.bold(' http://localhost:8090 ')}${chalk.yellow(' にアクセスしてください。')}`]
          }
        })
      ]
    })
  )
})

module.exports = webpackModules
