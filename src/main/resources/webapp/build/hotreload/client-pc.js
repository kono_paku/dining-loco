'use strict'
const webpackHotMiddlewareClient = require('webpack-hot-middleware/client?name=pc&path=http://localhost:8090/__webpack_hmr&quiet=true')

webpackHotMiddlewareClient.subscribe((payload) => {
  if (payload.action === 'reload' || payload.reload === true) {
    window.location.reload()
  }
})

module.exports = webpackHotMiddlewareClient
