'use strict'
const path = require('path')
const prompt = require('prompt')
const Rsync = require('rsync')
const config = require('../config.json')

module.exports = {
  imgCenterUrl: typeof process.env.ASSET_PATH !== 'undefined' ? process.env.ASSET_PATH === '/' ? '' : process.env.ASSET_PATH : 'https://s.yimg.jp',
  imgCenterServer: 'w201.imgctr.ci.bbt.yahoo.co.jp',
  imgCenterRoot: '/home/y/share/img-tool/htdocs',
  imgCenterPath: config.imgCenterPath,
  imgCenterVer: config.distVersion.replace(/\-/g, '/'),
  promptImgCenterSetting: function (_schema = null, cb) {
    const schema = _schema !== null ? _schema : {
      properties: {
        imgCenterVer: {
          description: '出力先のフォルダー名',
          type: 'string',
          default: this.imgCenterVer,
          required: true
        },
        imgCenterPath: {
          description: 'イメージセンターの格納パス',
          type: 'string',
          default: this.imgCenterPath,
          required: true
        }
      }
    }

    // concourseのために
    if (typeof process.env.BUILD_TYPE !== 'undefined' && ['ci', 'imgCenter'].includes(process.env.BUILD_TYPE)) {
      return new Promise((resolve, reject) => {
        this.imgCenterVer = this.imgCenterVer.split('/').filter(x => !!x).join('/')
        this.imgCenterPath = '/' + this.imgCenterPath.split('/').filter(x => !!x).join('/')
        if (typeof cb === 'function') {
          cb(this)
        }
        resolve(this)
      })
    }

    // コマンドラインパラメーター指定対応
    const args = process.argv.slice(2)
    if (args.length === 3) {
      console.log('コマンドラインパラメーターの指定がありました！' + args)
      return new Promise((resolve, reject) => {
        this.imgCenterVer = args[0].split('/').filter(x => !!x).join('/')
        this.imgCenterPath = '/' + args[1].split('/').filter(x => !!x).join('/')
        this.imgCenterPath = this.imgCenterPath == '/' ? '' : this.imgCenterPath
        if (typeof cb === 'function') {
          cb(this)
        }
        resolve(this)
      })
    }

    // default => prompt
    prompt.start()
    return new Promise((resolve, reject) => {
      prompt.get(schema, (err, result) => {
        if (err) {
          reject(err)
        }
        this.imgCenterVer = result.imgCenterVer.split('/').filter(x => !!x).join('/')
        this.imgCenterPath = '/' + result.imgCenterPath.split('/').filter(x => !!x).join('/')
        if (typeof cb === 'function') {
          cb(this)
        }
        resolve(this)
      })
    })
  },
  promptImgCenterUpload: function () {
    const schema = {
      properties: {
        needUpload: {
          description: 'イメージセンターにアップロードしますか？(y/n)',
          type: 'string',
          default: 'n',
          required: true
        }
      }
    }

    // concourseのために
    if (typeof process.env.BUILD_TYPE !== 'undefined') {
      let needUpload = 'n'
      if (process.env.BUILD_TYPE === 'imgCenter' && ['ci', 'imgCenter'].includes(process.env.BUILD_TYPE)) {
        needUpload = 'y'
      }
      return new Promise((resolve, reject) => {
        let result = { needUpload: needUpload }
        resolve(result)
      })
    }

    // コマンドラインパラメーター指定対応（concourseのために）
    const args = process.argv.slice(2)
    if (args.length === 3) {
      return new Promise((resolve, reject) => {
        let result = { needUpload: args[2] }
        resolve(result)
      })
    }

    prompt.start()

    return new Promise((resolve, reject) => {
      prompt.get(schema, (err, result) => {
        if (err) {
          reject(err)
        }
        resolve(result)
      })
    })
  },
  uploadToImgCenter: function (_bootstrap, cb) {
    let destFolder = _bootstrap.imgCenterVer.split('/').slice(0, -1).join('/')
    destFolder = destFolder !== '' ? '/' + destFolder : destFolder
    const rsync = new Rsync()
      .shell('ssh')
      .flags('azpO')
      .set('delete')
      .set('chmod=ugo=rwX')
      .source(path.join(__dirname, '..', `public/${_bootstrap.imgCenterVer}`))
      .destination(`${_bootstrap.imgCenterServer}:${_bootstrap.imgCenterRoot}${_bootstrap.imgCenterPath}${destFolder}`)

    rsync.execute((error, code, cmd) => {
      if (error) {
        console.log('Error while executing rsync.', error)
      } else {
        console.log()
        console.log('アップロード完了')
        if (typeof cb === 'function') {
          cb()
        }
      }
    })
  }
}
