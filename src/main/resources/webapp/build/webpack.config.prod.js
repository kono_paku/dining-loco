'use strict'
const baseArray = require('./webpack.config.base')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
// const CopyWebpackPlugin = require('copy-webpack-plugin')
const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const bootstrap = require('./bootstrap')

function resolve(dir) {
  return path.join(__dirname, '..', dir)
}

function onComplete(result) {
  let destFolder = result.imgCenterVer.split('/').slice(0, -1).join('/')
  destFolder = destFolder !== '' ? '/' + destFolder : destFolder
  console.log(`\n> html以外のファイルの出力先を[./public/${result.imgCenterVer}]に設定しました。`)
  console.log(`> イメージセンターのパスを[${result.imgCenterUrl}${result.imgCenterPath}${destFolder}]に設定しました。\n`)
}

function filesChosen() {
  let files = []
  if (typeof process.env.npm_config_files !== 'undefined') {
    files = [].concat(...process.env.npm_config_files
      .split(',')
      .map(f => {
        f = f.split('/').filter(s => s.length > 0)
        f[f.length - 1] = f[f.length - 1].split('.')[0]
        f = f.join('/')
        return [resolve(f + '.html'), resolve(f + '.js')]
      })
    )
  }
  return files
}

function assembleWebpackModule() {
  return bootstrap.promptImgCenterSetting(null, onComplete)
    .then(result => {
      let webpackModules = []
      baseArray.forEach(base => {
        base.plugins = base.plugins.filter(plugin => plugin.constructor.name !== 'ExtractTextPlugin') // extractTextPlugin を修正したくてオブジェクトになっているためリセットして新しいインスタンスを作成する

        // modify paths for production
        // イメージセンター対応のため
        //   file-loaderの出力パス修正
        base.module.rules.forEach(rule => {
          if (typeof rule.use !== 'undefined' && typeof rule.use.loader === 'string' && rule.use.loader === 'file-loader') {
            rule.use.options = {
              outputPath: `/${bootstrap.imgCenterVer}`,
              publicPath: `${bootstrap.imgCenterUrl}${bootstrap.imgCenterPath}`,
              name: f => {
                // @todo use regex
                if (f.indexOf(resolve('src')) !== -1) {
                  return f.replace(resolve('src'), '/asset')
                } else if (f.indexOf(resolve('asset')) !== -1) {
                  return f.replace(resolve('asset'), '/asset')
                }
                return '[name].[ext]'
              }
            }
          }

          // change css/foo.css to dist/pc|sp/css/foo.js
          if (typeof rule.use !== 'undefined' && typeof rule.use === 'object'
            && Object.keys(rule.use).length > 0 && typeof rule.use[0] !== 'undefined' && typeof rule.use[0].loader !== 'undefined'
            && rule.use[0].loader === resolve('node_modules/extract-text-webpack-plugin/dist/loader.js')) {
            const extractSass = new ExtractTextPlugin({
              filename: `${bootstrap.imgCenterVer}/${base.name}/css/[name].css`, // change css/foo.css to dist/pc|sp/css/foo.js
              allChunks: true
            })
            rule.use = extractSass.extract({
              use: [
                'css-loader',
                'sass-loader',
                'postcss-loader'
              ],
              fallback: 'vue-style-loader'
            })
            base.plugins.push(extractSass)
          }
        })
        base.output.publicPath = `${bootstrap.imgCenterUrl}${bootstrap.imgCenterPath}/` // change /pc/ to http://xxxx/foo/bar/
        base.output.path = base.output.path.replace(`/webapp/public/${base.name}`, '/webapp/public') // change publipath /webapp/public/pc|sp to /webapp/public
        base.output.filename = `${bootstrap.imgCenterVer}/${base.name}/${base.output.filename}` // change js/foo.js to dist/pc|sp/js/foo.js
        base.output.chunkFilename = `${bootstrap.imgCenterVer}/${base.name}/${base.output.chunkFilename}` // change js/foo.js to dist/pc|sp/js/foo.js
        // filter out unrelated files when files option is defined
        if (filesChosen().length > 0) {
          let newEntry = {}
          for (let entryKey in base.entry) {
            if (filesChosen().includes(base.entry[entryKey]) || entryKey === 'common') {
              newEntry[entryKey] = base.entry[entryKey]
            }
          }
          base.entry = newEntry
          base.plugins = base.plugins.filter(plugin => !(plugin.constructor.name === 'HtmlWebpackPlugin' && !filesChosen().includes(plugin.options.template)))
        }
        base.plugins.forEach(plugin => {
          if (plugin.constructor.name === 'HtmlWebpackPlugin') {
            plugin.options.filename = `${base.name}/${plugin.options.filename}` // change html/foo/bar.html to pc|sp/html/foo/bar.html
          }
        })

        // entry が空の場合ビルドしない
        if (Object.keys(base.entry).length > 1) {
          Object.keys(base.entry).forEach(function (name) {
            base.entry[name] = ['babel-polyfill'].concat(base.entry[name])
          })
          webpackModules.push(
            merge(base, {
              devtool: false,
              plugins: [
                new webpack.DefinePlugin({
                  'process.env.NODE_ENV': '"production"'
                }),
                new webpack.optimize.UglifyJsPlugin({
                  compress: { warnings: false }
                }),
                new webpack.optimize.ModuleConcatenationPlugin()
              ]
            })
          )
        }
      })
      return { webpackModules, bootstrap }
    })
    .catch(err => {
      console.log('Error building production. ' + err)
    })
}

module.exports = assembleWebpackModule()
