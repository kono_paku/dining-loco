'use strict'
const chalk = require('chalk')
const path = require('path')
const rm = require('rimraf')
// const ProgressBarPlugin = require('progress-bar-webpack-plugin')
const webpack = require('webpack')

const isProd = process.env.NODE_ENV === 'production'

const builder = (webpackConfig) => {
  return new Promise((resolve, reject) => {
    let compiler = webpack(webpackConfig, (err, stats) => {
      if (err) reject(err)

      // process.stdout.write(stats.toString({
      //   colors: true,
      //   modules: false,
      //   children: false,
      //   chunks: false,
      //   chunkModules: false,
      //   hash: false
      // }) + '\n\n')

      // @tofix 問題：webpack entry が css の場合 js も書き出されてる
      // ↓いい方法があれば、、、
      // @todo pc => all device loop
      // rm(path.resolve(__dirname, `../public/pc/js/common.bundle.js`), (err) => {
      //   if (err) throw err
      //   console.log(chalk.redBright('js/common.bundle.js\t[deleted]'))
      // })
      resolve(compiler)
    })
  })
}

function onComplete() {
  console.log(chalk.black.bgGreen(' DONE '))
}

function onStart() {
  return new Promise((resolve, reject) => {
    // ビルドファイルの指定がない時だけpublicを削除する
    if (typeof process.env.npm_config_files === 'undefined') {
      rm(path.join(__dirname, '..', 'public'), resolve)
    } else {
      resolve(true)
    }
  })
}

async function asyncBuilder() {
  await onStart()
  let webpackConfigArray = []
  let { webpackModules, bootstrap } = await require('./webpack.config.prod')
  if (isProd) {
    // webpackConfigArray = await require('./webpack.config.prod')
    webpackConfigArray = webpackModules
  } else {
    webpackConfigArray = require('./webpack.config.dev')
  }
  let compilers = []

  // for (const [, webpackConfig] of webpackConfigArray.entries()) {
  //   webpackConfig.plugins.push(
  //     new ProgressBarPlugin({
  //       format: `  [${webpackConfig.name}]ビルド中 [:bar] ${chalk.green.bold(':percent')} (:elapsed seconds)`,
  //       summary: false,
  //       summaryContent: chalk.green.bold(`  [${webpackConfig.name}]ビルド完了しました。`)
  //     })
  //   )
  //   compilers.push(await builder(webpackConfig))
  // }
  // console.log(chalk.black.bgGreen(' DONE '), chalk.black.bgYellow.bold(' npm run prod '), chalk.yellow('で動作確認することができます。\n'))
  console.log(chalk.yellow('ビルド頑張ってる最中...(PCによって2分ほどかかります)'))
  console.time('webpackBuildTime')
  await builder(webpackConfigArray)
  console.timeEnd('webpackBuildTime')
  console.log()
  await bootstrap.promptImgCenterUpload()
    .then(result => {
      if (result.needUpload.toLowerCase() === 'y') {
        console.log()
        bootstrap.uploadToImgCenter(bootstrap, onComplete)
      } else {
        console.log()
        onComplete()
      }
    })
    .catch(err => console.log('Error uploading to imagecenter. ' + err))
  return compilers
}

module.exports = asyncBuilder()
