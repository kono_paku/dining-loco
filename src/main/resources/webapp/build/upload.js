const bootstrap = require('./bootstrap')

const schema = {
  properties: {
    imgCenterVer: {
      description: 'アップロードするフォルダー名(public配下)',
      type: 'string',
      default: bootstrap.imgCenterVer,
      required: true
    },
    imgCenterPath: {
      description: 'イメージセンターの格納パス',
      type: 'string',
      default: bootstrap.imgCenterPath,
      required: true
    }
  }
}

function onComplete(result) {
  let destFolder = result.imgCenterVer.split('/').slice(0, -1).join('/')
  destFolder = destFolder !== '' ? '/' + destFolder : destFolder
  console.log(`\n> アップロードするフォルダーを[./public/${result.imgCenterVer}]に設定しました。`)
  console.log(`> イメージセンターのパスを[${result.imgCenterUrl}${result.imgCenterPath}${destFolder}]に設定しました。\n`)
}

bootstrap.promptImgCenterSetting(schema, onComplete).then(result => {
  bootstrap.uploadToImgCenter(result, function() {})
})
