'use strict'
const CustomWebpackPlugin = require('./custom-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const fs = require('fs')
const glob = require('glob')
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const webpack = require('webpack')

function resolve(dir) {
  return path.join(__dirname, '..', dir)
}

function pages(page, device) {
  return path.join(__dirname, '..', 'src', device, 'pages', page)
}

function styles(page, device) {
  const filePath = path.join(__dirname, '..', 'src', device, 'styles', page)
  return (fs.existsSync(filePath)) ? filePath : ''
}

// @todo: 重複改善＋promise化
function entries(device) {
  let entries = {}
  let files = glob.sync(pages('**/*.js', device))
  files.forEach((file) => {
    let filePath = file.split(`src/${device}/pages/`)[1]
    let fileName = filePath.split('.js')[0]
    entries[fileName] = pages(filePath, device)
  })
  entries['common'] = [styles('common.scss', device)] // common css <- @tofix よくないやり方（ファイル数増えると命名も保守も面倒）
  entries['common'] = entries['common'].filter((common) => common.length > 0)
  return entries
}

// @todo: 重複改善＋promise化
function htmls(device) {
  let htmlWebpackPlugins = []
  let files = glob.sync(pages('**/*.html', device))
  files.forEach((file) => {
    let filePath = file.split(`src/${device}/pages/`)[1]
    let filePathArray = filePath.split('/')
    if (filePathArray.length > 1 && filePathArray[0] === 'partial') {
      return
    }
    let fileName = filePath.split('.html')[0]
    htmlWebpackPlugins.push(
      new HtmlWebpackPlugin({
        inject: true,
        filename: `html/${fileName}.html`,
        template: pages(filePath, device),
        chunks: [fileName],
        // chunks: ['common', fileName], // 自動inject <- @候補 いらないcssのjsバンドルをなんとかする
        chunksSortMode: 'manual',
        alwaysWriteToDisk: true,
        minify: process.env.NODE_ENV === 'production' ? {
          ignoreCustomComments: [/\/\*\//], // <!--/*/ <th:block> /*/--> 無視対応
          removeComments: true,
          collapseWhitespace: true
        } : {}
      })
    )
  })

  return htmlWebpackPlugins
}

const extractSass = new ExtractTextPlugin({
  filename: 'css/[name].css',
  allChunks: true
})

const base = {
  context: path.resolve(__dirname),
  resolve: {
    extensions: ['.js', '.vue', '.json', '.scss'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@asset': resolve('asset'),
      '@pc': resolve('src/pc'),
      '@sp': resolve('src/sp'),
      '@lib': resolve('src/lib'),
      '@model': resolve('src/model')
    }
  },
  module: {
    rules: [
      {
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        include: [resolve('src'), resolve('test')],
        options: {
          fix: true
        }
      },
      {
        test: /\.js$/,
        // exclude: /node_modules\/(?!(dining-loco.*)).*/, // 共通モジュール用、3末以降必要かも
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.vue$/,
        // exclude: /node_modules\/(?!(dining-loco.*)).*/, // 共通モジュール用、3末以降必要かも
        loader: 'vue-loader',
        options: {
          preserveWhitespace: false,
          extractCSS: process.env.NODE_ENV === 'production'
        }
      },
      {
        test: /\.s[a|c]ss$/,
        use: extractSass.extract({
          use: [
            'css-loader',
            'sass-loader',
            'postcss-loader'
          ],
          fallback: 'vue-style-loader'
        })
      },
      {
        test: /\.html$/,
        loader: 'html-loader',
        options: {
          interpolate: 'require',
          // attrs: ['img:src'],
          // ignoreCustomFragments: [/\{\{.*?}}/], // これがややこしいので今後ejs導入するかも
          root: resolve('.')
        }
      },
      {
        test: /\.(png|svg|svgz|jpg|gif|eot|ttf|woff|woff2)$/,
        use: {
          loader: 'file-loader',
          options: {}
        }
      }

    ]
  },
  output: {
    publicPath: '/pc/'
  },
  plugins: [
    extractSass,
    new webpack.optimize.CommonsChunkPlugin({
      name: "vendor",
      minChunks: module => module.context && module.context.indexOf("node_modules") !== -1
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'commonchunk'
    }),
    new CustomWebpackPlugin()
  ]
}

// entry: Object.assign(entries(), {'vendor': ['vue', 'jquery']}),
// ~~↑@todo: vendor 対応~~
// ↑@tofix: commonChunk導入しました。cssと相性よくないな。
module.exports = [
  Object.assign({}, base, {
    name: 'pc',
    entry: entries('pc'),
    output: {
      path: resolve('public/pc'),
      publicPath: '/pc/',
      filename: 'js/[name].bundle.js',
      chunkFilename: 'js/commonchunk.bundle.js'
    },
    plugins: base.plugins.concat(htmls('pc'), [new HtmlWebpackHarddiskPlugin()])
  }),
  Object.assign({}, base, {
    name: 'sp',
    entry: entries('sp'),
    output: {
      path: resolve('public/sp'),
      publicPath: '/sp/',
      filename: 'js/[name].bundle.js',
      chunkFilename: 'js/commonchunk.bundle.js'
    },
    plugins: base.plugins.concat(htmls('sp'), [new HtmlWebpackHarddiskPlugin()])
  })
]
