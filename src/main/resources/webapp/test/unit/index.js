import Vue from 'vue'
import PlanRsvInput from '@pc/components/plan-rsv-input.vue'

Vue.config.productionTip = false

describe('plan-rsv-input.vue', () => {
  it('人数プロパティがバイドされています', () => {
    const Constructor = Vue.extend(PlanRsvInput)
    const propsData = {
      rsvNum: 3
    }
    const vm = new Constructor({ propsData }).$mount()
    expect(vm.$data.inputNum).to.equal(propsData.rsvNum)
  })
})
