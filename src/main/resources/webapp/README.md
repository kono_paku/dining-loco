# Tenpo Frontend

### 設計思想
    ・Thymeleafコンポーネント(同期レンダリングモジュール)単位とVueコンポーネント(非同期レンダリングモジュール)単位で開発を進めていきます。
    ・各画面ごとに1つずつ同名のJSファイルとHTMLファイルとCSSファイルがあります。
    ・各画面のHTMLファイルでThymeleafコンポーネントとVueコンポーネントを埋め込んで利用してください。
    ・CSSは「全体スコープのCSS」、「Vueコンポーネント内スコープのCSS」、「各画面内スコープのCSS」の3種類を利用します。

### フォルダ構成
|  1st   |  2nd  |    3rd     |   4th   |     description      |
|--------|-------|------------|---------|----------------------|
| build  |       |            |         | webpack config       |
| public |       |            |         | dist folder          |
| src    |       |            |         | source               |
|        | lib   |            |         | library              |
|        | pc    |            |         | pc folder            |
|        |       | components |         | vue components       |
|        |       | pages      |         | page's html & js     |
|        |       |            | partial | thymeleaf components |
|        |       | styles     |         | sass folder          |
|        | sp    |            |         | sp folder            |
|        |       | components |         | vue components       |
|        |       | pages      |         | page's html & js     |
|        |       |            | partial | thymeleaf components |
|        |       | styles     |         | sass folder          |
| test   |       |            |         | test folder          |
|        | unit  |            |         | unit test folder     |

### コーディング方針
    ・ES6は意識して、コーディングしてください。
    ・VueJSは『双方向バインディング』に特化したフレームワークです。UX的な処理はJQueryを利用してください。
    ・Vueクラスは各画面(1HTML)に1つ作成してください。定義する場所は各画面のJSファイルです。
    ・VueJSのBaseClassを作成していますので、 各画面毎のVueクラスで継承してください。
    ・JS側で叩くAPIのIFモデルは定義しません。どのようなデータが入っているかはIF仕様書を確認してください。

### コーディング規約
[デザインテンプレート ファイル命名規則](http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1425147440)

### 実装基盤
    ・BaseClass説明。
      ・共通パーツや共通フィルターをインポートしているので、各画面で利用してください。
      ・Ajaxの操作を行います。引数にURLやリクエストパラメータ、コールバック関数を渡して各画面から利用してください。
      ・共通Filterのインポートも行っています。必要に応じて各画面で利用してください。

### 開発基盤
    ・WebPackでは下記のタスクを実行しています。
      ・SassをCSSにトランスパイル
      ・ES6で書かれたJSとES5にトランスパイル
      ・HTML上でRequireされたThymeleafコンポーネントをインジェクト
      ・画面のJSファイルを、それに紐づくHTMLにインジェクト(ファイルパス、ファイル名が一致しているものを検知してバインディング)
      ・CommonCSSの作成
      ・各画面毎のCSSを作成(各画面のJSファイルで読み込んでください)
      ・JSのバンドル
      ・VueコンポーネントやJSファイルを修正して保存した際のホットリロード。(HTML側は動作しません)
    ・CSSとJSのパスはPC・SPを指定してください。画面へアクセスする際のパスはExpressで吸収しています。

### 依存パッケージ一覧
* メイン  
    ```vue express express-useragent```
* ビルド系  
    ```webpack webpack-merge webpack-dev-middleware webpack-hot-middleware babel-core babel-loader babel-preset-env html-webpack-plugin```
* ローダー系  
    ```css-loader vue-loader vue-template-compiler node-sass sass-loader vue-style-loader autoprefixer html-loader extract-text-webpack-plugin```
* リント系  
    ```eslint eslint-config-standard eslint-loader eslint-plugin-html eslint-plugin-import eslint-plugin-node eslint-plugin-promise eslint-plugin-standard babel-eslint```
* テスト系  
    ```mocha chai karma karma-chai karma-mocha karma-phantomjs-launcher karma-webpack```
* コンソールデコレーション
    ```chalk progress-bar-webpack-plugin friendly-errors-webpack-plugin```
* その他  
    ```cross-env rimraf```

---

### nodejsインストール

```bash
$ node -v
➜ v8.9.1

$ npm -v
➜ 5.5.1
```

### セットアップ

```bash
$ git clone xxx
$ cd xxx
$ npm install

# 開発
$ npm run dev

# 内部結合テスト
$ npm run test

# ビルド
$ npm run build

# 本番確認
$ npm run prod
```

### 環境
##### DEV環境
```bash
nodejs サーバを起動して、Java を起動します。

# cd /{プロジェクトフォルダー}/src/main/resoruces/webapp
$ npm run dev

# cd /{プロジェクトフォルダー}
$ mvn spring-boot:run -Drun.profiles=dev
```

##### PROD環境
```bash
nodejs 側でビルドしてから、Java を起動します。ビルドした dist を直接参照しますので、nodejs サーバは起動しなくでも大丈夫です。

# cd /{プロジェクトフォルダー}/src/main/resoruces/webapp
$ npm run build

# cd /{プロジェクトフォルダー}
$ mvn spring-boot:run
```
