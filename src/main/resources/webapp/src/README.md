## CSSルール

「WDお願いされたものの、ファイルによって書き方違うからどれ参考にしたらいいかわかんないなぁ…どうしよう」
初見さんでもこのようなことのないように統一された規則性のわかるコードを保っていきたいものです。

### class名の付け方
* BEM記法  
* lowerCamelCase  
* ルートのクラス名はファイル名をBlockにする  
例：ファイルが`calendar.vue`ならファイル内の一番外の要素にcalendarというclass名を付ける  
* マルチクラスにしない  
```html
NG（モディファイアがある場合はモディファイア付きのクラスのみ記述）
<h1 class="title title--main">タイトル</h1>
  
OK
<h1 class="title--main">タイトル</h1>
```
* 1つのファイル内にBlockが複数あるのはOK  
一つのBlockにElement詰め込み過ぎるとElementの名前付けるのも大変なので適切に分けましょう
* Elementは基本1語で書く、どうしても書けない時でも2語までにしましょう  
* ElementをBlockのように使わない  
```html
NG  
（textIconはbutton__textをBlockと考えてiconをElementにしているという考え方になる、この考え方だとbutton__text__iconということになってしまう）
<div class="button">
  <div class="buttton__text">ボタン<span class="button__textIcon">アイコン</span></div>
</div>
OK
<div class="button">
  <div class="buttton__text">ボタン<span class="button__icon">アイコン</span></div>
</div>
```
* 略語は人によって異なるのでNG  
例：txt,ttl,img
* 誤解しそうな単語を使うのはやめよう  
例：areaという単語は場所、地域を意味する言葉なのでYahoo!ロコではコンテンツ内容と捉えられる可能性があります  
可能であればbody,containerなど他の言葉を使いましょう 


### プロパティの記述順
1. 「視覚整形モデル」に関するプロパティ  
　 1. display  
　 2. visibility  
　 3. clip  
　 4. list-style  
　 5. position  
　 6. top  
　 7. right  
　 8. bottom  
　 9. left  
　10. z-index  
　11. float  
　12. clear  
　13. width  
　14. height  
　15. overflow  
2. 「ボックスモデル」に関するプロパティ  
　 1. margin  
　 2. padding  
　 3. border  
3. 「フォントとテキスト、色」に関するプロパティ  
　 1. color  
　 2. font  
　 3. font-family  
　 4. font-style  
　 5. font-variant  
　 6. font-weight  
　 7. font-size  
　 8. letter-spacing  
　 9. line-height  
　10. text-decoration  
　11. text-align  
　12. text-indent  
　13. text-shadow  
　14. text-transform  
　15. vertical-align  
　16. white-space  
　17. word-spacing  
　18. word-wrap  
　19. word-break  
4. 「背景」に関するプロパティ  
　 1. background  
5. 「表」に関するプロパティ  
　 1. table-layout  
　 2. border-collapse  
　 3. border-spacing  
6. CSS3の装飾プロパティ  
　 1. border-radius  
　 2. box-ordinal-group  
　 3. box-shadow  
　 4. box-sizing
7. その他
