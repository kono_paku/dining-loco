const changeClassName = {
  methods: {
    /**
     * ADが表示されるページか否かでクラス名の切り替え
     * @returns {boolean} true/false
     */
    toggleAdClassName () {
      const layoutSub = document.querySelector('.layout__sub')
      if (this.$root.isHiddenScType('yjAdsJs')) {
        layoutSub.className = 'layout__sub--pure'
      }
    }
  },
  mounted () {
    this.toggleAdClassName()
  }
}

export default changeClassName
