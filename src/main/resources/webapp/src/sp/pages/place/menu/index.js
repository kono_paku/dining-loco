import Vue from 'vue'
import Base from '@lib/baseModel'
import Ajax from '@lib/Ajax'

import changeClassName from '@sp/mixins/changeClassName.js'

import subNavigation from '@sp/components/place/subNavigation.vue'
import zeroMatch from '@sp/components/place/menu/zeroMatch.vue'
import list from '@sp/components/place/menu/list.vue'
import reserveButton from '@sp/components/place/reserveButton.vue'
import clipButton from '@sp/components/place/clipButton.vue'
import restaurantRecommend from '@sp/components/place/restaurantRecommend.vue'
import couponList from '@sp/components/place/coupon/couponList.vue'
import searchAssistance from '@sp/components/place/searchAssistance.vue'
import postButton from '@sp/components/place/postButton.vue'
import promotion from '@sp/components/place/promotion.vue'
import footNavigation from '@sp/components/place/footNavigation.vue'
import regularLink from '@sp/components/place/regularLink.vue'
import pageTop from '@sp/components/place/pageTop.vue'
import '@sp/styles/place/style.scss'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  extends: Base,
  components: {
    subNavigation,
    zeroMatch,
    list,
    reserveButton,
    clipButton,
    restaurantRecommend,
    couponList,
    searchAssistance,
    postButton,
    promotion,
    footNavigation,
    regularLink,
    pageTop
  },
  data: {
    tabItems: [],
    footerSearchInput: '', // フッターの検索窓
    currentUrl: window.location.href
  },
  mixins: [changeClassName],
  mounted () {
    this.createTabItems()
  },
  computed: {
    /**
     * グルメ店舗か判定
     * @returns {boolean} true/false
     */
    isGourmet () {
      if (this.tenpo.Feature[0].Property.Genre.length > 0) {
        return this.tenpo.Feature[0].Property.Genre[0].Code.substring(0, 2) === '01'
      }
      return false
    }
  },
  methods: {
    /**
     * タブ項目生成
     */
    createTabItems: function () {
      if (this.courseList.totalResults === 0) {
        return
      }
      const detail = this.tenpo.Feature[0].Property.Detail
      if (!detail.RecommendItemId1 && !detail.RecommendItemSequence1 && !detail.TopRankItemId1) {
        return
      }

      const baseUrl = '/place/g-' + this.tenpo.Feature[0].Gid
      const courseTitle = 'コース（' + this.courseList.totalResults + '）'
      this.tabItems.push({page: 'course', title: courseTitle, url: baseUrl + '/course/'})
      this.tabItems.push({page: 'menu', title: '料理', url: baseUrl + '/menu/'})
    },
    /**
     * ページ下部のインプットの値を空にする
     */
    footerSearchReset () {
      this.footerSearchInput = ''
    },
    setPcViewCookie () {
      Ajax.get(this.namespace.Url.setYjdPc).catch(() => {
        window.location.href = this.currentUrl
      })
    }
  }
})
