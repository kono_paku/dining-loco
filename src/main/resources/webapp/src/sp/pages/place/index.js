import Vue from 'vue'
import Base from '@lib/baseModel'
import Ajax from '@lib/Ajax'

import changeClassName from '@sp/mixins/changeClassName.js'

import signboard from '@sp/components/place/signboard.vue'
import calendar from '@sp/components/place/calendar.vue'
import reserveButton from '@sp/components/place/reserveButton.vue'
import introduction from '@sp/components/place/introduction.vue'
import courseSummary from '@sp/components/place/courseSummary.vue'
import couponList from '@sp/components/place/coupon/couponList.vue'
import reviewSummary from '@sp/components/place/review/reviewSummary.vue'
import businessHours from '@sp/components/place/businessHours.vue'
import viewChange from '@sp/components/place/map/viewChange.vue'
import planSummary from '@sp/components/place/planSummary.vue'
import information from '@sp/components/place/information.vue'
import rental from '@sp/components/place/rental.vue'
import flyer from '@sp/components/place/flyer.vue'
import clipButton from '@sp/components/place/clipButton.vue'
import searchResult from '@sp/components/place/searchResult.vue'
import cpList from '@sp/components/place/cpList.vue'
import restaurantRecommend from '@sp/components/place/restaurantRecommend.vue'
import searchAssistance from '@sp/components/place/searchAssistance.vue'
import postButton from '@sp/components/place/postButton.vue'
import promotion from '@sp/components/place/promotion.vue'
import footNavigation from '@sp/components/place/footNavigation.vue'
import regularLink from '@sp/components/place/regularLink.vue'
import pageTop from '@sp/components/place/pageTop.vue'
import '@sp/styles/place/style.scss'

/* eslint-disable no-new */
new Vue({
  el: '#app',

  extends: Base,
  data: {
    provider: false,
    footerSearchInput: '',
    currentUrl: window.location.href
  },
  mixins: [changeClassName],
  components: {
    signboard,
    calendar,
    reserveButton,
    introduction,
    courseSummary,
    couponList,
    reviewSummary,
    businessHours,
    viewChange,
    planSummary,
    information,
    rental,
    flyer,
    clipButton,
    searchResult,
    cpList,
    restaurantRecommend,
    searchAssistance,
    postButton,
    promotion,
    footNavigation,
    regularLink,
    pageTop
  },
  methods: {
    /**
     * ページ下部のインプットの値を空にする
     */
    footerSearchReset () {
      this.footerSearchInput = ''
    },
    setPcViewCookie () {
      Ajax.get(this.namespace.Url.setYjdPc).catch(() => {
        window.location.href = this.currentUrl
      })
    }
  },
  computed: {
    /**
     * グルメ店舗か判定
     * @returns {boolean} true/false
     */
    isGourmet () {
      if (this.tenpo.Feature[0].Property.Genre.length > 0) {
        return this.tenpo.Feature[0].Property.Genre[0].Code.substring(0, 2) === '01'
      }
      return false
    }
  }
})
