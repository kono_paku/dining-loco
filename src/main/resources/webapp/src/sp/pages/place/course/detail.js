import Vue from 'vue'
import Base from '@lib/baseModel'
import Ajax from '@lib/Ajax'
import moment from '@lib/moment-ja'

import changeClassName from '@sp/mixins/changeClassName.js'

import detailTitle from '@sp/components/place/course/detailTitle.vue'
import detailGuest from '@sp/components/place/course/detailGuest.vue'
import detailPrice from '@sp/components/place/course/detailPrice.vue'
import detailInput from '@sp/components/place/course/detailInput.vue'
import detailMenu from '@sp/components/place/course/detailMenu.vue'
import restaurantRecommend from '@sp/components/place/restaurantRecommend.vue'
import promotion from '@sp/components/place/promotion.vue'
import footNavigation from '@sp/components/place/footNavigation.vue'
import detailLogin from '@sp/components/place/course/detailLogin.vue'
import regularLink from '@sp/components/place/regularLink.vue'
import pageTop from '@sp/components/place/pageTop.vue'
import '@sp/styles/place/style.scss'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  extends: Base,
  components: {
    detailTitle,
    detailGuest,
    detailPrice,
    detailInput,
    detailMenu,
    promotion,
    restaurantRecommend,
    footNavigation,
    detailLogin,
    regularLink,
    pageTop
  },
  data: {
    footerSearchInput: '', // フッターの検索窓
    provider: false,
    reviewTooltip: false,
    clipTooltip: {
      show: false,
      clipped: false
    },
    singleColumn: true,
    canConfirm: true,
    course: {},
    timeStockResultCode: '',
    dailyStock: {},
    smartLoginUrl: '',
    searchCondition: {
      selectedDate: '',
      selectedPerson: 0,
      selectedTime: ''
    },
    detailButton: false,
    currentUrl: window.location.href
  },
  mixins: [changeClassName],
  beforeMount () {
    try {
      this.course = JSON.parse(this.getDocumentValue('#courseDeatilJson'))
    } catch (err) {
      this.error['course'] = err
    }
    this.updateStatusOf('course')
    this.getDailyStock()
  },
  computed: {
    /**
     * グルメ店舗か判定
     * @returns {boolean} true/false
     */
    isGourmet () {
      if (this.tenpo.Feature[0].Property.Genre.length > 0) {
        return this.tenpo.Feature[0].Property.Genre[0].Code.substring(0, 2) === '01'
      }
      return false
    },
    /**
     * 日付毎の予約ステータス取得
     * 予約ステータス
     *   0: 空席なし
     *   1: 予約可能
     *   2: リクエスト予約可能
     * @return {array} 日付：ステータスのarray
     */
    dailyStockFiltered () {
      let result = {}
      if (Object.keys(this.dailyStock).length === 0) return result
      for (const stock of this.dailyStock.entry[0].stocks) {
        result[moment(stock.date).format('YYYYMMDD')] = stock.status
      }
      return result
    }
  },
  methods: {
    /**
     * ページ下部のインプットの値を空にする
     */
    footerSearchReset () {
      this.footerSearchInput = ''
    },
    setSelectedDate (val) {
      this.searchCondition.selectedDate = val
    },
    setSelectedPerson (val) {
      this.searchCondition.selectedPerson = val
    },
    setSelectedTime (val) {
      this.searchCondition.selectedTime = val
    },
    setTimeStockResult (val) {
      this.timeStockResultCode = val
    },
    setConfirmFlag (val) {
      this.canConfirm = val
    },
    /**
     * 日付在庫を取得する
     */
    getDailyStock () {
      // 日次在庫APIからデータの取得
      this.bindRequest({
        name: 'dailyStock',
        url: this.namespace.API.dailyStock,
        params: {
          tenpoid: this.course.entry.tenpoId,
          planid: this.course.entry.plans[0].planId,
          period: 90
        },
        eappid: true,
        method: 'GET',
        dataType: 'jsonp'
      })
    },
    setSmartLoginUrl (val) {
      this.smartLoginUrl = val
    },
    setPcViewCookie () {
      Ajax.get(this.namespace.Url.setYjdPc).catch(() => {
        window.location.href = this.currentUrl
      })
    }
  }
})
