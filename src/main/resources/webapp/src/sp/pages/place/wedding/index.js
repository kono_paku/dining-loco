import Vue from 'vue'
import Base from '@lib/baseModel'
import Ajax from '@lib/Ajax'

import changeClassName from '@sp/mixins/changeClassName.js'

import subNavigation from '@sp/components/place/subNavigation.vue'
import plan from '@sp/components/place/wedding/plan.vue'
import report from '@sp/components/place/wedding/report.vue'
import error from '@sp/components/place/wedding/error.vue'
import clipButton from '@sp/components/place/clipButton.vue'
import restaurantRecommend from '@sp/components/place/restaurantRecommend.vue'
import couponList from '@sp/components/place/coupon/couponList.vue'
import searchAssistance from '@sp/components/place/searchAssistance.vue'
import postButton from '@sp/components/place/postButton.vue'
import promotion from '@sp/components/place/promotion.vue'
import footNavigation from '@sp/components/place/footNavigation.vue'
import regularLink from '@sp/components/place/regularLink.vue'
import pageTop from '@sp/components/place/pageTop.vue'
import '@sp/styles/place/style.scss'
import Wedding from '@lib/Wedding'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  extends: Base,
  components: {
    subNavigation,
    plan,
    report,
    error,
    clipButton,
    restaurantRecommend,
    couponList,
    searchAssistance,
    postButton,
    promotion,
    footNavigation,
    regularLink,
    pageTop
  },
  mixins: [changeClassName],
  data: function () {
    return {
      subNavigations: [], // サビナビゲーション
      plans: [], // プラン
      reports: [], // 体験者レポート
      isPlanViewable: false, // 表示条件を満たせばtrue
      isReportViewable: false, // 表示条件を満たせばtrue
      gid: '', // GID
      footerSearchInput: '', // フッターの検索窓
      currentUrl: window.location.href
    }
  },
  mounted () {
    // 式場オブジェクト生成
    if (this.status.wedding === 'binded') {
      const myWedding = new Wedding(this.wedding)
      // ナビゲーション情報取得
      this.subNavigations = myWedding.getSubNavigations()
      // プラン取得
      this.plans = myWedding.plan.getPlans()
      // 体験者レポート取得
      this.reports = myWedding.report.getReports()
      // CPURL
      this.cpUrl = myWedding.getCpUrl()
      // 表示判定
      this.isPlanViewable = myWedding.plan.isPlanViewable()
      this.isReportViewable = myWedding.report.isReportViewable()
      // GID
      this.gid = myWedding.getGid()
    }
  },
  methods: {
    /**
     * ページ下部のインプットの値を空にする
     */
    footerSearchReset () {
      this.footerSearchInput = ''
    },
    setPcViewCookie () {
      Ajax.get(this.namespace.Url.setYjdPc).catch(() => {
        window.location.href = this.currentUrl
      })
    }
  },
  computed: {
    /**
     * グルメ店舗か判定
     * @returns {boolean} true/false
     */
    isGourmet () {
      if (this.tenpo.Feature[0].Property.Genre.length > 0) {
        return this.tenpo.Feature[0].Property.Genre[0].Code.substring(0, 2) === '01'
      }
      return false
    }
  }
})
