import Vue from 'vue'
import Base from '@lib/baseComponent'
import restaurantRecommend from '@sp/components/place/restaurantRecommend.vue'

import '@sp/styles/place/style.scss'

/* eslint-disable no-new */

new Vue({
  el: '#app',
  extends: Base,
  components: {
    restaurantRecommend
  },
  data: {
    device: '',
    edgeDevice: '',
    footerSearchInput: '' // フッターの検索窓
  },
  methods: {
    /**
     * ページ下部のインプットの値を空にする
     */
    footerSearchReset () {
      this.footerSearchInput = ''
    }
  }
})
