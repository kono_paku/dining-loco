import Vue from 'vue'
import Base from '@lib/baseModel'
import Ajax from '@lib/Ajax'

import changeClassName from '@sp/mixins/changeClassName.js'

import couponList from '@sp/components/place/coupon/couponList.vue'
import note from '@sp/components/place/coupon/note.vue'
import loading from '@sp/components/place/loading.vue'
import clipButton from '@sp/components/place/clipButton.vue'
import restaurantRecommend from '@sp/components/place/restaurantRecommend.vue'
import searchAssistance from '@sp/components/place/searchAssistance.vue'
import postButton from '@sp/components/place/postButton.vue'
import promotion from '@sp/components/place/promotion.vue'
import footNavigation from '@sp/components/place/footNavigation.vue'
import regularLink from '@sp/components/place/regularLink.vue'
import pageTop from '@sp/components/place/pageTop.vue'
import '@sp/styles/place/style.scss'

/* eslint-disable no-new */

new Vue({
  el: '#app',
  components: {
    couponList,
    note,
    loading,
    clipButton,
    restaurantRecommend,
    searchAssistance,
    postButton,
    promotion,
    footNavigation,
    regularLink,
    pageTop
  },
  extends: Base,
  data: {
    provider: false,
    footerSearchInput: '', // フッターの検索窓
    currentUrl: window.location.href
  },
  mixins: [changeClassName],
  computed: {
    /**
     * グルメ店舗か判定
     * @returns {boolean} true/false
     */
    isGourmet () {
      if (this.tenpo.Feature[0].Property.Genre.length > 0) {
        return this.tenpo.Feature[0].Property.Genre[0].Code.substring(0, 2) === '01'
      }
      return false
    }
  },
  methods: {
    /**
     * ページ下部のインプットの値を空にする
     */
    footerSearchReset () {
      this.footerSearchInput = ''
    },
    setPcViewCookie () {
      Ajax.get(this.namespace.Url.setYjdPc).catch(() => {
        window.location.href = this.currentUrl
      })
    }
  }
})
