import Vue from 'vue'
import Base from '@lib/baseModel'
import Ajax from '@lib/Ajax'
import Wedding from '@lib/Wedding.js'

import changeClassName from '@sp/mixins/changeClassName.js'

import subNavigation from '@sp/components/place/subNavigation.vue'
import information from '@sp/components/place/marriage/information.vue'
import error from '@sp/components/place/marriage/error.vue'
import clipButton from '@sp/components/place/clipButton.vue'
import restaurantRecommend from '@sp/components/place/restaurantRecommend.vue'
import couponList from '@sp/components/place/coupon/couponList.vue'
import searchAssistance from '@sp/components/place/searchAssistance.vue'
import postButton from '@sp/components/place/postButton.vue'
import promotion from '@sp/components/place/promotion.vue'
import footNavigation from '@sp/components/place/footNavigation.vue'
import regularLink from '@sp/components/place/regularLink.vue'
import pageTop from '@sp/components/place/pageTop.vue'
import '@sp/styles/place/style.scss'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {
    subNavigation,
    information,
    error,
    clipButton,
    restaurantRecommend,
    couponList,
    searchAssistance,
    postButton,
    promotion,
    footNavigation,
    regularLink,
    pageTop
  },
  extends: Base,
  data: function () {
    return {
      subNavigations: [], // サブナビゲーションメニュー
      tenpoName: '', // 店舗名
      cpName: '', // CP名
      cpUrl: '', // CPURL
      marriageInfo: [], // 式場情報
      isViewable: false, // 表示条件を満たせばtrue
      footerSearchInput: '', // フッターの検索窓
      currentUrl: window.location.href
    }
  },
  mixins: [changeClassName],
  mounted () {
    // 式場オブジェクト生成
    if (this.status.wedding === 'binded') {
      const myWedding = new Wedding(this.wedding)
      // サブナビ
      this.subNavigations = myWedding.getSubNavigations()
      // 店舗名
      this.tenpoName = myWedding.getTenpoName()
      // CP名
      this.cpName = myWedding.getCpName()
      // CPURL
      this.cpUrl = myWedding.getCpUrl()
      // 式場情報
      this.marriageInfo = myWedding.marriage.getInfo()
      // 表示判定
      this.isViewable = myWedding.marriage.isViewable()
    }
  },
  computed: {
    /**
     * グルメ店舗か判定
     * @returns {boolean} true/false
     */
    isGourmet () {
      if (this.tenpo.Feature[0].Property.Genre.length > 0) {
        return this.tenpo.Feature[0].Property.Genre[0].Code.substring(0, 2) === '01'
      }
      return false
    }
  },
  methods: {
    /**
     * ページ下部のインプットの値を空にする
     */
    footerSearchReset () {
      this.footerSearchInput = ''
    },
    setPcViewCookie () {
      Ajax.get(this.namespace.Url.setYjdPc).catch(() => {
        window.location.href = this.currentUrl
      })
    }
  }
})
