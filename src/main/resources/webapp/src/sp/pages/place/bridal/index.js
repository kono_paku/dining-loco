import Vue from 'vue'
import Base from '@lib/baseModel'
import Ajax from '@lib/Ajax'
import Wedding from '@lib/Wedding.js'
import Pagination from '@lib/Pagination.js'

import changeClassName from '@sp/mixins/changeClassName.js'

import subNavigation from '@sp/components/place/subNavigation.vue'
import fair from '@sp/components/place/bridal/fair.vue'
import pagination from '@sp/components/place/bridal/pagination.vue'
import error from '@sp/components/place/bridal/error.vue'
import clipButton from '@sp/components/place/clipButton.vue'
import restaurantRecommend from '@sp/components/place/restaurantRecommend.vue'
import couponList from '@sp/components/place/coupon/couponList.vue'
import searchAssistance from '@sp/components/place/searchAssistance.vue'
import postButton from '@sp/components/place/postButton.vue'
import promotion from '@sp/components/place/promotion.vue'
import footNavigation from '@sp/components/place/footNavigation.vue'
import regularLink from '@sp/components/place/regularLink.vue'
import pageTop from '@sp/components/place/pageTop.vue'
import '@sp/styles/place/style.scss'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  extends: Base,
  components: {
    subNavigation,
    fair,
    pagination,
    error,
    clipButton,
    restaurantRecommend,
    couponList,
    searchAssistance,
    postButton,
    promotion,
    footNavigation,
    regularLink,
    pageTop
  },
  data: function () {
    return {
      provider: false,
      reviewTooltip: false,
      clipTooltip: {
        show: false,
        clipped: false
      },
      page: 1,
      itemsPerPage: 10,
      items: [],
      subNavigations: [],
      tenpoName: '',
      cpName: '',
      cpUrl: '',
      pagination: new Pagination(),
      isViewable: false,
      gid: '',
      footerSearchInput: '', // フッターの検索窓
      currentUrl: window.location.href
    }
  },
  mixins: [changeClassName],
  mounted () {
    // 式場オブジェクト生成
    if (this.status.wedding === 'binded') {
      const myWedding = new Wedding(this.wedding)
      // 総プランを取得
      this.items = myWedding.bridal.getFairs()
      // ページ番号を初期化
      this.updatePage(1)
      // ナビゲーション情報取得
      this.subNavigations = myWedding.getSubNavigations()
      // 店舗名取得
      this.tenpoName = myWedding.getTenpoName()
      // CP名取得
      this.cpName = myWedding.getCpName()
      // CPURL取得
      this.cpUrl = myWedding.getCpUrl()
      // 表示判定でNGなら店舗トップへ飛ばす
      this.isViewable = myWedding.bridal.isViewable()
      // GID
      this.gid = myWedding.getGid()
    }
  },
  computed: {
    /**
     * グルメ店舗か判定
     * @returns {boolean} true/false
     */
    isGourmet () {
      if (this.tenpo.Feature[0].Property.Genre.length > 0) {
        return this.tenpo.Feature[0].Property.Genre[0].Code.substring(0, 2) === '01'
      }
      return false
    }
  },
  methods: {
    // 子コンポーネントからのイベントを補足
    // 子→親
    paging (value) {
      // 0以下は無視
      if (value < 1) {
        return
      }
      // データを更新
      this.updatePage(value)
    },
    // ページ番号を更新して子コンポーネントに伝播させる
    // 親→子
    updatePage (value) {
      this.page = value
    },
    /**
     * ページ下部のインプットの値を空にする
     */
    footerSearchReset () {
      this.footerSearchInput = ''
    },
    setPcViewCookie () {
      Ajax.get(this.namespace.Url.setYjdPc).catch(() => {
        window.location.href = this.currentUrl
      })
    }
  }
})
