import Vue from 'vue'
import Base from '@lib/baseModel'
import Ajax from '@lib/Ajax'
import Pagination from '@lib/Pagination'

import changeClassName from '@sp/mixins/changeClassName.js'

import routeMap from '@sp/components/place/map/routeMap.vue'
import route from '@sp/components/place/map/route.vue'
import viewChange from '@sp/components/place/map/viewChange.vue'
import clipButton from '@sp/components/place/clipButton.vue'
import restaurantRecommend from '@sp/components/place/restaurantRecommend.vue'
import couponList from '@sp/components/place/coupon/couponList.vue'
import searchAssistance from '@sp/components/place/searchAssistance.vue'
import postButton from '@sp/components/place/postButton.vue'
import promotion from '@sp/components/place/promotion.vue'
import footNavigation from '@sp/components/place/footNavigation.vue'
import regularLink from '@sp/components/place/regularLink.vue'
import pageTop from '@sp/components/place/pageTop.vue'
import '@sp/styles/place/style.scss'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  extends: Base,
  components: {
    routeMap,
    route,
    viewChange,
    clipButton,
    restaurantRecommend,
    couponList,
    searchAssistance,
    postButton,
    promotion,
    footNavigation,
    regularLink,
    pageTop
  },
  data: {
    footerSearchInput: '', // フッターの検索窓
    currentUrl: window.location.href
  },
  mixins: [changeClassName],
  /**
   * URLパラメータ「fr=poi_tab」がある場合はアプリのダウンロードを促す
   */
  mounted: function () {
    // urlパラメータの取得
    const urlParams = new Pagination().getSearchParameters()
    if (urlParams.fr === 'poi_tab') {
      this.displayMappAppModal()
    }
  },
  computed: {
    /**
     * グルメ店舗か判定
     * @returns {boolean} true/false
     */
    isGourmet () {
      if (this.tenpo.Feature[0].Property.Genre.length > 0) {
        return this.tenpo.Feature[0].Property.Genre[0].Code.substring(0, 2) === '01'
      }
      return false
    }
  },
  methods: {
    /**
     * 地図アプリのダウンロードを促すポップアップを表示する
     */
    displayMappAppModal: function () {
      if (window.confirm('地図アプリをダウンロードしますか?')) {
        const userAgent = navigator.userAgent.toLowerCase()
        const storeUrl = (userAgent.search(/iphone|ipad|ipod/) > -1)
          ? 'https://itunes.apple.com/jp/app/yahoo-map/id289567151?mt=8'
          : 'https://play.google.com/store/apps/details?id=jp.co.yahoo.android.apps.map'

        location.href = storeUrl
      }
    },
    /**
     * ページ下部のインプットの値を空にする
     */
    footerSearchReset () {
      this.footerSearchInput = ''
    },
    setPcViewCookie () {
      Ajax.get(this.namespace.Url.setYjdPc).catch(() => {
        window.location.href = this.currentUrl
      })
    }
  }
})
