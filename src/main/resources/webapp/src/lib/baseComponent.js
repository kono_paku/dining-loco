import Vue from 'vue'
import Ajax from './Ajax'
import namespace from './namespace'
import vueSmoothScroll from 'vue-smoothscroll'
import VueLazyload from 'vue-lazyload'

// Vueの拡張
Vue.use(vueSmoothScroll)
Vue.use(VueLazyload, {
  preLoad: 1.2,
  loading: 'https://s.yimg.jp/images/loco/front/images/common/noImage/1x1.png',
  observer: true,
  observerOptions: {
    rootMargin: '100px',
    threshold: 0
  }
})

/* eslint-disable no-new */
export default Vue.extend({
  data: () => ({
    errors: {},
    namespace,
    status: {},
    resizedImages: {}
  }),

  methods: {
    /**
     * APIにリクエストを送り、レスポンスをバインドする。
     * 落ち着いたらこの関数をdeprecatedにしてasync-await化したい...
     * @param {string} name: バインドする変数名。レスポンスがdata[name]に格納される。
     * @param {string} url: GETリクエストを送るURL。
     * @param {Object} params: GETリクエストのパラメータ。
     * @param {boolean} eappid: eappidが必要なAPIの場合、trueを指定すると自動でeappidを渡す。
     * @param {string} method: httpリクエストメソッドの指定
     * @param {string} dataType: Cross-domainなら'jsonp'、そうでなければ'json'
     * @param {number} timeout: リクエストタイムアウト[ms]
     * @param {Object => Object} translator: レスポンスを変換したい場合に使用。
     * @returns {Promise.<Object>} レスポンスのPromise
     */
    bindRequest ({
      name,
      url,
      params = {},
      eappid = false,
      method = 'GET',
      dataType,
      timeout,
      translator = x => x,
      headers
    } = {}) {
      if (typeof name !== 'string') {
        throw new Error('nameをstringで指定してください')
      }
      if (typeof url !== 'string') {
        throw new Error('urlをstringで指定してください')
      }
      if (['get', 'post', 'delete'].includes(method.toLowerCase())) {
        return this.bindPromise(name, this[method.toLowerCase()](url, params, {eappid, dataType, timeout}).then(translator))
      } else {
        throw new Error('リクエストメソッドの指定が正しくありません')
      }
    },
    /**
     * 非同期なオブジェクトをバインドする。
     * async-await使って代入文でexplicitにバインドしたいが、学習コスト等を考えてimplicitにバインドする
     * 落ち着いたらこの関数をdeprecatedにしてasync-await化したい
     * @param {string} name: バインドする変数名。Promiseのresolve内容がdata[name]に格納される。
     * @param {Promise(Object)} value: バインドしたいPromise
     * @returns {Promise(Object)} 受け取ったPromise
     */
    bindPromise (name, value) {
      const reservedNames = ['errors', 'namespace', 'status']
      if (reservedNames.includes(name)) {
        throw new Error(`nameに${reservedNames.join(',')}を指定しないでください`)
      }

      Vue.set(this.status, name, 'on-binding')
      this.updateStatusOf(name)

      return value.then((data) => {
        this[name] = data
        return data
      })
        .catch((err) => {
          this.errors[name] = err
          this.updateStatusOf(name)
          throw err
        })
        .then((data) => {
          this.updateStatusOf(name)
          return data
        })
    },
    /**
     * 画像サイズ取得
     * @param {string} url: 画像パス
     * @returns {Promise(Object)} 受け取ったPromise {width, height}
     */
    getImageSize (url) {
      return new Promise((resolve, reject) => {
        const img = new Image()
        img.addEventListener('load', function () {
          resolve({ width: this.naturalWidth, height: this.naturalHeight })
        })
        img.addEventListener('error', function () {
          const error = '写真ロードに失敗しました'
          reject(error)
        })
        img.src = url
      })
    },
    /**
     * 最適画像サイズの計算
     * @param {string} orginalSize: 画像元サイズ
     * @param {string} preferSize: 画像変換したいサイズ
     * @returns {Object} resizeImage に必要なパラメーター {width, height, cropWidthCenter, cropHeightCenter}
     */
    calResizeImageOptions (orginalSize, preferSize) {
      const [ oWidth, oHeight ] = orginalSize
      const [ pWidth, pHeight ] = preferSize
      let width, height

      // 短辺x希望サイズ優先
      if (oWidth <= oHeight) {
        width = pWidth
        height = ~~((oHeight / oWidth) * pWidth)
        // 再調整
        if (height < pHeight) {
          width = ~~((width / height) * pHeight)
          height = pHeight
        }
      } else {
        width = ~~((oWidth / oHeight) * pHeight)
        height = pHeight
        // 再調整
        if (width < pWidth) {
          height = ~~((height / width) * pWidth)
          width = pWidth
        }
      }

      return {
        width: width,
        height: height,
        cropWidthCenter: pWidth,
        cropHeightCenter: pHeight
      }
    },
    /**
     * 画像変換
     * 例）http://foo.pngを400x300にリサイズした画像のurlは
     * this.resizedImages['http://foo.png']['400x300']に入る
     *
     * @param {string} gid: 店舗のgid（画像変換プロキシAPIのキャッシュに使用します）
     * @param {[Object]} queries: 変換したい画像の配列
     * queriesの例: [{urls: ['http://foo.png', 'http://bar.jpg'], width: 400, height: 300}, {urls: ['http://baz.jpg'], width: 200, height: 150, priority: 'l', crop=true}]
     */
    resizeImages (gid, queries, name = 'resizedImages') {
      let images = []
      for (const {urls, width, height, priority, quality, crop, cropWidthCenter, cropHeightCenter} of queries) {
        images.push(...urls.map((url) => ({url, width, height, priority, quality, crop, cropWidthCenter, cropHeightCenter})))
      }
      return this.bindRequest({
        name: name,
        url: this.namespace.API.resizeImage,
        params: {gid, images},
        method: 'POST',
        dataType: 'json',
        translator: (response) => this.mappizeResizedImage(images, response)
      })
    },
    /**
     * 画像変換APIのレスポンスを整形
     * 変形前：[{OriginalUrl: string, Url: string}]
     * 変形後：{<OriginalUrl>: {<width>x<height>: string}}
     * 例）http://foo.pngを400x300にリサイズした画像のurlは
     * output['http://foo.png']['400x300']に入る
     * @param {Object} images: 画像変換APIに渡すパラメータ
     * @param {Object} response: 画像変換APIのレスポンス
     */
    mappizeResizedImage (images, response) {
      const results = response.ResultSet.Result
      let output = {}
      for (const [image, result] of images.map((q, i) => [q, results[i]])) {
        if (!(image.url in output)) {
          output[image.url] = {}
        }
        const resizedUrl = this.namespace.Url.iwizBase + result.Url
        output[image.url][image.width + 'x' + image.height] = resizedUrl
      }
      return output
    },

    /**
     * APIにPOSTリクエストを送り、レスポンスのPromiseを返す
     * @param {string} url: GETリクエストを送るURL。
     * @param {Object} params: GETリクエストのパラメータ。
     * @param {boolean} eappid: eappidが必要なAPIの場合、trueを指定すると自動でeappidを渡す。
     * @param {string} dataType: 受け取るデータの形式 e.g.）'json', 'jsonp', 'auto'
     * @param {number} timeout: タイムアウト
     * @returns {Promise.<Object>} レスポンスのProimse
     */
    get (url, params = {}, {eappid = false, dataType, timeout} = {}) {
      if (eappid) {
        params.eappid = this.eappid
      }
      return Ajax.get(url, params, {dataType, timeout})
    },

    /**
     * APIにPOSTリクエストを送り、レスポンスのPromiseを返す
     * @param {string} url: POSTリクエストを送るURL。
     * @param {Object} params: POSTリクエストのパラメータ。
     * @param {boolean} eappid: eappidが必要なAPIの場合、trueを指定すると自動でeappidを渡す。
     * @param {string} dataType: 受け取るデータの形式 e.g.）'json', 'jsonp', 'auto'
     * @param {number} timeout: タイムアウト
     * @returns {Promise.<Object>} レスポンスのProimse
     */
    post (url, params = {}, {eappid = false, dataType, timeout} = {}) {
      if (eappid) {
        params.eappid = this.eappid
      }
      return Ajax.post(url, params, {dataType, timeout})
    },

    /**
     * APIにDELETEリクエストを送り、レスポンスのPromiseを返す
     * @param {string} url: DELETEリクエストを送るURL。
     * @param {Object} params: DELETEリクエストのパラメータ。
     * @param {boolean} eappid: eappidが必要なAPIの場合、trueを指定すると自動でeappidを渡す。
     * @param {string} dataType: 受け取るデータの形式 e.g.）'json', 'jsonp', 'auto'
     * @param {number} timeout: タイムアウト
     * @returns {Promise.<Object>} レスポンスのProimse
     */
    delete (url, params = {}, {eappid = false, dataType, timeout} = {}) {
      if (eappid) {
        params.eappid = this.eappid
      }
      return Ajax.delete(url, params, {dataType, timeout})
    },

    /**
     * html上の要素のidを指定してvalueを取得する
     * @todo Controllerから変数を受け取るために作成したが、そこを綺麗に実装できるようになれば削除したい
     * @param {string} querySelector
     * @return {string} 要素のvalue
     */
    getDocumentValue (querySelector) {
      const element = this.$el.querySelector(querySelector)
      if (!element) {
        throw new Error(`${querySelector} is not found`)
      }
      return element.value
    },
    hasAnyErrors () {
      return this.errors.length > 0
    },
    hasErrorOf (name) {
      return name in this.errors
    },
    getErrorsOf (name) {
      return this.errors[name]
    },
    updateStatusOf (name) {
      if (this.hasErrorOf(name)) {
        this.status[name] = 'error'
      } else if (Object.keys(this[name]).length > 0) {
        this.status[name] = 'binded'
      } else {
        this.status[name] = 'on-binding'
      }
    },

    /**
     * foo.bar.baz - Cannot read property baz of undefined の対応
     * Optional Chaning の代わり（雑）
     * @param {Object} data データオブジェクト (eg. this.tenpo)
     * @param {string} field データフィルド (eg. 'Feature[0].Property.Detail')
     * @return {Object|string|undefined} 要素のvalue
     * @example g(tenpo, 'Feature[0].Property.Detail.onlineReserveRequestFlag')
     */
    g (data, field) {
      return field
        .replace(/\[(\d)\]/g, (m, p, o, s) => `.${p}`)
        .split('.').reduce(
          (acc, cur) =>
            acc !== null && typeof acc !== 'undefined' && typeof acc[cur] !== 'undefined' ? acc[cur] : undefined
          , data)
    },

    /**
     * 画像のロードを扱う
     * @param src 画像URL
     * @returns {Promise<any>}
     */
    loadImage (src) {
      return new Promise(resolve => {
        const image = new Image()
        image.src = src
        image.onload = function () {
          resolve(this)
        }
      })
    },

    /**
     * 特定なエレメントIDまでスムーズスクロースする
     * @param id DOM id
     */
    scrollToElementId (id) {
      this.$SmoothScroll(document.getElementById(id))
    }
  }
})
