import Vue from 'vue'
import Base from './baseComponent'

/* eslint-disable no-new */
export default Vue.extend({
  extends: Base,
  data: () => ({
    tenpo: {},
    promotion: {},
    userInfo: {},
    wedding: {},
    courseList: {},
    comment: {},
    eappid: '',
    adInfeed: {},
    pageParam: {},
    sr: window.sr,
    rapid: '',
    miffy: window.YAHOO.JP.MF.Client,
    spaceId: '',
    device: '',
    edgeDevice: '',
    scType: ''
  }),
  beforeMount () {
    this.getTenpo()
    this.getEappId()
    this.getSpaceId()
    this.getDevice()
    this.getEdgeDevice()
    this.getPageParam()
    this.getPromotion()
    this.getUserInfo()
    this.getWedding()
    this.getCourseList()
    this.fixHotpepperData()
    this.getAdInfeed()
    this.getComment()
    this.saveGidToLocalStrage()
    document.addEventListener('DOMContentLoaded', () => {
      this.rapid = this.initRapid()
      this.addParameters()
    })
    this.getScType()
    this.filterCourseList()
  },
  computed: {
    rapidReady () {
      return !!this.rapid
    }
  },
  methods: {
    /**
     * 店舗情報取得
     */
    getTenpo () {
      try {
        this.tenpo = JSON.parse(this.getDocumentValue('#tenpojson'))
      } catch (err) {
        this.errors['tenpo'] = err
      }
      this.updateStatusOf('tenpo')
    },
    /**
     * eappId取得
     */
    getEappId () {
      try {
        this.eappid = this.getDocumentValue('#eappid')
      } catch (err) {
        this.errors['eappid'] = err
      }
      this.updateStatusOf('eappid')
    },
    /**
     * spaceId取得
     */
    getSpaceId () {
      try {
        this.spaceId = this.getDocumentValue('#spaceId')
      } catch (err) {
        this.errors['spaceId'] = err
      }
      this.updateStatusOf('spaceId')
    },
    /**
     * Device種別取得
     */
    getDevice () {
      try {
        this.device = this.getDocumentValue('#device')
      } catch (err) {
        this.errors['device'] = err
      }
      this.updateStatusOf('device')
    },
    /**
     * Device種別取得
     */
    getEdgeDevice () {
      try {
        this.edgeDevice = this.getDocumentValue('#edgeDevice')
      } catch (err) {
        this.errors['edgeDevice'] = err
      }
      this.updateStatusOf('edgeDevice')
    },
    /**
     * ページパラメータ取得
     */
    getPageParam () {
      try {
        this.pageParam = JSON.parse(this.getDocumentValue('#pageParam'))
      } catch (err) {
        this.errors['pageParam'] = err
      }
      this.updateStatusOf('pageParam')
    },
    /**
     * 販促Json取得
     */
    getPromotion () {
      try {
        this.promotion = JSON.parse(this.getDocumentValue('#promotionJson'))
      } catch (err) {
        this.errors['promotion'] = err
      }
      this.updateStatusOf('promotion')
    },
    /**
     * ユーザー情報取得
     */
    getUserInfo () {
      try {
        this.userInfo = JSON.parse(this.getDocumentValue('#userInfoJson'))
      } catch (err) {
        this.errors['userInfo'] = err
      }
      this.updateStatusOf('userInfo')
    },
    /**
     * 式場情報取得
     */
    getWedding () {
      try {
        this.wedding = JSON.parse(this.getDocumentValue('#weddingJson'))
      } catch (err) {
        this.errors['wedding'] = err
      }
      this.updateStatusOf('wedding')
    },
    /**
     * コース一覧情報取得
     */
    getCourseList () {
      try {
        this.courseList = JSON.parse(this.getDocumentValue('#courseListJson'))
      } catch (err) {
        this.errors['courseList'] = err
      }
      this.updateStatusOf('courseList')
    },
    /**
     * コース一覧情報のホットペッパー対応
     */
    fixHotpepperData () {
      const hotpepper = this.tenpo.Feature[0].Children.find(
        (child) => child.CassetteId === this.namespace.cid.hotpepperGourmet)
      if (!(typeof hotpepper === 'undefined' || hotpepper === null) && hotpepper.OnlineReserveRequestFlag) {
        const hotpepperCourse = {
          name: 'コースを指定せずに席のみ予約',
          cassette: this.namespace.cid.hotpepperGourmet,
          image: '',
          reservationType: 1,
          price: 0,
          urls: {
            yahoo: '',
            original: hotpepper.ReservationPcUrl1
          }
        }
        this.courseList.totalResults += 1
        this.courseList.entry.push(hotpepperCourse)
      }
    },
    /**
     * Rapidの初期化設定
     * @param  {array} trackingModules RapidでトラッキングするモジュールのIDリスト
     * @param  {String} testType       A/Bテストの種類
     */
    initRapid (trackingModules, testType = '') {
      const miffyParam = this.miffy.getULTParam()
      if (Object.keys(miffyParam).length > 0) {
        this.pageParam['vtgrpid'] = miffyParam.vtgrpid
        this.pageParam['vtestid'] = miffyParam.vtestid
        // memo: 「v_pkjp」はライブテストのフィルタリングに利用する。複数のテストを走らせる場合に、テストごとに設定する。
        if (testType !== '') {
          this.pageParam['v_pkjp'] = testType
        }
      }
      return this.sr
        .setSpaceid(this.spaceId)
        .setPageParam(this.pageParam)
        .trackedClass()
        .initRapid()
    },
    /**
     * Rapid beaconの送信
     * @param  {string} sec
     * @param  {String} slk
     * @param  {int} pos
     */
    sendClickBeacon (sec, slk, pos) {
      if (!this.rapid) {
        return
      }
      if (!sec) {
        return
      }
      pos = (pos) || 0
      this.rapid.beaconClick(sec, slk, pos)
    },
    /**
     * インフィード広告JSONの取得
     */
    getAdInfeed () {
      try {
        this.adInfeed = JSON.parse(this.getDocumentValue('#adInfeedJson'))
      } catch (err) {
        this.errors['adInfeed'] = err
      }
      this.updateStatusOf('adInfeed')
    },

    /**
     * コメントの取得（件数表示のため）
     */
    getComment () {
      this.bindRequest({
        name: 'comment',
        url: this.namespace.API.kuchikomi,
        params: {
          gid: this.tenpo.Feature[0].Gid
        },
        dataType: 'json'
      })
    },
    /**
     * GIDをローカルストレージに保存する
     */
    saveGidToLocalStrage () {
      // local storageが使用できない場合なにもしない
      if (!('localStorage' in window) || !(window.localStorage !== null)) {
        return
      }
      // グルメジャンルの店舗以外は閲覧履歴に表示しない
      if (this.tenpo.Feature[0].Property.Genre == null || this.tenpo.Feature[0].Property.Genre.length === 0 || this.tenpo.Feature[0].Property.Genre[0].Code.slice(0, 2) !== '01') {
        return
      }

      let gid = [this.tenpo.Feature[0].Gid]
      if (localStorage.getItem('Gid') !== null) {
        const historyGid = JSON.parse(localStorage.getItem('Gid'))
        if (!historyGid.includes(this.tenpo.Feature[0].Gid)) {
          gid = gid.concat(historyGid)
        } else {
          gid = historyGid
        }
      }
      localStorage.setItem('Gid', JSON.stringify(gid.slice(0, 5)))
    },
    /**
     * URLパラメータのsc_typeを取得する
     */
    getScType () {
      const params = new URLSearchParams(window.location.search)
      this.scType = params.get('sc_type')
    },
    /**
     * URLパラメータ「sc_type」に応じてコース一覧をフィルタする
     */
    filterCourseList () {
      if (this.scType === '1' && this.courseList.totalResults > 0) {
        this.courseList.entry = this.courseList.entry.filter((l) => l.cassette === this.namespace.cid.yahoo)
        this.courseList.totalResults = this.courseList.entry.length
      }
    },
    /**
     * URLパラメータのsc_typeにより非表示化する要素か判定するメソッド
     * @param {string} id
     */
    isHiddenScType (id) {
      if (this.scType === '1') {
        // 非表示IDリスト
        const scType1HiddenIds = ['sale_prm', 'areareco', 'history', 'ranking', 'research', 'srv_prm', 'summary', 'coupon', 'info', 'wcal_grm', 'rvlist', 'yjAdsJs']
        if (scType1HiddenIds.includes(id)) {
          return true
        }
      }
      return false
    },
    /**
     * 静的にレンダリングされたaタグのhrefのURLにパラメータを追加する
     */
    addParameters () {
      if (this.scType === '1') {
        // hrefにロコダイニングのURLが指定されているaタグ要素を取得
        const aTags = Array.prototype.slice.call(document.getElementsByTagName('a')).filter(a => {
          return a.hasAttribute('href') && (a.getAttribute('href').startsWith('/place') || a.getAttribute('href').includes('loco.yahoo.co.jp/place/'))
        })

        // hrefのURLにパラメータ追加
        let scTypeParam = 'sc_type=' + this.scType
        aTags.forEach(function (a) {
          let params = new URLSearchParams(a.href.split('?')[1])
          if (!params.get('sc_type')) {
            a.href = a.href + (a.href.indexOf('?') >= 0 ? ('&' + scTypeParam) : ('?' + scTypeParam))
          }
        })
      }
    },
    /**
     * 指定されたURLに指定パラメータを付与する
     * @param {string} baseUrl
     * @param {string} paramNames
     */
    addUrlParameters (baseUrl, paramNames) {
      if (baseUrl && paramNames) {
        let url = baseUrl
        const params = new URLSearchParams(baseUrl.split('?')[1])
        paramNames.forEach(function (paramName) {
          if (paramName === 'sc_type' && !params.get('sc_type') && this.scType === '1') {
            // scTypeかつURLにsc_typeが設定されている場合
            const value = 'sc_type=' + this.scType
            url = url + (url.indexOf('?') >= 0 ? ('&' + value) : ('?' + value))
          }
        }, this)
        return url
      }
    }
  },
  created () {
    // YjAdJsの移動
    document.addEventListener('DOMContentLoaded', () => {
      const adsEls = document.querySelectorAll('.yjAdsJs')
      if (adsEls.length > 0) {
        Array.prototype.forEach.call(adsEls, adsEl => { // IE対応のためArrayのforEach (polyfill)を使います
          if (typeof adsEl.dataset.to !== 'undefined') {
            const dest = document.querySelector('#' + adsEl.dataset.to)
            if (dest !== null) {
              dest.parentNode.insertBefore(adsEl, dest)
              dest.parentNode.removeChild(dest) // IE対応のためremoveは使えません
            } else {
              adsEl.parentNode.removeChild(adsEl) // IE対応のためremoveは使えません
            }
          }
        })
        // PCのoverlay広告の表示制御
        const lrec = document.querySelector('.lrec')
        if (lrec != null) {
          // lrecの位置を取得
          const lrecOffsetTop = lrec.offsetTop
          const lrecHeight = lrec.offsetHeight
          const lrecUnderPosition = lrecOffsetTop + lrecHeight
          // overlay広告
          const overlayAd = document.querySelector('.ydnOverlay')
          let windowScrolled = 0
          window.addEventListener('scroll', () => {
            windowScrolled = window.pageYOffset
            if (windowScrolled > lrecUnderPosition) {
              overlayAd.style.display = 'block'
            } else {
              overlayAd.style.display = 'none'
            }
          })
        }
      }
    })
  }
})
