import logoIkyu from '@asset/images/common/cp/logo_ikyu.png'
import logoGnavi from '@asset/images/common/cp/logo_gnavi.png'
import logoOzmall from '@asset/images/common/cp/logo_ozmall.png'
const logoDining = 'https://s.yimg.jp/images/loco/front/images/common/cp/logo_dining.svgz'
const logoHotpepper = 'https://s.yimg.jp/images/loco/front/images/common/cp/logo_hotpepper.svgz'

/**
 * baseModelを継承するとthis.namespaceとしてアクセスできます
 * 環境によってprod.jsと切り替わります
 * @type {Object}
 */
const namespace = {
  API: {
    lsbe: `/localapi/tenpo/CQgD-fVDZ_M`,
    coupon: (gid) => `/localapi/coupon/CQgD-fVDZ_M`,
    comment: `/localapi/commentSearch/roL6V4pxoog`,
    commentDetail: `/localapi/commentSearch/CQgD-fVDZ_M_detail_stub1`,
    commentPosted: `/localapi/commentSearch/CQgD-fVDZ_M_posted_stub1`,
    dailyStock: 'https://reservation-restaurant.yahooapis.jp/v1/stock/daily',
    timeStock: 'https://reservation-restaurant.yahooapis.jp/v1/stock/time',
    kuchikomi: '/localapi/commentSearch/mUAAmbQeOk6',
    planStock: '/v1/api/planStock',
    latestInformation: '/localapi/latestInformation/CGBtPVWV6EE',
    resizeImage: '/v1/api/resizeImage',
    holiday: '/v1/api/generalPurposeCalendar',
    rdsig: '/v1/api/rdsig',
    imageSearch: '/localapi/imageSearch/q2Ek09b0io6',
    secure: '/localapi/secure/5QZW7VJEDDHSMD7MGX7VZV6F5Q',
    clipList: '/localapi/clip/clipList',
    addClip: '/localapi/clip/addClip',
    removeClip: (gid) => `/localapi/clip/removeClip`,
    clipTenpoList: `/localapi/tenpo/multitenpo/multitenpo`,
    research: '/localapi/tenpo/multitenpo/multitenpo',
    announcement: '/v1/api/planPolicy'
  },
  cid: {
    asoview: '0d9cfe2d3f3c3fc56ea2c95fbe0093d7',
    demaekan: 'bf16f724fb6d576424d5fc3085b6715c', // 出前館
    gurunavi: 'a6e27b84663abc4336dc275c6fb7f978',
    yahoo: 'ccbf79674338330c7ce5557ace5a18ec',
    zexy: '78ef8c916628ffb0acaafadb8a6e1842', // ゼクシィ
    yahooLoco1: '6d079a52dff50d920c08ddcc3a624e72',
    yahooLoco2: '9b2cd020801fb855e09df64433ce1dcb',
    locoKuchikomi: '40f68cf163a82aeb127f0387eb314b4b',
    yahooSportSky: 'ab1e5420799bd5c194d952a50ae603bd',
    jyukuNavi: '68caed36458f5a7187a67f252f13150e', // 塾ナビ
    mentsu: '65af37f496012d54e20d2f9393497532',
    niftySweets: 'ca91043966340f3ffd88b6ee984f4c7f',
    ikyu: '1852ed1e9b6d12c23cd34e79a4be4c71', // 一休
    hotpepperGourmet: '6a3b6ed40d949a607ebbf0d058cdd838', // ホットペッパーグルメ
    hotpepperBeauty: 'f6869ef765e5edc762f365f6727a1123', // ホットペッパービューティ美容
    hotpepperBeautyKirei: 'f010f11f7b21c41ce69e7c8e18d6aceb', // ホットペッパービューティキレイ
    eparkBeauty: '5cf6c7ee36974973f4c41a8a2530d912', // EPARKビューティ
    epark: '1bf84daa1328418fdf967e4b104d6f9f', // SGS(EPARK)※CassetHeaderはローカルプレイスになっている
    eparkDental: '92408efd109ceada4b65ae5b3f083b9d', // 歯科予約[EPARK]
    eparkEsthe: '3cf99081f2de70cd4cfad507521bc1ef', // リラク＆エステ予約[EPARK]
    jaranGolf: '7415bf1b866f9f3045ed0fd72d73498b', // じゃらんゴルフ
    tabirai: 'feff4a8e8e295faffd2bb38861721a30', // たびらい
    mrso: 'be98cee8588435cf3120a872e2972c6c', // 人間・検診マーソ‎：mrso
    kokokarada: '7e32b6dc66b4e168a2f4cc4827bff36b', // 人間ドックのここカラダ
    eparkPharmacy: 'b00ce56d4236aafd425b4083b0daddb3', // EPARKくすりの窓口
    yahooTravel: '8024afaf82bcaaa4c33b91deea651df1', // Yahoo!トラベル
    ipoca: '73d5e573ab9d1d7dea843fef5457be1f', // ipoca
    tokubai: '36cd4c492aff188aa9ec7f144bf586f3', // トクバイ
    bTimes: 'cc12d71d8765819ea087c6c51e282c0a', // B-Times
    eparkSweats: '82af20a74fd2952c1a88a07559852bf1', // epark sweats
    gourmetCidGroup: [
      'ccbf79674338330c7ce5557ace5a18ec', // Dining,
      '1852ed1e9b6d12c23cd34e79a4be4c71', // Ikyu,
      'a6e27b84663abc4336dc275c6fb7f978', // Gnavi,
      '20c4cc8c6339cc78326d4d5f0395dcb0', // Ozmall,
      '3f48a559dbfef9bd7ba6b6ef784e23b8', // Ozmall,
      '6a3b6ed40d949a607ebbf0d058cdd838' // Hotpepper
    ],
    beautyCidGroup: [
      'f6869ef765e5edc762f365f6727a1123', // ホットペッパービューティ美容
      'f010f11f7b21c41ce69e7c8e18d6aceb', // ホットペッパービューティキレイ
      '5cf6c7ee36974973f4c41a8a2530d912', // EPARKビューティ
      '1bf84daa1328418fdf967e4b104d6f9f', // SGS(EPARK)※CassetHeaderはローカルプレイスになっている
      '92408efd109ceada4b65ae5b3f083b9d', // 歯科予約[EPARK]
      '3cf99081f2de70cd4cfad507521bc1ef', // リラク＆エステ予約[EPARK]
      '7415bf1b866f9f3045ed0fd72d73498b', // じゃらんゴルフ
      'feff4a8e8e295faffd2bb38861721a30', // たびらい
      '82af20a74fd2952c1a88a07559852bf1' // epark sweats
    ],
    medicalCidGroup: [
      'be98cee8588435cf3120a872e2972c6c', // 人間・検診マーソ‎：mrso
      '7e32b6dc66b4e168a2f4cc4827bff36b' // 人間ドックのここカラダ
    ],
    // ロコの口コミ詳細をもつCP
    moveLocoDetailCidGroup: [
      'ab1e5420799bd5c194d952a50ae603bd',
      '68caed36458f5a7187a67f252f13150e',
      '65af37f496012d54e20d2f9393497532',
      '6d079a52dff50d920c08ddcc3a624e72',
      '9b2cd020801fb855e09df64433ce1dcb',
      'ca91043966340f3ffd88b6ee984f4c7f'
    ]
  },
  /**
   * 外部リンクのUrl
   *
   */
  Url: {
    // ログインURL
    login: 'https://login.yahoo.co.jp/config/login',
    // Y!地図
    map: 'https://map.yahoo.co.jp',
    // Y!カーナビ
    car: 'https://carnavi.yahoo.co.jp',
    // Y!路線
    transit: 'https://transit.yahoo.co.jp',
    // ImageWizardのBaseUrl
    iwizBase: 'http://iwiz-loco.c.yimg.jp',
    // Y!検索
    search: 'https://search.yahoo.co.jp',
    // Y!しごと検索
    job: 'https://job.yahoo.co.jp',
    // 予約フォーム
    reserveForm: 'https://r.reservation.yahoo.co.jp/reserve/input/',
    // プレミアム会員登録
    registPremium: 'https://premium.yahoo.co.jp/order/card_reg/',
    // プラン詳細入力画面のプレミアム会員登録
    registPremiumLinkCode: 'P_2021.C_C6dng097',
    // プラン詳細確認画面のプレミアム会員登録
    registPremiumButtonCode: 'P_2021.C_C6dng099',
    // プラン詳細入力画面のスマートログインURL
    planDetailInputSmartLogin: 'https://carrier.login.yahoo.co.jp/softbank/start?src=loco&from_id=link_loco_course_dtl&done=',
    // プラン詳細確認画面のスマートログインURL
    planDetailConfirmSmartLogin: 'https://carrier.login.yahoo.co.jp/softbank/start?src=loco&from_id=link_loco_course_dtl_conf&done=',
    // PCのYJDクッキーセット用エンドポイント
    setYjdPc: '/yjdset/pc',
    // SPのYJDクッキーセット用エンドポイント
    setYjdSp: '/yjdset/smartphone'
  },
  /**
   * コースで表示するロゴ
   */
  courseLogo: {
    'ccbf79674338330c7ce5557ace5a18ec': logoDining,
    '1852ed1e9b6d12c23cd34e79a4be4c71': logoIkyu,
    'a6e27b84663abc4336dc275c6fb7f978': logoGnavi,
    '20c4cc8c6339cc78326d4d5f0395dcb0': logoOzmall,
    '3f48a559dbfef9bd7ba6b6ef784e23b8': logoOzmall,
    '6a3b6ed40d949a607ebbf0d058cdd838': logoHotpepper
  },
  /**
   * NoImage画像のパス
   */
  noImage: {
    '200x200': 'https://s.yimg.jp/images/loco/front/images/common/noImage/200x200.png',
    '400x400': 'https://s.yimg.jp/images/loco/front/images/common/noImage/400x400.png',
    '130x90': 'https://s.yimg.jp/images/loco/front/images/common/noImage/130x90.png',
    '270x198': 'https://s.yimg.jp/images/loco/front/images/common/noImage/270x198.png'
  },
  /**
   * アプリへ飛ばすためのリンク（Approachを使用）
   */
  Approach: {
    // Y!地図アプリ
    map: (param) => 'https://stg-approach.yahoo.co.jp/r/KDPxGU?src=' + encodeURIComponent(param),
    // Y!カーナビアプリ
    car: (param) => 'https://stg-approach.yahoo.co.jp/r/I1JWIZ?src=' + encodeURIComponent(param)
  },
  cidPriority: {
    'efca7abc05c8e23eb6730ea3666055cf': 1, // JRシステム
    'ccafb5a32d608190f3e6c8dd35847773': 1, // ロコプレミアムカセット
    'fc79df0c92570d73837433fa66f70a2c': 1, // 東京カフェ360
    'bb181b59aa335ed7ecc83f3f1411ae7f': 2, // ヒトサラ
    '1852ed1e9b6d12c23cd34e79a4be4c71': 2, // 一休
    '6a3b6ed40d949a607ebbf0d058cdd838': 5, // ホットペッパー
    '518d328f20aa43f667ce53a27bd6cd75': 5, // yoyaQ
    '132c700e0ebf5a1d386e9b725d7d1e6b': 6, // ロコプレイス
    '8024afaf82bcaaa4c33b91deea651df1': 6, // Yahooトラベル
    'a6e27b84663abc4336dc275c6fb7f978': 7, // ぐるなび
    '98c3fee387a42040f9cd0cb8a2889214': 7, // フォートラベル（国内）
    '495994ae74f963272cb2911309aa63d6': 7, // フォートラベル（国外）
    '354fa32c107eec4bd1d2f5ea72f71bee': 8, // Retty
    '464c71b5570d5e0fb005a0b11a67e146': 9, // 食べログ
    'fae87802e3fdc839611859a7bb3fe738': 13, // ピクニック
    'default': 12 // デフォルト
  },
  /**
   * ULT用CP名一覧
   * TODO: yahoo がない
   */
  CpName: {
    'd8a23e9e64a4c817227ab09858bc1330': 'navit',
    'd115e2a62c8f28cb03a493dc407fa03f': 'mcdonalds',
    'ae8d8d10ea8c3ff00f55896ede3c97d4': 'nipponrentacar',
    '508c00c69133da7dfd69a99a9cb1ef54': 'nakau',
    '5dd0aca2e9197087f68cfe2d4bac33ee': 'bigboy',
    '4522a323ef7dd08c7680bdf11b32d1b8': 'hamazushi',
    'f18e934ca9d6e7d6e0db41de6bfd1a87': 'kyubeiya',
    '8cfa8ed3d825ba896fa8d7cffd28cd67': 'sukiya',
    'ba76de2ad713c8a24e2af1bd97370d46': 'gyuan',
    '88e0c2535f4804835069954e3c7837cb': 'morivacoffee',
    '93140ad6cd07edb9ec38e26ad7590ce3': 'hanayayohei',
    'c56080f497b4e9fc8d006d33d9aff823': 'cocos',
    'd5f06aa93b21ce84f20dfe38a7072aae': 'takarajima',
    'a9f65bf4dae2bf3f69d463c00415334a': 'carlifenavi',
    '6ee7f59476555c28ad003417985e04fc': 'couples',
    '4136755b3cd84823420aec02ac373152': 'sonet',
    'eab81106af45f5456d1fb602c66cd743': 'nota',
    '72dbf05e5bdf74120d0deb3bb92343cf': 'ikyu',
    '6e6c4795b23a5e45540addb5ff6f0d00': 'doutor',
    'd0054197a8725a024ff8fe92d4ce12a8': 'jefb',
    '7a0b0c98e2c57bcc0b5e6d9473896053': 'maruzen',
    '1852ed1e9b6d12c23cd34e79a4be4c71': 'restaurantikyu',
    '246d30b0cd1da37a2ff4350f8c580bed': 'sumitomordrievenhouse',
    '5ddc942d9691acec4acf6dfb79106c4f': 'sumitomordparking',
    'b380a0c2254fe2a7f557dfa5a6cdbd26': 'sumitomordesforta',
    '99b9b6aa7fe725ec6399eb375019549d': 'sumitomordbellesalle',
    '3f48a559dbfef9bd7ba6b6ef784e23b8': 'ozmallprixfixe',
    '20c4cc8c6339cc78326d4d5f0395dcb0': 'ozmallgourmet',
    '9001cb326636c79e34eaf2782fcf617c': 'ozmallhotel',
    'c51913d0617a942c6855ea988edbe278': 'takumen',
    '8cab50f5fa1e91e1c5aafdb6b602b061': 'saizeriya',
    'fedc86009af58c19c9bed49fc98d691e': 'tokyudept',
    '49a0f9a95cd2a5cfef870d96638c73c9': 'ministop',
    '04dfaa08e72a293f512172ab2c22ed73': 'psyfa',
    '464c71b5570d5e0fb005a0b11a67e146': 'tabelog',
    '2ffe4dc7c6df792fb14fb1ec4d3ffe57': 'alike',
    'ff14afb7b35118f27d1d0cd68fed1174': 'lotteria',
    '410bb5f98baf6367d1fb32f07c571eda': 'daiei',
    '9338d10cb766d637cbdfad25974ea469': 'olympic',
    '9c24b3890149f3730c8a7da31b2fbab3': 'inageya',
    '6a7826d9d372bbb2a24389323cd3c91f': 'mitsuifudosan',
    '98c3fee387a42040f9cd0cb8a2889214': 'kokunai4travel',
    '495994ae74f963272cb2911309aa63d6': 'kaigai4travel',
    '42811a342fdc5dfde417d65d192e60f7': 'mitsubishiretail',
    '7c64afac298f96290dfa003cdd623804': 'alikehotel',
    '2a154f21af6def6cc66b715c6cec1f04': 'nissanrentacar',
    '4f0374cef8a9b8dd5df5c083755dc26b': 'tbc',
    '40be135c6cd1e3e813c0b901cd13467f': 'hanacupid',
    '78e0307fb67c6911d70915fcf09fdad0': 'autocamp',
    '95c35a4e0c967ebc497e956e3d151e39': 'npc24h',
    '2db2ccbe99584031d2f94f552e8210f5': 'taiyakan',
    'de595b4b366062dc00b5ccb36b0321d5': 'happyhotel',
    '5d28716a4c08f5713c136e72946c9b0e': 'gogogs',
    'bb181b59aa335ed7ecc83f3f1411ae7f': 'usen',
    '8f1a8b98050264310879e5ae1dbd8ca2': 'sbmg',
    '88b7809e38a53132ed97682fd9b98e2a': 'appleworld',
    '5a45c0e859e86a133398f7da523bb2bd': 'ana',
    'a7f643648bfb8a297437f2d1aa4379f7': 'qlife',
    'c27d6ed02bcd330b012062d1af72aefc': 'repark',
    '1df61a553b5ccc30479e58a17612d7cd': 'gyao',
    'd91427ef429bcbe19c9bdf4d8549d50d': 'arukikata',
    '6a3b6ed40d949a607ebbf0d058cdd838': 'hotpepper',
    '5cf6c7ee36974973f4c41a8a2530d912': 'kamimado',
    'baaac0da45c7295aacf99f7fea684fc0': 'kusupa',
    'a245f1c98852a791821faf21c2913f69': 'hoteljp',
    '4f20d833838a6e5eb70e55811ae36a2f': 'zenrin',
    'd588f76228463f6838591e5e376b3738': 'locanavi',
    '518d328f20aa43f667ce53a27bd6cd75': 'yoyaq',
    '6ec22374cecd0a9e2d60f089852a358c': 'sainavi',
    'e15cccabf3cd8f8f85748e07be45ed0c': 't_kouyou',
    'e0beeecdfa56e18a4626319999f262ae': 'happymama',
    '199f3a79b6f612c6dba0fa7906c48a2c': 'kamakura',
    '93073a97535d22576556103a4be599d0': 'osakainfogourmet',
    '546c076cef82ef488b05c789806e8340': 'osakainfoshop',
    '97d15dff85a24d84eaa46b82f897672f': 'osakainfokanko',
    '32e09c74efdda89f6b33586281934916': 'aptinet',
    '9f55f50256fafe92e805f3fac026adfe': 'superjois',
    'd22029501bd0c1636d1329c392225f8f': 'maeda',
    '69d1b18764c4b2c762b251ac95e065e3': 'supermaruto',
    'a82b5126a906d1bfeadf0338ab80a948': 'ujie',
    'cd246abc0da32d9d0e4990a54e3bf0e4': 'otera',
    'ee23dcabfc2c8dc43ef582e4b98ea84c': 'fubic',
    '8e9c99f2227b2113f8e5c92091743f81': 'hatostop',
    '5ae7aa8cbb10e632a29cf4ea960f7fd3': 'hatoeigyosyo',
    '4e7027c0f23b6a0c9d493edf6a37f12e': 'allabout',
    '2062ceb3e71be93ccf52720cc56e811a': 'petnomori',
    'fa768dcaf80e74238e6779e76104b1ee': 'gemasen',
    'd9a261af00d3046444e78e8179272fe4': 'parco',
    'a05b449585f4bafff020413e0793e355': 'd_aeon',
    'e74633aa88bcbd9177b1cd47b9516936': 'd_ministop',
    '6323cd03fade0af478c41aeef6dc261b': 'navita',
    '9387c078180a308de5da5fc9f0fbc459': 'coinpa',
    '5e9db9cedf2927bf5dc02cb9c860e49c': 'fashioncity',
    'e7045438550dcbf61baa79e136bda227': 'clisktsuushinsei_new',
    '8f9a5d20026cfc995034ea1ad20e5a29': 'busiseitai',
    '93deba9c9aa422db47d88128a76ca4d1': 'busisekkotu',
    '5a06392ab4d9c846a86f2c66d5dacb72': 'seibubus',
    'f8597dbd45a02f5a605f78745a2ce828': 'tabihatsu',
    '02c85114d54c51493b1d7652ce820144': 'oikura',
    '83a21dee7c162c51ddb42fad2e82ed75': 'taxisite',
    '99e3735f9e4a5c0439246a2fa5e2fede': 'seiyu',
    '097e67acf9d44a3ccb59b42f345169e7': 'yamadadenki',
    '662fa598e299c505df326a938daa4637': 'yunavi',
    '29fb62deea329b4d12c9f343f03bb851': 'mkyosho',
    'b2674b78187b971005f814fc04e355d9': 'ticketpia',
    '13a1d4c267343ffdcad244875915afcb': 'claej',
    '7627e483b7b75ba4e03ce172e32030f8': 'aco',
    'abaf9dcc22f2d6bf3e7656f73a179be2': 'bassennavi',
    '0239ef1233ea70500d7a320e06f439ea': 'lococom',
    '482d6a560d5cf55638e678f89d0893a3': 'goobeauty',
    '995b2f37404c1c7b755ffc425b35d3da': 'googourmet',
    '190618aaf900fae4b1ebbf2dd85b49ea': 'goohobby',
    '40589b9085052912b0eca43f59c333d8': 'goolife',
    '668d62d4659e4100d5f90f99dcfb8cb5': 'gooschool',
    'eb0e80d0d3cfeb17eb48c2f162e790f9': 'museumcafe',
    'c080862a560d823cb8127b76fda16d22': 'futone',
    '354fa32c107eec4bd1d2f5ea72f71bee': 'retty',
    '18c8cca1bc366ab707e8e27e99804b20': 'internetmuseum',
    '5eaf00ddd7c58ac5181cb5651f2265b8': 'gymco',
    'd645c0ac3d46e5a61155f0f3aff27fa3': 'qbhouse',
    '1e9b3973d45937e179ff569ae3ba086f': 'qb_quatrebeaute',
    '9a2d4ee07d314068d8c7dd0075c2bd61': 'qb_ikka',
    '42f0c3a4c38d0d10b329ee7d3fb90e5e': 'qb_fass',
    '094c35894a9a066e2a50cee7c0ff5b82': 'tabiplaza',
    '350d615b095005d20d2479f76668cd66': 'kokosilginza',
    'fae87802e3fdc839611859a7bb3fe738': 'picknic',
    '2bb6ce926df44bbf68ec6d71a3389354': 'taito',
    '5e06375ef0eb0d20bada987fd3fa3194': 'umi_beach',
    '1797decd7bfdbe57b678096939ae132f': 'umi_house',
    '929161aad43c0a6ab630af3facef3036': 'ikoyo',
    'f6c0acee48bccac4cd11007f033f5249': 's_ramendb',
    '1ce563045a57dcf322cf37697bc052ff': 's_currydb',
    '8170d3e4421ba0349f8437201db63c1a': 's_chahandb',
    '0adbb2eb6a4a89999fda6944c6785d0d': 's_gyouzadb',
    '47a5b8f36f9ffca1620e82c462a8cd4c': 's_udondb',
    '87e137ef107ce2c8c34094f96182a465': 's_sobadb',
    '2d473a273b718fed75c9f5f0e0cdc63b': 'qwintet',
    'e54362bc9cbf7d3e7955cb7d1da93f3e': 'busibenriya',
    'b93a109b57ebcacba74228fcaffa1290': 'innhome',
    'eb9de5955fdd65c6eb5ddb89b3ad353c': 'webrentacar',
    '3b1fbcb8f3296382eb67f7aa0bcfedcd': 'sagasix',
    '9fead1f8755a87b566599f7a9f798cd7': 'carsharing',
    '68caed36458f5a7187a67f252f13150e': 'jyukunavi',
    'ca91043966340f3ffd88b6ee984f4c7f': 'niftysweets',
    '1db65834a724b77c9cda45b03ade74e8': 'konest',
    'cedee23d68ddae2d3ec86fc64b0ad705': 'shufoo',
    '87987ac40905a2661c3b8ae0a76da65d': 'a_edogawa',
    '3058cdd9691b83d9421a8977b7fae388': 'a_koto',
    '51925c6f21a9bab81f52d465bc6f4332': 'a_sumida',
    'ad7b137a8f494afe2b3310364b88a1b3': 'a_tsuzuki',
    '3e65735e14fc05e6ba4775c0c1921533': 'a_aoba',
    'e12ca8530b2d50dbe405da3fdca2180c': 'a_miyamae',
    '0c52154c177af2d6ba40a7ebf59f0d6e': 'a_takatsu',
    '07aee8b0e30f4e63cea48ead4beab601': 'smbc',
    '6c9ed36d3d634816abbe1f1b9f79ad1d': 'smbc_app',
    '3922dad82772c904acce8ea2e49025d7': 'eshops',
    '2597c2166acbb3d6f8ef1e71a4224e09': 'familymart',
    '7fb475e0ea4d6621bd658e5eb5276b06': 'c_tsutaya',
    '8a37dfc11807248129e504cb737e800c': 'n_tokubai',
    '65af37f496012d54e20d2f9393497532': 'mentsu',
    'fd63d70aaa4d1a9239096237b7990e01': 'machikoe',
    'f004593f66bed563cac9013936855bac': 'mizumono',
    'cd5b7a902ece45051ba05cbb9f064077': 't_tachikawa',
    '0fb198d275a5a1f75700287ddb201e80': 'sbmwifispot',
    'e6b0ade40f685c169bff96d7597dc8bf': 't_pokanavi',
    '81394748cf639c1f890a78cf89bf1d4b': 'mizuhobank',
    'a6e27b84663abc4336dc275c6fb7f978': 'gurunavi',
    '681d64a86afe18b8e27cde8029f1e5d7': 'ana_gourmet',
    '1cc42368110a8e0606cfa2ad050e754c': 'ana_spa',
    '1728fd996e6ea5b825567b74f002cc44': 'ana_omiyage',
    '5ed7c55e833b960e37e229451e710d38': 'ana_hairsalon',
    '91ee1befc42c6f4d93e4fc74fd2f7d0b': 'ana_pet',
    '5b964c7ade910c7fbb7467953bbb5394': 'ana_iyashi',
    'b5acadfff6430020f493915793311fbf': 'tokyo_churinjo',
    'd5f0deed2aa10ce90e86a7f4d846b513': 'autophoto',
    'ecb21673dbea72e2eaa02f6be4c9ddf1': 's_lawson',
    '78ef8c916628ffb0acaafadb8a6e1842': 'zexy',
    'bf16f724fb6d576424d5fc3085b6715c': 'demaekan',
    'f452be4b573b791352b5fdbd27fed4b7': 'komeri',
    'b63dcc0f26b6305faf9e2438c3e83dc9': 'depart',
    '58130138c22bff0f92c04c1cb439c87c': 'sbtlcomwifispot',
    '2a87eb1a929eca03f6eff528161fba05': 'mainichi',
    '3ac5ff36627638e92fed608ea54a6e3c': 'ccc_tpoint',
    'b3eb889cef30f48d7da041ea8bbcab8c': 'kentucky_2nd',
    'a24a9616bb72645a73e5e3358fc1b54a': 'bookoff_2nd',
    '4e0af38d5e9d77ab597d584a903b20a7': 'pizzahut_2nd',
    '2d5aa8e0e0da7c53bcaff84b96e8953c': 'gulliver',
    '1f30d698e747f888721df3c8f072b363': 'gulliver_coupon',
    '708b83b6a657e30684b775666b650f02': 'skylark_gusto',
    'abc9b947758bfb0a7a1d48616851b06b': 'skylark_gusto_coupon',
    'f5b28752049f754b1a538903ef56cac0': 'jrs_ski',
    '63a6f53cba5d0a6195688431426b9d55': 'yoshinoya',
    'ef6cdb09d95109983a05342dd8c625ad': 'autobacs_2nd',
    '8a754f03fbaa2c7bbe38b9f8d01660c9': 'refuge',
    '4668a2ac74660ac4d6e4e606f5b4e172': 'area_point',
    '8e2a60cf58059e24cd764d215a5878da': 'sbmwifi_en',
    'ce6e0eddc1403781d6354e1e5be7465e': 'proto',
    'cf02862fe31b76eb20ba494c7ab50a32': 'zenrin_en',
    '6b56e6ac7f724f7eba241bb7e2d94065': 'chizuapp_mapsymbol',
    '416e60b3195a6eb0b684c66561acfa34': 'chizuapp_representativepoint',
    'f70254ee653275d0c956d899f9f0055b': 'hakoboonmini_famima',
    '1079bad62ba16e20474191940f54c183': 'z_indoor',
    '92408efd109ceada4b65ae5b3f083b9d': 'dental',
    'e6f583fa8f62b2655db02d1b2f2d148f': 'tourdetohoku',
    '6d079a52dff50d920c08ddcc3a624e72': 'jwa_snow',
    '4640bb5a4be84592ff04dbc31be748bf': 't_hanami',
    '552f155e3b7fdcf2b3ba18a17a72a8cf': 'y_event',
    'add29ed4b874b7cffe1af5d4d72a7dbc': 'e_enopo',
    '6b7ac417a0fa730af08685104bf318b3': 'e_machicon',
    'c8892c8ad8cdc2eb9cb69ad0b289516a': 'jrs_kouyou',
    '5dd3c74e644c2d8d7d9f4559f51a09fd': 'ymshop',
    '73d5e573ab9d1d7dea843fef5457be1f': 'ipoca',
    '1bf84daa1328418fdf967e4b104d6f9f': 'sgs',
    '69e0af1c2366e95158637065350a3076': 'meat',
    '07329828173b119e1ba8d29e3e436332': 'imj_parking',
    '476aa59332a29bae1488663653f9fe69': 'dental_reserve',
    '8746aac46ac96505307ecb4c8b151ce1': 'chizuapp_enenpi',
    'b3e8ffe5b1803fabc755b8e1cb7e08d0': 'kuronekoyamato',
    'f6e86dbe8389dcbd931efb48e4e0e741': 'jafnavi',
    '61843281d08ba8a3e7c40011cbc37c28': 'zenrin_ko',
    '1007799d40a72b2be72a148934b51fbb': 'zenrin_zh_cn',
    '2244fa5663d0476232643f20f5e4ad6a': 'zenrin_zh_tw',
    'da880213f806aff62c5f67922ff4e9de': 'cosmo_oil',
    'efca7abc05c8e23eb6730ea3666055cf': 'jrs_cherry_blossom',
    '23e05d819c8ba11c0f6b54e2c07c38c1': 'y_healthcare',
    '822aafca0ad9c2849ba705a45546ddb1': 'gap',
    '11db10160685cd9a28f2c0aa27928fb7': 's_iroha',
    'e7443a5eae0e4ce3f791edaab12d3066': 'kuronekoyamato_group',
    '7e32b6dc66b4e168a2f4cc4827bff36b': 'cocokarada',
    'b65e030269b56f340afe4f415018109e': 'eneos_ss',
    '17c9c2cd9b548cff973d5812a1678755': 'dennys',
    'be98cee8588435cf3120a872e2972c6c': 'mrso',
    'ccbf79674338330c7ce5557ace5a18ec': 'dining'
  }
}

export default namespace
