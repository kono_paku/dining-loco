import $ from 'jquery'
import cookie from '@lib/cookie'

export default class Ajax {
  /**
   * httpリクエスト
   * @param {string} url URL
   * @param {string} type リクエストメソッド
   * @param {Object} params リクエストパラメータ
   * @param {string} dataType サーバから受け取るデータの形式 'auto'の場合はurlでcross-originか判定 e.g.) 'json', 'jsonp'
   * @param {string} contentType サーバに渡すデータの形式
   * @param {number} timeout タイムアウトの設定[ms]
   * @returns {Promise.<Object>} レスポンスのPromise
   */
  static request (
    url,
    type,
    {
      params = {},
      dataType = 'auto',
      contentType = 'application/json; charset=utf-8',
      timeout = 0
    } = {}) {
    if (dataType === 'auto') {
      dataType = /^\/(v1|localapi)/.test(url) ? 'json' : 'jsonp'
    }

    const headers = url.startsWith('/v1/api') ? {'X-XSRF-TOKEN': cookie()['XSRF-TOKEN']} : {}

    return new Promise((resolve, reject) => {
      $.ajax({
        type,
        url,
        data: params,
        dataType,
        contentType,
        headers,
        crossDomain: true,
        timeout,
        cache: false, // キャッシュなしにしないとIE11が動作しなくなるので注意
        xhrFields: {
          withCredentials: true
        }
      })
        .done(resolve)
        .fail((jqHXR, textStatus, errorThrown) => {
          reject(textStatus)
        })
    })
  }
  static get (url, params = {}, {dataType = 'auto', timeout} = {}) {
    return Ajax.request(url, 'GET', {params, dataType, timeout})
  }
  static post (url, params = {}, {dataType = 'auto', timeout} = {}) {
    return Ajax.request(
      url,
      'POST',
      {
        params: JSON.stringify(params),
        dataType,
        contentType: 'application/json',
        timeout
      })
  }
  static delete (url, params = {}, {dataType = 'auto', timeout} = {}) {
    return Ajax.request(
      url,
      'DELETE',
      {
        params: JSON.stringify(params),
        dataType,
        contentType: 'application/json',
        timeout
      })
  }
}
