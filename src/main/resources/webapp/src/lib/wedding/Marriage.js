export default class Marriage {
  /**
   * コンストラクタ
   * @param val 店舗情報
   */
  constructor (val) {
    // LSBEの式場情報の対象キー名
    this.mapping = {
      WeddingPartyFoodCharge1: '料理料金',
      WeddingAfterParty1: '2次会の利用',
      WeddingUsePossibleTime: '利用可能時間',
      WeddingChargeDesk1: '担当',
      WededingCeremonialStyle1: '挙式スタイル', // APIのレスポンスそのまま
      WededingCeremonialCapacity: '収容人数', // APIのレスポンスそのまま
      WeddingFacilitiesComment: '設備',
      WeddingAccommodation: '宿泊施設',
      WeddingPickUp: '送迎',
      WeddingPayment: '挙式支払い方法',
      WeddingComment1: '備考',
      WeddingBusinessHour: '営業時間',
      WeddingReferenceComment: 'お問い合わせ先備考',
      WeddingReservation: '予約',
      WeddingParking: '駐車場の有無'
    }

    // 対象店舗の式場情報
    this.info = []

    // 表示できるかどうか確認
    this.viewStatus = this.checkViewable(val)
    // 表示ステータスがOKならデータを設定する
    if (this.viewStatus) {
      this.info = this.makeInfo(val)
    } else {
      // throw new Error('式場情報が不足しています')
    }
  }

  /**
   * 式場情報として十分条件を満たすかどうか確認する
   * @param val
   * @returns {boolean}
   */
  checkViewable (val) {
    const detail = val.Feature[0].Property.Detail
    return Object.keys(this.mapping).some(element => detail[element] != null)
  }
  /**
   * 式場情報を返す
   * @param val
   * @returns {Array}
   */
  makeInfo (val) {
    const detail = val.Feature[0].Property.Detail
    let info = []

    for (const key in this.mapping) {
      if (detail.hasOwnProperty(key) && detail[key]) {
        info.push({
          name: this.mapping[key],
          val: detail[key]
        })
      }
    }

    return info
  }

  /**
   * 表示可能かどうか返す
   * @returns {boolean|*}
   */
  isViewable () {
    return this.viewStatus
  }

  /**
   * 式場情報を返す
   * @returns {Array|*}
   */
  getInfo () {
    return this.info
  }
}
