export default class Plan {
  /**
   * コンストラクタ
   * @param val 店舗情報
   */
  constructor (val) {
    // 最大解析数
    this.cnt = 60

    // 体験者レポートの数
    this.max_report = 6

    // LSBEの挙式プランの対象キー名
    this.mapping = [
      'ReportTitle',
      'ReportDetailUrl',
      'ReportSchejule'
    ]

    // 体験者レポート（絞る）
    this.reports = this.setReports(val).splice(0, this.max_report)
  }

  /**
   * 体験者レポートを設定する
   * @param val
   * @returns {Array}
   */
  setReports (val) {
    const detail = val.Feature[0].Property.Detail
    let reports = []

    for (let i = 1; i <= this.cnt; i++) {
      const title = this.fixName('ReportTitle', i)
      const detailUrl = this.fixName('ReportDetailUrl', i)
      const schedule = this.fixName('ReportSchejule', i)
      // リンク名とリンク先がなければ次
      if (!detail.hasOwnProperty(title) || !detail.hasOwnProperty(detailUrl)) {
        continue
      }
      if (detail[title] === '' || detail[detailUrl] === '') {
        continue
      }
      // テンプレート側ではtitle,detailUrl,scheduleで参照しているので注意
      let data = {}
      if (detail.hasOwnProperty(title)) {
        data.title = detail[title]
      }
      if (detail.hasOwnProperty(detailUrl)) {
        data.detailUrl = detail[detailUrl]
      }
      if (detail.hasOwnProperty(schedule)) {
        data.schedule = detail[schedule]
      }
      // 配列に入れる
      if (Object.keys(data).length > 0) {
        reports.push(data)
      }
      // 十分な量が取れたら終わり
      if (reports.length >= this.max_report) {
        break
      }
    }

    return reports
  }

  /**
   * 体験者レポートを返す
   * @returns {Array}
   */
  getReports () {
    return this.reports
  }

  /**
   * 表示判定
   * @returns {boolean}
   */
  isReportViewable () {
    return this.reports.length > 0
  }

  /**
   * 2つ目の以降の要素にはなぜか'Wedding'ってつかないのでここで補正する
   * @param name
   * @param i 1つ目は0ではなく1
   * @see https://jira.corp.yahoo.co.jp/browse/POTARA-1541
   */
  fixName (name, i) {
    if (i === 1) {
      name = 'Wedding' + name
    }

    return name + i
  }
}
