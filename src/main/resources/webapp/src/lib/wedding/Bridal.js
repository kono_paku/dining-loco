import moment from 'moment'
export default class Bridal {
  /**
   * コンストラクタ
   * @param val 店舗情報
   */
  constructor (val) {
    // 最大解析数
    this.cnt = 100

    // LSBEのブライダルフェアの対象キー名
    this.mapping = [
      'WeddingFairName',
      'WeddingFairDetailUrl',
      'WeddingFairSeat', // 在庫
      'WeddingFairContent',
      'WeddingFairSchejule', // 綴り間違ってるけどロコAPIがこうなってるのでこのまま
      'WeddingFairStartTime',
      'WeddingFairEndTime',
      'WeddingFairKind', // 内容
      'WeddingFairBenefit',
      'WeddingFairReservationPcUrl',
      'WeddingFairReservationSmartPhoneUrl',
      'WeddingFairImageUrl',
      'WeddingFairImageCaption'
    ]

    // ブライダルフェアをセット
    this.fairs = this.setFairs(val)
  }

  /**
   * ブライダルフェアを設定する
   * @param val
   * @returns {Array}
   */
  setFairs (val) {
    moment.locale('ja')
    const detail = val.Feature[0].Property.Detail
    let fairs = []

    for (let i = 1; i <= this.cnt; i++) {
      const name = 'WeddingFairName' + i
      const detailUrl = 'WeddingFairDetailUrl' + i
      const reservationPcUrl = 'WeddingFairReservationPcUrl' + i
      const reservationSpUrl = 'WeddingFairReservationSmartPhoneUrl' + i
      // リンク名とリンク先がなければ次
      if (!detail.hasOwnProperty(name) || !detail.hasOwnProperty(detailUrl) || !detail.hasOwnProperty(reservationPcUrl) || !detail.hasOwnProperty(reservationSpUrl)) {
        continue
      }
      if (detail[name] === '' || detail[detailUrl] === '' || detail[reservationPcUrl] === '' || detail[reservationSpUrl] === '') {
        continue
      }
      let data = {}
      for (const val of this.mapping) {
        if (detail.hasOwnProperty(val + i)) {
          if (val === 'WeddingFairSchejule') {
            data[val] = moment(detail[val + i], 'YYYY年MM月DD日').format('YYYY年MM月DD日(ddd)')
          } else {
            data[val] = detail[val + i]
          }
        }
      }
      if (Object.keys(data).length > 0) {
        fairs.push(data)
      }
    }

    return fairs
  }

  /**
   * ブライダルフェアを返す
   * @returns {*}
   */
  getFairs () {
    return this.fairs
  }

  /**
   * 表示判定
   * @returns {boolean}
   */
  isViewable () {
    return this.fairs.length > 0
  }
}
