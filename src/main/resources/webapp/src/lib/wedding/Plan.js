export default class Plan {
  /**
   * コンストラクタ
   * @param val 店舗情報
   */
  constructor (val) {
    // 最大解析数
    this.cnt = 60

    // LSBEの挙式プランの対象キー名
    this.mapping = [
      // プラン系
      'WeddingPlanName',
      'WeddingPlanTitle',
      'WeddingPlanDescription',
      'WeddingPlanPrice',
      'WeddingPlanPeriod',
      'WeddingPlanPeopleNumber',
      'WeddingPlanComment',
      'WeddingPlanBenefit',
      'WeddingPlanUrl',
      'WeddingPlanImage',
      'WeddingPlanImageCaption'
    ]

    // ウェディングプラン
    this.plans = this.setPlans(val)
  }

  /**
   * ウェディングプランを設定する
   * @param val
   * @returns {Array}
   */
  setPlans (val) {
    const detail = val.Feature[0].Property.Detail
    let plans = []

    for (let i = 1; i <= this.cnt; i++) {
      // リンク名とリンク先がなければ次
      if (!detail.hasOwnProperty('WeddingPlanName' + i) || !detail.hasOwnProperty('WeddingPlanUrl' + i)) {
        continue
      }
      if (detail['WeddingPlanName' + i] === '' || detail['WeddingPlanUrl' + i] === '') {
        continue
      }
      let data = {}
      for (const val of this.mapping) {
        if (detail.hasOwnProperty(val + i)) {
          data[val] = detail[val + i]
        }
      }
      if (Object.keys(data).length > 0) {
        plans.push(data)
      }
    }

    return plans
  }

  /**
   * ウェディングプランを返す
   * @returns {Array|*}
   */
  getPlans () {
    return this.plans
  }

  /**
   * 表示判定
   * @returns {boolean}
   */
  isPlanViewable () {
    return this.plans.length > 0
  }
}
