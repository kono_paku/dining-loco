export default function () {
  // [[key1, value1], [key2, value2]...] の形式
  const cookiesList = document.cookie.split(/; */).map((keyValue) => keyValue.split('='))
  // {key1: value1, key2: value2...} の形式
  let cookiesDict = {}
  for (const [key, value] of cookiesList) {
    cookiesDict[key] = value
  }
  return cookiesDict
}
