export default class Url {
  /**
   * GETパラメーター取得
   * @returns {Object}
   */
  static getParameters () {
    // GETパラメーターが空なら空オブジェクト
    if (window.location.search.substring(1) === '') {
      return {}
    }

    let params = {}
    for (const param of window.location.search.substring(1).split('&')) {
      const [key, value] = param.split('=')
      if (key !== '' && value != null) {
        params[key] = decodeURI(value)
      }
    }

    return params
  }

  /**
   * GETパラメータの生成
   * @param {Object} GETパラメータのオブジェクト e.g.) {lat: 35, lon: 135}
   * @returns {string} e.g.) '?lat=35&lon=135'
   */
  static buildParamString (params) {
    const keys = Object.keys(params)
    return '?' + keys.map((key) => `${key}=${params[key] != null ? params[key] : ''}`).join('&')
  }
}
