import namespaceProd from './namespace/prod'
import namespaceDev from './namespace/dev'

export default process.env.NODE_ENV === 'production' ? namespaceProd : namespaceDev
