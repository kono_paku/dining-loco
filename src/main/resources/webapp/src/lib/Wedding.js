import namespace from './namespace'
import Marriage from './wedding/Marriage'
import Plan from './wedding/Plan'
import Report from './wedding/Report'
import Bridal from './wedding/Bridal'

export default class Wedding {
  /**
   * コンストラクタ
   * @param val 店舗情報
   */
  constructor (val) {
    // 店舗名
    this.tenpoName = ''
    // CP名
    this.cpName = ''
    // CPId
    this.cpId = ''
    // CPURL
    this.cpUrl = ''
    // 式場情報オブジェクト
    this.marriage = new Marriage(val)
    // 挙式プランオブジェクト
    this.plan = new Plan(val)
    // 体験レポートオブジェクト
    this.report = new Report(val)
    // ブライダルフェアオブジェクト
    this.bridal = new Bridal(val)
    // 対象店舗の店舗URL e.g. https://loco.yahoo.co.jp/place/g-561qbuoW3to
    this.yurl = ''
    // GID
    this.gid = ''
    // 店舗データを設定する
    this.setTenpoData(val)
  }

  /**
   * 「サービス、他」をオンマウスしたときのプルダウンメニューの生成やサブメニューの表示に使う
   * @returns {Array}
   * @memo lsbeのyurlは最後にスラッシュがつかないが、be.olpのyurlは最後にスラッシュがつく
   */
  getSubNavigations () {
    let navigations = []

    if (this.marriage.isViewable()) {
      navigations.push({
        page: 'marriage',
        title: '式場情報',
        url: this.yurl.replace(/\/$/, '') + '/marriage/'
      })
    }
    if (this.plan.isPlanViewable() && this.report.isReportViewable()) {
      navigations.push({
        page: 'wedding',
        title: '挙式プラン',
        url: this.yurl.replace(/\/$/, '') + '/wedding/'
      })
    }
    if (this.bridal.isViewable()) {
      navigations.push({
        page: 'bridal',
        title: 'ブライダルフェア',
        url: this.yurl.replace(/\/$/, '') + '/bridal/'
      })
    }

    return navigations
  }

  /**
   * 店舗データを設定する
   * @param val 店舗情報
   */
  setTenpoData (val) {
    this.gid = val.Feature[0].Gid
    this.tenpoName = val.Feature[0].Name
    this.yurl = val.Feature[0].Property.Detail.YUrl
    // 数あるCPの中からリストの中にあるCPにヒットしたらそれを使う
    // 今のところゼクシィのみ
    if (val.Feature[0].Property.CassetteId === namespace.cid.zexy) {
      this.cpId = val.Feature[0].Property.CassetteId
      this.cpName = val.Feature[0].Property.Detail.CassetteHeader
      this.cpUrl = val.Feature[0].Property.Detail.PcUrl1
    }
  }

  /**
   * 店舗名を返す
   * @returns {string|*}
   */
  getTenpoName () {
    return this.tenpoName
  }

  /**
   * CP名を返す
   * @returns {string|*}
   */
  getCpName () {
    return this.cpName
  }

  /**
   * CPIdを返す
   * @returns {string|*}
   */
  getCpId () {
    return this.cpId
  }

  /**
   * CPURLを返す
   * @returns {string|*}
   */
  getCpUrl () {
    return this.cpUrl
  }

  /**
   * GIDを返す
   * @returns {string|*}
   */
  getGid () {
    return this.gid
  }
}
