/**
 * URLのpage=<pageNum>をハンドリングし、渡されたコールバック関数を実行します
 * history.back用に各ページでwindow.onpopstateを設定してください
 * IE11のサポートが切れたらURLSearchParamsの利用を検討する
 */
export default class Pagination {
  constructor () {
    // GETパラメーターの情報をオブジェクト形式で取得
    this.params = this.getSearchParameters()

    // ページ情報がなければ1に設定
    if (!this.params.hasOwnProperty('page')) {
      this.params.page = 1
    }
  }

  /**
   * GETパラメーターをオブジェクトで返す
   * @returns {{}}
   * @see https://qiita.com/Evolutor_web/items/c9b940f752883676b35d
   */
  getSearchParameters () {
    let vars = {}

    // GETパラメーターがなければ終了
    if (window.location.search.substring(1) === '') {
      return vars
    }

    const param = window.location.search.substring(1).split('&')
    for (const value of param) {
      const [x, y] = value.split('=')
      if (x !== '') {
        vars[x] = decodeURI(y)
      }
    }

    return vars
  }

  /**
   * URLを更新する
   * @see https://eureka.ykyuen.info/2015/04/08/javascript-add-query-parameter-to-current-url-without-reload/
   * @see https://qiita.com/cafelasm/items/2eb34eb3403e762e97dc
   */
  updateUrl () {
    const url = '?' + this.httpBuildQuery(this.params)
    if (window.history.pushState) {
      window.history.pushState(this.params, '', url)
    } else {
      // historyが使えなければ遷移させる
      window.location.href = url
    }
  }

  /**
   * オブジェクトをGETパラメーターに直す
   * @param parameters
   * @returns {string}
   */
  httpBuildQuery (parameters) {
    // オブジェクトじゃなければ終了
    if (!Object.keys(parameters).length) {
      throw new Error('No value found in parameters.')
    }

    // =で結合
    let parametersWithEqual = []
    for (const key in parameters) {
      parametersWithEqual.push(key + '=' + parameters[key])
    }

    // &で結合して返却
    return parametersWithEqual.join('&')
  }

  /**
   * URLのページ番号を1つ減らす
   */
  previous () {
    // ページ番号が1以下なら終了
    if (this.params.page <= 1) {
      return
    }

    this.paging(--this.params.page)
  }

  /**
   * URLのページ番号を1つ増やす
   */
  next () {
    this.paging(++this.params.page)
  }

  /**
   * URLを指定したページ番号に更新する
   * @param page
   * @param params ページ番号以外でGETパラメーターを指定したい場合に使う
   */
  paging (page, params = {}) {
    // URL更新
    this.params.page = page

    if (Object.keys(params).length) {
      for (const key in params) {
        if (key !== 'page') {
          this.params[key] = params[key]
        }
      }
    }
    this.updateUrl()
  }

  getPage () {
    // GETパラメーターの情報をオブジェクト形式で取得
    const params = this.getSearchParameters()

    // ページ情報がなければ1に設定
    if (params.hasOwnProperty('page') && parseInt(params.page, 10)) {
      return parseInt(params.page)
    } else {
      return 1
    }
  }
}
