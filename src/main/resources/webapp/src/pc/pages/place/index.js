import Vue from 'vue'
import Base from '@lib/baseModel'
import Ajax from '@lib/Ajax'

import signboard from '@pc/components/place/signboard.vue'
import calendar from '@pc/components/place/calendar.vue'
import reserveButton from '@pc/components/place/reserveButton.vue'
import introduction from '@pc/components/place/introduction.vue'
import courseSummary from '@pc/components/place/courseSummary.vue'
import planSummary from '@pc/components/place/planSummary.vue'
import reviewSummary from '@pc/components/place/review/reviewSummary.vue'
import information from '@pc/components/place/information.vue'
import rental from '@pc/components/place/rental.vue'
import flyer from '@pc/components/place/flyer.vue'
import searchResult from '@pc/components/place/searchResult.vue'
import cpList from '@pc/components/place/cpList.vue'
import pageTop from '@pc/components/place/pageTop.vue'
import restaurantRecommend from '@pc/components/place/restaurantRecommend.vue'
import couponList from '@pc/components/place/coupon/couponList.vue'
import searchAssistance from '@pc/components/place/searchAssistance.vue'
import route from '@pc/components/place/map/route.vue' // partial内placeData.htmlで呼んでる
import post from '@pc/components/place/post.vue'
import share from '@pc/components/place/share.vue'
import myClip from '@pc/components/place/myClip.vue'
import promotion from '@pc/components/place/promotion.vue'
import regularLink from '@pc/components/place/regularLink.vue'
import clipButton from '@pc/components/place/clipButton.vue'
import '@pc/styles/place/style.scss'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {
    signboard,
    calendar,
    reserveButton,
    introduction,
    courseSummary,
    planSummary,
    reviewSummary,
    information,
    rental,
    flyer,
    searchResult,
    cpList,
    pageTop,
    restaurantRecommend,
    couponList,
    searchAssistance,
    route,
    post,
    share,
    myClip,
    promotion,
    regularLink,
    clipButton
  },
  extends: Base,
  data: {
    provider: false,
    reviewTooltip: false,
    detailButton: true,
    comment: {},
    hasReserveButton: false,
    showMenuSubNavigation: false,
    currentUrl: window.location.href
  },
  beforeMount () {
    this.$on('reserveButtonReady', (hasReserveButton) => {
      this.hasReserveButton = hasReserveButton
    })
  },
  computed: {
    /**
     * グルメ店舗か判定
     * @returns {boolean} true/false
     */
    isGourmet () {
      if (this.tenpo.Feature[0].Property.Genre.length > 0) {
        return this.tenpo.Feature[0].Property.Genre[0].Code.substring(0, 2) === '01'
      }
      return false
    }
  },
  methods: {
    setSpViewCookie () {
      Ajax.get(this.namespace.Url.setYjdSp).catch(() => {
        window.location.href = this.currentUrl
      })
    }
  }
})
