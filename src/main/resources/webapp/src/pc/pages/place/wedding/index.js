import Vue from 'vue'
import Base from '@lib/baseModel'
import Ajax from '@lib/Ajax'

import subNavigation from '@pc/components/place/subNavigation.vue'
import anchor from '@pc/components/place/wedding/anchor.vue'
import plan from '@pc/components/place/wedding/plan.vue'
import report from '@pc/components/place/wedding/report.vue'
import error from '@pc/components/place/wedding/error.vue'
import pageTop from '@pc/components/place/pageTop.vue'
import restaurantRecommend from '@pc/components/place/restaurantRecommend.vue'
import couponList from '@pc/components/place/coupon/couponList.vue'
import searchAssistance from '@pc/components/place/searchAssistance.vue'
import post from '@pc/components/place/post.vue'
import share from '@pc/components/place/share.vue'
import myClip from '@pc/components/place/myClip.vue'
import promotion from '@pc/components/place/promotion.vue'
import regularLink from '@pc/components/place/regularLink.vue'
import route from '@pc/components/place/map/route.vue' // partial内placeData.htmlで呼んでる
import '@pc/styles/place/style.scss'
import Wedding from '@lib/Wedding'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {
    subNavigation,
    anchor,
    plan,
    report,
    error,
    pageTop,
    restaurantRecommend,
    couponList,
    searchAssistance,
    post,
    share,
    myClip,
    promotion,
    regularLink,
    route
  },
  extends: Base,
  data: {
    provider: false,
    reviewTooltip: false,
    clipTooltip: {
      show: false,
      clipped: false
    },
    detailButton: true,
    subNavigations: [], // サブナビゲーションメニュー
    tenpoName: '', // 店舗名
    cpId: '', // CP ID
    cpName: '', // CP名
    cpUrl: '', // CPURL
    plans: [], // ウェディングプラン
    reports: [], // 体験者レポート
    isPlanViewable: false, // 表示条件を満たせばtrue
    isReportViewable: false, // 表示条件を満たせばtrue
    gid: '', // GID
    showMenuSubNavigation: false,
    currentUrl: window.location.href
  },
  computed: {
    /**
     * グルメ店舗か判定
     * @returns {boolean} true/false
     */
    isGourmet () {
      if (this.tenpo.Feature[0].Property.Genre.length > 0) {
        return this.tenpo.Feature[0].Property.Genre[0].Code.substring(0, 2) === '01'
      }
      return false
    }
  },
  mounted () {
    // 式場オブジェクト生成
    if (this.status.wedding === 'binded') {
      const myWedding = new Wedding(this.wedding)
      // ナビゲーション情報取得
      this.subNavigations = myWedding.getSubNavigations()
      // 店舗名
      this.tenpoName = myWedding.getTenpoName()
      // CP名取得
      this.cpName = myWedding.getCpName()
      // CP ID
      this.cpId = myWedding.getCpId()
      // CPURL取得
      this.cpUrl = myWedding.getCpUrl()
      // プラン取得
      this.plans = myWedding.plan.getPlans()
      // レポート取得
      this.reports = myWedding.report.getReports()
      // 表示判定
      this.isPlanViewable = myWedding.plan.isPlanViewable()
      this.isReportViewable = myWedding.report.isReportViewable()
      // GID
      this.gid = myWedding.getGid()
    }
  },
  methods: {
    setSpViewCookie () {
      Ajax.get(this.namespace.Url.setYjdSp).catch(() => {
        window.location.href = this.currentUrl
      })
    }
  }
})
