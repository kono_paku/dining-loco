import Vue from 'vue'
import Base from '@lib/baseComponent'
import restaurantRecommend from '@pc/components/place/restaurantRecommend.vue'

import '@pc/styles/place/style.scss'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  extends: Base,
  data: {
    edgeDevice: '',
    device: ''
  },
  components: {
    restaurantRecommend
  }
})
