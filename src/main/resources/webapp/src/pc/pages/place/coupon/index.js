import Vue from 'vue'
import Base from '@lib/baseModel'
import Ajax from '@lib/Ajax'

import couponList from '@pc/components/place/coupon/couponList.vue'
import note from '@pc/components/place/coupon/note.vue'
import pageTop from '@pc/components/place/pageTop.vue'
import restaurantRecommend from '@pc/components/place/restaurantRecommend.vue'
import searchAssistance from '@pc/components/place/searchAssistance.vue'
import post from '@pc/components/place/post.vue'
import share from '@pc/components/place/share.vue'
import route from '@pc/components/place/map/route.vue' // partial内placeData.htmlで呼んでる
import myClip from '@pc/components/place/myClip.vue'
import promotion from '@pc/components/place/promotion.vue'
import regularLink from '@pc/components/place/regularLink.vue'
import clipButton from '@pc/components/place/clipButton.vue'
import '@pc/styles/place/style.scss'

/* eslint-disable no-new  */
new Vue({
  el: '#app',
  components: {
    couponList,
    note,
    pageTop,
    restaurantRecommend,
    searchAssistance,
    post,
    share,
    route,
    myClip,
    promotion,
    regularLink,
    clipButton
  },
  extends: Base,
  data: {
    provider: false,
    reviewTooltip: false,
    clipTooltip: {
      show: false,
      clipped: false
    },
    detailButton: true,
    comment: {},
    showMenuSubNavigation: false,
    currentUrl: window.location.href
  },
  computed: {
    /**
     * グルメ店舗か判定
     * @returns {boolean} true/false
     */
    isGourmet () {
      if (this.tenpo.Feature[0].Property.Genre.length > 0) {
        return this.tenpo.Feature[0].Property.Genre[0].Code.substring(0, 2) === '01'
      }
      return false
    }
  },
  methods: {
    setSpViewCookie () {
      Ajax.get(this.namespace.Url.setYjdSp).catch(() => {
        window.location.href = this.currentUrl
      })
    }
  }
})
