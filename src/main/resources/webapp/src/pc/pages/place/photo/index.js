import Vue from 'vue'
import Base from '@lib/baseModel'
import Ajax from '@lib/Ajax'
import Pagination from '@lib/Pagination.js'

import error from '@pc/components/place/photo/error.vue'
import zeroMatch from '@pc/components/place/photo/zeroMatch.vue'
import pagination from '@pc/components/place/pagination.vue'
import loading from '@pc/components/place/loading.vue'
import list from '@pc/components/place/photo/list.vue'
import pageTop from '@pc/components/place/pageTop.vue'
import restaurantRecommend from '@pc/components/place/restaurantRecommend.vue'
import couponList from '@pc/components/place/coupon/couponList.vue'
import searchAssistance from '@pc/components/place/searchAssistance.vue'
import post from '@pc/components/place/post.vue'
import share from '@pc/components/place/share.vue'
import myClip from '@pc/components/place/myClip.vue'
import promotion from '@pc/components/place/promotion.vue'
import regularLink from '@pc/components/place/regularLink.vue'
import clipButton from '@pc/components/place/clipButton.vue'
import route from '@pc/components/place/map/route.vue' // partial内placeData.htmlで呼んでる
import '@pc/styles/place/style.scss'

// 写真画像幅、高
const photoImgWidth = 600
const photoImgHeight = 400

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {
    error,
    zeroMatch,
    loading,
    list,
    pagination,
    pageTop,
    restaurantRecommend,
    couponList,
    searchAssistance,
    post,
    share,
    myClip,
    promotion,
    regularLink,
    clipButton,
    route
  },
  extends: Base,
  data: {
    provider: false,
    reviewTooltip: false,
    clipTooltip: {
      show: false,
      clipped: false
    },
    detailButton: true,
    status: {},
    requestData: {
      count: 1, // Requestに利用
      maxCount: 300, // 取得最大数
      perRequest: 100 // 1Requestの枚数
    },
    photo: {
      'totalCount': 0,
      'name': '',
      'imageInfo': []
    },
    photoRequest: {},
    photoRequestImage: [],
    photoRequestFixed: {},
    page: {
      'pageId': 0, // 表示するページ番号
      'pageDisplaySize': 40, //  画面に表示枚数
      'pageCount': 0, // 総ページ数
      'dataCount': null, // 総件数
      'displayFlag': false, // 要素表示フラグ
      'modal': false,
      'photoId': ''
    },
    resizedImages01: {},
    resizedImages02: {},
    resizedImages03: {},
    pagination: new Pagination(),
    showMenuSubNavigation: false,
    currentUrl: window.location.href
  },
  mounted () {
    this.requestImage()
  },
  computed: {
    /**
     * グルメ店舗か判定
     * @returns {boolean} true/false
     */
    isGourmet () {
      if (this.tenpo.Feature[0].Property.Genre.length > 0) {
        return this.tenpo.Feature[0].Property.Genre[0].Code.substring(0, 2) === '01'
      }
      return false
    }
  },
  methods: {
    setSpViewCookie () {
      Ajax.get(this.namespace.Url.setYjdSp).catch(() => {
        window.location.href = this.currentUrl
      })
    },
    /**
     * [写真一覧・詳細]写真情報の取得と成型
     */
    requestImage () {
      this.bindRequest({
        name: 'photoRequest',
        url: this.namespace.API.imageSearch,
        params: {
          gid: this.tenpo.Feature[0].Gid,
          results: this.requestData.perRequest,
          start: this.requestData.count
        },
        dataType: 'json'
      }).then((photoRequest) => {
        if (photoRequest.ResultInfo.Count > 0) {
          this.photoRequestImage.push(photoRequest.Feature)
        }
        this.requestData.count += this.requestData.perRequest
        if (this.requestData.count <= this.requestData.maxCount && photoRequest.ResultInfo.Count !== 0) {
          this.requestImage()
        } else {
          let photoRequestImageList = []
          for (var countImage = 0; countImage < this.photoRequestImage.length; countImage++) {
            for (var itemNum = 0; itemNum < this.photoRequestImage[countImage].length; itemNum++) {
              photoRequestImageList.push(this.photoRequestImage[countImage][itemNum])
            }
          }
          this.photoRequest['Feature'] = photoRequestImageList
          this.castPhotoData()
          this.photoDataSort()
          this.pages()
          this.getUrlPhotoId()
          this.resizePhotoImage() // リサイズ処理
        }
      })
    },
    /*
     * [写真一覧・詳細]ページング情報の設定
     * pageId 表示ページ番号
     * pageCount ページ数
     * dataCount 総件数
     */
    pages () {
      this.page.pageCount = Math.ceil(this.photo.imageInfo.length / this.page.pageDisplaySize)
      this.page.dataCount = this.photo.imageInfo.length

      if (this.pagination.getPage() > 0 && this.pagination.getPage() <= this.page.pageCount) {
        this.page.pageId = this.pagination.getPage() - 1
      }
      if (this.page.dataCount !== 0) {
        this.page.displayFlag = true
      }
    },
    /*
     * ajaxで受け取ったjsonオブジェクトの並び替え
     * 順位 1. ダイニング,トラベル 2. その他CP 3. ユーザ投稿
     * 注意 医療系のものはユーザ投稿なし
     */
    photoDataSort () {
      const YahooDiningCassette = 'ccbf79674338330c7ce5557ace5a18ec'
      const YahooTravelCassette = '8024afaf82bcaaa4c33b91deea651df1'
      const YahooLocoCassette = '40f68cf163a82aeb127f0387eb314b4b'
      const JrCassette = 'c8892c8ad8cdc2eb9cb69ad0b289516a'

      // 先頭ヤフー系カセットの探索
      var photoDataYahoo = this.photoRequestFixed.filter(function (item, index) {
        return (item.cassetteId === YahooDiningCassette || item.cassetteId === YahooTravelCassette)
      })
      this.setPhotoData(photoDataYahoo)

      // その他CPの情報の抽出
      var photoDataCP = this.photoRequestFixed.filter(function (item, index) {
        return (
          item.cassetteId !== YahooDiningCassette &&
          item.cassetteId !== YahooTravelCassette &&
          item.cassetteId !== YahooLocoCassette &&
          item.cassetteId !== JrCassette // JRカセットはlsbeから取得できないので画像を使用しない
        )
      })
      // その他CPの情報のセット
      this.setPhotoData(photoDataCP)

      // ロコのカセット ユーザ投稿写真の抽出
      var photoDataLoco = this.photoRequestFixed.filter(function (item, index) {
        return (
          item.cassetteId === YahooLocoCassette
        )
      })

      // ロコのカセット ユーザ投稿写真のセット
      this.setPhotoData(photoDataLoco)

      // 店舗名の設定
      this.photo.name = this.tenpo.Feature[0].Name
      // 写真の総件数の設定
      this.photo.totalCount = this.photo.imageInfo.length
    },
    /*
     * [写真一覧・詳細]photoオブジェクト用にjsonの並び替えをセット
     */
    setPhotoData (photoData) {
      for (let i = 0; i < photoData.length; i++) {
        for (let y = 0; y < photoData[i].imageInfo.length; y++) {
          // pushするデータの成形
          photoData[i].imageInfo[y].cassetteName = photoData[i].cassetteName
          photoData[i].imageInfo[y].cpUrl = photoData[i].cpUrl
          this.photo.imageInfo.push(photoData[i].imageInfo[y])
        }
      }
    },
    /*
     * [写真詳細]URLからphotoIdパラメータ情報を取得
     * あれば modal 表示する page.modal を true に設定
     */
    getUrlPhotoId: function () {
      var urlPhotoId = ''
      var params = location.search.substring(1).split('&')
      for (var i = 0; params[i]; i++) {
        var kv = params[i].split('=')// kvはkey-value
        if (kv[0] === 'photoId') {
          urlPhotoId = kv[1]
          if (this.isCheckPhotoId(urlPhotoId)) {
            this.page.modal = true
            this.page.photoId = urlPhotoId
            break
          }
        }
      }
    },
    /*
     * [写真詳細]URLにある写真IDが存在するものか判定
     * @return boolean
     */
    isCheckPhotoId: function (photoId) {
      let resultCheckId = false
      for (var checkNum in this.photoRequestFixed) {
        for (var checkId in this.photoRequestFixed[checkNum].imageInfo) {
          if (this.photoRequestFixed[checkNum].imageInfo[checkId].Id === photoId) {
            return (resultCheckId = true)
          }
        }
      }
      return resultCheckId
    },
    /*
    * [写真一覧・写真詳細]APIから受け取った画像情報にカセットIDや必要な項目を付与
    */
    castPhotoData: function () {
      var setImageList = new Set() // Gidから索引されたカセットIDのリストを作成
      const YahooDiningCassette = 'ccbf79674338330c7ce5557ace5a18ec'
      for (var imageList in this.photoRequest.Feature) {
        // cassetteIDのリストを配列で新規に作成する
        setImageList.add(this.photoRequest.Feature[imageList].Property.CassetteId)
      }

      // photoRequestの中身をcassetteId毎にImageを持つようにする
      let imageInfoData // リクエストのImageの中身を一時的に補完する箱
      let photoRequestFixed = [] // APIのリクエストに必要な項目を追加する箱
      let imageInfoCP // カセット毎の配列にするための箱

      for (var cpId of setImageList.values()) {
        imageInfoData = [] // 初期化
        imageInfoCP = [] // 初期化
        for (var y = 0; y < this.photoRequest.Feature.length; y++) {
          if (cpId === this.photoRequest.Feature[y].Property.CassetteId) {
            imageInfoData.push(this.photoRequest.Feature[y].Property.Image)
          }
        }
        // cp名とURLの情報を付与 ロコCPのみtenpoにない場合があるため
        const YahooLocoCassette = '40f68cf163a82aeb127f0387eb314b4b'
        if (cpId === YahooLocoCassette) {
          imageInfoCP = {
            'cassetteId': YahooLocoCassette,
            'cassetteName': 'ヤフーロコ',
            'cpUrl': 'https://loco.yahoo.co.jp'
          }
        }
        // tenpo に登録されているカセットIDと名前,URLを追加
        for (var i = 0; i < this.tenpo.Feature[0].Children.length; i++) {
          if (cpId === this.tenpo.Feature[0].Children[i].CassetteId) {
            var cassetteName = this.tenpo.Feature[0].Children[i].CassetteHeader
            // Y!ダイニングCPの場合にはカセット名を”Yahoo!Japan”にする
            if (cpId === YahooDiningCassette) {
              cassetteName = 'Yahoo! JAPAN'
            }
            imageInfoCP = {
              'cassetteId': this.tenpo.Feature[0].Children[i].CassetteId,
              'cassetteName': cassetteName,
              'cpUrl': this.tenpo.Feature[0].Children[i].PcUrl1
            }
          }
        }

        imageInfoCP['cassetteId'] = cpId
        imageInfoCP['imageInfo'] = imageInfoData
        photoRequestFixed.push(imageInfoCP)
      }
      // data photoRequestFixed に成型後のデータを格納
      this.photoRequestFixed = photoRequestFixed
    },
    /*
     * [写真一覧・詳細]写真サイズを更新する
     */
    resizePhotoImage () {
      // 全画像を配列に格納
      var imgUrls = []
      let loopCount = Math.ceil(this.photo.imageInfo.length / this.requestData.perRequest)
      var limitNum = 0
      var imageCount = 0

      for (var l = 1; l <= loopCount; l++) {
        if (l === 1) {
          limitNum = this.requestData.perRequest
        } else {
          limitNum = l * this.requestData.perRequest
        }

        for (imageCount; imageCount < limitNum; imageCount++) {
          if (imageCount < this.photo.imageInfo.length) {
            imgUrls.push(this.photo.imageInfo[imageCount].ImageUrl)
          }
        }
        this.requestResize(imgUrls, 'resizedImages0' + l)

        imgUrls = []
      }
    },
    /**
     * リサイズの実行
     * @param imgUrls
     * @param name
     */
    requestResize (imgUrls, name = 'resizedImages') {
      this.resizeImages(this.tenpo.Feature[0].Gid, [{urls: imgUrls, width: photoImgWidth, height: photoImgHeight, crop: false}], name)
    }
  }
})
