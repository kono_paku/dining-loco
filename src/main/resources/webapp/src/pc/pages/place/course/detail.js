import Vue from 'vue'
import Base from '@lib/baseModel'
import Ajax from '@lib/Ajax'
import moment from '@lib/moment-ja'

import restaurantRecommend from '@pc/components/place/restaurantRecommend.vue'
import post from '@pc/components/place/post.vue'
import promotion from '@pc/components/place/promotion.vue'
import regularLink from '@pc/components/place/regularLink.vue'

import detailMenu from '@pc/components/place/course/detailMenu.vue'
import restaurantAnnouncement from '@pc/components/place/course/restaurantAnnouncement.vue'
import detailDescription from '@pc/components/place/course/detailDescription.vue'
import pageTop from '@pc/components/place/pageTop.vue'
// import detailOptionalMenu from '@pc/components/place/course/detailOptionalMenu.vue'
import detailReserveInput from '@pc/components/place/course/detailReserveInput.vue'
import detailReserveInputConfirm from '@pc/components/place/course/detailReserveInputConfirm.vue'
import clipButton from '@pc/components/place/clipButton.vue'
import route from '@pc/components/place/map/route.vue' // partial内placeData.htmlで呼んでる

import '@pc/styles/place/style.scss'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {
    restaurantRecommend,
    post,
    promotion,
    regularLink,
    detailMenu,
    restaurantAnnouncement,
    detailDescription,
    pageTop,
    // detailOptionalMenu,
    detailReserveInput,
    detailReserveInputConfirm,
    clipButton,
    route
  },
  extends: Base,
  data: {
    provider: false,
    reviewTooltip: false,
    clipTooltip: {
      show: false,
      clipped: false
    },
    singleColumn: true,
    canConfirm: true,
    course: {},
    smartLoginUrl: '',
    dailyStock: {},
    searchCondition: {
      selectedDate: '',
      selectedPerson: 0,
      selectedTime: ''
    },
    timeStockResultCode: '',
    detailButton: false,
    showMenuSubNavigation: false,
    currentUrl: window.location.href
  },
  beforeMount () {
    try {
      this.course = JSON.parse(this.getDocumentValue('#courseDetailJson'))
    } catch (err) {
      this.error['course'] = err
    }
    this.updateStatusOf('course')
    this.getDailyStock()
    this.setSmartLoginUrl()
  },
  computed: {
    /**
     * グルメ店舗か判定
     * @returns {boolean} true/false
     */
    isGourmet () {
      if (this.tenpo.Feature[0].Property.Genre.length > 0) {
        return this.tenpo.Feature[0].Property.Genre[0].Code.substring(0, 2) === '01'
      }
      return false
    },
    /**
     * 日付毎の予約ステータス取得
     * 予約ステータス
     *   0: 空席なし
     *   1: 予約可能
     *   2: リクエスト予約可能
     * @return {array} 日付：ステータスのarray
     */
    dailyStockFiltered () {
      let result = {}
      if (Object.keys(this.dailyStock).length === 0) return result
      for (const stock of this.dailyStock.entry[0].stocks) {
        result[moment(stock.date).format('YYYYMMDD')] = stock.status
      }
      return result
    }
  },
  methods: {
    setSpViewCookie () {
      Ajax.get(this.namespace.Url.setYjdSp).catch(() => {
        window.location.href = this.currentUrl
      })
    },
    /**
     * グルメ店舗か判定
     * @returns {boolean} true/false
     */
    isGourmet () {
      if (this.tenpo.Feature[0].Property.Genre.length > 0) {
        return this.tenpo.Feature[0].Property.Genre[0].Code.substring(0, 2) === '01'
      }
      return false
    },
    canReserveConform () {
      return this.searchCondition.selectedTime !== '' && this.canConfirm
    },
    setSelectedDate (val) {
      this.searchCondition.selectedDate = val
    },
    setSelectedPerson (val) {
      this.searchCondition.selectedPerson = val
    },
    setSelectedTime (val) {
      this.searchCondition.selectedTime = val
    },
    setConfirmFlag (val) {
      this.canConfirm = val
    },
    setTimeStockResult (val) {
      this.timeStockResultCode = val
    },
    setSmartLoginUrl () {
      let doneUrl = ''
      if (this.canReserve) {
        doneUrl = 'https://reservation.yahoo.co.jp/restaurant/detail/' + this.course.entry.tenpoId +
                  '/plan/P' + this.course.entry.plans[0].planId +
                  '/?date=' + this.searchCondition.selectedDate +
                  '&people_counts=' + this.searchCondition.selectedPerson +
                  '&time=' + this.searchCondition.selectedTime +
                  '&ref=loco&sc_e=yloc_plan'
        this.smartLoginUrl = this.namespace.Url.planDetailInputSmartLogin + encodeURIComponent(doneUrl)
      } else {
        doneUrl = 'https://loco.yahoo.co.jp/place/g-' + this.tenpo.Feature[0].Gid +
                  '/course/P' + this.course.entry.plans[0].planId +
                  '/?date=' + this.searchCondition.selectedDate +
                  '&people_counts=' + this.searchCondition.selectedPerson +
                  '&time=' + this.searchCondition.selectedTime +
                  '&sc_e=yloc_plan'
        this.smartLoginUrl = this.namespace.Url.planDetailConfirmSmartLogin + encodeURIComponent(doneUrl)
      }
    },
    /**
     * 日付在庫を取得する
     */
    getDailyStock () {
      // 日次在庫APIからデータの取得
      this.bindRequest({
        name: 'dailyStock',
        url: this.namespace.API.dailyStock,
        params: {
          tenpoid: this.course.entry.tenpoId,
          planid: this.course.entry.plans[0].planId,
          period: 90
        },
        eappid: true,
        method: 'GET',
        dataType: 'jsonp'
      })
    }
  }
})
