import Vue from 'vue'
import Base from '@lib/baseModel'
import Ajax from '@lib/Ajax'
import Wedding from '@lib/Wedding.js'

import subNavigation from '@pc/components/place/subNavigation'
import error from '@pc/components/place/bridal/error.vue'
import fair from '@pc/components/place/bridal/fair.vue'
import pageTop from '@pc/components/place/pageTop.vue'
import restaurantRecommend from '@pc/components/place/restaurantRecommend.vue'
import couponList from '@pc/components/place/coupon/couponList.vue'
import searchAssistance from '@pc/components/place/searchAssistance.vue'
import post from '@pc/components/place/post.vue'
import share from '@pc/components/place/share.vue'
import myClip from '@pc/components/place/myClip.vue'
import promotion from '@pc/components/place/promotion.vue'
import regularLink from '@pc/components/place/regularLink.vue'
import clipButton from '@pc/components/place/clipButton'
import route from '@pc/components/place/map/route.vue' // partial内placeData.htmlで呼んでる
import '@pc/styles/place/style.scss'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {
    subNavigation,
    error,
    fair,
    pageTop,
    restaurantRecommend,
    couponList,
    searchAssistance,
    post,
    share,
    myClip,
    promotion,
    regularLink,
    clipButton,
    route
  },
  extends: Base,
  data: function () {
    return {
      provider: false,
      reviewTooltip: false,
      clipTooltip: {
        show: false,
        clipped: false
      },
      detailButton: true,
      items: [],
      subNavigations: [],
      tenpoName: '',
      cpName: '',
      cpUrl: '',
      isViewable: false,
      gid: '',
      showMenuSubNavigation: false,
      currentUrl: window.location.href
    }
  },
  beforeMount () {
    // 式場オブジェクト生成
    if (this.status.wedding === 'binded') {
      const myWedding = new Wedding(this.wedding)
      // 総プランを取得
      this.items = myWedding.bridal.getFairs()
      // ナビゲーション情報取得
      this.subNavigations = myWedding.getSubNavigations()
      // 店舗名取得
      this.tenpoName = myWedding.getTenpoName()
      // CP名取得
      this.cpName = myWedding.getCpName()
      // CPURL取得
      this.cpUrl = myWedding.getCpUrl()
      // 表示判定
      this.isViewable = myWedding.bridal.isViewable()
      // GID
      this.gid = myWedding.getGid()
    }
  },
  computed: {
    /**
     * グルメ店舗か判定
     * @returns {boolean} true/false
     */
    isGourmet () {
      if (this.tenpo.Feature[0].Property.Genre.length > 0) {
        return this.tenpo.Feature[0].Property.Genre[0].Code.substring(0, 2) === '01'
      }
      return false
    }
  },
  methods: {
    setSpViewCookie () {
      Ajax.get(this.namespace.Url.setYjdSp).catch(() => {
        window.location.href = this.currentUrl
      })
    }
  }
})
