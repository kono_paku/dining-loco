import Vue from 'vue'
import Base from '@lib/baseModel'
import Ajax from '@lib/Ajax'

import zeroMatch from '@pc/components/place/course/zeroMatch.vue'
import subNavigation from '@pc/components/place/subNavigation.vue'
import pageTitle from '@pc/components/place/course/pageTitle.vue'
import calendar from '@pc/components/place/calendar.vue'
import reserve from '@pc/components/place/course/reserve.vue'
import closed from '@pc/components/place/course/closed.vue'
import list from '@pc/components/place/course/list.vue'
import pageTop from '@pc/components/place/pageTop.vue'
import restaurantRecommend from '@pc/components/place/restaurantRecommend.vue'
import couponList from '@pc/components/place/coupon/couponList.vue'
import cpList from '@pc/components/place/cpList.vue'
import searchAssistance from '@pc/components/place/searchAssistance.vue'
import post from '@pc/components/place/post.vue'
import share from '@pc/components/place/share.vue'
import myClip from '@pc/components/place/myClip.vue'
import promotion from '@pc/components/place/promotion.vue'
import regularLink from '@pc/components/place/regularLink.vue'
import clipButton from '@pc/components/place/clipButton'
import route from '@pc/components/place/map/route.vue' // partial内placeData.htmlで呼んでる
import '@pc/styles/place/style.scss'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {
    zeroMatch,
    subNavigation,
    pageTitle,
    calendar,
    reserve,
    closed,
    list,
    pageTop,
    restaurantRecommend,
    couponList,
    cpList,
    searchAssistance,
    post,
    share,
    myClip,
    promotion,
    regularLink,
    clipButton,
    route
  },
  extends: Base,
  data: {
    provider: false,
    reviewTooltip: false,
    clipTooltip: {
      show: false,
      clipped: false
    },
    detailButton: true,
    tabItems: [],
    showMenuSubNavigation: false,
    currentUrl: window.location.href
  },
  mounted () {
    this.createTabItems()
  },
  computed: {
    /**
     * グルメ店舗か判定
     * @returns {boolean} true/false
     */
    isGourmet () {
      if (this.tenpo.Feature[0].Property.Genre.length > 0) {
        return this.tenpo.Feature[0].Property.Genre[0].Code.substring(0, 2) === '01'
      }
      return false
    }
  },
  methods: {
    setSpViewCookie () {
      Ajax.get(this.namespace.Url.setYjdSp).catch(() => {
        window.location.href = this.currentUrl
      })
    },
    /**
     * タブ項目生成
     */
    createTabItems: function () {
      if (this.courseList.totalResults === 0) {
        return
      }
      const detail = this.tenpo.Feature[0].Property.Detail
      if (!detail.RecommendItemId1 && !detail.RecommendItemSequence1 && !detail.TopRankItemId1) {
        return
      }

      const baseUrl = '/place/g-' + this.tenpo.Feature[0].Gid
      const courseTitle = 'コース（' + this.courseList.totalResults + '）'
      this.tabItems.push({page: 'course', title: courseTitle, url: baseUrl + '/course/'})
      this.tabItems.push({page: 'menu', title: '料理', url: baseUrl + '/menu/'})
    }
  }
})
