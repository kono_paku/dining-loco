package jp.co.yahoo.dining.frontend.domain;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 店舗情報モデル
 * 検索APIのIF定義です。
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tenpo implements ExternalEntity, Serializable {

  @JsonProperty("ResultInfo")
  private ResultInfo resultInfo;

  @JsonProperty("Feature")
  private Feature[] feature;

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class ResultInfo {

    @JsonProperty("Count")
    private Integer count;
    @JsonProperty("Total")
    private Integer total;
    @JsonProperty("Start")
    private Long start;
    @JsonProperty("SearchInfo")
    private SearchInfo status;
    @JsonProperty("Latency")
    private BigDecimal latency;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class SearchInfo {
    @JsonProperty("QueryCategory")
    private String queryCategory;
    @JsonProperty("ResultSort")
    private String resultSort;
    @JsonProperty("QueryClass")
    private String queryClass;
    @JsonProperty("WebTopicFrom")
    private String webTopicFrom;
    @JsonProperty("WebTopicTo")
    private String webTopicTo;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Feature {

    @JsonProperty("Id")
    private String id;
    @JsonProperty("Gid")
    private String gid;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Geometry")
    private Geometry geometry;
    @JsonProperty("Category")
    private String[] category;
    @JsonProperty("Description")
    private String description;
    @JsonProperty("Children")
    private Children[] children;
    @JsonProperty("Internal")
    private Internal internal;
    @JsonProperty("Property")
    private Property property;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Geometry {

    @JsonProperty("Type")
    private String type;
    @JsonProperty("Coordinates")
    private String coordinates;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Internal {

    @JsonProperty("UidList")
    private String[] uidList;
    @JsonProperty("CidList")
    private String[] cidList;
    @JsonProperty("NameList")
    private String[] nameList;
    @JsonProperty("LogVersion")
    private String logVersion;
    @JsonProperty("LifeMagazine")
    private LifeMagazine[] lifeMagazine;
    @JsonProperty("Partial")
    private Partial partial;
    @JsonProperty("RealTime")
    private RealTime realTime;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Partial {
    @JsonProperty("AvgRating")
    private String avgRating;
    @JsonProperty("ReviewCount")
    private String reviewCount;
    @JsonProperty("KeepCount")
    private String keepCount;
    @JsonProperty("WebTopic")
    private WebTopic webTopic;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class RealTime {
    @JsonProperty("AvailabilityCode")
    private String availabilityCode;
    @JsonProperty("WaitingTime")
    private String waitingTime;
    @JsonProperty("WaitingNumber")
    private String waitingNumber;
    @JsonProperty("RoomAvailabilityCode")
    private String roomAvailabilityCode;
    @JsonProperty("MaintenanceStartDatetime")
    private String maintenanceStartDatetime;
    @JsonProperty("MaintenanceEndDatetime")
    private String maintenanceEndDatetime;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class WebTopic {
    @JsonProperty("Comment")
    private String comment;
    @JsonProperty("CommentDate")
    private String commentDate;
    @JsonProperty("Label")
    private String label;
    @JsonProperty("Score")
    private String score;
    @JsonProperty("Buzz")
    private String buzz;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Children {
    @JsonProperty("id")
    private String id;
    @JsonProperty("CassetteId")
    private String cassetteId;
    @JsonProperty("CassetteOwnerLogoImage")
    private String cassetteOwnerLogoImage;
    @JsonProperty("CassetteHeader")
    private String cassetteHeader;
    @JsonProperty("CassetteOwner")
    private String cassetteOwner;
    @JsonProperty("CassetteOwnerUrl")
    private String cassetteOwnerUrl;
    @JsonProperty("OnlineReserveFlag")
    private Boolean onlineReserveFlag;
    @JsonProperty("OnlineReserveRequestFlag")
    private Boolean onlineReserveRequestFlag;
    @JsonProperty("PcUrl1")
    private String pcUrl1;
    @JsonProperty("SmartPhoneUrl1")
    private String smartPhoneUrl1;
    @JsonProperty("ReservationPcUrl1")
    private String reservationPcUrl1;
    @JsonProperty("ReservationSmartPhoneUrl1")
    private String reservationSmartPhoneUrl1;
    @JsonProperty("ReservationRequestPcUrl1")
    private String reservationRequestPcUrl1;
    @JsonProperty("ReservationRequestSmartPhoneUrl1")
    private String reservationRequestSmartPhoneUrl1;
    @JsonProperty("ReservationLabelCode")
    private String reservationLabelCode;
    @JsonProperty("ReservationDisplayFlag")
    private String reservationDisplayFlag;
    @JsonProperty("ReservationDisplayCode")
    private String reservationDisplayCode;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("LeadImage")
    private String leadImage;
    @JsonProperty("Tel1")
    private String tel1;
    @JsonProperty("Address")
    private String address;
    @JsonProperty("Rating")
    private String rating;
    @JsonProperty("BudgetLunch")
    private String budgetLunch;
    @JsonProperty("BudgetDinner")
    private String budgetDinner;
    @JsonProperty("Category")
    private String category;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  public static class Property {
    @JsonProperty("Detail")
    private Detail detail;
    @JsonProperty("SmartPhoneCouponFlag")
    private Boolean smartPhoneCouponFlag;
    @JsonProperty("AddressMatchingLevel")
    private String addressMatchingLevel;
    @JsonProperty("CassetteId")
    private String cassetteId;
    @JsonProperty("CouponFlag")
    private Boolean couponFlag;
    @JsonProperty("CreateDate")
    private String createDate;
    @JsonProperty("LeadImage")
    private String leadImage;
    @JsonProperty("OutputAcl")
    private String outputAcl;
    @JsonProperty("Price")
    private String price;
    @JsonProperty("Rating")
    private String rating;
    @JsonProperty("StorageUrl")
    private String storageUrl;
    @JsonProperty("UpdateDate")
    private String updateDate;
    @JsonProperty("ParkingFlag")
    private String parkingFlag;
    @JsonProperty("Access1")
    private String access1;
    @JsonProperty("CatchCopy")
    private String catchCopy;
    @JsonProperty("CreditcardFlag")
    private String creditcardFlag;
    @JsonProperty("DayBeforeHolidayBusinessHour")
    private String dayBeforeHolidayBusinessHour;
    @JsonProperty("FridayBusinessHour")
    private String fridayBusinessHour;
    @JsonProperty("HolidayBusinessHour")
    private String holidayBusinessHour;
    @JsonProperty("MondayBusinessHour")
    private String mondayBusinessHour;
    @JsonProperty("SaturdayBusinessHour")
    private String saturdayBusinessHour;
    @JsonProperty("SundayBusinessHour")
    private String sundayBusinessHour;
    @JsonProperty("ThursdayBusinessHour")
    private String thursdayBusinessHour;
    @JsonProperty("TuesdayBusinessHour")
    private String tuesdayBusinessHour;
    @JsonProperty("WednesdayBusinessHour")
    private String wednesdayBusinessHour;
    @JsonProperty("Uid")
    private String uid;
    @JsonProperty("Yomi")
    private String yomi;
    @JsonProperty("Address")
    private String address;
    @JsonProperty("WebTopic")
    private WebTopic webTopic;
    @JsonProperty("PlaceInfo")
    @JsonDeserialize(using = ObjectAndArrayDeserializer.class)
    private PlaceInfo[] placeInfo;
    @JsonProperty("SearchArea")
    private SearchArea[] searchArea;
    @JsonProperty("Genre")
    private Genre[] genre;
    @JsonProperty("Coupon")
    private Coupon[] coupon;
    @JsonProperty("AddressElement")
    private AddressElement[] addressElement;
    @JsonProperty("Station")
    private Station[] station;
    //ItemInfo
    @JsonProperty("LocoRating")
    private String locoRating;
    @JsonProperty("LocoReviewCount")
    private String locoReviewCount;
    @JsonProperty("KeepCount")
    private Long keepCount;
    @JsonProperty("TodayBusinessHour")
    private String todayBusinessHour;
    @JsonProperty("Tel1")
    private String tel1;
    @JsonProperty("PpcDescription")
    private String ppcDescription;

  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class PlaceInfo {
    @JsonProperty("FloorLevel")
    private String floorLevel;
    @JsonProperty("FloorName")
    private String floorName;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class SearchArea {
    @JsonProperty("Code")
    private String code;
    @JsonProperty("Name")
    private String name;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Genre {
    @JsonProperty("Code")
    private String code;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Type")
    private String type;
    @JsonProperty("Level")
    private String level;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Coupon {
    //TODO : プレミアム会員判定IF確認必要
    @JsonProperty("Cid")
    private String cid;
    @JsonProperty("PremiumFlag")
    private Boolean premiumFlag;
    @JsonProperty("DisplayFlag")
    private Boolean displayFlag;
    @JsonProperty("EndDay")
    private String endDay;
    @JsonProperty("Id")
    private String id;
    @JsonProperty("MobileFlag")
    private Boolean mobileFlag;
    @JsonProperty("MobileUrl")
    private String mobileUrl;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("PcUrl")
    private String pcUrl;
    @JsonProperty("ParentPcUrl")
    private String parentPcUrl;
    @JsonProperty("SmartPhoneUrl")
    private String smartPhoneUrl;
    @JsonProperty("ParentSmartPhoneUrl")
    private String parentSmartPhoneUrl;
    @JsonProperty("ParentMobileUrl")
    private String parentMobileUrl;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class AddressElement {
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Kana")
    private String kana;
    @JsonProperty("Level")
    private String level;
    @JsonProperty("Code")
    private String code;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Station {
    @JsonProperty("Id")
    private String id;
    @JsonProperty("SubId")
    private String subId;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Railway")
    private String railway;
    @JsonProperty("Exit")
    private String exit;
    @JsonProperty("ExitId")
    private String exitId;
    @JsonProperty("Distance")
    private String distance;
    @JsonProperty("Time")
    private String time;
    @JsonProperty("Geometry")
    private Geometry geometry;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Detail {
    @JsonProperty("AccommodationNotes")
    private String accommodationNotes;
    @JsonProperty("AreaName")
    private String areaName;
    @JsonProperty("Amenity")
    private String amenity;
    @JsonProperty("Annex")
    private String annex;
    @JsonProperty("Baths")
    private String baths;
    @JsonProperty("Budget")
    private String budget;
    @JsonProperty("BudgetLunch")
    private String budgetLunch;
    @JsonProperty("BudgetDinner")
    private String budgetDinner;
    @JsonProperty("BudgetRemarks")
    private String budgetRemarks;
    @JsonProperty("BusinessHoliday")
    private String businessHoliday;
    @JsonProperty("BusinessHour")
    private String businessHour;
    @JsonProperty("BusinessHourComment")
    private String businessHourComment;
    @JsonProperty("Charge")
    private String charge;
    @JsonProperty("Seat")
    private String seat;
    @JsonProperty("DressCode")
    private String dressCode;
    @JsonProperty("DinnerPrice")
    private String dinnerPrice;
    @JsonProperty("HaircutPrice")
    private String haircutPrice;
    @JsonProperty("LunchFlag")
    private Boolean lunchFlag;
    @JsonProperty("Item")
    private Item[] item;
    @JsonProperty("MobileUrl1")
    private String mobileUrl1;
    @JsonProperty("MobileUrl2")
    private String mobileUrl2;
    @JsonProperty("MobileUrl3")
    private String mobileUrl3;
    @JsonProperty("MobileUrl4")
    private String mobileUrl4;
    @JsonProperty("PcUrl1")
    private String pcUrl1;
    @JsonProperty("StylistNumber")
    private String stylistNumber;
    @JsonProperty("Title1")
    private String title1;
    @JsonProperty("YUrl")
    private String yUrl;
    @JsonProperty("AfterPartyFlag")
    private Boolean afterPartyFlag;
    @JsonProperty("AllYouCanDrinkFlag")
    private Boolean allYouCanDrinkFlag;
    @JsonProperty("AllYouCanEatDessertFlag")
    private Boolean allYouCanEatDessertFlag;
    @JsonProperty("AllYouCanEatFlag")
    private Boolean allYouCanEatFlag;
    @JsonProperty("CourseMenuFlag")
    private Boolean courseMenuFlag;
    @JsonProperty("AnniversaryFlag")
    private Boolean anniversaryFlag;
    @JsonProperty("BarrierFreeFlag")
    private Boolean barrierFreeFlag;
    @JsonProperty("BirthdayFlag")
    private Boolean birthdayFlag;
    @JsonProperty("BreakfastMenuFlag")
    private Boolean breakfastMenuFlag;
    @JsonProperty("BrownBaggingFlag")
    private Boolean brownBaggingFlag;
    @JsonProperty("BrunchMenuFlag")
    private Boolean brunchMenuFlag;
    @JsonProperty("CalorieDataFlag")
    private Boolean calorieDataFlag;
    @JsonProperty("Capacity")
    private String capacity;
    @JsonProperty("ChildFriendlyFlag")
    private Boolean childFriendlyFlag;
    @JsonProperty("ConvivialPartyFlag")
    private Boolean convivialPartyFlag;
    @JsonProperty("DatingFlag")
    private Boolean datingFlag;
    @JsonProperty("EntertainFlag")
    private Boolean entertainFlag;
    @JsonProperty("FamilyFlag")
    private Boolean familyFlag;
    @JsonProperty("GuestRooms")
    private String guestRooms;
    @JsonProperty("HealthierMenuFlag")
    private Boolean healthierMenuFlag;
    @JsonProperty("HorigotatsuFlag")
    private Boolean horigotatsuFlag;
    @JsonProperty("HypoallergenicMenuFlag")
    private Boolean hypoallergenicMenuFlag;
    @JsonProperty("InfoLatLon")
    private String infoLatLon;
    @JsonProperty("LadysPartyFlag")
    private Boolean ladysPartyFlag;
    @JsonProperty("LargeGroupFlag")
    private Boolean largeGroupFlag;
    @JsonProperty("LunchAfter14MenuFlag")
    private Boolean lunchAfter14MenuFlag;
    @JsonProperty("LunchBuffetFlag")
    private Boolean lunchBuffetFlag;
    @JsonProperty("LunchIncludedDessertFlag")
    private Boolean lunchIncludedDessertFlag;
    @JsonProperty("LunchIncludedDrinkFlag")
    private Boolean lunchIncludedDrinkFlag;
    @JsonProperty("LunchLimitedMenuFlag")
    private Boolean lunchLimitedMenuFlag;
    @JsonProperty("LunchPlentyMenuFlag")
    private Boolean lunchPlentyMenuFlag;
    @JsonProperty("LunchPrice")
    private String lunchPrice;
    @JsonProperty("LunchRefillBreadAndRiceFlag")
    private Boolean lunchRefillBreadAndRiceFlag;
    @JsonProperty("LunchSaladBarFlag")
    private Boolean lunchSaladBarFlag;
    @JsonProperty("LunchServiceTimeFlag")
    private Boolean lunchServiceTimeFlag;
    @JsonProperty("LunchWeekendSpetialMenuFlag")
    private Boolean lunchWeekendSpetialMenuFlag;
    @JsonProperty("MatchmakingPartyFlag")
    private Boolean matchmakingPartyFlag;
    @JsonProperty("OnlineReserveFlag")
    private Boolean onlineReserveFlag;
    @JsonProperty("OnlineReserveRequestFlag")
    private Boolean onlineReserveRequestFlag;
    @JsonProperty("ParlorFlag")
    private Boolean parlorFlag;
    @JsonProperty("PersistencyImage1")
    private String persistencyImage1;
    @JsonProperty("PersistencyImage2")
    private String persistencyImage2;
    @JsonProperty("PersistencyImage3")
    private String persistencyImage3;
    @JsonProperty("PersistencyImage4")
    private String persistencyImage4;
    @JsonProperty("PersistencyImage5")
    private String persistencyImage5;
    @JsonProperty("PersistencyTitle1")
    private String persistencyTitle1;
    @JsonProperty("PersistencyTitle2")
    private String persistencyTitle2;
    @JsonProperty("PersistencyTitle3")
    private String persistencyTitle3;
    @JsonProperty("PersistencyTitle4")
    private String persistencyTitle4;
    @JsonProperty("PersistencyTitle5")
    private String persistencyTitle5;
    @JsonProperty("PersistencyUrl")
    private String persistencyUrl;
    @JsonProperty("Pet")
    private String pet;
    @JsonProperty("PetFlag")
    private Boolean petFlag;
    @JsonProperty("Pool")
    private String pool;
    @JsonProperty("ReferenceEmail")
    private String referenceEmail;
    @JsonProperty("RecommendItemId1")
    private String recommendItemId1;
    @JsonProperty("RecommendItemId2")
    private String recommendItemId2;
    @JsonProperty("RecommendItemId3")
    private String recommendItemId3;
    @JsonProperty("RecommendItemImage1")
    private String recommendItemImage1;
    @JsonProperty("RecommendItemImage2")
    private String recommendItemImage2;
    @JsonProperty("RecommendItemImage3")
    private String recommendItemImage3;
    @JsonProperty("RecommendItemName1")
    private String recommendItemName1;
    @JsonProperty("RecommendItemName2")
    private String recommendItemName2;
    @JsonProperty("RecommendItemName3")
    private String recommendItemName3;
    @JsonProperty("RecommendItemRank1")
    private String recommendItemRank1;
    @JsonProperty("RecommendItemRank2")
    private String recommendItemRank2;
    @JsonProperty("RecommendItemRank3")
    private String recommendItemRank3;
    @JsonProperty("ReligiousMenuFlag")
    private Boolean religiousMenuFlag;
    @JsonProperty("ReservationLabelCode")
    private String reservationLabelCode;
    @JsonProperty("ReservationPcUrl1")
    private String reservationPcUrl1;
    @JsonProperty("ReservationRequestPcUrl1")
    private String reservationRequestPcUrl1;
    @JsonProperty("ReservationRequestSmartPhoneUrl1")
    private String reservationRequestSmartPhoneUrl1;
    @JsonProperty("Smoking")
    private String smoking;
    @JsonProperty("SmokingFlag")
    private Boolean smokingFlag;
    @JsonProperty("NonSmokingFlag")
    private Boolean nonSmokingFlag;
    @JsonProperty("SeparationSmokingFlag")
    private Boolean separationSmokingFlag;
    @JsonProperty("WiFiFreeFlag")
    private Boolean wiFiFreeFlag;
    @JsonProperty("TableSeatFlag")
    private Boolean tableSeatFlag;
    @JsonProperty("ParkingAvailability")
    private String parkingAvailability;
    @JsonProperty("ParkingAvailabilityCode")
    private String parkingAvailabilityCode;
    @JsonProperty("ParkingCapacity")
    private String parkingCapacity;
    @JsonProperty("MonthlyParkingCapacity")
    private String monthlyParkingCapacity;
    @JsonProperty("MeteredParkingSpace")
    private String meteredParkingSpace;
    @JsonProperty("BikeParkingCapacity1")
    private String bikeParkingCapacity1;
    @JsonProperty("MonthlyBikeParkingCapacity1")
    private String monthlyBikeParkingCapacity1;
    @JsonProperty("MeteredBikeParkingSpace1")
    private String meteredBikeParkingSpace1;
    @JsonProperty("ParkingComment")
    private String parkingComment;
    @JsonProperty("ParkingCharge")
    private String parkingCharge;
    @JsonProperty("ParkingTime")
    private String parkingTime;
    @JsonProperty("ParkingType")
    private String parkingType;
    @JsonProperty("ParkingLimitHight1")
    private String parkingLimitHight1;
    @JsonProperty("ParkingLimitWidth1")
    private String parkingLimitWidth1;
    @JsonProperty("ParkingLimitLength1")
    private String parkingLimitLength1;
    @JsonProperty("ParkingLimitWeight1")
    private String parkingLimitWeight1;
    @JsonProperty("ParkingManager1")
    private String parkingManager1;
    @JsonProperty("ParkingFacilityType1")
    private String parkingFacilityType1;
    @JsonProperty("ParkingEquipmentType1")
    private String parkingEquipmentType1;
    @JsonProperty("PickUp")
    private String pickUp;
    @JsonProperty("MKPCardSellFlag")
    private String mKPCardSellFlag;
    @JsonProperty("CarLength")
    private String carLength;
    @JsonProperty("CarWidth")
    private String carWidth;
    @JsonProperty("CarHeight")
    private String carHeight;
    @JsonProperty("Payment")
    private String payment;
    @JsonProperty("RentalWheelChairFlag")
    private String rentalWheelChairFlag;
    @JsonProperty("BrailleElevator")
    private String brailleElevator;
    @JsonProperty("BarrierFreeRoomToiletForWheelChair")
    private String barrierFreeRoomToiletForWheelChair;
    @JsonProperty("AntiSteps")
    private String antiSteps;
    @JsonProperty("HandicappedParkingCapacity")
    private String handicappedParkingCapacity;
    @JsonProperty("OtherFacilities")
    private String otherFacilities;
    @JsonProperty("EventComment")
    private String eventComment;
    @JsonProperty("TopRankItem1")
    private String topRankItem1;
    @JsonProperty("TopRankItem2")
    private String topRankItem2;
    @JsonProperty("TopRankItem3")
    private String topRankItem3;
    @JsonProperty("TopRankItem4")
    private String topRankItem4;
    @JsonProperty("TopRankItem5")
    private String topRankItem5;
    @JsonProperty("TopRankItemId1")
    private String topRankItemId1;
    @JsonProperty("TopRankItemId2")
    private String topRankItemId2;
    @JsonProperty("TopRankItemId3")
    private String topRankItemId3;
    @JsonProperty("TopRankItemId4")
    private String topRankItemId4;
    @JsonProperty("TopRankItemId5")
    private String topRankItemId5;
    @JsonProperty("TopRankItemImage1")
    private String topRankItemImage1;
    @JsonProperty("TopRankItemImage2")
    private String topRankItemImage2;
    @JsonProperty("TopRankItemImage3")
    private String topRankItemImage3;
    @JsonProperty("TopRankItemRating1")
    private String topRankItemRating1;
    @JsonProperty("TopRankItemRating2")
    private String topRankItemRating2;
    @JsonProperty("TopRankItemRating3")
    private String topRankItemRating3;
    @JsonProperty("TopRankItemRating4")
    private String topRankItemRating4;
    @JsonProperty("TopRankItemRating5")
    private String topRankItemRating5;
    @JsonProperty("VegetarianMenuFlag")
    private Boolean vegetarianMenuFlag;
    @JsonProperty("WhatsNewUrl")
    private String whatsNewUrl;
    @JsonProperty("YourselfFlag")
    private Boolean yourselfFlag;
    @JsonProperty("ZipCode")
    private String zipCode;
    @JsonProperty("CreditCardsAMEXFlag")
    private Boolean creditCardsAMEXFlag;
    @JsonProperty("CreditCardsDCFlag")
    private Boolean creditCardsDCFlag;
    @JsonProperty("CreditCardsDinersFlag")
    private Boolean creditCardsDinersFlag;
    @JsonProperty("CreditCardsJCBFlag")
    private Boolean creditCardsJCBFlag;
    @JsonProperty("CreditCardsMasterFlag")
    private Boolean creditCardsMasterFlag;
    @JsonProperty("CreditCardsNICOSFlag")
    private Boolean creditCardsNICOSFlag;
    @JsonProperty("CreditCardsOtherFlag")
    private Boolean creditCardsOtherFlag;
    @JsonProperty("CreditCardsSaisonFlag")
    private Boolean creditCardsSaisonFlag;
    @JsonProperty("CreditCardsUCFlag")
    private Boolean creditCardsUCFlag;
    @JsonProperty("CreditCardsUFJFlag")
    private Boolean creditCardsUFJFlag;
    @JsonProperty("CreditCardsVISAFlag")
    private Boolean creditCardsVISAFlag;
    @JsonProperty("MenuPageUrl1")
    private String menuPageUrl1;
    @JsonProperty("MenuPageUrl2")
    private String menuPageUrl2;
    @JsonProperty("MenuPageUrl3")
    private String menuPageUrl3;
    @JsonProperty("MobileImageUrl1")
    private String mobileImageUrl1;
    @JsonProperty("MobileImageUrl2")
    private String mobileImageUrl2;
    @JsonProperty("MobileImageUrl3")
    private String mobileImageUrl3;
    @JsonProperty("Altitude")
    private String altitude;
    @JsonProperty("Copyright")
    private String copyright;
    @JsonProperty("JunkFlag")
    private Boolean junkFlag;
    @JsonProperty("AvailabilityCode")
    private String availabilityCode;
    @JsonProperty("WaitingTime")
    private String waitingTime;
    @JsonProperty("WaitingNumber")
    private String waitingNumber;
    @JsonProperty("AlternativeTel")
    private String alternativeTel;
    @JsonProperty("PpcNumber")
    private String ppcNumber;
    @JsonProperty("PpcDescription")
    private String ppcDescription;
    @JsonProperty("MedicalProfession")
    private String medicalProfession;
    @JsonProperty("Beds")
    private String beds;
    @JsonProperty("HighMedicalEquipment")
    private String highMedicalEquipment;
    @JsonProperty("StartDay")
    private String startDay;
    @JsonProperty("HotelType")
    private String hotelType;
    @JsonProperty("HotelService")
    private String hotelService;
    @JsonProperty("Attention")
    private String attention;
    @JsonProperty("AdditionalServise")
    private String additionalServise;
    @JsonProperty("OpenTwentyFourHours")
    private String openTwentyFourHours;
    @JsonProperty("BudgetMax")
    private String budgetMax;
    @JsonProperty("EMoney")
    private String eMoney;
    @JsonProperty("Receipt")
    private String receipt;
    @JsonProperty("ThousandYenBill")
    private String thousandYenBill;
    @JsonProperty("ParkingControlArea")
    private String parkingControlArea;
    @JsonProperty("MinGroundClearance")
    private String minGroundClearance;
    @JsonProperty("MinicarLimit")
    private String minicarLimit;
    @JsonProperty("OpenSeason")
    private String openSeason;
    @JsonProperty("VideoProgramName")
    private String videoProgramName;
    @JsonProperty("VideoName")
    private String videoName;
    @JsonProperty("PriceFrom1")
    private String priceFrom1;
    @JsonProperty("PriceTo1")
    private String priceTo1;
    // TODO : Yahooの部分はLSBE反映が終わったらIFもう一回確認必要
    @JsonProperty("PRTitle1")
    private String prTitle1;
    @JsonProperty("PRDescription1")
    private String prDescription1;
    @JsonProperty("PRImageUrl1")
    private String prImageUrl1;
    @JsonProperty("PRTitle2")
    private String prTitle2;
    @JsonProperty("PRDescription2")
    private String prDescription2;
    @JsonProperty("PRImageUrl2")
    private String prImageUrl2;
    @JsonProperty("PRTitle3")
    private String prTitle3;
    @JsonProperty("PRDescription3")
    private String prDescription3;
    @JsonProperty("PRImageUrl3")
    private String prImageUrl3;
    @JsonProperty("PoolFlag")
    private Boolean poolFlag;
    @JsonProperty("OnsenFlag")
    private Boolean onsenFlag;
    @JsonProperty("RotenburoFlag")
    private Boolean rotenburoFlag;
    @JsonProperty("CharteredBathFlag")
    private Boolean charteredBathFlag;
    @JsonProperty("KonyokuRotenFlag")
    private Boolean konyokuRotenFlag;
    @JsonProperty("BigCommonBathFlag")
    private Boolean bigCommonBathFlag;
    @JsonProperty("SaunaFlag")
    private Boolean saunaFlag;
    @JsonProperty("FreePickUpFlag")
    private Boolean freePickUpFlag;
    @JsonProperty("ConvenienceStoreFlag")
    private Boolean convenienceStoreFlag;
    @JsonProperty("ParkingFlag")
    private Boolean parkingFlag;
    @JsonProperty("SmallGroupFlag")
    private Boolean smallGroupFlag;
    @JsonProperty("LateNightFlag")
    private Boolean lateNightFlag;
    @JsonProperty("PrivateDiningFlag")
    private Boolean privateDiningFlag;
    @JsonProperty("ReservedSeatFlag")
    private Boolean reservedSeatFlag;
    @JsonProperty("StoreFlag")
    private Boolean storeFlag;
    @JsonProperty("RestroomFlag")
    private Boolean restroomFlag;
    @JsonProperty("BanquetFlag")
    private Boolean banquetFlag;
    @JsonProperty("FriendsFlag")
    private Boolean friendsFlag;
    @JsonProperty("WineFlag")
    private Boolean wineFlag;
    @JsonProperty("ShochuFlag")
    private Boolean shochuFlag;
    @JsonProperty("RiceWineFlag")
    private Boolean riceWineFlag;
    @JsonProperty("BeerFlag")
    private Boolean beerFlag;
    @JsonProperty("CocktailFlag")
    private Boolean cocktailFlag;
    @JsonProperty("TakeOutFlag")
    private Boolean takeOutFlag;
    @JsonProperty("WeddingAnniversaryFlag")
    private Boolean weddingAnniversaryFlag;
    @JsonProperty("BuddhistMemorialServiceFlag")
    private Boolean buddhistMemorialServiceFlag;
    @JsonProperty("MemorialDayFlag")
    private Boolean memorialDayFlag;
    @JsonProperty("NightSceneFlag")
    private Boolean nightSceneFlag;
    @JsonProperty("GoodViewFlag")
    private Boolean goodViewFlag;
    @JsonProperty("EkichikaFlag")
    private Boolean ekichikaFlag;
    @JsonProperty("EkinakaFlag")
    private Boolean ekinakaFlag;
    @JsonProperty("CalmSpaceFlag")
    private Boolean calmSpaceFlag;
    @JsonProperty("KaraokeFlag")
    private Boolean karaokeFlag;
    @JsonProperty("ProjectorFlag")
    private Boolean projectorFlag;
    @JsonProperty("OpenTerraceFlag")
    private Boolean openTerraceFlag;
    @JsonProperty("EnglishSpeakingFlag")
    private Boolean englishSpeakingFlag;
    @JsonProperty("TobaccoFlag")
    private Boolean tobaccoFlag;
    @JsonProperty("LiquorFlag")
    private Boolean liquorFlag;

    private Map<String, Object> others = new LinkedHashMap<>();

    @JsonAnyGetter
    public Map<String, Object> getOthers() {
      return others;
    }

    @JsonAnySetter
    public void setOthers(String name, Object value) {
      others.put(name, value);
    }

  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Item {
    @JsonProperty("ItemCode1")
    private String itemCode1;
    @JsonProperty("ItemDescription1")
    private String itemDescription1;
    @JsonProperty("ItemImageUrl1")
    private String itemImageUrl1;
    @JsonProperty("ItemPcUrl1")
    private String itemPcUrl1;
    @JsonProperty("ItemPrice1")
    private String itemPrice1;
    @JsonProperty("ItemPriceType1")
    private int itemPriceType1;
    @JsonProperty("ItemSmartPhoneUrl1")
    private String itemSmartPhoneUrl1;
    @JsonProperty("ItemTitle1")
    private String itemTitle1;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class LifeMagazine {
    @JsonProperty("ArticleId")
    private String articleId;
    @JsonProperty("ArticleTitle")
    private String articleTitle;
    @JsonProperty("ArticleImageUrl")
    private String articleImageUrl;
    @JsonProperty("ArticleWriter")
    private String articleWriter;
  }

  @Slf4j
  public static class ObjectAndArrayDeserializer extends JsonDeserializer<PlaceInfo[]> {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Override
    public Tenpo.PlaceInfo[] deserialize(@NotNull JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
      ObjectCodec codec = jsonParser.getCodec();
      JsonNode tree = codec.readTree(jsonParser);
      if (tree.isArray()) {
        return OBJECT_MAPPER.convertValue(tree, Tenpo.PlaceInfo[].class);
      }

      log.debug("ObjectタイプをList化します。");
      return new Tenpo.PlaceInfo[]{OBJECT_MAPPER.convertValue(tree, Tenpo.PlaceInfo.class)};
    }
  }
}
