package jp.co.yahoo.dining.frontend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * ImageWizardからのレスポンス
 * //PropertyNamingStrategy.UpperCamelCaseStrategy
 */
@Data
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
public class ResizedImage implements ExternalEntity, Serializable {
  private final Long serialVersionUID = 1l;

  @JsonIgnoreProperties(ignoreUnknown = true)
  private ResultSet resultSet;

  @Data
  @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
  public static class ResultSet implements Serializable {
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonProperty("@totalResultsAvailable")
    private String totalResultsAvailable;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonProperty("@totalResultsReturned")
    private String totalResultsReturned;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonProperty("@firstResultPosition")
    private String firstResultPosition;

    @JsonIgnoreProperties(ignoreUnknown = true)
    private Result[] result;
  }

  @Data
  @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
  public static class Result implements Serializable {
    @JsonIgnoreProperties(ignoreUnknown = true)
    private String url;

    @JsonIgnoreProperties(ignoreUnknown = true)
    private String originalPath;

    @JsonIgnoreProperties(ignoreUnknown = true)
    private String originalUrl;

    @JsonIgnoreProperties(ignoreUnknown = true)
    private String signature;
  }
}
