package jp.co.yahoo.dining.frontend.controller.form;

import lombok.Data;
import org.apache.commons.lang.StringUtils;

@Data
public class RdsigInfo {
  // TODO : BeanValidation追加
  private String[] urls;

  private String label = StringUtils.EMPTY;

}
