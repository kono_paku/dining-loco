package jp.co.yahoo.dining.frontend.infrastructure.rest;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "extension.api.planStock")
public class PlanStockAPIConfig {
  private String planStock;

  private String appId;
}
