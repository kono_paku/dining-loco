package jp.co.yahoo.dining.frontend.controller;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import jp.co.yahoo.dining.frontend.infrastructure.ncookie.NcookieService;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;


/**
 * Cookie情報をリクエストスコープで保持します。
 */
@Component
@RequestScope
@Profile({"staging", "prod"})
public class LocoRequestHeaderImpl implements LocoRequestHeader {

  /**
   * トレースID
   */
  private String traceId;
  /**
   * デバイス種別
   */
  private final String device;
  /**
   * デバイス種別
   */
  private final String yjdDevice;
  /**
   * リクエスト元ドメイン
   */
  private final String domain;
  /**
   * N Cookie
   */
  private final String cookie;
  // TODO: BCoocieとかYTCookieも取得する
  /**
   * GUID
   */
  private final String guid;
  /**
   * YID
   */
  private final String yid;

  /**
   * リクエストスコープで、Cookie情報を保持する
   *
   * @param request サーブレットリクエストオブジェクト
   */
  public LocoRequestHeaderImpl(final HttpServletRequest request, final NcookieService ncookieService) throws IOException {
    // TODO: Edgeとかのバグでリクエストヘッダが取得できなかった場合を考える。
    this.traceId = UUID.randomUUID().toString();
    this.domain = Optional.of(request.getServerName()).orElse("loco.yahoo.co.jp");
    this.cookie = Optional.ofNullable(request.getHeader("Cookie")).orElse(StringUtils.EMPTY);
    this.device = getDeviceType(request.getHeader("X-Devcat-Category"));
    this.yjdDevice = Optional.ofNullable(request.getHeader("X-YJDSession-Category")).orElse("none");
    this.guid = StringUtils.isNotEmpty(cookie) ? ncookieService.getGuid(request) : StringUtils.EMPTY;
    this.yid = StringUtils.isNotEmpty(cookie) ? ncookieService.getYid(request) : StringUtils.EMPTY;
  }

  /**
   * デバイス判定を行う
   *
   * @param device デバイス種別, can not be {@code null}
   */
  private String getDeviceType(final String device) {
    if (StringUtils.isNotEmpty(device) && StringUtils.endsWith(device, "phone")) {
      return "smartphone";
    }
    return "pc";
  }

  @Override
  public String getTraceId() {
    return this.traceId;
  }

  @Override
  public String getDevice() {
    return this.device;
  }

  @Override
  public String getYJDDevice() {
    return this.yjdDevice;
  }

  @Override
  public String getCookie() {
    return this.cookie;
  }

  @Override
  public String getDomain() {
    return this.domain;
  }

  @Override
  public String getGuid() {
    return this.guid;
  }

  @Override
  public String getYid() {
    return this.yid;
  }

  @Override
  public String getActualDevice(){
    if (StringUtils.equals(device, "smartphone")) {
      return (StringUtils.equals(yjdDevice, "pc")) ? "pc" : "smartphone";
    } else {
      return "pc";
    }
  }

}
