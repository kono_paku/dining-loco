package jp.co.yahoo.dining.frontend.repository;

import jp.co.yahoo.dining.frontend.controller.form.ImagesInfo;
import jp.co.yahoo.dining.frontend.domain.ResizedImage;

public interface ImageWizardTemplate {
  ResizedImage resizeImages(ImagesInfo imagesInfo);
}
