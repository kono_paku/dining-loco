package jp.co.yahoo.dining.frontend.infrastructure.rest;

import jp.co.yahoo.dining.frontend.domain.PlanPolicy;
import jp.co.yahoo.dining.frontend.repository.PolicyTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

@Component
public class PolicyTemplateImpl extends LocoRestTemplate implements PolicyTemplate {

  @Autowired
  @Qualifier("policy")
  private RestTemplate restTemplate;

  @Autowired
  private PolicyAPIConfig policyAPIConfig;

  @Override
  public RestTemplate rt() {
    return restTemplate;
  }

  @Override
  protected HttpHeaders setAuthority(HttpHeaders headers) {
    return headers;
  }

  @Override
  public PlanPolicy getPolicyList(String tenpoId, Optional<String> planId, Optional<String> datetime) {
    UriComponentsBuilder builder = createRequest(policyAPIConfig.getPlanPolicy())
      .queryParam("tenpoid", tenpoId)
      .queryParam("appid", policyAPIConfig.getAppId());

    planId.ifPresent(plan -> builder.queryParam("planid", plan));
    datetime.ifPresent(date -> builder.queryParam("datetime", date));

    return getEntity(PlanPolicy.class, builder);
  }
}
