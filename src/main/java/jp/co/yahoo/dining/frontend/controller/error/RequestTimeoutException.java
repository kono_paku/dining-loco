package jp.co.yahoo.dining.frontend.controller.error;


/**
 * ロコ用のExceptionクラス
 */
public class RequestTimeoutException extends AbstractErrorResponse {
  private static final String ERROR_TYPE = "APIリクエストにタイムアウトが発生してます";

  public RequestTimeoutException(Throwable e) {
    super(ERROR_TYPE + " : " + e.getMessage(), e);
  }

}
