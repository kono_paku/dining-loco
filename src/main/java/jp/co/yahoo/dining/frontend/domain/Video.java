package jp.co.yahoo.dining.frontend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Video implements ExternalEntity, Serializable {
  @JsonProperty("ResultSet")
  private ResultSet resultSet;

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Device {
    @JsonProperty("Item")
    private String[] item;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Result {
    @JsonProperty("contentid")
    private String contentId;
    @JsonProperty("Title")
    private String title;
    @JsonProperty("ChannelTitle")
    private String channelTitle;
    @JsonProperty("Duration")
    private int duration;
    @JsonProperty("Description")
    private String description;
    @JsonProperty("Copyright")
    private String copyright;
    @JsonProperty("StartDate")
    private String startDate;
    @JsonProperty("EndDate")
    private String endDate;
    @JsonProperty("ChannelId")
    private int channelId;
    @JsonProperty("CooperationId")
    private String cooperationId;
    @JsonProperty("Tag")
    private String tag;
    @JsonProperty("Device")
    private Device device;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class ResultSet {
    private String totalResultsAvailable;
    private String totalResultsReturned;
    private String firstResultPosition;
    @JsonProperty("Result")
    private Result[] result;

  }

  @Data
  @AllArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Thumbnail {
    @JsonProperty("ThumbnailUrl")
    private String thumbnailUrl;
    @JsonProperty("ThumbnailWidth")
    private int thumbnailWidth;
    @JsonProperty("ThumbnailHeight")
    private int thumbnailHeight;
    @JsonProperty("ThumbnailOriginalUrl")
    private String thumbnailOriginalUrl;
  }

}
