package jp.co.yahoo.dining.frontend.controller.api;

import jp.co.yahoo.dining.frontend.controller.form.ImagesInfo;
import jp.co.yahoo.dining.frontend.domain.ResizedImage;
import jp.co.yahoo.dining.frontend.service.ImageResizeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
public class ImageResizeController {

  private final ImageResizeService service;

  /**
   * ImageWizardの画像変換APIのプロキシを提供します。
   * @param info 変換元画像
   * @return 変換後画像
   */
  @PostMapping("/v1/api/resizeImage")
  public ResizedImage resizeImage(@Valid @RequestBody final ImagesInfo info){
    return service.resizedImages(info);
  }
}
