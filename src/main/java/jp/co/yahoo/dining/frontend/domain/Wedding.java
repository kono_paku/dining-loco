package jp.co.yahoo.dining.frontend.domain;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Wedding implements ExternalEntity, Serializable {
  private ResultInfo resultInfo;
  private Feature[] feature;

  @Data
  @NoArgsConstructor
  @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class ResultInfo {
    private Integer count;
    private Integer total;
    private Long start;
    private BigDecimal latency;
  }

  @Data
  @NoArgsConstructor
  @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Feature {
    private String id;
    private String gid;
    private String name;
    private Children[] children;
    private Property property;
  }

  @Data
  @NoArgsConstructor
  @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Property {
    private Detail detail;
    private String cassetteId;
  }

  @Data
  @NoArgsConstructor
  @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Detail {
    private String cassetteHeader;
    private String pcUrl1;
    private String weddingAccommodation;
    private String weddingAfterParty1;
    private String weddingBusinessHour;
    private String weddingChargeDesk1;
    private String weddingComment1;
    private String weddingFacilitiesComment;
    private String weddingParking;
    private String weddingPartyFoodCharge1;
    private String weddingPayment;
    private String weddingPickUp;
    private String weddingReferenceComment;
    private String weddingReservation;
    private String weddingUsePossibleTime;
    private String wededingCeremonialCapacity; // -> TODO:スペルミス。ロコに修正依頼を投げる
    private String wededingCeremonialStyle1; // -> TODO:スペルミス。ロコに修正依頼を投げる
    private String yUrl;
    private String weddingReportDetailUrl1; // ReportDetailUrl<num>と同じ扱いのフィールドです。
    private String weddingReportSchejule1; // ReportSchejule<num>と同じ扱いのフィールドです。  // -> TODO:スペルミス(正しくは「Schedule」)。ロコに修正依頼を投げる。
    private String weddingReportTitle1; // ReportTitle<num>と同じ扱いのフィールドです。

    /**
     * othersに入ってくるであろうフィールド一覧
     * <ul>
     *  <li>WeddingFairBenefit<num></li>
     *  <li>WeddingFairContent<num></li>
     *  <li>WeddingFairDetailUrl<num></li>
     *  <li>WeddingFairEndTime<num></li>
     *  <li>WeddingFairImageCaption<num></li>
     *  <li>WeddingFairImageUrl<num></li>
     *  <li>WeddingFairKind<num></li>
     *  <li>WeddingFairName<num></li>
     *  <li>WeddingFairReservationPcUrl<num></li>
     *  <li>WeddingFairReservationSmartPhoneUrl<num></li>
     *  <li>WeddingFairSchejule<num></li>
     *  <li>WeddingFairSeat<num></li>
     *  <li>WeddingFairStartTime<num></li>
     *  <li>WeddingPlanBenefit<num></li>
     *  <li>WeddingPlanComment<num></li>
     *  <li>WeddingPlanDescription<num></li>
     *  <li>WeddingPlanImage<num></li>
     *  <li>WeddingPlanImageCaption<num></li>
     *  <li>WeddingPlanName<num></li>
     *  <li>WeddingPlanPeopleNumber<num></li>
     *  <li>WeddingPlanPeriod<num></li>
     *  <li>WeddingPlanPrice<num></li>
     *  <li>WeddingPlanTitle<num></li>
     *  <li>WeddingPlanUrl<num></li>
     *  <li>ReportDetailUrl<num></li>
     *  <li>ReportSchejule<num></li>
     *  <li>ReportTitle<num></li>
     * </ul>
     */
    private Map<String, Object> others = new LinkedHashMap<>();

    @JsonAnyGetter
    public Map<String, Object> getOthers() {
      return others;
    }

    @JsonAnySetter
    public void setOthers(String name, Object value) {
      others.put(name, value);
    }
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Children {
    private String id;
    private String cassetteId;
  }
}
