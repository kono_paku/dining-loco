package jp.co.yahoo.dining.frontend.controller.error;


/**
 * ロコ用のExceptionクラス
 */
public class TenpoNotFoundException extends AbstractErrorResponse {

  public TenpoNotFoundException(String message) {
    super(message);
  }

}
