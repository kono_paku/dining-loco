package jp.co.yahoo.dining.frontend.infrastructure.rdsig;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import jp.co.yahoo.dining.frontend.domain.Rdsig;
import jp.co.yahoo.dining.frontend.infrastructure.rdsig.model.RdsigRequest;
import jp.co.yahoo.dining.frontend.infrastructure.rdsig.model.RdsigResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by jchung on 2017/12/08.
 */
@Slf4j
@Service
public class RdsigService {

  private static final int RDSIG_REQUEST_SIG = 1;

  @Value("${extension.rdsig.url}")
  private String apiUrl;

  private final RestTemplate restTemplate;

  private final ParameterizedTypeReference<Map<String, RdsigResponse>> responseType;

  public RdsigService(RestTemplateBuilder builder) {
    this.restTemplate = builder.build();
    this.responseType = new ParameterizedTypeReference<Map<String, RdsigResponse>>() {};
  }

  public List<Rdsig> getRdsidUrlList(String[] originUrls, String label) {
    Map<String, RdsigResponse> response = getRdsigUrlResponse(originUrls, label);
    // モデル変換
    List<Rdsig> rdsigList = convertToRdsig(response);
    return rdsigList;
  }

  public Map<String, RdsigResponse> getRdsigUrlResponse(String[] originUrls, String label) {
    // ヘッダ作成
    HttpEntity<List<RdsigRequest>> requestHttpEntity = getRequestHttpEntity(originUrls, label);
    // Httpリクエスト
    return postForEntity(requestHttpEntity);
  }

  private HttpEntity<List<RdsigRequest>> getRequestHttpEntity(String[] originUrls, String label) {
    List<RdsigRequest> requestBody = new ArrayList<>();
    for (String url : originUrls) {
      requestBody.add(new RdsigRequest(url, label, RDSIG_REQUEST_SIG));
    }
    return new HttpEntity<>(requestBody);
  }

  private Map<String, RdsigResponse> postForEntity(HttpEntity<List<RdsigRequest>> request) {
    ResponseEntity<Map<String, RdsigResponse>> exchange = restTemplate.exchange(apiUrl, HttpMethod.POST, request, responseType);
    return exchange.getBody();
  }

  private List<Rdsig> convertToRdsig(Map<String, RdsigResponse> response) {
    List<Rdsig> rdsigList = new ArrayList<>();
    for (String originUrl : response.keySet()) {
      RdsigResponse rdsigResponse = response.get(originUrl);
      String wrappingUrl = rdsigResponse.getUrl();
      if (StringUtils.isNotEmpty(wrappingUrl)) {
        rdsigList.add(new Rdsig(originUrl, wrappingUrl));
      }
    }
    return rdsigList;
  }

}
