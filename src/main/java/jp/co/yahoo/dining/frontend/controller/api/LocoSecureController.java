package jp.co.yahoo.dining.frontend.controller.api;

import jp.co.yahoo.dining.frontend.domain.LocoSecure;
import jp.co.yahoo.dining.frontend.service.LocoSecureService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class LocoSecureController {
  private final LocoSecureService service;
  @GetMapping("/v1/api/locoSecure")
  public LocoSecure getImageSearch(@Valid @RequestParam final String guid) {
    // TODO: GUIDのアノテーションを作成して、バリデーションする
    return service.getEncryptYUID(guid);
  }
}
