package jp.co.yahoo.dining.frontend.controller.api;

import javax.validation.Valid;

import jp.co.yahoo.dining.frontend.controller.form.ImageSearchInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jp.co.yahoo.dining.frontend.domain.ImageSearch;
import jp.co.yahoo.dining.frontend.service.ImageSearchService;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class ImageSearchController {

  private final ImageSearchService imageSearchService;

  @GetMapping("/v1/api/imageSearch")
  public ImageSearch getImageSearch(@Valid final ImageSearchInfo imageSearchInfo) {
    return imageSearchService.getImageSearch(imageSearchInfo);
  }

}
