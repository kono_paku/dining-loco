package jp.co.yahoo.dining.frontend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class PlanPolicy implements ExternalEntity {
  private Double latency;
  private String requestDate;
  private Integer totalResults;
  private Entry entry;

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
  public static class Entry {
    private String deadlineNotice;
    private String requestDeadlineNotice;
    private String freeText;
    private String showMail;
    private String checkTypeRainy;
    private String courseAgeLimit;
    private String courseCancelCarge;
    private String courseChangeNum;
    private String courseCourseNotice;
    private String courseLimitCoupon;
    private String courseLimitDriver;
    private String courseLimitFood;
    private String courseLimitReserve;
    private String coursePointingSeat;
    private String courseReturnTel;
    private String courseServicePrice;
    private String courseDiscountLimit;
  }
}
