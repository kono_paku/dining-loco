package jp.co.yahoo.dining.frontend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by jchung on 2017/12/08.
 * PlanAPIはCamel形なので別に@JsonPropertyをつけない。(例あり)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanDetail implements ExternalEntity, Serializable {

  private float latency;
  private String requestDate;
  private PlanDetailEntry entry;

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class ChildMenuInfo {
    private ChildMenu[] childMenu;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class ChildMenu {
    private Integer childMenuNo;
    private String childMenuName;
    private Integer price;
    private Integer terminationDays;
    private String salesDateFrom;
    private String salesDateTo;
    private Integer displayOrder;
    private String childMenuImageUrl;
    private String childMenuDescription;
    private Integer maxSalesCount;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Classification {
    private String key;
    private String title;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Option {
    private Integer optionNo;
    private String optionName;
    private Integer price;
    private Integer terminationDays;
    private Boolean stockSettingFlag;
    private String salesDateFrom;
    private String salesDateTo;
    private Integer attributeCode;
    private Integer displayOrder;
    private String optionImageUrl;
    private String optionDescription;
    private Integer maxSalesCount;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class OptionInfo {
    private Option[] option;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class PlanCaution {
    @JsonProperty("deadline_notice")
    private String deadlineNotice;
    @JsonProperty("policy")
    private Policy policy;
    @JsonProperty("webtel_deadline_notice")
    private String webtelDeadlineNotice;
    @JsonProperty("webtel_policy")
    private Policy webtelPolicy;
    @JsonProperty("request_deadline_notice")
    private String requestDeadlineNotice;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class PlanDetailEntry {
    private String tenpoId;
    private String requestReservationFlg;
    private String cardPaymentDescription;
    private Integer smokingCode;
    private String smokingDescription;
    private String dressCodeDescription;
    private Integer parkingCode;
    private String parkingDescription;
    private Integer seatCount;
    private String seatCountDescription;
    private Integer childAcceptanceFlag;
    private String childDescription;
    private Integer childAgeLimitCode;
    private Integer childMenuCode;
    private Integer childSeatLimitCode;
    private Integer childChairCode;
    private Integer childTimeLimitCode;
    private PlanInfo[] plans;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class PlanInfo {
    private String planId;
    private String name;
    private Integer price;
    private Integer fixedNumMax;
    private Integer fixedNumMin;
    private String[] image;
    private Boolean seatOnlyFlg;
    private Integer tokutenType;
    private String tokutenText;
    private Boolean premiumOnlyFlag;
    private Integer originalPrice;
    private Integer discountRate;
    private Integer discountType;
    private String catchPhrase;
    private String introduction;
    private String menuIntroduction;
    private String menu;
    private String drinkIntroduction;
    private String drink;
    private PlanCaution planCautionList;
    private Property[] property;
    private int stayTime;
    private Classification classification;
    @JsonDeserialize(using = PlanDetail.ObjectAndArrayDeserializer.class)
    private Reserve reserve;
    // こちらからは一休のみのデータ
    private String menuText;
    private String planCautionText;
    private String timeZoneCode;
    private String visitDateFrom;
    private String visitDateTo;
    private String reserveDateFrom;
    private String reserveDateTo;
    private OptionInfo optionInfo;
    private ChildMenuInfo childMenuInfo;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Policy {
    @JsonProperty("show_mail")
    private String showMail;
    @JsonProperty("check_type_rainy")
    private String checkTypeRainy;
    @JsonProperty("course_age_limit")
    private String courseAgeLimit;
    @JsonProperty("course_cancel_carge")
    private String courseCancelCarge;
    @JsonProperty("course_change_num")
    private String courseChangeNum;
    @JsonProperty("course_course_notice")
    private String courseCourseNotice;
    @JsonProperty("course_limit_coupon")
    private String courseLimitCoupon;
    @JsonProperty("course_limit_driver")
    private String courseLimitDriver;
    @JsonProperty("course_limit_food")
    private String courseLimitFood;
    @JsonProperty("course_limit_reserve")
    private String courseLimitReserve;
    @JsonProperty("course_return_tel")
    private String courseReturnTel;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Property {
    private String key;
    private String title;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Reserve {
    private String startDate;
    private String closeDate;
    private String insertionStart;
    private String insertionEnd;
  }


  @Slf4j
  public static class ObjectAndArrayDeserializer extends JsonDeserializer<Reserve> {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    @Override
    public Reserve deserialize(@NotNull JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
      ObjectCodec codec = jsonParser.getCodec();
      JsonNode tree = codec.readTree(jsonParser);
      if (tree.isArray()) {
        return null;
      }
      log.info("ObjectタイプをList化します。");
      return OBJECT_MAPPER.convertValue(tree, Reserve.class);
    }
  }
}
