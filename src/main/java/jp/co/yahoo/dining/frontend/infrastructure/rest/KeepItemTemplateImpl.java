package jp.co.yahoo.dining.frontend.infrastructure.rest;

import jp.co.yahoo.dining.frontend.domain.KeepItem;
import jp.co.yahoo.dining.frontend.domain.KeepItemList;
import jp.co.yahoo.dining.frontend.infrastructure.Dto;
import jp.co.yahoo.dining.frontend.repository.KeepItemTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

@Component
public class KeepItemTemplateImpl extends LocoRestTemplate implements KeepItemTemplate {

  private final RestTemplate rt;
  private final KeepItemAPIConfig keepItemAPIConfig;

  public KeepItemTemplateImpl(@Qualifier("keepItem") RestTemplate rt, KeepItemAPIConfig keepItemAPIConfig) {
    this.rt = rt;
    this.keepItemAPIConfig = keepItemAPIConfig;
  }

  @Override
  public RestTemplate rt() {
    return rt;
  }

  @Override
  protected HttpHeaders setAuthority(HttpHeaders headers) {
    return headers;
  }

  @Override
  public KeepItem addItem(String cookie, String createBy, String gid, String tenpoName, String uid, Optional<Float> latitude, Optional<Float> longitude) {
    HttpHeaders headers = new HttpHeaders();
    headers.add("Cookie", cookie);

    UriComponentsBuilder builder = createRequest(keepItemAPIConfig.getBaseApiPath())
      .queryParam("appid", keepItemAPIConfig.getAppId());

    AddKeepParameters body  = new AddKeepParameters();
    body.addGid(gid);
    body.addTenpoName(tenpoName);
    body.addUid(uid);
    body.addCreateBy(createBy);

    if (latitude.isPresent() && longitude.isPresent()) {
      body.addLatAndLng(latitude.get(), longitude.get());
    }

    return postEntity(KeepItem.class, headers, builder.build().toUriString(), body);
  }

  @Override
  public KeepItem removeItem(String cookie, String gid) {
    HttpHeaders headers = new HttpHeaders();
    headers.add("Cookie", cookie);

    String additionalPath = "/gid:" + gid;

    UriComponentsBuilder builder = createRequest(keepItemAPIConfig.getBaseApiPath() + additionalPath)
      .queryParam("appid", keepItemAPIConfig.getAppId())
      .queryParam(".src", "loco");

    return deleteEntity(KeepItem.class, headers, builder.build().toUriString());
  }

  @Override
  public KeepItemList getItemList(String cookie) {
    HttpHeaders headers = new HttpHeaders();
    headers.add("Cookie", cookie);

    UriComponentsBuilder builder = createRequest(keepItemAPIConfig.getBaseApiPath())
      .queryParam("appid", keepItemAPIConfig.getAppId())
      .queryParam("type", "gid")
      .queryParam(".src", "loco");

    return getEntity(KeepItemList.class, headers, builder);
  }

  public static class AddKeepParameters extends LinkedMultiValueMap<String, String> implements Dto {

    public AddKeepParameters() {
      super();
      super.add(".src", "loco");
      super.add("type", "gid");
    }

    public void addGid(String gid) {
      super.add("typeKey", gid);
    }

    public void addTenpoName(String tenpoName) {
      super.add("name", tenpoName);
    }

    public void addUid(String uid) {
      super.add("uid", uid);
    }

    public void addCreateBy(String createBy) {
      super.add("createBy", createBy);
    }

    public void addLatAndLng(Float lat, Float lng) {
      super.add("lat", lat.toString());
      super.add("lng", lng.toString());
    }
  }

}
