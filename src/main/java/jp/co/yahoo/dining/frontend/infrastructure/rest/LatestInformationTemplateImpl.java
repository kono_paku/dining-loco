package jp.co.yahoo.dining.frontend.infrastructure.rest;

import jp.co.yahoo.dining.frontend.domain.LatestInformation;
import jp.co.yahoo.dining.frontend.repository.LatestInformationTemplate;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Component
public class LatestInformationTemplateImpl extends LocoRestTemplate implements LatestInformationTemplate {

  public LatestInformationTemplateImpl(@Qualifier("ipoca") RestTemplate rt, IpocaAPIConfig ipocaAPIConfig) {
    this.rt = rt;
    this.ipocaAPIConfig = ipocaAPIConfig;
  }

  private final RestTemplate rt;
  private final IpocaAPIConfig ipocaAPIConfig;

  @Override
  public RestTemplate rt() {
    return rt;
  }

  @Override
  protected HttpHeaders setAuthority(HttpHeaders headers) {
    return headers;
  }

  /**
   * 指定した店舗の最新情報を取得する
   *
   * @param gid 最新情報を取得したい店舗のgid
   * @return 指定したgidの店舗の最新情報一覧
   */
  @Override
  public LatestInformation getLatestInformation(final String gid) {
    UriComponentsBuilder uriComponentsBuilder = createRequest(ipocaAPIConfig.getLatestInformation())
      .queryParam("gid", gid)
      .queryParam("output", "json");
    return getEntity(LatestInformation.class, uriComponentsBuilder);
  }
}
