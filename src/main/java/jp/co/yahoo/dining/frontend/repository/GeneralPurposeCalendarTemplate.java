package jp.co.yahoo.dining.frontend.repository;

import jp.co.yahoo.dining.frontend.domain.Holiday;

public interface GeneralPurposeCalendarTemplate {
  Holiday searchHolidays(Integer year);
}
