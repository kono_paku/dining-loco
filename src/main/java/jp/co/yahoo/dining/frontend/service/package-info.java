/**
 * サービス層
 *
 * 業務ロジックはこのレイヤーに記述してください。
 */
package jp.co.yahoo.dining.frontend.service;
