package jp.co.yahoo.dining.frontend.infrastructure.rest;

import jp.co.yahoo.dining.frontend.domain.Video;
import jp.co.yahoo.dining.frontend.repository.VideoTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class VideoTemplateImpl extends LocoRestTemplate implements VideoTemplate {

  @Qualifier("video")
  @Autowired
  public RestTemplate rt;

  @Autowired
  public VideoAPIConfig videoAPIConfig;

  @Override
  public RestTemplate rt() {
    return rt;
  }


  @Override
  public Video getVideo(String cooperationId) {
    UriComponentsBuilder builder = createRequest(videoAPIConfig.getContentId())
      .queryParam("appid", videoAPIConfig.getAppId())
      .queryParam("channel_id", videoAPIConfig.getChannelId())
      .queryParam("output", "json")
      .queryParam("cooperation_id", cooperationId);

    return getEntity(Video.class, builder);
  }

  @Override
  protected HttpHeaders setAuthority(HttpHeaders headers) {
    return headers;
  }
}
