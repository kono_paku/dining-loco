package jp.co.yahoo.dining.frontend.infrastructure.rest;

import jp.co.yahoo.dining.frontend.domain.LocoSecure;
import jp.co.yahoo.dining.frontend.repository.LocoSecureTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class LocoSecureTemplateImpl extends LocoRestTemplate implements LocoSecureTemplate {

  private final RestTemplate rt;
  private final String getYUID;

  public LocoSecureTemplateImpl(@Qualifier("locoSecure") RestTemplate rt, @Value("${extension.api.locoSecure.getYUID}") String getYUID) {
    this.rt = rt;
    this.getYUID = getYUID;
  }

  @Override
  public RestTemplate rt() {
    return rt;
  }

  @Override
  protected HttpHeaders setAuthority(HttpHeaders headers) {
    return headers;
  }

  @Override
  public LocoSecure getEncryptYUID(String guid) {
    UriComponentsBuilder builder =  createRequest(getYUID).queryParam("guid", guid);
    return getEntity(LocoSecure.class, builder);
  }
}
