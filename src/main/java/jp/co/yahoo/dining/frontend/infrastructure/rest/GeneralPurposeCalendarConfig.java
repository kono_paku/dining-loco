package jp.co.yahoo.dining.frontend.infrastructure.rest;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "extension.api.generalPurposeCalendar")
public class GeneralPurposeCalendarConfig {
  private String calendarPath;
  private String sid;
  private String wt;
  private Integer rows;
}
