package jp.co.yahoo.dining.frontend.controller.form;

import jp.co.yahoo.dining.frontend.controller.constraint.CassetteId;
import jp.co.yahoo.dining.frontend.controller.constraint.Dept;
import lombok.Data;

/**
 * Created by jchung on 2017/12/07.
 */
@Data
public class UserInfo {

  private String name;

  @Dept
  private String dept;

  @CassetteId(min = 1000000, max = 2000000)
  private int id;

}
