package jp.co.yahoo.dining.frontend.service;

import jp.co.yahoo.dining.frontend.domain.PlanStock;
import jp.co.yahoo.dining.frontend.repository.PlanStockTemplate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PlanStockService {

  private final PlanStockTemplate planStockTemplate;

  /**
   * 店舗の在庫情報を取得する。
   *
   * @param gid
   * @return PlanStock planStockAPI のレスポンス
   */
  public PlanStock getPlanStock(final String gid) {
    return planStockTemplate.getPlanStocks(gid);
  }
}
