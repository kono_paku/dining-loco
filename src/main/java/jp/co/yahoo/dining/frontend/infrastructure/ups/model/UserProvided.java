package jp.co.yahoo.dining.frontend.infrastructure.ups.model;

import java.util.Map;
import lombok.Data;

/**
 * UPSレスポンス
 */
@Data
public class UserProvided {
  private Map<String, String> credentials;
  private String label;

}

