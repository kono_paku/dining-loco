package jp.co.yahoo.dining.frontend.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JacksonXmlRootElement(localName = "Results")
@XmlAccessorType(XmlAccessType.NONE)
public class AdInfeed implements ExternalEntity {

  @JacksonXmlProperty(localName = "KeywordsRatingSet")
  private KeywordsRatingSet keywordsRatingSet;

  @JacksonXmlProperty(localName = "ResultSet")
  private ResultSet resultSet;

  @Data
  @NoArgsConstructor
  public static class KeywordsRatingSet {
    @JacksonXmlProperty(localName = "keywords", isAttribute = true)
    private String keywords;

    @JacksonXmlProperty(localName = "Market")
    private String market;
  }

  @Data
  @NoArgsConstructor
  public static class ResultSet {
    @JacksonXmlElementWrapper(useWrapping=false)
    @JacksonXmlProperty(localName = "Listing")
    private Listing[] listing;
  }

  @Data
  @NoArgsConstructor
  public static class Listing {
    @JacksonXmlProperty(localName = "rank", isAttribute = true)
    private String rank;
    @JacksonXmlProperty(localName = "title", isAttribute = true)
    private String title;
    @JacksonXmlProperty(localName = "description", isAttribute = true)
    private String description;
    @JacksonXmlProperty(localName = "siteHost", isAttribute = true)
    private String siteHost;
    @JacksonXmlProperty(localName = "ClickUrl")
    private ClickUrl clickUrl;
  }

  @Data
  @NoArgsConstructor
  public static class ClickUrl {
    @JacksonXmlProperty(localName = "type", isAttribute = true)
    private String type;

    @JacksonXmlText
    private String innerText;
  }

}
