package jp.co.yahoo.dining.frontend.infrastructure.rest;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import jp.co.yahoo.dining.frontend.infrastructure.rdsig.RdsigService;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties("extension.restTemplate")
@RequiredArgsConstructor
@Setter
public class RestTemplateConfig {

  private final RequestHeaderInterceptor interceptor;

  // local search API
  private Integer yolpAPIConnectTimeout;
  private Integer yolpAPIReadTimeout;
  private String yolpAPIURL;

  // lifetool API
  private Integer lifetoolAPIConnectTimeout;
  private Integer lifetoolAPIReadTimeout;
  private String lifetoolAPIURL;

  // Video Publisher
  private Integer videoAPIConnectTimeout;
  private Integer videoAPIReadTimeout;
  private String videoAPIURL;

  // Coupon Publisher
  private Integer couponAPIConnectTimeout;
  private Integer couponAPIReadTimeout;
  private String couponAPIURL;

  // MastHead API
  private Integer mastHeadAPIConnectTimeout;
  private Integer mastHeadAPIReadTimeout;
  private String mastHeadAPIURL;

  // Promotion API
  private Integer promotionAPIConnectTimeout;
  private Integer promotionAPIReadTimeout;
  private String promotionAPIURL;

  // IpocaAPI
  private Integer ipocaAPIConnectTimeout;
  private Integer ipocaAPIReadTimeout;
  private String ipocaAPIURL;

  // ImageWizard API
  private Integer imageWizardAPIConnectTimeout;
  private Integer imageWizardAPIReadTimeout;
  private String imageWizardAPIURL;

  // PlanStock API
  private Integer planStockAPIConnectTimeout;
  private Integer planStockAPIReadTimeout;
  private String planStockAPIURL;

  // ImageSearch API
  private Integer imageSearchAPIConnectTimeout;
  private Integer imageSearchAPIReadTimeout;
  private String imageSearchAPIURL;

  // Kuchikomi API
  private Integer kuchikomiAPIConnectTimeout;
  private Integer kuchikomiAPIReadTimeout;
  private String kuchikomiAPIURL;

  // 汎用カレンダーAPI
  private Integer generalPurposeCalendarAPIConnectTimeout;
  private Integer generalPurposeCalendarAPIReadTimeout;
  private String generalPurposeCalendarAPIURL;

  // wedding
  private Integer weddingAPIConnectTimeout;
  private Integer weddingAPIReadTimeout;
  private String weddingAPIURL;

  // LocoSecureAPI(暗号化YUID取得API)
  private Integer locoSecureAPIConnectTimeout;
  private Integer locoSecureAPIReadTimeout;
  private String locoSecureAPIURL;

  // LocoSecureAPI(暗号化YUID取得API)
  private Integer policyAPIConnectTimeout;
  private Integer policyAPIReadTimeout;
  private String policyAPIURL;

  // AdInfeed API
  private Integer adInfeedAPIConnectTimeout;
  private Integer adInfeedAPIReadTimeout;
  private String adInfeedAPIURL;

  // KeepItem API (お気に入りAPI)
  private Integer keepItemAPIConnectTimeout;
  private Integer keepItemAPIReadTimeout;
  private String keepItemAPIURL;

  @Bean(name = "tenpo")
  RestTemplate tenpoRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(yolpAPIConnectTimeout, "yolpAPIConnectTimeout must not be null");
    Assert.notNull(yolpAPIReadTimeout, "yolpAPIReadTimeout must not be null");
    return restTemplateBuilder
        .rootUri(yolpAPIURL)
        .setConnectTimeout(yolpAPIConnectTimeout)
        .setReadTimeout(yolpAPIReadTimeout)
        .interceptors(interceptor).build();
  }

  @Bean(name = "lifetool")
  RestTemplate lifeToolRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(lifetoolAPIConnectTimeout, "lifetoolAPIConnectTimeout must not be null");
    Assert.notNull(lifetoolAPIReadTimeout, "lifetoolAPIReadTimeout must not be null");
    return restTemplateBuilder
        .rootUri(lifetoolAPIURL)
        .setConnectTimeout(lifetoolAPIConnectTimeout)
        .setReadTimeout(lifetoolAPIReadTimeout)
        .interceptors(interceptor).build();
  }

  @Bean(name = "video")
  RestTemplate videoRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(videoAPIConnectTimeout, "videoAPIConnectTimeout must not be null");
    Assert.notNull(videoAPIReadTimeout, "videoAPIReadTimeout must not be null");
    return restTemplateBuilder
      .rootUri(videoAPIURL)
      .setConnectTimeout(videoAPIConnectTimeout)
      .setReadTimeout(videoAPIReadTimeout)
      .interceptors(interceptor).build();
  }

  @Bean(name = "coupon")
  RestTemplate couponRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(couponAPIConnectTimeout, "couponAPIConnectTimeout must not be null");
    Assert.notNull(couponAPIReadTimeout, "couponAPIReadTimeout must not be null");
    return restTemplateBuilder
      .rootUri(couponAPIURL)
      .setConnectTimeout(couponAPIConnectTimeout)
      .setReadTimeout(couponAPIReadTimeout)
      .interceptors(interceptor).build();
  }

  @Bean(name = "mastHead")
  RestTemplate mastHeadRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(mastHeadAPIConnectTimeout, "couponAPIConnectTimeout must not be null");
    Assert.notNull(mastHeadAPIReadTimeout, "couponAPIReadTimeout must not be null");
    return restTemplateBuilder
      .rootUri(mastHeadAPIURL)
      .setConnectTimeout(mastHeadAPIConnectTimeout)
      .setReadTimeout(mastHeadAPIReadTimeout)
      .interceptors(interceptor).build();
  }

  @Bean(name = "promotion")
  RestTemplate promotionRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(promotionAPIConnectTimeout, "promotionAPIConnectTimeout must not be null");
    Assert.notNull(promotionAPIReadTimeout, "promotionAPIReadTimeout must not be null");
    return restTemplateBuilder
      .rootUri(promotionAPIURL)
      .setConnectTimeout(promotionAPIConnectTimeout)
      .setReadTimeout(promotionAPIReadTimeout)
      .interceptors(interceptor).build();
  }

  @Bean(name = "ipoca")
  RestTemplate ipocaRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(ipocaAPIConnectTimeout,
      "ipocaAPIConnectTimeout must not be null");
    Assert.notNull(ipocaAPIReadTimeout, "ipocaAPIReadTimeout must not be null");
    return restTemplateBuilder.rootUri(ipocaAPIURL)
      .setConnectTimeout(ipocaAPIConnectTimeout)
      .setReadTimeout(ipocaAPIReadTimeout).interceptors(interceptor).build();
  }

  @Bean(name = "imageWizard")
  RestTemplate imageWizardRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(imageWizardAPIConnectTimeout, "imageWizardAPIConnectTimeout must not be null");
    Assert.notNull(imageWizardAPIReadTimeout, "imageWizardAPIReadTimeout must not be null");
    return restTemplateBuilder
      .rootUri(imageWizardAPIURL)
      .setConnectTimeout(imageWizardAPIConnectTimeout)
      .setReadTimeout(imageWizardAPIReadTimeout)
      .interceptors(interceptor).build();
  }

  @Bean(name = "planStock")
  RestTemplate planStockRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(planStockAPIConnectTimeout, "planStockAPIConnectTimeout must not be null");
    Assert.notNull(planStockAPIReadTimeout, "planStockAPIReadTimeout must not be null");
    return restTemplateBuilder
      .rootUri(planStockAPIURL)
      .setConnectTimeout(planStockAPIConnectTimeout)
      .setReadTimeout(planStockAPIReadTimeout)
      .interceptors(interceptor).build();
  }

  @Bean(name = "imageSearch")
  RestTemplate imageSearchRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(imageSearchAPIConnectTimeout, "imageSearchAPIConnectTimeout must not be null");
    Assert.notNull(imageSearchAPIReadTimeout, "imageSearchAPIReadTimeout must not be null");
    return restTemplateBuilder
      .rootUri(imageSearchAPIURL)
      .setConnectTimeout(imageSearchAPIConnectTimeout)
      .setReadTimeout(imageSearchAPIReadTimeout)
      .interceptors(interceptor).build();
  }

  @Bean(name = "kuchikomi")
  RestTemplate kuchikomiRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(kuchikomiAPIConnectTimeout, "kuchikomiAPIConnectTimeout must not be null");
    Assert.notNull(kuchikomiAPIReadTimeout, "kuchikomiAPIReadTimeout must not be null");
    return restTemplateBuilder
      .rootUri(kuchikomiAPIURL)
      .setConnectTimeout(kuchikomiAPIConnectTimeout)
      .setReadTimeout(kuchikomiAPIReadTimeout)
      .interceptors(interceptor).build();
  }

  @Bean(name = "wedding")
  RestTemplate weddingRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(weddingAPIConnectTimeout, "weddingAPIConnectTimeout must not be null");
    Assert.notNull(weddingAPIReadTimeout, "weddingAPIReadTimeout must not be null");
    return restTemplateBuilder
      .rootUri(weddingAPIURL)
      .setConnectTimeout(weddingAPIConnectTimeout)
      .setReadTimeout(weddingAPIReadTimeout)
      .interceptors(interceptor).build();
  }

  @Bean(name = "generalPurposeCalendar")
  RestTemplate generalPurposeCalendarRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(generalPurposeCalendarAPIConnectTimeout, "generalPurposeCalendarAPIConnectTimeout must not be null");
    Assert.notNull(generalPurposeCalendarAPIReadTimeout, "generalPurposeCalendarAPIReadTimeout must not be null");
    return restTemplateBuilder
      .rootUri(generalPurposeCalendarAPIURL)
      .setConnectTimeout(generalPurposeCalendarAPIConnectTimeout)
      .setReadTimeout(generalPurposeCalendarAPIReadTimeout)
      .interceptors(interceptor).build();
  }

  @Bean(name = "locoSecure")
  RestTemplate locoSecureRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(locoSecureAPIConnectTimeout, "locoSecureAPIConnectTimeout must not be null");
    Assert.notNull(locoSecureAPIReadTimeout, "locoSecureAPIReadTimeout must not be null");
    return restTemplateBuilder
      .rootUri(locoSecureAPIURL)
      .setConnectTimeout(locoSecureAPIConnectTimeout)
      .setReadTimeout(locoSecureAPIReadTimeout)
      .interceptors(interceptor).build();
  }

  @Bean(name = "policy")
  RestTemplate policyRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(policyAPIConnectTimeout, "policyAPIConnectTimeout must not be null");
    Assert.notNull(policyAPIReadTimeout, "policyAPIReadTimeout must not be null");
    return restTemplateBuilder
      .rootUri(policyAPIURL)
      .setConnectTimeout(policyAPIConnectTimeout)
      .setReadTimeout(policyAPIReadTimeout)
      .interceptors(interceptor).build();
  }

  @Bean(name = "adInfeed")
  RestTemplate adInfeedRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(adInfeedAPIConnectTimeout, "adInfeedAPIConnectTimeout must not be null");
    Assert.notNull(adInfeedAPIReadTimeout, "adInfeedAPIReadTimeout must not be null");

    if(adInfeedAPIURL.equals("http://xml.listing.yahoo.co.jp")) {
      ObjectMapper xmlMapper = new XmlMapper();
      xmlMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
      List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
      messageConverters.add(new MappingJackson2HttpMessageConverter(xmlMapper));
      return restTemplateBuilder
        .messageConverters(messageConverters)
        .rootUri(adInfeedAPIURL)
        .setConnectTimeout(adInfeedAPIConnectTimeout)
        .setReadTimeout(adInfeedAPIReadTimeout)
        .interceptors(interceptor).build();
    }

    return restTemplateBuilder
      .rootUri(adInfeedAPIURL)
      .setConnectTimeout(adInfeedAPIConnectTimeout)
      .setReadTimeout(adInfeedAPIReadTimeout)
      .interceptors(interceptor).build();
  }

  @Bean(name = "keepItem")
  RestTemplate keepItemRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    Assert.notNull(keepItemAPIConnectTimeout, "keepItemAPIConnectTimeout must not be null");
    Assert.notNull(keepItemAPIReadTimeout, "keepItemAPIReadTimeout must not be null");
    return restTemplateBuilder
      .rootUri(keepItemAPIURL)
      .setConnectTimeout(keepItemAPIConnectTimeout)
      .setReadTimeout(keepItemAPIReadTimeout)
      .interceptors(interceptor).build();
  }
}
