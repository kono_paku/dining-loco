package jp.co.yahoo.dining.frontend.controller.api;

import jp.co.yahoo.dining.frontend.controller.constraint.Year;
import jp.co.yahoo.dining.frontend.domain.Holiday;
import jp.co.yahoo.dining.frontend.service.GeneralPurposeCalendarService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@Validated
public class GeneralPurposeCalendarController {

  private final GeneralPurposeCalendarService generalPurposeCalendarService;

  @GetMapping("/v1/api/generalPurposeCalendar")
  public Holiday searchHolidays(@RequestParam("year") @Valid @Year Integer year) {
    return generalPurposeCalendarService.searchHolidays(year);
  }
}
