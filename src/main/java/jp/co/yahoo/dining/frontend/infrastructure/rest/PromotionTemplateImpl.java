package jp.co.yahoo.dining.frontend.infrastructure.rest;

import jp.co.yahoo.dining.frontend.domain.Promotion;
import jp.co.yahoo.dining.frontend.repository.PromotionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class PromotionTemplateImpl extends LocoRestTemplate implements PromotionTemplate {

  @Qualifier("promotion")
  @Autowired
  public RestTemplate rt;

  @Autowired
  public PromotionAPIConfig promotionAPIConfig;

  @Override
  public RestTemplate rt() {
    return rt;
  }

  @Override
  protected HttpHeaders setAuthority(HttpHeaders headers) {
    return headers;
  }

  @Override
  public Promotion getPromotion() {
    UriComponentsBuilder builder = createRequest(promotionAPIConfig.getPromotionList());
    return getEntity(Promotion.class, builder);
  }
}
