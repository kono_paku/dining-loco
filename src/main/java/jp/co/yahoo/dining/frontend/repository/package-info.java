/**
 * データアクセス層
 *
 * インターフェイスを定義してください。
 * 実装は、インフラ層で行ってください。
 */
package jp.co.yahoo.dining.frontend.repository;
