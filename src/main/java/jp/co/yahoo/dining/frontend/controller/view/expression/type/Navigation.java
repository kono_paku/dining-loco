package jp.co.yahoo.dining.frontend.controller.view.expression.type;

/**
 * Created by aksakuma on 2018/04/03.
 */
public enum Navigation {
  NONE(""),
  MENU("menu"),
  PLAN("plan"),
  SERVICE("service");

  private final String type;

  private Navigation(final String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }
}
