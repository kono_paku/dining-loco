package jp.co.yahoo.dining.frontend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Promotion implements ExternalEntity, Serializable {

  @JsonProperty("feature_module")
  private FeatureModule featureModule;

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class FeatureModule {
    @JsonProperty("list")
    private List<PromotionDetail> list;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class PromotionDetail {
    @JsonProperty("publishStart")
    private String publishStart;
    @JsonProperty("publishEnd")
    private String publishEnd;
    @JsonProperty("isolate")
    private List<Isolate> isolateList;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Isolate {
    @JsonProperty("title")
    private String title;
    @JsonProperty("linkUrl")
    private String linkUrl;
    @JsonProperty("imageUrl")
    private String imageUrl;
    @JsonProperty("Text")
    private String text;
  }

}
