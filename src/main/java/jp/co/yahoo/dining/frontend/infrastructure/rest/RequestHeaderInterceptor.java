package jp.co.yahoo.dining.frontend.infrastructure.rest;


import jp.co.yahoo.dining.frontend.controller.LocoRequestHeader;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class RequestHeaderInterceptor implements ClientHttpRequestInterceptor {

  private final LocoRequestHeader locoRequestHeader;

  @Override
  public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
    // リクエスト実行
    long start = System.nanoTime();
    ClientHttpResponse response = execution.execute(request, body);
    long end = System.nanoTime();

    HttpMethod method = request.getMethod();
    String uri = request.getURI().toString();

    log.info("[ " + locoRequestHeader.getTraceId() + "] API リクエスト : [" + method + "] " + uri + " (" + response.getStatusCode() + " - " + (end - start) / 1000000f + "ms)");
    log.info("-> リクエストヘッダー : " + createLogHeaders(request.getHeaders()));

    if (method != HttpMethod.GET) {
      log.info("-> リクエスト" + new String(body, StandardCharsets.UTF_8));
    }

    if (log.isDebugEnabled()) {
      log.debug("--> レスポンスヘッダー : " + response.getHeaders().toString());
      log.debug("--> レスポンスボディー : " + response.getBody().toString());
    }

    // ContentTypeにJSONをセット
    response.getHeaders().setContentType(MediaType.APPLICATION_JSON);

    return response;
  }

  /**
   * ログ出力用のHttpHeadersインスタンスを作成する
   * HttpヘッダにCookieを含む場合は、Cookieのキーのみ設定する
   * @param httpHeaders ログ出力対象のHttpHeaders
   * @return ログ出力用のHttpHeadersインスタンス
   */
  private HttpHeaders createLogHeaders(HttpHeaders httpHeaders) {
    HttpHeaders logHeaders = httpHeaders;

    if(httpHeaders != null && logHeaders.containsKey("Cookie")) {
      // リクエストヘッダにCookieが含まれている場合
      logHeaders = new HttpHeaders();
      for (Map.Entry<String, List<String>> entry : httpHeaders.entrySet()) {
        String key = entry.getKey();
        if(StringUtils.equals(key, "Cookie")) {
          // Cookieの場合は、キーのみにする
          for (String headerValue : entry.getValue()) {
            logHeaders.put(key, getCookieKeys(headerValue));
          }
        } else {
          logHeaders.put(key, entry.getValue());
        }
      }
    }

    return logHeaders;
  }

  /**
   * Cookie値文字列からCookieキーのリストを作成する
   * @param cookieValue Cookie文字列(;区切りの文字列)
   * @return Cookieキーリスト
   */
  private List<String> getCookieKeys(String cookieValue ) {
    return Arrays.stream(cookieValue.split("; "))
      .filter(val -> val.indexOf("=") > 0)
      .map(val -> val.substring(0, val.indexOf("=")))
      .collect(Collectors.toList());
  }
}
