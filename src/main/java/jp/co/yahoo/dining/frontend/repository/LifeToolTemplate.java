package jp.co.yahoo.dining.frontend.repository;

import jp.co.yahoo.dining.frontend.domain.PlanDetail;
import jp.co.yahoo.dining.frontend.domain.PlanList;

import java.util.Optional;

/**
 * ライフツールAPIを操作するクラス
 */
public interface LifeToolTemplate {

  /**
   * 一つのプランの詳細情報API
   * @param tenpoId
   * @param planId
   * @return
   */
  PlanDetail getPlanDetail(String tenpoId, String planId);

  /**
   * 指定した店舗のコース一覧
   * @param gid: 表示店舗のgid
   * @return 掲載期間内のコースリスト
   */
  PlanList getPlanList(String gid);

  /**
   * 指定した店舗のコース一覧
   * @param gid: 表示店舗のgid
   * @param date: 取得したい日付
   * @return 掲載期間内のコースリスト
   */
  PlanList getPlanList(String gid, Optional<String> date);


}
