package jp.co.yahoo.dining.frontend.infrastructure.rest;

import jp.co.yahoo.dining.frontend.domain.Wedding;
import jp.co.yahoo.dining.frontend.repository.WeddingTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class WeddingTemplateImpl extends LocoRestTemplate implements WeddingTemplate {

  public WeddingTemplateImpl(@Qualifier("wedding") RestTemplate rt, WeddingAPIConfig weddingAPIConfig) {
    this.rt = rt;
    this.weddingAPIConfig = weddingAPIConfig;
  }

  private RestTemplate rt;
  private WeddingAPIConfig weddingAPIConfig;

  @Override
  public RestTemplate rt() { return rt; }

  @Override
  public Wedding getWeddingInfo(String gid) {
    UriComponentsBuilder builder = createRequest(weddingAPIConfig.getWeddingInfo())
      .queryParam("appid", weddingAPIConfig.getAppId())
      .queryParam("output", "json")
      .queryParam("gid", gid)
      .queryParam("detail", "full")
      .queryParam("results", 1)
      .queryParam("cid", String.join(",", weddingAPIConfig.getCassetteIds()));
    return getEntity(Wedding.class, builder);
  }

  @Override
  protected HttpHeaders setAuthority(HttpHeaders headers) { return headers; }
}
