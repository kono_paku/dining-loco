package jp.co.yahoo.dining.frontend.controller.api;

import jp.co.yahoo.dining.frontend.controller.error.RequestTimeoutException;
import jp.co.yahoo.dining.frontend.controller.error.UnknownException;
import jp.co.yahoo.dining.frontend.controller.error.ValidationException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientResponseException;

import javax.servlet.http.HttpServletRequest;
import java.net.SocketTimeoutException;
import java.util.Map;

/**
 * RestAPIのエラーハンドラ−
 * Throwされたエラーを横断的にキャッチします。
 */
@Slf4j
@RestControllerAdvice(basePackages = "jp.co.yahoo.dining.frontend.controller.api")
public class RestExceptionHandler {

  /**
   * パラメータバリデーションエラー処理
   *
   * @param exception
   * @return
   */
  @ExceptionHandler
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public void wrongParameter(HttpServletRequest request, ValidationException exception) {
    log.warn("パラメータバリデーションエラー : " + request.getRequestURL(), exception.getMessage());
    throw new ValidationException(ValidationException.ValidationMessage.PARAMETER_VALID_ERROR, exception);
  }

  /**
   * API 処理にタイムアウトが発生した場合
   *
   * @param exception
   * @return
   */
  @ExceptionHandler
  @ResponseStatus(HttpStatus.REQUEST_TIMEOUT)
  public void handleException(HttpServletRequest request, SocketTimeoutException exception) {
    log.error("API タイムアウト発生 : " + request.getRequestURL(), exception.getMessage());
    throw new RequestTimeoutException(exception);
  }

  /**
   * リソースアクセスエラー(例: LocoResourceLoaderを利用している箇所等)
   *
   * @param exception
   * @return
   */
  @ExceptionHandler
  @ResponseStatus(HttpStatus.REQUEST_TIMEOUT)
  public void handleException(HttpServletRequest request, ResourceAccessException exception) {
    log.error("API リクエストに問題発生 : " + request.getRequestURL(), exception.getMessage());
    throw new RequestTimeoutException(exception);
  }

  /**
   * 外部APIへのアクセス時のエラー
   *
   * @param exception
   * @return
   */
  @ExceptionHandler
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public void handleException(HttpServletRequest request, RestClientResponseException exception) {
    if(StringUtils.equals(request.getMethod(), HttpMethod.GET.toString()) && StringUtils.equals(request.getRequestURI(), "/v1/api/keep/get")) {
      // GetKeepItemは暫定的にログレベルwarnで出力し、Exceptionを握りつぶす
      log.warn("API 処理に問題発生 : " + request.getRequestURL() + "(" + exception.getRawStatusCode() + ")", exception.getMessage());
    } else {
      log.error("API 処理に問題発生 : " + request.getRequestURL() + "(" + exception.getRawStatusCode() + ")", exception.getMessage());
      throw new ValidationException(ValidationException.ValidationMessage.HTTP_METHOD_ERROR, exception.getMessage());
    }
  }

  /**
   * API処理に問題があった場合
   *
   * @param exception
   * @return
   */
  @ExceptionHandler
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public void handleException(HttpServletRequest request, HttpClientErrorException exception) {
    log.error("API 処理に問題発生 : " + request.getRequestURL() + "(" + exception.getRawStatusCode() + ")", exception.getMessage());
    throw new ValidationException(ValidationException.ValidationMessage.HTTP_METHOD_ERROR, exception);
  }

  /**
   * サーバに処理エラーが発生した場合
   *
   * @param exception
   * @return
   */
  @ExceptionHandler
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public void serverStatusHandling(HttpServletRequest request, Throwable exception) {
    log.error("サーバーサイドでエラーが発生しました。ロジックの実装ミスの可能性もあります。: " + request.getRequestURL(), exception);
    throw new UnknownException(exception);
  }
}
