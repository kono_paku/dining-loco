package jp.co.yahoo.dining.frontend.infrastructure.rest;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix = "extension.api.promotion")
public class PromotionAPIConfig {
  private String promotionList;
}
