package jp.co.yahoo.dining.frontend.service;

import jp.co.yahoo.dining.frontend.controller.error.ValidationException;
import jp.co.yahoo.dining.frontend.domain.KeepItem;
import jp.co.yahoo.dining.frontend.domain.KeepItemList;
import jp.co.yahoo.dining.frontend.infrastructure.ncookie.NcookieService;
import jp.co.yahoo.dining.frontend.repository.KeepItemTemplate;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class KeepItemService {

  private final KeepItemTemplate keepItemTemplate;
  private final NcookieService ncookieService;

  public KeepItem addItem(String cookie, String device, String gid, String tenpoName, String uid, String lat, String lng) {
    String createBy = getCreateBy(device);
    Optional<Float> latitude = Optional.ofNullable(convertFromStrToFloat(lat));
    Optional<Float> longitude = Optional.ofNullable(convertFromStrToFloat(lng));
    return keepItemTemplate.addItem(cookie, createBy, gid, tenpoName, uid, latitude, longitude);
  }

  public KeepItem removeItem(String cookie, String gid) {
    return keepItemTemplate.removeItem(cookie, gid);
  }

  public KeepItemList getItemList(HttpServletRequest request, String cookie) throws IOException {
    if (ncookieService.validateCookie(request)) return keepItemTemplate.getItemList(cookie);
    throw new ValidationException(ValidationException.ValidationMessage.COOKIE_VALID_ERROR, "Cookie情報に異常があります。");
  }

  private String getCreateBy(String device) {
    return StringUtils.equalsIgnoreCase(device, "smartphone") ? "loco.spweb" : "loco.pcweb";
  }

  private Float convertFromStrToFloat (String floatStr) {
    try {
      return new Float(floatStr);
    } catch (NumberFormatException e) {
      return null;
    }
  }

}
