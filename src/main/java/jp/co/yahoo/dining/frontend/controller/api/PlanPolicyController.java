package jp.co.yahoo.dining.frontend.controller.api;

import jp.co.yahoo.dining.frontend.controller.form.PlanPolicyInfo;
import jp.co.yahoo.dining.frontend.domain.PlanPolicy;
import jp.co.yahoo.dining.frontend.service.PlanPolicyService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@Validated
public class PlanPolicyController {

  private final PlanPolicyService planPolicyService;

  @GetMapping("/v1/api/planPolicy")
  public PlanPolicy getPlanStock(@Valid final PlanPolicyInfo info) {
    return planPolicyService.getPlanPolicy(info);
  }
}
