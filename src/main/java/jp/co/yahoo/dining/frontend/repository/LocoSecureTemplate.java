package jp.co.yahoo.dining.frontend.repository;


import jp.co.yahoo.dining.frontend.domain.LocoSecure;

public interface LocoSecureTemplate {
  /**
   * 一つのプランの詳細情報API
   * @param guid
   * @return LocoSecure
   */
  LocoSecure getEncryptYUID(String guid);
}
