package jp.co.yahoo.dining.frontend;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleCacheErrorHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.SerializationException;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@Configuration
public class ApplicationConfig extends CachingConfigurerSupport {

  private final RedisTemplate<Object, Object> redisTemplate;

  @Bean
  @Override
  public CacheResolver cacheResolver() {
    return null;
  }

  @Bean
  @Override
  public KeyGenerator keyGenerator() {
    return (target, method, params) -> {
      StringBuilder sb = new StringBuilder();
      sb.append(target.getClass().getName());
      sb.append(method.getName());
      for (Object param : params) {
        sb.append(param.toString());
      }
      return sb.toString();
    };
  }

  @Bean
  @Override
  public CacheErrorHandler errorHandler() {
    return new SimpleCacheErrorHandler() {
      @Override
      public void handleCacheGetError(RuntimeException exception, Cache cache, Object key) {
        // キャッシュからのデシリアライズに失敗した場合は、エラーにはせずに対象のメソッドをそのまま呼ぶ（その結果はキャッシュされる）
        if (isSerializationError(exception)) {
          log.warn(cache.getName() + "のデシリアライズに失敗しました。: " + cache.get(key));
          return;
        }
        throw new IllegalStateException(cache.getName() + "のデシリアライズ中に予期せぬエラーが発生しました。: " + cache.get(key));
      }
      private boolean isSerializationError(RuntimeException exception) {
        if (exception instanceof SerializationException) {
          return true;
        }
        Throwable cause = exception.getCause();
        return (cause != null && cause instanceof SerializationException);
      }
    };
  }

  @Bean
  @Override
  public CacheManager cacheManager() {
    RedisCacheManager cacheManager = new RedisCacheManager(redisTemplate);
    cacheManager.setUsePrefix(true);
    Map<String, Long> expires = new HashMap<>();
    expires.put("images", 84600L);
    expires.put("holidays", 2592000L); // 30日
    cacheManager.setExpires(expires);
    cacheManager.setDefaultExpiration(300L);
    cacheManager.afterPropertiesSet();
    return cacheManager;
  }
}
