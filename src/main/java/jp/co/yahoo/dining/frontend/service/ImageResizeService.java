package jp.co.yahoo.dining.frontend.service;

import jp.co.yahoo.dining.frontend.controller.form.ImagesInfo;
import jp.co.yahoo.dining.frontend.domain.ResizedImage;
import jp.co.yahoo.dining.frontend.repository.ImageWizardTemplate;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * 画像変換サービスを提供します。
 */
@RequiredArgsConstructor
@Service
public class ImageResizeService {

  private final ImageWizardTemplate imageWizardTemplate;

  /**
   * ImageWizardの画像変換APIのプロキシを提供します。
   * @param info 変換元画像
   * @return 変換後画像
   */
  @Cacheable(cacheNames = "images")
  public ResizedImage resizedImages(final ImagesInfo info){
    // Cacheを確認
    ResizedImage result = imageWizardTemplate.resizeImages(info);
    // Cacheにセット
    return result;
  }
}
