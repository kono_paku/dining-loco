package jp.co.yahoo.dining.frontend.infrastructure.rest;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.Optional;

import jp.co.yahoo.dining.frontend.domain.PlanDetail;
import jp.co.yahoo.dining.frontend.domain.PlanList;
import jp.co.yahoo.dining.frontend.repository.LifeToolTemplate;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@Slf4j
public class LifeToolTemplateImpl extends LocoRestTemplate implements LifeToolTemplate {

  private final RestTemplate rt;

  private final LifeToolAPIConfig lifeToolAPIConfig;

  private static final String DATEFORMAT = "uuuuMMdd";

  public LifeToolTemplateImpl(@Qualifier("lifetool") RestTemplate rt, LifeToolAPIConfig lifeToolAPIConfig) {
    this.rt = rt;
    this.lifeToolAPIConfig = lifeToolAPIConfig;
  }

  @Override
  public RestTemplate rt() {
    return rt;
  }

  @Override
  protected HttpHeaders setAuthority(HttpHeaders headers) {
    return headers;
  }

  /**
   * 指定した店舗のコース一覧
   * @param gid      : 表示店舗のgid
   * @return 掲載期間内のコースリスト
   */
  @Override
  public PlanList getPlanList(String gid) {
    UriComponentsBuilder builder = createRequest(lifeToolAPIConfig.getPlanList())
      .queryParam("appid", lifeToolAPIConfig.getAppId())
      .queryParam("gid", gid);
    return getEntity(PlanList.class, builder);
  }

  /**
   * 指定した店舗のコース一覧
   *
   * @param gid  : 表示店舗のgid
   * @param date : 取得したい日付
   * @return 掲載期間内のコースリスト
   */
  @Override public PlanList getPlanList(String gid, Optional<String> date) {
    UriComponentsBuilder builder = createRequest(lifeToolAPIConfig.getPlanList())
      .queryParam("appid", lifeToolAPIConfig.getAppId())
      .queryParam("gid", gid);
    date.ifPresent(dateString -> {
      if(validDateTime(dateString)) {
        builder.queryParam("datetime", dateString);
      }
    });
    return getEntity(PlanList.class, builder);
  }

  @Override
  public PlanDetail getPlanDetail(String tenpoId, String planId) {
    UriComponentsBuilder builder = createRequest(lifeToolAPIConfig.getPlanDetail())
        .queryParam("appid", lifeToolAPIConfig.getAppId())
        .queryParam("tenpoid", tenpoId)
        .queryParam("planid", planId);
    return getEntity(PlanDetail.class, builder);
  }

  /**
   * dateパラメータが指定された時のバリデーションチェック
   *
   * @param date 入力されたdateパラメータ
   * @return boolean フォーマットエラー, 過去日付の際にはfalseで返す
   */
  private boolean validDateTime(String date) {
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATEFORMAT)
      .withResolverStyle(ResolverStyle.STRICT);
    try {
      LocalDate dateParam = LocalDate.parse(date, dateTimeFormatter);
      if(LocalDate.now().isAfter(dateParam)) {
        return false;
      }
    } catch (DateTimeParseException dtp){
      log.info("datetimeのフォーマットエラーです");
      return false;
    }
    return true;
  }

}
