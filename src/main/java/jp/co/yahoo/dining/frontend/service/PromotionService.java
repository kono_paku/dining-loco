package jp.co.yahoo.dining.frontend.service;


import jp.co.yahoo.dining.frontend.domain.Promotion;
import jp.co.yahoo.dining.frontend.repository.PromotionTemplate;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PromotionService {

  private final PromotionTemplate promotionTemplate;

  public Promotion getPromotion() {
    return promotionTemplate.getPromotion();
  }

}
