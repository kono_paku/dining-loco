package jp.co.yahoo.dining.frontend.repository;

import java.util.Optional;

import jp.co.yahoo.dining.frontend.domain.Kuchikomi;

public interface KuchikomiTemplate {
  Kuchikomi getKuchikomiList(
    String gid,
    Optional<String> sort,
    Optional<Integer> start,
    Optional<Integer> results,
    Optional<String> guid,
    Optional<String> customexact1
  );
}
