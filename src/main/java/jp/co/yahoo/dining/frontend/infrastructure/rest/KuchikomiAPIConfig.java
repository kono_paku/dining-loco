package jp.co.yahoo.dining.frontend.infrastructure.rest;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "extension.api.kuchikomi")
public class KuchikomiAPIConfig {
  private String kuchikomiList;
  private String appId;
}
