package jp.co.yahoo.dining.frontend.infrastructure.masthead.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * マストヘッド情報
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class YahooHead implements Serializable {
  private String position;
  private String html;
}
