package jp.co.yahoo.dining.frontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableCaching
@SpringBootApplication
@EnableWebMvc
public class DiningLocoTenpoApplication {
  public static void main(String[] args) {
    SpringApplication.run(DiningLocoTenpoApplication.class, args);
  }
}
