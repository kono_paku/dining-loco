package jp.co.yahoo.dining.frontend.repository;

import jp.co.yahoo.dining.frontend.domain.KeepItem;
import jp.co.yahoo.dining.frontend.domain.KeepItemList;

import java.util.Optional;

public interface KeepItemTemplate {

  KeepItem addItem(String cookie, String createBy, String gid, String tenpoName, String uid, Optional<Float> latitude, Optional<Float> longitude);

  KeepItem removeItem(String cookie, String gid);

  KeepItemList getItemList(String cookie);

}
