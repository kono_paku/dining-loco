package jp.co.yahoo.dining.frontend.controller.error;

public abstract class AbstractErrorResponse extends RuntimeException {

  public AbstractErrorResponse(String message) {
    super(message);
  }

  public AbstractErrorResponse(String message, Throwable e) {
    super(message,e);
  }

}
