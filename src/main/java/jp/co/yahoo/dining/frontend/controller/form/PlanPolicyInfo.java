package jp.co.yahoo.dining.frontend.controller.form;

import jp.co.yahoo.dining.frontend.controller.constraint.TenpoId;
import jp.co.yahoo.dining.frontend.infrastructure.Dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlanPolicyInfo implements Dto, Serializable {

  @TenpoId
  private String tenpoId;

  @Pattern(regexp = "[0-9]*")
  private String planId;

  private String datetime;

}
