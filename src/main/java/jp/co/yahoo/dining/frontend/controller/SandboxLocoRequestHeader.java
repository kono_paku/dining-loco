package jp.co.yahoo.dining.frontend.controller;

import java.util.Optional;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import jp.co.yahoo.dining.frontend.infrastructure.ncookie.NcookieService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;


/**
 * Cookie情報をリクエストスコープで保持します。
 */
@Component
@RequestScope
@Profile({"local", "local-prod"})
public class SandboxLocoRequestHeader implements LocoRequestHeader {

  /**
   * トレースID
   */
  private String traceId;
  /**
   * デバイス種別
   */
  private String device;
  /**
   * デバイス種別
   */
  private String yjdDevice;
  /**
   * リクエスト元ドメイン
   */
  private String domain;
  /**
   * N Cookie
   */
  @Value("${extension.testRequest.testCookie}")
  private String nCookie;

  @Value("${extension.testRequest.testGuid}")
  private String guid;

  @Value("${extension.testRequest.testYid}")
  private String yid;

  public SandboxLocoRequestHeader(final HttpServletRequest request) {
    this.traceId = UUID.randomUUID().toString();
    this.domain = Optional.of(request.getServerName()).orElse("localhost");
    this.device = getDeviceType(request.getHeader("User-Agent"));
    this.yjdDevice = "smartphone";
  }

  private String getDeviceType(final String browserInfo) {
    return (StringUtils.isNotEmpty(browserInfo) && StringUtils.contains(browserInfo, "Mobile")) ? "smartphone" : "pc";
  }

  @Override
  public String getTraceId() {
    return this.traceId;
  }

  @Override
  public String getDevice() {
    return this.device;
  }

  @Override
  public String getYJDDevice() {
    return this.yjdDevice;
  }

  @Override
  public String getCookie() {
    return this.nCookie;
  }

  @Override
  public String getDomain() {
    return this.domain;
  }

  @Override
  public String getGuid() {
    return this.guid;
  }

  @Override
  public String getYid() {
    return this.yid;
  }

  @Override
  public String getActualDevice(){
    if (StringUtils.equals(device, "smartphone")) {
      return (StringUtils.equals(yjdDevice, "pc")) ? "pc" : "smartphone";
    } else {
      return "pc";
    }
  }

}
