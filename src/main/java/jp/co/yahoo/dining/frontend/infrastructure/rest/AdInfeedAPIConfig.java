package jp.co.yahoo.dining.frontend.infrastructure.rest;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix = "extension.api.adInfeed")
public class AdInfeedAPIConfig {
  private String ad;
  private String partner;
  private String partnerSp;
  private String serverUrl;
  private String affilData;
  private String did;
  private String didSp;
  private String outputCharEnc;
  private String keywordCharEnc;
  private String type;
  private int maxCount;
}
