package jp.co.yahoo.dining.frontend.controller.view.expression.type;

public enum TelType {
  NON(0),
  ALTERNATIVE_TEL(1),
  PPC_NUMBER(2),
  TEL(3);

  private final int id;

  private TelType(final int id) {
    this.id = id;
  }

  public int getId() {
    return id;
  }
}
