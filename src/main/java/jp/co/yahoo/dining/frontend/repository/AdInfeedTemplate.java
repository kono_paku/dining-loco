package jp.co.yahoo.dining.frontend.repository;

import jp.co.yahoo.dining.frontend.domain.AdInfeed;;

public interface AdInfeedTemplate {

  AdInfeed getAdInfeed(String keyword);

}
