package jp.co.yahoo.dining.frontend.repository;

import jp.co.yahoo.dining.frontend.domain.PlanStock;

public interface PlanStockTemplate {
  PlanStock getPlanStocks(String gid);
}
