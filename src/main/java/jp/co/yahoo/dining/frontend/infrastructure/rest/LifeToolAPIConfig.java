package jp.co.yahoo.dining.frontend.infrastructure.rest;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix = "extension.api.lifetool")
public class LifeToolAPIConfig {
  private String planDetail;
  private String planList;
  private String timeStock;
  private String dailyStock;
  private String appId;
}
