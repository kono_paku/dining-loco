package jp.co.yahoo.dining.frontend.controller;

import jp.co.yahoo.dining.frontend.SpaceId;
import jp.co.yahoo.dining.frontend.controller.view.type.LocoViewName;
import jp.co.yahoo.dining.frontend.domain.PlanDetail;
import jp.co.yahoo.dining.frontend.domain.Tenpo;
import jp.co.yahoo.dining.frontend.domain.UserInfo;
import jp.co.yahoo.dining.frontend.infrastructure.LocoResourceLoader;
import jp.co.yahoo.dining.frontend.infrastructure.masthead.YahooHeadService;
import jp.co.yahoo.dining.frontend.infrastructure.masthead.model.YahooHead;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;

import static jp.co.yahoo.dining.frontend.controller.view.type.LocoViewName.COURSE_DETAIL;
import static jp.co.yahoo.dining.frontend.controller.view.type.LocoViewName.ERROR;
import static jp.co.yahoo.dining.frontend.controller.view.type.LocoViewName.getLocoViewName;

@Component
@RequiredArgsConstructor
public class ControllerIntercepter implements HandlerInterceptor {

  private static final String PC_PATH = "pc/html/";
  private static final String SP_PATH = "sp/html/";

  private final LocoRequestHeader locoRequestHeader;
  private final LocoResourceLoader locoResourceLoader;
  private final YahooHeadService yahooHeadService;
  @Value("${extension.cidList.yahoo}")
  private String YAHOO_DINING_CASSETTE_ID;

  /**
   * コントローラー実行前処理
   */
  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    return true;
  }

  /**
   * コントローラー実行後処理
   */
  @Override
  public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    if (modelAndView == null) {
      return;
    }

    // リダイレクトさせるときの処理
    if(StringUtils.startsWith(modelAndView.getViewName(), "redirect")) {
      String redirectUrl = modelAndView.getViewName();
      // リダイレクト先にmodelを渡す必要はないので削除(削除しないと情報過多で処理が落ちる)
      modelAndView.clear();
      modelAndView.setViewName(redirectUrl);
      return;
    }

    String originalViewName = modelAndView.getViewName();
    String devicePath = this.getDevicePath(locoRequestHeader.getActualDevice());
    modelAndView.setViewName(devicePath + originalViewName);
    modelAndView.addObject("device", locoRequestHeader.getActualDevice());
    modelAndView.addObject("edgeDevice", locoRequestHeader.getDevice());
    LocoViewName locoViewName = getLocoViewName(originalViewName);
    modelAndView.addObject("originalViewName", locoViewName);
    modelAndView.addObject("genrecd", Optional.ofNullable(request.getParameter("genrecd")).orElse(""));
    String spaceId = getErrorSpaceId();
    if (!originalViewName.equals(ERROR.getPath())) {
      PageParam pageParam = getPageParam(locoViewName, modelAndView);
      modelAndView.addObject("pageParam", pageParam);
      modelAndView.addObject("pageParamJson", locoResourceLoader.getJson(pageParam));
      spaceId = getSpaceId(modelAndView);
      modelAndView.addObject("spaceId", spaceId);
    }
    List<YahooHead> yahooHeadList = getMastHeaderData(request, spaceId);
    for (YahooHead headData : yahooHeadList) {
      modelAndView.addObject(headData.getPosition(), headData.getHtml());
    }

    String scType = request.getParameter("sc_type");
    if (StringUtils.isNotEmpty(scType) && NumberUtils.isDigits(scType)) {
      modelAndView.addObject("scType", scType);
    }
  }

  /**
   * エラーページ用のSpaceIdを取得する
   */
  private String getErrorSpaceId() {
    return locoRequestHeader.getActualDevice().equals("smartphone") ? "2080047631" : "2080042987";
  }

  /**
   * マストヘッドデータを取得する
   */
  private List<YahooHead> getMastHeaderData(HttpServletRequest request, final String spaceId) {
    String path = request.getServletPath();
    String cookie = locoRequestHeader.getCookie();
    String device = locoRequestHeader.getActualDevice();
    return yahooHeadService.getYahooHeadList(cookie, spaceId, device, path);
  }

  /**
   * Modelから店舗情報取得
   */
  private Tenpo getTenpo(ModelAndView modelAndView) {
    return (Tenpo) modelAndView.getModelMap().get("tenpo");
  }

  /**
   * Modelからユーザー情報取得
   */
  private UserInfo getUserInfo(ModelAndView modelAndView) {
    return (UserInfo) modelAndView.getModelMap().get("userInfo");
  }

  /**
   * Modelからコース情報取得
   */
  private PlanDetail getPlanDetail(ModelAndView modelAndView) {
    return (PlanDetail) modelAndView.getModelMap().get("course");
  }


  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
  }

  /**
   * ページパラメータ取得
   */
  private PageParam getPageParam(final LocoViewName originalViewName, final ModelAndView modelAndView) {
    Tenpo tenpo = getTenpo(modelAndView);
    UserInfo userInfo = getUserInfo(modelAndView);
    PlanDetail course = getPlanDetail(modelAndView);
    PageParam param = new PageParam();
    param.setService("loco");
    param.setPagetype("detail");
    param.setConttype(getConttype(tenpo, course, originalViewName));
    param.setOpttype(locoRequestHeader.getActualDevice());
    param.setApptype("web");
    param.setStatus(StringUtils.isNotEmpty(locoRequestHeader.getCookie()) ? "login" : "logout");
    param.setCatPath(getCatPath(tenpo));
    param.setPrtnr(getPrtnr(tenpo));
    param.setCorsid(getCorsid(originalViewName, course));
    param.setCtsid(tenpo.getFeature()[0].getGid());
    param.setMetakwd(getMetakwd(tenpo));
    param.setArea(getArea(tenpo));
    param.setGenre(tenpo.getFeature()[0].getProperty().getGenre().length > 0 ? tenpo.getFeature()[0].getProperty().getGenre()[0].getCode() : "");
    param.setUid(tenpo.getFeature()[0].getProperty().getUid());
    param.setCid(tenpo.getFeature()[0].getProperty().getCassetteId());
    param.setLocoarea(getArea(tenpo));
    param.setStation(getStation(tenpo));
    param.setWwd(getWwd(tenpo));
    param.setPremium(getPremium(userInfo));
    param.setIsdining(isDining(tenpo));
    return param;
  }

  /**
   * 画面識別子取得
   */
  private String getConttype(final Tenpo tenpo, PlanDetail course, final LocoViewName originalViewName) {
    switch (originalViewName) {
      case PLACE_DETAIL:
        return "place";
      case COURSE_LIST:
        return "crslst";
      case MENU_DETAIL:
        return "foods";
      case PHOTO_LIST:
        return "picture";
      case REVIEW_LIST:
        return "rvwlist";
      case MARRIAGE_DETAIL:
        return "crehall";
      case WEDDING_DETAIL:
        return "wedding";
      case BRIDAL_DETAIL:
        return "bridal";
      case COUPON_LIST:
        return "cpnlst";
      case MAP_DETAIL:
        return "map";
      case COURSE_DETAIL:
        return "crsdtl";
      case REVIEW_DETAIL:
        return "rvwdtl";
      case PLAN_LIST:
        return "plnlist";
      case ERROR:
      default:
        return "";
    }
  }

  /**
   * メタキーワード取得
   */
  private String getMetakwd(final Tenpo tenpo) {
    StringBuilder searchArea = new StringBuilder();
    for (Tenpo.SearchArea area : tenpo.getFeature()[0].getProperty().getSearchArea()) {
      if (area.getCode().length() <= 8) searchArea.append(area.getName());
    }
    String category = (tenpo.getFeature()[0].getCategory().length > 0) ? "," + tenpo.getFeature()[0].getCategory()[0] : "";
    return tenpo.getFeature()[0].getName() + "," + searchArea.toString() + category;
  }

  /**
   * エリアID取得
   */
  private String getCatPath(final Tenpo tenpo) {
    return (tenpo.getFeature()[0].getProperty().getSearchArea().length > 0) ?
      tenpo.getFeature()[0].getProperty().getSearchArea()[tenpo.getFeature()[0].getProperty().getSearchArea().length - 1].getCode().substring(1, 3)
        + ',' + tenpo.getFeature()[0].getProperty().getSearchArea()[tenpo.getFeature()[0].getProperty().getSearchArea().length - 1].getCode() : "";
  }

  /**
   * エリアID取得
   */
  private String getArea(final Tenpo tenpo) {
    return (tenpo.getFeature()[0].getProperty().getSearchArea().length > 0) ?
      tenpo.getFeature()[0].getProperty().getSearchArea()[tenpo.getFeature()[0].getProperty().getSearchArea().length - 1].getCode() : "";
  }

  /**
   * 駅情報取得
   */
  private String getStation(final Tenpo tenpo) {
    StringJoiner stationArea = new StringJoiner(",");
    if (tenpo.getFeature()[0].getProperty().getStation().length > 0) {
      for (Tenpo.Station station : tenpo.getFeature()[0].getProperty().getStation()) {
        stationArea.add(station.getId());
      }
    }
    return stationArea.toString();
  }

  /**
   * カセット名取得
   */
  private String getPrtnr(final Tenpo tenpo) {
    String cpHeader = "";
    for (Tenpo.Children child : tenpo.getFeature()[0].getChildren()) {
      if (child.getCassetteId().equals(tenpo.getFeature()[0].getProperty().getCassetteId())) {
        cpHeader = child.getCassetteHeader();
        break;
      }
    }
    return cpHeader;
  }

  /**
   * コースID取得
   * コース詳細ページのみ表示する
   */
  private String getCorsid(final LocoViewName originalViewName, final PlanDetail course) {
    return (originalViewName.equals(COURSE_DETAIL.getPath())) ? course.getEntry().getPlans()[0].getPlanId() : "";
  }

  /**
   * 話題度取得
   */
  private String getWwd(final Tenpo tenpo) {
    return (tenpo.getFeature()[0].getProperty().getWebTopic() != null) ? tenpo.getFeature()[0].getProperty().getWebTopic().getScore() : "";
  }

  /**
   * 表示するデバイス用の画面へのパスを取得
   */
  private String getDevicePath(String device) {
    return (StringUtils.equals(device, "smartphone")) ? SP_PATH : PC_PATH;
  }

  /**
   * プレミアム会員かどうか
   */
  private String getPremium(final UserInfo userInfo) {
    if (userInfo != null) {
      return (userInfo.getUserDetail().getIsPremium() != null && userInfo.getUserDetail().getIsPremium()) ? "premium" : "none";
    }
    return "";
  }

  /**
   * ChildrenにダイニングCIDが含まれているかどうか
   */
  private Integer isDining(final Tenpo tenpo) {
    // memo: LSBEのChildrenのCassetteIdがNullで来ることはないはず
    return Arrays.stream(tenpo.getFeature()[0].getChildren()).anyMatch(childTenpo -> childTenpo.getCassetteId().equals(YAHOO_DINING_CASSETTE_ID)) ? 1 : 0;
  }

  /**
   * SpaceIDを取得する。
   * SpaceID情報はJsonで管理する
   */
  private String getSpaceId(final ModelAndView modelAndView) throws IOException {
    Tenpo tenpo = getTenpo(modelAndView);
    String spaceId = "";
    SpaceId spaceIdList = locoResourceLoader.getResource(SpaceId.class, "classpath:json/spaceId/spaceId.json");
    for (SpaceId.Device device : spaceIdList.getDevices()) {
      // デバイス判定
      if (device.getDevice().equals(locoRequestHeader.getActualDevice())) {
        for (SpaceId.Genre genre : device.getGenres()) {
          // ジャンル判定
          String tenpoGenre = (tenpo.getFeature()[0].getProperty().getGenre().length > 1) ? tenpo.getFeature()[0].getProperty().getGenre()[0].getCode().substring(0, 2) : "03";
          if (genre.getCode().equals(tenpoGenre)) {
            for (SpaceId.Prefecture prefectures : genre.getPrefectures()) {
              // 都道府県判定
              String tenpoSearchArea = (tenpo.getFeature()[0].getProperty().getSearchArea().length > 1) ? tenpo.getFeature()[0].getProperty().getSearchArea()[0].getCode().substring(1, 3) : "13";
              if (prefectures.getCode().equals(tenpoSearchArea)) {
                spaceId = prefectures.getSpaceId();
              }
            }
          }
        }
      }
    }
    return spaceId;
  }
}
