package jp.co.yahoo.dining.frontend.infrastructure.masthead;

import jp.co.yahoo.dining.frontend.infrastructure.masthead.model.YahooHead;
import jp.co.yahoo.dining.frontend.infrastructure.masthead.model.YahooHeadWrapper;
import jp.co.yahoo.dining.frontend.repository.MastHeadTemplate;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jchung on 2017/12/05.
 */
@Slf4j
@Service
@AllArgsConstructor
public class YahooHeadService {

  private final MastHeadTemplate mastHeadTemplate;
  public List<YahooHead> getYahooHeadList(String cookie, String spaceId, String device, String done) {
    YahooHeadWrapper mastHead = mastHeadTemplate.getMastHead(cookie, spaceId, device, done);
    return mastHead.getResults();
  }

}
