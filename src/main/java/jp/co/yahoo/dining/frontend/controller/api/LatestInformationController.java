package jp.co.yahoo.dining.frontend.controller.api;

import javax.validation.Valid;

import jp.co.yahoo.dining.frontend.controller.constraint.GId;
import jp.co.yahoo.dining.frontend.domain.LatestInformation;
import jp.co.yahoo.dining.frontend.service.LatestInformationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@Validated
public class LatestInformationController {

  private final LatestInformationService latestInformationService;

  @GetMapping("/v1/api/latestInformation")
  public LatestInformation getLatestInformation(@Valid @GId String gid) {
    return latestInformationService.getLatestInformation(gid);
  }

}
