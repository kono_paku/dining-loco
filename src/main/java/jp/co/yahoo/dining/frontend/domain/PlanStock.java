package jp.co.yahoo.dining.frontend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class PlanStock implements ExternalEntity {
  private Double latency;
  private String requestDate;
  private Integer totalResults;
  private Integer startIndex;
  private Integer itemsPerPage;
  private Entry[] entry;

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Entry {
    private String gid;
    private String tenpoId;
    private String name;
    private Stocks[] stocks;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Stocks {
    private Date date;
    private boolean status;
    private DateTime[] dateTime;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class DateTime {
    private Date time;
    private boolean status;
  }
}
