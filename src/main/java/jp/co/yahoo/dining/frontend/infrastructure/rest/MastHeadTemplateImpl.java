package jp.co.yahoo.dining.frontend.infrastructure.rest;

import jp.co.yahoo.dining.frontend.infrastructure.masthead.model.YahooHeadWrapper;
import jp.co.yahoo.dining.frontend.repository.MastHeadTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class MastHeadTemplateImpl extends LocoRestTemplate implements MastHeadTemplate {

  private static final String DOMAIN = "loco.yahoo.co.jp";

  @Qualifier("mastHead")
  @Autowired
  public RestTemplate rt;

  @Autowired
  public MastHeadAPIConfig mastHeadAPIConfig;

  @Override
  public RestTemplate rt() {
    return rt;
  }

  @Override
  protected HttpHeaders setAuthority(HttpHeaders headers) {
    return headers;
  }

  @Override
  public YahooHeadWrapper getMastHead(String cookie, String spaceId, String device, String done) {
    HttpHeaders headers = new HttpHeaders();
    headers.add("Cookie", cookie);

    UriComponentsBuilder builder = createRequest(mastHeadAPIConfig.getMastHead())
      .queryParam("domain", DOMAIN)
      .queryParam("device_id", device)
      .queryParam("space_id", spaceId)
      .queryParam("done", done);

    return getEntity(YahooHeadWrapper.class, headers, builder);
  }
}
