package jp.co.yahoo.dining.frontend.controller.form;

import jp.co.yahoo.dining.frontend.infrastructure.Dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;

/**
 * 画像変換APIパラメータ
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImagesInfo implements Dto, Serializable {
  @NotNull
  private String gid;
  @Valid
  private List<Image> images;

  @Data
  public static class Image implements Serializable {
    @NotNull
    private String url;
    @NotNull
    private Integer width;
    @NotNull
    private Integer height;
    @Max(100)
    @Min(0)
    private Integer quality;
    @Pattern(regexp = "^s|l|b|x|y$")
    private String priority;
    private Boolean crop = true;
    private Integer cropWidthCenter;
    private Integer cropHeightCenter;
  }
}


