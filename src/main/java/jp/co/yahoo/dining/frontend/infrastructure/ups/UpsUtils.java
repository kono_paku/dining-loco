package jp.co.yahoo.dining.frontend.infrastructure.ups;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import jp.co.yahoo.dining.frontend.infrastructure.ups.model.UserProvided;
import jp.co.yahoo.dining.frontend.infrastructure.ups.model.UserProvidedContainer;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@UtilityClass
public class UpsUtils {

  private static final String UPS_KEY = "VCAP_SERVICES";
  private static final String INSTANCE_IP_KEY = "CF_INSTANCE_IP";
  private static final String LABEL_USER_PROVIDED = "user-provided";
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  {
    OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  public String getUserProvidedValue(String key) {
    try {
      String upsValue = System.getenv(UPS_KEY);
      UserProvidedContainer upsContainer = OBJECT_MAPPER.readValue(upsValue, UserProvidedContainer.class);
      List<UserProvided> userProvideds = upsContainer.getUserProvideds();
      for(UserProvided userProvide : userProvideds){
        if (userProvide.getLabel().equals(LABEL_USER_PROVIDED)) {
          Map<String, String> credentials = userProvide.getCredentials();
          if (credentials.containsKey(key)) {
            return credentials.get(key);
          }
        }
      }
    } catch (IOException e) {
      log.warn("想定されないプロパティが含まれています。 ： " + key, e);
    } catch (Exception e) {
      log.warn("UPSに該当のキーが登録されていません。 ： " + key, e);
    }
    return null;
  }

  public String getInstanceIp() {
    return System.getenv(INSTANCE_IP_KEY);
  }

}
