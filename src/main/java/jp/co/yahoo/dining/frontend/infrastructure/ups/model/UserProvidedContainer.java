package jp.co.yahoo.dining.frontend.infrastructure.ups.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

/**
 * UPSレスポンス
 */
@Data
public class UserProvidedContainer {
  @JsonProperty("user-provided")
  private List<UserProvided> userProvideds;

}
