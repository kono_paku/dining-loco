package jp.co.yahoo.dining.frontend.repository;

import jp.co.yahoo.dining.frontend.domain.Wedding;

/**
 * 式場のデータ取得クラス
 */
public interface WeddingTemplate {
  Wedding getWeddingInfo(String gid);
}
