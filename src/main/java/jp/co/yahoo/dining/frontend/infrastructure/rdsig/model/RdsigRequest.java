package jp.co.yahoo.dining.frontend.infrastructure.rdsig.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RdsigRequest {
  private String dest;
  private String label;
  private Integer sig;
}
