package jp.co.yahoo.dining.frontend.service;

import jp.co.yahoo.dining.frontend.controller.form.ImageSearchInfo;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;

import jp.co.yahoo.dining.frontend.domain.ImageSearch;
import jp.co.yahoo.dining.frontend.repository.ImageSearchTemplate;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ImageSearchService {

  private final ImageSearchTemplate imageSearchTemplate;

  /**
   * ImageSearchAPIからgid指定で画像を取得する
   * @param imageSearchInfo イメージ検索APIのパラメータ
   * @return
   */
  public ImageSearch getImageSearch(final ImageSearchInfo imageSearchInfo) {
    return imageSearchTemplate.getImageSearch(setDefault(imageSearchInfo));
  }

  private ImageSearchInfo setDefault(ImageSearchInfo imageSearchInfo) {
    if (imageSearchInfo.getResults() == null) {
      imageSearchInfo.setResults(10);
    }
    if (imageSearchInfo.getStart() == null) {
      imageSearchInfo.setStart(1);
    }
    return imageSearchInfo;
  }

}
