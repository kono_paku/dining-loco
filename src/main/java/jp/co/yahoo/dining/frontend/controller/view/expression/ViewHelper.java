package jp.co.yahoo.dining.frontend.controller.view.expression;

import com.google.common.collect.ImmutableMap;
import jp.co.yahoo.dining.frontend.controller.view.expression.type.Navigation;
import jp.co.yahoo.dining.frontend.controller.view.expression.type.TelType;
import jp.co.yahoo.dining.frontend.controller.view.type.LocoViewName;
import jp.co.yahoo.dining.frontend.domain.PlanList;
import jp.co.yahoo.dining.frontend.domain.Tenpo;
import jp.co.yahoo.dining.frontend.domain.Tenpo.Detail;
import jp.co.yahoo.dining.frontend.domain.Tenpo.SearchArea;
import jp.co.yahoo.dining.frontend.domain.Wedding;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateUtils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static jp.co.yahoo.dining.frontend.controller.view.type.LocoViewName.*;
import static jp.co.yahoo.dining.frontend.controller.view.expression.type.Navigation.*;

/**
 * 特徴のフラグから名称を返す変換して出す
 * 『』
 */
@Component
public class ViewHelper {

  // Image1000 対策
  private final Integer TENPO_IMAGE_MAX_INDEX = 1000;
  private final String BREAK_PATTERN = "<br>|<br/>|<br />|<BR>|<BR/>|<BR />|\r\n|\n|\r";
  private final int WEDDING_MAX_INDEX = 60; // ゼクシィのフィールドの最大インデックス

  @Value("${extension.cidList.asoview}")
  private String ASOVIEW_CID;

  @Value("${extension.cidList.zexy}")
  private String ZEXY_CID;

  public String concatAddressElement(final Tenpo tenpo, int limit) {
    return Arrays.stream(tenpo.getFeature()[0].getProperty().getAddressElement())
      .limit(limit)
      .map(address -> address.getName())
      .reduce("", (acc, name) -> acc + name);
  }

  public String getBudget(final String budget) {
    if (budget == null) {
      // ヌルポでレンダリングが止まるよりは予算表示に空文字列が出るほうがまし
      return "";
    }

    boolean isNormalBudget = budget.matches("[0-9円\\\\〜~０１２３４５６７８９, 　]+");
    if (isNormalBudget) {
      return budget.contains("\\") ? budget.replace("\\", "¥")
			  : budget.endsWith("円") ? budget : budget + "円";
    } else {
      // 「営業時間外」などの場合はそのまま返す
      return budget;
    }
  }

  public String getFirstObjectOfOthers(final Tenpo tenpo, final String baseKeyName) {
    Map<String, Object> others = tenpo.getFeature()[0].getProperty().getDetail().getOthers();
    return IntStream.rangeClosed(1, TENPO_IMAGE_MAX_INDEX)
      .mapToObj(i -> others.getOrDefault(baseKeyName + i, ""))
      .map(String::valueOf)
      .filter(str -> !str.isEmpty())
      .findFirst().orElse(null);
  }

  public String getTPointRate(final Tenpo tenpo) {
    Map<String, Object> others = tenpo.getFeature()[0].getProperty().getDetail().getOthers();

    String givePointUnitAmount = others.getOrDefault("GivePointUnitAmount", "").toString();
    if (givePointUnitAmount.isEmpty()) {
      return null;
    }

    String givePointRate = others.getOrDefault("GivePointRate", "").toString();
    String givePoint = others.getOrDefault("GivePoint", "").toString();
    if (givePointRate.isEmpty() && givePoint.isEmpty()) {
      return null;
    }
    String tax = others.getOrDefault("Tax", "").toString();

    StringBuilder sb = new StringBuilder();
    sb.append(givePointUnitAmount + "円");
    if (!tax.isEmpty()) {
      sb.append("（" + tax + "）");
    }
    if (!givePointRate.isEmpty()) {
      givePoint = String.valueOf((int) (Float.parseFloat(givePointRate) * 100));
    }
    sb.append("につき" + givePoint + "ポイント");

    return sb.toString();
  }

  public SearchArea spBreadcrumbsArea(final Tenpo tenpo) {
	  List<Predicate<? super SearchArea>> predicates = new ArrayList<>();
    // 小エリア判定
	  predicates.add(area -> !area.getCode().substring(5, 8).equals("000"));
    // 中エリア判定
    predicates.add(area -> (!area.getCode().substring(3, 5).equals("000"))
        && (area.getCode().substring(5, 8).equals("000")));
    // 大エリア判定
    predicates.add(area -> (!area.getCode().substring(0, 2).equals("000"))
            && (area.getCode().substring(3, 8).equals("000000")));

    for (Predicate<? super SearchArea> predicate : predicates) {
      SearchArea area = Arrays.stream(tenpo.getFeature()[0].getProperty().getSearchArea())
			  .filter(predicate)
        .findFirst().orElse(null);
      if (area != null) {
        return area;
      }
    }
    return null;
  }

  /**
   * 電話番号種別取得
   * @param property
   * @return TelType
   */
  public TelType getTelType(final Tenpo.Property property) {
    if (property.getDetail().getAlternativeTel() != null && property.getDetail().getAlternativeTel().length() > 0) {
      return TelType.ALTERNATIVE_TEL;
    }

    if (property.getDetail().getPpcNumber() != null && property.getDetail().getPpcNumber().length() > 0) {
      return TelType.PPC_NUMBER;
    }

    if (property.getTel1() != null && property.getTel1().length() > 0) {
      return TelType.TEL;
    }

    return TelType.NON;
  }

  /**
   * LSBE API のレスポンスに brタグ(BRとかCRLF含む)が含まれる場合に取り除くメソッド
   * th:utext を使えばbrタグも表示できるが、XSS が怖いので th:text を使う
   * 使い方
   * <th:block th:each="text,stat:${#view.explodeBreak(tenpo.Feature[0].Property.Detail.TpointComment1)}">
   * <span th:text="${text}" th:remove="tag">略</span>
   * <br th:if="${!stat.last}" />
   * </th:block>
   * @param dirtyString 改行コードを含む文字列
   * @return 改行コード 箇所で分割した文字列LIST
   */
  public List<String> explodeBreak(final String dirtyString) {
    return Arrays.asList(this.unescapeHtml(dirtyString).split(this.BREAK_PATTERN));
  }

  /**
   * &lt; などのエスケープを元に戻す
   * @param unescapeString
   * @return
   */
  public String unescapeHtml(final String unescapeString) {
    return StringEscapeUtils.unescapeHtml(unescapeString);
  }

  /**
   * 特徴が設定されているかどうか判定
   * @param property tenpo.Feature[].Property
   * @return Boolean 特徴があるかどうか
   */
  public Boolean hasFeature(final Tenpo.Property property) {
    Detail detail = property.getDetail();
    return StringUtils.equals(property.getCreditcardFlag(), "true") // カード可
      || StringUtils.equals(property.getParkingFlag(), "true") // 駐車場あり
      || Boolean.TRUE.equals(property.getCouponFlag()) // クーポンあり
      || StringUtils.equals(detail.getSmoking(), "true") // 喫煙可能
      || Boolean.TRUE.equals(detail.getSmokingFlag()) // 全席喫煙可能
      || Boolean.TRUE.equals(detail.getNonSmokingFlag()) // 全席禁煙
      || Boolean.TRUE.equals(detail.getSeparationSmokingFlag()) // 分煙
      || Boolean.TRUE.equals(detail.getWiFiFreeFlag()) // WiFiあり
      || Boolean.TRUE.equals(detail.getBarrierFreeFlag()) // バリアフリー
      || Boolean.TRUE.equals(detail.getLunchFlag()) // ランチあり
      || Boolean.TRUE.equals(detail.getAllYouCanEatFlag()) // 飲み放題あり
      || Boolean.TRUE.equals(detail.getAllYouCanDrinkFlag()) // 飲み放題あり
      || Boolean.TRUE.equals(detail.getCourseMenuFlag()) // コースメニューあり
      || Boolean.TRUE.equals(detail.getBanquetFlag()) // 宴会・飲み会・パーティー
      || Boolean.TRUE.equals(detail.getFriendsFlag()) // 友人・同僚
      || Boolean.TRUE.equals(detail.getDatingFlag()) // デート
      || Boolean.TRUE.equals(detail.getEntertainFlag()) // 接待
      || Boolean.TRUE.equals(detail.getMatchmakingPartyFlag()) // 合コン
      || Boolean.TRUE.equals(detail.getLadysPartyFlag()) // 女子会
      || Boolean.TRUE.equals(detail.getFamilyFlag()) // ファミリー
      || Boolean.TRUE.equals(detail.getYourselfFlag()) // 一人でも可
      || Boolean.TRUE.equals(detail.getSmallGroupFlag()) // 少人数
      || Boolean.TRUE.equals(detail.getChildFriendlyFlag()) // 子ども連れ可
      || Boolean.TRUE.equals(detail.getPetFlag()) // ペット可
      || Boolean.TRUE.equals(detail.getWineFlag()) // ワインが豊富
      || Boolean.TRUE.equals(detail.getShochuFlag()) // 焼酎が豊富
      || Boolean.TRUE.equals(detail.getRiceWineFlag()) // 日本酒が豊富
      || Boolean.TRUE.equals(detail.getBeerFlag()) // ビールが豊富
      || Boolean.TRUE.equals(detail.getCocktailFlag()) // カクテルが豊富
      || Boolean.TRUE.equals(detail.getPrivateDiningFlag()) // 個室あり
      || Boolean.TRUE.equals(detail.getTakeOutFlag()) // テイクアウト可
      || Boolean.TRUE.equals(detail.getReservedSeatFlag()) // 貸し切り可
      || Boolean.TRUE.equals(detail.getBirthdayFlag()) // 誕生日
      || Boolean.TRUE.equals(detail.getWeddingAnniversaryFlag()) // 結婚記念日
      || Boolean.TRUE.equals(detail.getBuddhistMemorialServiceFlag()) // 慶事 法事
      || Boolean.TRUE.equals(detail.getMemorialDayFlag()) // その他記念日
      || Boolean.TRUE.equals(detail.getNightSceneFlag()) // 夜景が見える
      || Boolean.TRUE.equals(detail.getGoodViewFlag()) // 眺めがいい
      || Boolean.TRUE.equals(detail.getEkichikaFlag()) // 駅チカ
      || Boolean.TRUE.equals(detail.getEkinakaFlag()) // 駅ナカ
      || Boolean.TRUE.equals(detail.getCalmSpaceFlag()) // 落ち着いた空間
      || Boolean.TRUE.equals(detail.getKaraokeFlag()) // エンタメ設備（カラオケ・ライブ）
      || Boolean.TRUE.equals(detail.getProjectorFlag()) // プロジェクタ・スクリーン
      || Boolean.TRUE.equals(detail.getParlorFlag()) // 座敷あり
      || Boolean.TRUE.equals(detail.getOpenTerraceFlag()) // オープンエア
      || Boolean.TRUE.equals(detail.getEnglishSpeakingFlag()) // 英語対応
      || Boolean.TRUE.equals(detail.getTobaccoFlag()) // タバコ販売
      || Boolean.TRUE.equals(detail.getLiquorFlag()) // 酒類販売
    ;
  }
  /**
   * カテゴリが設定されているかどうか判定
   * @param feature tenpo.Feature
   * @return Boolean カテゴリがあるかどうか
   */
  public Boolean hasCategory(final Tenpo.Feature feature) {
    return (feature.getCategory() != null && feature.getCategory().length > 0);
  }

  /**
   * 施設コメントが設定されているかどうか判定
   * getFacilityPhrase で判定すると重いため
   * @memo LSBE API のレスポンスは Facility ではなく Facilty
   * @param detail tenpo.Feature[].Property.Detail
   * @return Boolean 施設コメントがあるかどうか
   */
  public Boolean hasFacility(final Tenpo.Detail detail) {
    // 中身が空のことはあるが、Name1が用意されていれば1000までのどこかにはデータが入稿されている
    return detail.getOthers().containsKey("FaciltyName1");
  }

  /**
   * 中にHTMLタグがある文字列のHTMLタグを削除する
   * @param str
   * @return
   */
  public String removeHTMLTag(final String str) {
    return StringUtils.isEmpty(str) ? str : StringEscapeUtils.unescapeHtml(str).replaceAll("<(/)?([a-zA-Z]*)(\\\\s[a-zA-Z]*=[^>]*)?(\\\\s)*(/)?>", "");
  }

  /**
   * 施設コメントの取得
   * @memo LSBE API のレスポンスは Facility ではなく Facilty
   * @param detail tenpo.Feature[].Property.Detail
   * @return ArrayList<String> 表示する文字列。改行位置で分割
   * <FaciltyName><br><FaciltyComment><br>場所：<FaciltyLocation><br>電話番号：<FaciltyTel><br>営業時間：<FaciltyBusinessHour>
   */
  public ArrayList<String> getFacilityPhrase(final Tenpo.Detail detail) {
    ArrayList<String> facilityPhrase = new ArrayList<String>();
    for (int i = 1; i <= this.TENPO_IMAGE_MAX_INDEX; i++) {
      String facilityName = String.valueOf(detail.getOthers().getOrDefault("FaciltyName" + String.valueOf(i), ""));
      if(facilityName.isEmpty()) {
        // 施設名がないのにその他の情報を出すのはおかしいので continue する
        continue;
      }
      facilityPhrase.add(facilityName);

      String facilityComment = String.valueOf(detail.getOthers().getOrDefault("FaciltyComment" + String.valueOf(i), ""));
      if(!facilityComment.isEmpty()) {
        facilityPhrase.add(facilityComment);
      }

      String facilityLocation = String.valueOf(detail.getOthers().getOrDefault("FaciltyLocation" + String.valueOf(i), ""));
      if(!facilityLocation.isEmpty()) {
        facilityPhrase.add(facilityLocation);
      }

      String facilityTel = String.valueOf(detail.getOthers().getOrDefault("FaciltyTel" + String.valueOf(i), ""));
      if(!facilityTel.isEmpty()) {
        facilityPhrase.add(facilityTel);
      }

      String facilityBusinessHour = String.valueOf(detail.getOthers().getOrDefault("FaciltyBusinessHour" + String.valueOf(i), ""));
      if(!facilityBusinessHour.isEmpty()) {
        facilityPhrase.add(facilityBusinessHour);
      }

      facilityPhrase.add("");
    }

    return facilityPhrase;
  }

  public List<String> getParkingInfo(final Tenpo.Detail detail) {
    List<String> parkingInfo = new ArrayList<String>();

    if (StringUtils.isNotEmpty(detail.getParkingType())) {
      parkingInfo.add(detail.getParkingType());
    }
    if (StringUtils.isNotEmpty(detail.getParkingCapacity())) {
      List<String> parkingCapacity = explodeBreak(detail.getParkingCapacity());
      if (!parkingCapacity.isEmpty()) {
        parkingCapacity.set(0, "駐車場台数/" + parkingCapacity.get(0));
        parkingInfo.addAll(parkingCapacity);
      }
    }
    if (StringUtils.isNotEmpty(detail.getMonthlyParkingCapacity())) {
      parkingInfo.add("月極め駐車場台数/" + detail.getMonthlyParkingCapacity());
    }
    if (StringUtils.isNotEmpty(detail.getMeteredParkingSpace())) {
      parkingInfo.add("時間貸車室数/" + detail.getMeteredParkingSpace());
    }
    if (StringUtils.isNotEmpty(detail.getBikeParkingCapacity1())) {
      parkingInfo.add("駐輪場台数/" + detail.getBikeParkingCapacity1());
    }
    if (StringUtils.isNotEmpty(detail.getMonthlyBikeParkingCapacity1())) {
      parkingInfo.add("月極め駐輪場台数/" + detail.getMonthlyBikeParkingCapacity1());
    }
    if (StringUtils.isNotEmpty(detail.getMeteredBikeParkingSpace1())) {
      parkingInfo.add("時間貸駐輪場台数/" + detail.getMeteredBikeParkingSpace1());
    }
    if (StringUtils.isNotEmpty(detail.getParkingLimitHight1())) {
      parkingInfo.add("駐車制限（高さ）/" + detail.getParkingLimitHight1());
    }
    if (StringUtils.isNotEmpty(detail.getParkingLimitWidth1())) {
      parkingInfo.add("駐車制限（幅）/" + detail.getParkingLimitWidth1());
    }
    if (StringUtils.isNotEmpty(detail.getParkingLimitLength1())) {
      parkingInfo.add("駐車制限（長さ）/" + detail.getParkingLimitLength1());
    }
    if (StringUtils.isNotEmpty(detail.getParkingLimitWeight1())) {
      parkingInfo.add("駐車制限（重さ）/" + detail.getParkingLimitWeight1());
    }
    if (StringUtils.isNotEmpty(detail.getParkingManager1())) {
      parkingInfo.add("駐車場係員/" + detail.getParkingManager1());
    }
    if (StringUtils.isNotEmpty(detail.getParkingFacilityType1())) {
      parkingInfo.add("駐車場施設形態/" + detail.getParkingFacilityType1());
    }
    if (StringUtils.isNotEmpty(detail.getParkingEquipmentType1())) {
      parkingInfo.add("駐車場装置形態/" + detail.getParkingEquipmentType1());
    }
    return parkingInfo;
  }

  /**
   * 画面のタイトル取得
   * @param feature 店舗情報
   * @return タイトル
   */
  public String getTitle(final Tenpo.Feature feature, final LocoViewName viewName) {
    String viewDescription = "";
    String supplementation = "";
    switch(viewName) {
      case PHOTO_LIST:
        viewDescription = "写真：";
        supplementation = "";
        break;
      case REVIEW_LIST:
        viewDescription = "口コミ：";
        supplementation = "";
        break;
      case REVIEW_DETAIL:
        // memo: VueJSでtitleの先頭に『{クチコミのタイトル} ({投稿者名}さん)：』を挿入する
        // memo: VueJSでog:titleの先頭には	『{クチコミのタイトル} ：』のみ挿入する
        viewDescription = "";
        supplementation = "の口コミ";
        break;
      case COURSE_LIST:
        viewDescription = "コース一覧：";
        supplementation = "";
        break;
      case MENU_DETAIL:
        viewDescription = "人気料理・メニュー：";
        supplementation = "";
        break;
      case COUPON_LIST:
        viewDescription = "クーポン一覧：";
        supplementation = "";
        break;
      case MARRIAGE_DETAIL:
        viewDescription = "結婚式場情報：";
        supplementation = "";
        break;
      case WEDDING_DETAIL:
        viewDescription = "挙式プラン一覧：";
        supplementation = "";
        break;
      case BRIDAL_DETAIL:
        viewDescription = "ブライダルフェア一覧：";
        supplementation = "";
        break;
    }
    return viewDescription + feature.getName() + supplementation + getAreaCategory(feature);
  }

  /**
   * Description取得
   * @param feature 店舗情報
   * @return Description
   */
  public String getDescription(final Tenpo.Feature feature, final String device, final LocoViewName viewName) {
    switch(viewName) {
      case PLACE_DETAIL:
        return createDescription(feature, device, "の店舗詳細情報です。", "口コミ、写真、地図など、");
      case PHOTO_LIST:
        return createDescription(feature, device, "の写真一覧。", "口コミ、写真、地図など、");
      case REVIEW_LIST:
        return createDescription(feature, device, "の口コミ・レビュー一覧。", "写真、地図など、");
      case REVIEW_DETAIL:
        return createDescription(feature, device, "の口コミ・レビューです。", "写真、地図など、");
      case COURSE_LIST:
        return createDescription(feature, device, "のお得な人気コース一覧です。", "口コミ、写真、地図など、");
      case MENU_DETAIL:
        return createDescription(feature, device, "のお得な人気料理・メニューの一覧です。", "口コミ、写真、地図など、");
      case COUPON_LIST:
        return createDescription(feature, device, "のお得なクーポンの一覧です。", "口コミ、写真、地図など、");
      case MAP_DETAIL:
        return createDescription(feature, device, "の地図（マップ）とアクセス情報です。", "口コミ、写真など、");
      case MARRIAGE_DETAIL:
        return feature.getName() + getAreaCategory(feature) + "の結婚式場情報です。結婚式会場の写真や挙式・披露宴の感想・口コミも掲載。ウェディング・ブライダルフェアの情報は日本最大級の地域情報サイトYahoo!ロコで！";
      case WEDDING_DETAIL:
        return feature.getName() + getAreaCategory(feature) + "の挙式プラン一覧です。結婚式会場の写真や挙式・披露宴の感想・口コミも掲載。ウェディング・ブライダルフェアの情報は日本最大級の地域情報サイトYahoo!ロコで！";
      case BRIDAL_DETAIL:
        return feature.getName() + getAreaCategory(feature) + "のブライダルフェア一覧です。結婚式会場の写真や挙式・披露宴の感想・口コミも掲載。ウェディング・ブライダルフェアの情報は日本最大級の地域情報サイトYahoo!ロコで！";
      default:
        return "";
    }
  }

  private String createDescription(final Tenpo.Feature feature, final String device, final String viewDescription, final String pageDescription) {
    String onlineReserve = (canOnlineReserve(feature)) ? "ネット予約OK。" : "";
    String recommendFoods = (hasRecommendFoods(feature)) ? "人気メニュー、" : "";
    String coupon = (hasCoupon(feature, device)) ? "クーポン、" : "";
    return  feature.getName() + getAreaCategory(feature) + viewDescription + onlineReserve
      + "施設情報、" + recommendFoods + coupon + pageDescription
      + "グルメ・レストラン情報は日本最大級の地域情報サイトYahoo!ロコで！　周辺のおでかけスポット情報も充実。";
  }
  /**
   * MetaKeywords取得
   * @param feature 店舗情報
   * @return MetaKeywords
   */
  public String getKeywords(final Tenpo.Feature feature, final LocoViewName viewName) {
    String viewDescription = "";
    switch(viewName) {
      case PLACE_DETAIL:
        viewDescription = "";
        break;
      case PHOTO_LIST:
        viewDescription = "写真,";
        break;
      case REVIEW_LIST:
        viewDescription = "口コミ,レビュー,";
        break;
      case REVIEW_DETAIL:
        viewDescription = "口コミ,レビュー,";
        break;
      case COURSE_LIST:
        viewDescription = "コース,メニュー,";
        break;
      case MENU_DETAIL:
        viewDescription = "料理,メニュー,";
        break;
      case COUPON_LIST:
        viewDescription = "クーポン,";
        break;
      case MAP_DETAIL:
        viewDescription = "地図,マップ,アクセス,";
        break;
      case MARRIAGE_DETAIL:
        viewDescription = "結婚式場,ウェディング,ブライダル,";
        break;
      case WEDDING_DETAIL:
        viewDescription = "挙式,プラン,結婚式,ウェディング,ブライダル,";
        break;
      case BRIDAL_DETAIL:
        viewDescription = "ブライダルフェア,結婚式場,ウェディング,挙式,";
        break;
    }
    return viewDescription + feature.getName() + getArea(feature, ",") + getCategory(feature, ",");
  }

  /**
   * Canonical取得
   * @param feature 店舗情報
   * @param viewName 画面のパス
   * @return Canonical
   */
  public String getUrl(final Tenpo.Feature feature, final LocoViewName viewName) {
    String viewPath= "";
    switch(viewName) {
      case PLACE_DETAIL:
        viewPath = "";
        break;
      case PHOTO_LIST:
        viewPath = "photo/";
        break;
      case REVIEW_LIST:
        viewPath = "review/";
        break;
      case REVIEW_DETAIL:
        viewPath = "review/";
        break;
      case COURSE_LIST:
        viewPath = "course/";
        break;
      case MENU_DETAIL:
        viewPath = "menu/";
        break;
      case COUPON_LIST:
        viewPath = "coupon/";
        break;
      case MAP_DETAIL:
        viewPath = "map/";
        break;
      case MARRIAGE_DETAIL:
        viewPath = "marriage/";
        break;
      case WEDDING_DETAIL:
        viewPath = "wedding/";
        break;
      case BRIDAL_DETAIL:
        viewPath = "bridal/";
        break;
    }
    return "https://loco.yahoo.co.jp/place/g-" + feature.getGid() + "/" + viewPath;
  }

  private String getAreaCategory (final Tenpo.Feature feature) {
    String area = getArea(feature, "");
    String category = (getCategory(feature, "/"));
    return (area.length() == 0 && category.length() == 0) ? "" : "(" + area + category + ")";
  }

  private String getArea (final Tenpo.Feature feature, final String prefix) {
    StringBuilder areaBuilder = new StringBuilder();
    if (feature.getProperty().getAddressElement().length > 0) {
      for (Tenpo.AddressElement area : feature.getProperty().getAddressElement()) {
        if (area.getCode().length() <= 8) {
          areaBuilder.append(area.getName());
        }
      }
    }
    return (areaBuilder.toString().length() > 0) ? prefix + areaBuilder.toString() : "";
  }

  private String getCategory (final Tenpo.Feature feature, final String prefix) {
    return (feature.getCategory().length > 0) ? prefix + feature.getCategory()[0] : "";
  }

  public boolean canOnlineReserve (final Tenpo.Feature feature) {
    return (feature.getProperty().getDetail().getOnlineReserveFlag() != null && feature.getProperty().getDetail().getOnlineReserveFlag())
      || (feature.getProperty().getDetail().getOnlineReserveRequestFlag() != null && feature.getProperty().getDetail().getOnlineReserveRequestFlag());
  }

  private boolean hasRecommendFoods (final Tenpo.Feature feature) {
    return feature.getProperty().getDetail().getTopRankItem1() != null || feature.getProperty().getDetail().getRecommendItemName1() != null;
  }

  private boolean hasCoupon (final Tenpo.Feature feature, final String device) {
    if (device.equals("pc")) {
      return (feature.getProperty().getCouponFlag() != null) ? feature.getProperty().getCouponFlag() : false;
    } else {
      return (feature.getProperty().getSmartPhoneCouponFlag() != null) ? feature.getProperty().getSmartPhoneCouponFlag() : false;
    }
  }

  /**
   * こだわり一覧を取得
   * @param detail
   * @return
   */
  public List<String> getParticulars(final Tenpo.Detail detail)
  {
    List<String> particulars = new ArrayList<>();

    addParticular(particulars, detail.getPoolFlag(), "プール");
    addParticular(particulars, detail.getOnsenFlag(), "温泉");
    addParticular(particulars, detail.getRotenburoFlag(), "露天風呂");
    addParticular(particulars, detail.getCharteredBathFlag(), "貸切風呂");
    addParticular(particulars, detail.getKonyokuRotenFlag(), "混浴露天");
    addParticular(particulars, detail.getBigCommonBathFlag(), "大浴場");
    addParticular(particulars, detail.getSaunaFlag(), "サウナ");
    addParticular(particulars, detail.getFreePickUpFlag(), "無料送迎");
    addParticular(particulars, detail.getConvenienceStoreFlag(), "コンビニ");
    addParticular(particulars, detail.getHorigotatsuFlag(), "掘りごたつ席");
    addParticular(particulars, detail.getParlorFlag(), "座敷");
    addParticular(particulars, detail.getTableSeatFlag(), "テーブル席");
    addParticular(particulars, detail.getParkingFlag(), "駐車場あり");
    addParticular(particulars, detail.getBarrierFreeFlag(), "バリアフリー");
    addParticular(particulars, detail.getChildFriendlyFlag(), "お子様連れ可");
    addParticular(particulars, detail.getPetFlag(), "ペット可");
    addParticular(particulars, detail.getAllYouCanEatFlag(), "食べ放題");
    addParticular(particulars, detail.getAllYouCanDrinkFlag(), "飲み放題");
    addParticular(particulars, detail.getAllYouCanEatDessertFlag(), "デザート食べ放題");
    addParticular(particulars, detail.getCalorieDataFlag(), "カロリー表示");
    addParticular(particulars, detail.getBirthdayFlag(), "誕生日対応");
    addParticular(particulars, detail.getBrownBaggingFlag(), "ドリンク持込可");
    addParticular(particulars, detail.getBreakfastMenuFlag(), "朝食あり");
    addParticular(particulars, detail.getLunchFlag(), "ランチあり");
    addParticular(particulars, detail.getVegetarianMenuFlag(), "ベジタリアンメニュー");
    addParticular(particulars, detail.getReligiousMenuFlag(), "国・宗教別メニュー");
    addParticular(particulars, detail.getHypoallergenicMenuFlag(), "低アレルゲンメニュー");
    addParticular(particulars, detail.getLadysPartyFlag(), "女子会向き");
    addParticular(particulars, detail.getDatingFlag(), "デート向き");
    addParticular(particulars, detail.getAfterPartyFlag(), "二次会向き");
    addParticular(particulars, detail.getAnniversaryFlag(), "バースデー・記念日向き");
    addParticular(particulars, detail.getMatchmakingPartyFlag(), "合コン向き");
    addParticular(particulars, detail.getYourselfFlag(), "1人でもOK");
    addParticular(particulars, detail.getFamilyFlag(), "家族向き");
    addParticular(particulars, detail.getLargeGroupFlag(), "大人数OK");
    addParticular(particulars, detail.getEntertainFlag(), "接待・おもてなし向き");
    addParticular(particulars, detail.getConvivialPartyFlag(), "会食向き");
    addParticular(particulars, detail.getSmallGroupFlag(), "少人数OK");
    addParticular(particulars, detail.getLateNightFlag(), "深夜営業");
    addParticular(particulars, detail.getPrivateDiningFlag(), "個室");
    addParticular(particulars, detail.getReservedSeatFlag(), "貸切可");
    addParticular(particulars, detail.getStoreFlag(), "売店あり");
    addParticular(particulars, detail.getRestroomFlag(), "トイレあり");

    return particulars;
  }

  /**
   * フラグのnullチェック
   * @param particulars
   * @param flag
   * @param label
   */
  private void addParticular(List<String> particulars, Boolean flag, String label) {
    if (flag != null && flag) {
      particulars.add(label);
    }
  }

  @Value("${extension.approach.pinWeb}")
  private String approachPinWeb;

  /**
   * アプリがあれば地図アプリ（ピン）
   * アプリがなければ地図ページ
   * に遷移するようなリンク
   *
   * 地図ページに遷移するときはfr=poi_tabをパラメータに付与する
   * これによって遷移時にポップアップで地図ダウンロードを促す
   */
  public String pinWebURL(final Tenpo tenpo, Integer scType) throws UnsupportedEncodingException {
    final String coordinates[] = tenpo.getFeature()[0].getGeometry().getCoordinates().split(",");
    final String longitude = coordinates[0];
    final String latitude = coordinates[1];

    UriComponentsBuilder builder = UriComponentsBuilder.newInstance();
    builder.scheme("https");
    builder.host("loco.yahoo.co.jp");
    builder.path("/place/g-" + tenpo.getFeature()[0].getGid() + "/map");
    builder.queryParam("hlat", latitude);
    builder.queryParam("hlon", longitude);
    builder.queryParam("z", "18");
    builder.queryParam("uid", tenpo.getFeature()[0].getProperty().getUid());
    builder.queryParam("mode", "map");
    builder.queryParam("client", "loco.yahoo.co.jp");
    builder.queryParam("layer", "none");
    builder.queryParam("fr", "poi_tab");
    if (scType != null) {
      builder.queryParam("sc_type", scType);
    }
    
    final String mapUrl = builder.toUriString();

    return UriComponentsBuilder.fromHttpUrl(this.approachPinWeb).queryParam("src", mapUrl).toUriString();
  }

  /**
   *
   */
  public boolean isGourmet(final Tenpo.Genre[] genres) {
    if (genres == null) {
      return false;
    }
    return Arrays.stream(genres)
      .anyMatch(genre -> genre.getCode().startsWith("01"));
  }
  /**
   * spパンくずリストで表示するページタイトル（e.g.: 「メニュー」「写真」）
   * @param servletPath
   * @return
   */
  public String spBreadcrumbsTitle (String servletPath) {
    final String[] paths = servletPath.split("/");
    if (paths.length <= 3) {
      return null;
    }

    Map<String, String> dict = ImmutableMap.<String, String>builder()
      .put("photo", "写真")
      .put("menu", "メニュー")
      .put("course", "コース")
      .put("plan", "プラン")
      .put("marriage", "式場情報")
      .put("wedding", "挙式プラン")
      .put("bridal", "ブライダルフェア")
      .put("review", "クチコミ")
      .put("coupon", "クーポン")
      .put("map", "地図")
      .build();

    return dict.getOrDefault(paths[3], null);
  }

  /**
   * 詳細ページ系かどうか
   */
  public boolean isDetailPage(String servletPath) {
    return servletPath.matches("^/place/g-[^/]+/[^/]+/.+");
  }

  /**
   * 詳細ページの上の階層のパスを返す
   */
  public String nonDetailPage(String servletPath) {
    return servletPath.replaceAll("^/place/g-([^/]+)/([^/]+)/.+", "/place/g-$1/$2");
  }

  public String getCurrentYear() {
    return String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
  }

  /**
   * ジャンルがあるかどうか
   */
  public boolean hasGenre(final Tenpo.Property property) {
    return property.getGenre() != null && property.getGenre().length > 0;
  }

  /**
   * 駅があるかどうか
   */
  public boolean hasStation(final Tenpo.Property property) {
    return property.getStation() != null && property.getStation().length > 0;
  }

  /**
   * 昼予算があるかどうか
   */
  public boolean hasBudgetLunch(final Tenpo.Property property) {
    return property.getDetail().getBudgetLunch() != null && !property.getDetail().getBudgetLunch().isEmpty();
  }

  /**
   * 夜予算があるかどうか
   */
  public boolean hasBudgetDinner(final Tenpo.Property property) {
    return property.getDetail().getBudgetDinner() != null && !property.getDetail().getBudgetDinner().isEmpty();
  }

  /**
   * 定休日があるかどうか
   */
  public boolean hasBusinessHoliday(final Tenpo.Property property) {
    return property.getDetail().getBusinessHoliday() != null && !property.getDetail().getBusinessHoliday().isEmpty();
  }

  /**
   * SP PlaceSummary の情報があるかどうか
   */
  public boolean hasSpPlaceSummary(final Tenpo.Property property) {
    return this.hasGenre(property)
      || this.hasStation(property)
      || this.hasBudgetLunch(property)
      || this.hasBudgetDinner(property)
      || this.hasBusinessHoliday(property);
  }

  /**
   * クレジットカード要素の表示確認
   */
  public boolean isCredit(final Tenpo.Property property) {
    return StringUtils.equals(property.getCreditcardFlag(), "true") &&
      (Boolean.TRUE.equals(property.getDetail().getCreditCardsVISAFlag())
      || Boolean.TRUE.equals(property.getDetail().getCreditCardsMasterFlag())
      || Boolean.TRUE.equals(property.getDetail().getCreditCardsJCBFlag())
      || Boolean.TRUE.equals(property.getDetail().getCreditCardsAMEXFlag())
      || Boolean.TRUE.equals(property.getDetail().getCreditCardsDinersFlag())
      || Boolean.TRUE.equals(property.getDetail().getCreditCardsSaisonFlag())
      || Boolean.TRUE.equals(property.getDetail().getCreditCardsDCFlag())
      || Boolean.TRUE.equals(property.getDetail().getCreditCardsUCFlag())
      || Boolean.TRUE.equals(property.getDetail().getCreditCardsUFJFlag())
      || Boolean.TRUE.equals(property.getDetail().getCreditCardsNICOSFlag())
      || Boolean.TRUE.equals(property.getDetail().getCreditCardsOtherFlag())
      );
  }

  public Navigation getNavigationPriority(String genrecd, boolean isGourmet, boolean isAsoview, boolean isZexy) {
    // パラメータ指定がある場合
    if (genrecd != null) {
      if (genrecd.startsWith("01") && isGourmet) return MENU;
      if (genrecd.startsWith("0414") && isZexy) return SERVICE;
    }
    // パラメータ指定がない場合
    if (isGourmet) return MENU;
    if (isAsoview) return PLAN;
    if (isZexy) return SERVICE;
    return NONE;
  }

  public boolean isGourmet(boolean hasMenu, boolean hasCourse) {
    return hasMenu || hasCourse;
  }

  public boolean hasMenu(Tenpo.Detail detail) {
    boolean result1 = detail.getRecommendItemName1() != null;
    boolean result2 = detail.getTopRankItemId1() != null;
    return result1 || result2;
  }

  public boolean hasCourse(PlanList courseList) {
    if (courseList == null ) return false;
    return courseList.getTotalResults() > 0;
  }

  public boolean isAsoview(Tenpo.Feature feature) {
    return Arrays.stream(feature.getChildren()).filter(children -> children.getCassetteId().equals(ASOVIEW_CID)).count() > 0 && feature.getProperty().getDetail().getItem() != null && feature.getProperty().getDetail().getItem().length > 0;
  }

  public boolean isZexy (Tenpo.Feature feature, Wedding wedding, boolean hasMarriageProperty, boolean hasWeddingProperty, boolean hasBridalProperty) {
    return Arrays.stream(feature.getChildren()).filter(children -> children.getCassetteId().equals(ZEXY_CID)).count() > 0 && hasAnyWeddingLikeProperty(wedding, hasMarriageProperty, hasWeddingProperty, hasBridalProperty);
  }

  private boolean hasAnyWeddingLikeProperty (Wedding wedding, boolean hasMarriageProperty, boolean hasWeddingProperty, boolean hasBridalProperty) {
    if (wedding == null) return false;
    return hasMarriageProperty || hasWeddingProperty || hasBridalProperty;
  }

  // 式場情報がある
  public boolean hasMarriageProperty (Wedding wedding) {
    if (wedding == null) return false;
    Wedding.Detail detail = wedding.getFeature()[0].getProperty().getDetail();
    return detail.getWeddingPartyFoodCharge1() != null
      || detail.getWeddingAfterParty1() != null
      || detail.getWeddingUsePossibleTime() != null
      || detail.getWeddingChargeDesk1() != null
      || detail.getWededingCeremonialStyle1() != null // memo: typoじゃありません。ロコ側に直してもらうべきフィールドです。
      || detail.getWededingCeremonialCapacity() != null // memo: typoじゃありません。ロコ側に直してもらうべきフィールドです。
      || detail.getWeddingFacilitiesComment() != null
      || detail.getWeddingAccommodation() != null
      || detail.getWeddingPickUp() != null
      || detail.getWeddingPayment() != null
      || detail.getWeddingComment1() != null
      || detail.getWeddingBusinessHour() != null
      || detail.getWeddingReferenceComment() != null
      || detail.getWeddingReservation() != null
      || detail.getWeddingParking() != null;
  }

  // 挙式プランに関する情報がある
  public boolean hasWeddingProperty(Wedding wedding) throws NoSuchFieldException {
    if (wedding == null) return false;
    Map<String, Object> others = wedding.getFeature()[0].getProperty().getDetail().getOthers();
    boolean hasWeddingProperty = false;
    for (int i = 1; i <= WEDDING_MAX_INDEX; i++) {
      if (others.get("WeddingPlanName" + i) != null || others.get("WeddingPlanUrl" + i) != null) {
        hasWeddingProperty = true;
        break;
      }
    }
    return hasWeddingProperty;
  }

  // ブライダルフェアに関する情報がある
  public boolean hasBridalProperty (Wedding wedding) throws NoSuchFieldException {
    if (wedding == null) return false;
    Map<String, Object> others = wedding.getFeature()[0].getProperty().getDetail().getOthers();
    boolean hasBridalProperty = false;
    for (int i = 1; i <= WEDDING_MAX_INDEX; i++) {
      if (others.get("WeddingFairName" + i) != null || others.get("WeddingFairDetailUrl" + i) != null || others.get("WeddingFairReservationPcUrl" + i) != null) {
        hasBridalProperty = true;
        break;
      }
    }
    return hasBridalProperty;
  }

  public String getMenuText (LocoViewName locoViewName, Navigation navigationPriority) {
    if (locoViewName == MENU_DETAIL || locoViewName == COURSE_LIST || locoViewName == COURSE_DETAIL) {
      return "メニュー";
    } else if (locoViewName == PLAN_LIST) {
      return "プラン";
    } else if (locoViewName == WEDDING_DETAIL || locoViewName == MARRIAGE_DETAIL || locoViewName == BRIDAL_DETAIL) {
      return "サービス、他";
    }
    if (navigationPriority == MENU) {
      return "メニュー";
    } else if (navigationPriority == PLAN) {
      return "プラン";
    } else if (navigationPriority == SERVICE) {
      return "サービス、他";
    } else {
      return "メニュー";
    }
  }

  public String getMenuLink(Tenpo.Feature feature, Navigation navigationPriority, boolean hasCourse, boolean hasMarriageProperty, boolean hasWeddingProperty) {
    StringJoiner url = new StringJoiner("/");
    url.add("/place/g-" + feature.getGid());
    if (navigationPriority == MENU) {
      // gourmet
      String path = hasCourse ? "course/" : "menu/";
      url.add(path);
    } else if (navigationPriority == PLAN) {
      // asoview
      url.add("plan/");
    } else if (navigationPriority == SERVICE) {
      // zexy
      String path = hasMarriageProperty ? "marriage/" : hasWeddingProperty ? "wedding/" : "bridal/";
      url.add(path);
    }
    return url.toString();
  }

  public List<SubNavigation> getMenuSubNavigations(Tenpo.Feature feature, Navigation navigationPriority, boolean hasMenu, boolean hasCourse, boolean hasMarriageProperty, boolean hasWeddingProperty, boolean hasBridalProperty) {
    List<SubNavigation> subNavigations = new ArrayList<SubNavigation>();
    if (navigationPriority == MENU) {
      // gourmet
      if (hasCourse) {
        subNavigations.add(new SubNavigation("コース", "/place/g-" + feature.getGid() + "/course/"));
      }
      if (hasMenu) {
        subNavigations.add(new SubNavigation("料理", "/place/g-" + feature.getGid() + "/menu/"));
      }
    } else if (navigationPriority == PLAN) {
      // asoview
    } else if (navigationPriority == SERVICE) {
      // wedding
      if (hasMarriageProperty) {
        subNavigations.add(new SubNavigation("式場情報", "/place/g-" + feature.getGid() + "/marriage/"));
      }
      if (hasWeddingProperty) {
        subNavigations.add(new SubNavigation("挙式プラン", "/place/g-" + feature.getGid() + "/wedding/"));
      }
      if (hasBridalProperty) {
        subNavigations.add(new SubNavigation("ブライダルフェア", "/place/g-" + feature.getGid() + "/bridal/"));
      }
    }
    return subNavigations;
  }

  public String getMenuType(LocoViewName locoViewName, boolean isGourmet, boolean isAsoview, boolean isZexy) {
    switch (locoViewName) {
      case MENU_DETAIL:
      case COURSE_LIST:
      case PLAN_LIST:
      case COURSE_DETAIL:
      case BRIDAL_DETAIL:
      case WEDDING_DETAIL:
      case MARRIAGE_DETAIL:
        return "active";
      default: // 何もしない
    }
    if (isGourmet || isAsoview || isZexy) {
      return "enable";
    }
    return "disable";
  }

  /**
   * 店舗ナビのULT
   */
  public String getMenuLinkUlt(String menuLink) {
    if (menuLink.contains("menu") || menuLink.contains("course")) {
      return "slk:menu;pos:0";
    }
    if (menuLink.contains("plan")) {
      return "slk:plan;pos:0";
    }
    if (menuLink.contains("marriage") || menuLink.contains("wedding") || menuLink.contains("bridal")) {
      return "slk:service;pos:0";
    }
    return "";
  }

  /**
   * 店舗ナビのULT
   */
  public String getSubMenuLinkUlt(String menuLink) {
    if (menuLink.equals("menu")) {
      return "slk:dish;pos:0";
    }
    if (menuLink.equals("course")) {
      return "slk:corse;pos:0";
    }
    if (menuLink.equals("marriage")) {
      return "slk:crehall;pos:0";
    }
    if (menuLink.equals("wedding")) {
      return "slk:wedding;pos:0";
    }
    if (menuLink.equals("bridal")) {
      return "slk:bridal;pos:0";
    }
    return "";
  }

  /**
   * SPの店舗ナビで利用
   */
  public String getCouponProperty(Tenpo.Feature feature, LocoViewName locoViewName) {
    Tenpo.Coupon[] coupons = feature.getProperty().getCoupon();
    if (coupons.length == 0)
      return "";
    if (locoViewName == COUPON_LIST)
      return "current";
    for (Tenpo.Coupon coupon : coupons) {
      if (isValidCoupon(coupon)) {
        return "enable";
      }
    }
    return "disable";
  }

  private boolean isValidCoupon(Tenpo.Coupon coupon) {
    if (coupon == null) return false;
    if (coupon.getDisplayFlag() != null && !coupon.getDisplayFlag()) return false;
    if (coupon.getEndDay() == null) return true;
    return availableCoupon(coupon.getEndDay(), "yyyy/M/d") || availableCoupon(coupon.getEndDay(), "yyyyMMdd");
  }

  private boolean availableCoupon(final String endDay, final String format) {
    DateFormat dateFormat = new SimpleDateFormat(format);
    dateFormat.setLenient(false);
    try {
      Date formatEndDay = dateFormat.parse(endDay);
      return LocalDateTime.now().isBefore(LocalDateTime
        .ofInstant(formatEndDay.toInstant(), ZoneId.systemDefault()).with(LocalTime.MAX));
    } catch (ParseException pe) {
      return false;
    }
  }

  public List<MenuProperty> getMenuProperty(Tenpo.Feature feature, LocoViewName locoViewName, Navigation navigationPriority, boolean hasMenu, boolean hasCourse, boolean hasMarriageProperty, boolean hasWeddingProperty, boolean hasBridalProperty) {
    List<MenuProperty> menuProperties = new ArrayList<MenuProperty>();
    switch (locoViewName){
      case MENU_DETAIL:
      case COURSE_LIST:
      case COURSE_DETAIL:
        menuProperties.add(new MenuProperty("current", "メニュー", ""));
        return menuProperties;
      case PLAN_LIST:
        menuProperties.add(new MenuProperty("current", "プラン", ""));
        return menuProperties;
      case MARRIAGE_DETAIL:
      case WEDDING_DETAIL:
      case BRIDAL_DETAIL:
        menuProperties.add(new MenuProperty("current", "サービス、他", ""));
        return menuProperties;
      default:
    }
    // ---- enable系 ----
    if (navigationPriority == MENU) {
      // enable - gourmet
      menuProperties.add(new MenuProperty("enable", "メニュー", hasCourse ? "course" : "menu"));
      return menuProperties;
    } else if (navigationPriority == PLAN) {
      // enable - asoview
      menuProperties.add(new MenuProperty("enable", "プラン", "plan"));
      return menuProperties;
    } else if (navigationPriority == SERVICE) {
      // enable - zexy
      String type = "enable";
      String word = "サービス、他";
      String path = hasMarriageProperty ? "marriage" : hasWeddingProperty ? "wedding" : "bridal";
      menuProperties.add(new MenuProperty(type, word, path));
      return menuProperties;
    } else {
      // disable
      menuProperties.add(new MenuProperty("disable", "メニュー", ""));
      return menuProperties;
    }
  }

  /**
   * ApproachのURLを取得する
   * @param feature tenpo.feature
   * @param scType URLパラメータに設定されているsc_typeの値
   * @return ApproachのURL
   * @throws UnsupportedEncodingException
   */
  public String getApproachUrl (Tenpo.Feature feature, Integer scType) throws UnsupportedEncodingException {
    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("https://loco.yahoo.co.jp/place/g-" + feature.getGid() + "/map/");
    String lat;
    String lon;
    if (feature.getGeometry() != null && feature.getGeometry().getCoordinates() != null) {
      String[] coordinates = feature.getGeometry().getCoordinates().split(",");
      lat = coordinates[1];
      lon = coordinates[0];
    } else {
      lat = null;
      lon = null;
    }
    builder.queryParam("hlat", lat);
    builder.queryParam("hlon", lon);
    builder.queryParam("uid", feature.getProperty().getUid());
    builder.queryParam("z", 18);
    builder.queryParam("client", "loco.yahoo.co.jp");
    builder.queryParam("fr", "poi_tab");
    builder.queryParam("mode", "map");
    builder.queryParam("layer", "none");
    if (scType != null) {
      builder.queryParam("sc_type", scType);
    }
    return "https://approach.yahoo.co.jp/r/HiQSGM?src=" + URLEncoder.encode(builder.toUriString(), "UTF-8").replace("+", "%20");
  }

  /**
   * PriceRangeの文字列を取得する
   * 金額は、「￥」記号 + 3桁区切りでフォーマットする
   * 金額(FROM/TO)が未設定か数値に変換できない場合は空文字を返す
   * @param feature 店舗のfeature
   * @return PriceRangeの文字列
   */
  public String getPriceRange(final Tenpo.Feature feature) {
    String priceRange = "";
    try {
      String priceFrom = feature.getProperty().getDetail().getPriceFrom1();
      String priceTo = feature.getProperty().getDetail().getPriceTo1();

      // 値に文字「円」が含まれる場合があるため、「円」を取り除く
      if(StringUtils.contains(priceFrom, "円")) {
        priceFrom = priceFrom.replace("円", "");
      }
      if(StringUtils.contains(priceTo, "円")) {
        priceTo = priceTo.replace("円", "");
      }

      if (StringUtils.isNotEmpty(priceFrom) && NumberUtils.isNumber(priceFrom) && StringUtils.isNotEmpty(priceTo) && NumberUtils.isNumber(priceTo)) {
        priceRange = String.format("￥%1$,3d", Integer.parseInt(priceFrom)) + "〜" + String.format("￥%1$,3d", Integer.parseInt(priceTo));
      }
    } catch (NumberFormatException ex) {
      // 例外発生時には空文字を返す
    }

    return priceRange;
  }

  /**
   * StreetAddress(都道府県名、市区町村名を除いた住所)を取得する
   * @param feature 店舗のfeature
   * @return StreetAddress
   */
  public String getStreetAddress(final Tenpo.Feature feature) {
    String streetAddress = "";

    String addressRegion = ""; // 都道府県名
    String addressLocality = ""; // 市区町村名
    for(Tenpo.AddressElement addressElement : feature.getProperty().getAddressElement()) {
      switch (addressElement.getLevel()) {
        case "prefecture" :
          addressRegion = addressElement.getName();
          break;
        case "city" :
          addressLocality = addressElement.getName();
          break;
      }
    }
    String address = feature.getProperty().getAddress();
    streetAddress = address.replaceFirst(addressRegion + addressLocality, "");

    return streetAddress;
  }

  /**
   * DOMのidとURLパラメータの「sc_type」の値から要素を非表示にするか確認する
   * @param id DOMのid
   * @param scType scTypeの値
   * @return 非表示の場合はtrue、表示の場合はfalse
   */
  public boolean isHiddenScType(String id, Integer scType) {
    boolean isHidden = false;

    if (scType == null || StringUtils.isEmpty(id)) {
      return isHidden;
    }

    if (scType == 1 && StringUtils.equals(id, "yjAdsJs")) {
      isHidden = true;
    }

    return isHidden;
  }

  @Data
  @AllArgsConstructor
  public static class SubNavigation {
    private String title;
    private String url;
  }

  @Data
  @AllArgsConstructor
  public static class MenuProperty {
    private String type;
    private String word;
    private String path;
  }

}
