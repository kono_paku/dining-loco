package jp.co.yahoo.dining.frontend.config;

import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile({"local", "local-prod"})
@EnableZuulProxy
@Configuration
public class DevConfig {

}
