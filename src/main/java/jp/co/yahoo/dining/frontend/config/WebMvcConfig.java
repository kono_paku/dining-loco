package jp.co.yahoo.dining.frontend.config;

import jp.co.yahoo.dining.frontend.controller.AccessLogInterceptor;
import jp.co.yahoo.dining.frontend.controller.ControllerIntercepter;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@AllArgsConstructor
public class WebMvcConfig extends WebMvcConfigurerAdapter {

  private final AccessLogInterceptor accessLogInterceptor;

  private final ControllerIntercepter controllerIntercepter;


  /**
   * リソースアクセスパスを設定。
   * 注意：設定したリソースパスの中で404エラーが発生した場合、エラーハンドリングをしてない。
   *
   * @param registry
   */
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/dist/**")
      .addResourceLocations("classpath:/webapp/public/dist/");
    registry.addResourceHandler("favicon.ico")
      .addResourceLocations("classpath:/webapp/public/dist/favicon.ico");
    // 今後リソースのアクセスが必要だった場合に入れてください
  }


  /**
   * Interceptorを登録
   * 1. デバイス判定してViewパスを切替
   * 2. アクセスログ
   *
   * @param registry
   */
  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    // デバイス判定してViewパスを切替
    registry.addInterceptor(controllerIntercepter).excludePathPatterns("/api/**");
    // アクセスログ
    registry.addInterceptor(accessLogInterceptor).addPathPatterns("/place/**").addPathPatterns("/v1/**");
  }

}
