package jp.co.yahoo.dining.frontend.controller.error;


/**
 * ロコ用のExceptionクラス
 */
public class ValidationException extends AbstractErrorResponse {

  public ValidationException(String errorType, String msg) {
    super(errorType + " : " + msg);
  }

  public ValidationException(String errorType, Throwable e) {
    super(errorType + " : " + e.getMessage(), e);
  }

  public interface ValidationMessage {
    String PARAMETER_VALID_ERROR = "パラメーターにエラーが発生します";
    String HTTP_METHOD_ERROR = "このMethodは支援してないです";
    String COOKIE_VALID_ERROR = "Y/T/NCookieが不正です";
  }
}
