package jp.co.yahoo.dining.frontend.service;

import jp.co.yahoo.dining.frontend.controller.form.PlanPolicyInfo;
import jp.co.yahoo.dining.frontend.domain.PlanPolicy;
import jp.co.yahoo.dining.frontend.repository.PolicyTemplate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PlanPolicyService {

  private final PolicyTemplate policyTemplate;

  public PlanPolicy getPlanPolicy(final PlanPolicyInfo info) {
    Optional<String> planId = Optional.ofNullable(info.getPlanId());
    Optional<String> datetime = Optional.ofNullable(info.getDatetime());
    return policyTemplate.getPolicyList(info.getTenpoId(), planId, datetime);
  }
}
