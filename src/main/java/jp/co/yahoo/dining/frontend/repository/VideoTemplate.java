package jp.co.yahoo.dining.frontend.repository;

import jp.co.yahoo.dining.frontend.domain.Video;

/**
 * 動画のデータ取得クラス
 */
public interface VideoTemplate {

  Video getVideo(String cooperationId);

}
