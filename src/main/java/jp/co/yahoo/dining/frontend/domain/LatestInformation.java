package jp.co.yahoo.dining.frontend.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class LatestInformation implements ExternalEntity, Serializable {

  @JsonProperty("ResultSet")
  private ResultSet resultSet;

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class ResultSet {
    @JsonProperty("Result")
    private Result[] result;
    private int totalResultsAvailable;
    private int firstResultPosition;
    private int totalResultsReturned;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Result {
    private String itemId;
    private String gid;
    private String uid;
    private String shopName;
    private String parentGid;
    private String parentUid;
    private String parentName;
    private String title;
    private String label;
    private String content;
    private String defaultPrice;
    private String salePrice;
    private String periodStart;
    private String periodEnd;
    private String categoryName;
    private String nearlyUrl;
    private String pickId;
    private String price;
    private String taxFlag;
    private String makerAndSize;
    private String labelComment;
    private String itemUrl;
    private String timeSaleStart;
    private String timeSaleEnd;
    private ImageList imageList;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class ImageList {
    private String[] Image;
  }
}
