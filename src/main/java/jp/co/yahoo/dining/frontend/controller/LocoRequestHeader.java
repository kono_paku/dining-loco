package jp.co.yahoo.dining.frontend.controller;

public interface LocoRequestHeader {

  String getTraceId();

  String getDevice();

  String getYJDDevice();

  String getCookie();

  String getDomain();

  String getGuid();

  String getYid();

  String getActualDevice();

}
