package jp.co.yahoo.dining.frontend.service;

import jp.co.yahoo.dining.frontend.domain.Wedding;
import jp.co.yahoo.dining.frontend.repository.WeddingTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WeddingService {
  @Autowired
  WeddingTemplate weddingTemplate;

  public Wedding getWeddingInfo(String gid) {
    return weddingTemplate.getWeddingInfo(gid);
  }
}
