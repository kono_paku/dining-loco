package jp.co.yahoo.dining.frontend.service;

import jp.co.yahoo.dining.frontend.domain.LocoSecure;
import jp.co.yahoo.dining.frontend.repository.LocoSecureTemplate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LocoSecureService {
  private final LocoSecureTemplate locoSecureTemplate;

  public LocoSecure getEncryptYUID(final String guid){
    return locoSecureTemplate.getEncryptYUID(guid);
  }
}
