package jp.co.yahoo.dining.frontend.infrastructure.eappid;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.net.URI;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Created by jchung on 2017/12/06.
 */
@Slf4j
@Service
public class EappIdService {

  // TODO : miniy環境にはホスト設定が必要ですが、PCF環境でホスト設定ができるかどうか確認必要
  private static final String EAPPID_URL = "http://developer.yahooapis.jp/v1/eappid";

  @Value("${extension.eappid.appid}")
  private String appId;

  private final RestTemplate restTemplate;

  public EappIdService() {
    this.restTemplate = new RestTemplateBuilder()
        .build();
  }

  @Cacheable("eappid")
  public String getEappId() {
    URI requestUri = buildRequestUrl();
    try {
      EappIdWrapper eappIdWapper = restTemplate.getForObject(requestUri, EappIdWrapper.class);
      if (eappIdWapper != null && eappIdWapper.getEAppId() != null) {
        return eappIdWapper.getEAppId();
      }
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    }
    throw new RuntimeException("EappID取得に失敗しました。");
  }

  private URI buildRequestUrl() {
    return UriComponentsBuilder.fromHttpUrl(EAPPID_URL)
        .queryParam("appid", appId)
        .queryParam("output", "json")
        .build(true)
        .toUri();
  }

  @Data
  public static class EappIdWrapper implements Serializable {

    @JsonProperty("eappid")
    private String eAppId;
  }

}
