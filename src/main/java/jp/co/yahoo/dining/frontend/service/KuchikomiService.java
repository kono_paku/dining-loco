package jp.co.yahoo.dining.frontend.service;

import jp.co.yahoo.dining.frontend.domain.Kuchikomi;
import jp.co.yahoo.dining.frontend.repository.KuchikomiTemplate;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class KuchikomiService {
  private final KuchikomiTemplate kuchikomiTemplate;

  public Kuchikomi getKuchikomiList(
    final String gid,
    final Optional<String> sort,
    final Optional<Integer> start,
    final Optional<Integer> results,
    final Optional<String> guid,
    final Optional<String> customexact1
  ) {
    return kuchikomiTemplate.getKuchikomiList(gid, sort, start, results, guid, customexact1);
  }
}
