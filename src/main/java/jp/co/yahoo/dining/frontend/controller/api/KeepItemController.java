package jp.co.yahoo.dining.frontend.controller.api;

import jp.co.yahoo.dining.frontend.controller.LocoRequestHeader;
import jp.co.yahoo.dining.frontend.controller.constraint.GId;
import jp.co.yahoo.dining.frontend.domain.KeepItem;
import jp.co.yahoo.dining.frontend.domain.KeepItemList;
import jp.co.yahoo.dining.frontend.service.KeepItemService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientResponseException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;

@RestController
@AllArgsConstructor
@Validated
public class KeepItemController {

  private final KeepItemService keepItemService;

  private final LocoRequestHeader header;

  @GetMapping("/v1/api/keep/add")
  public KeepItem addKeepItem(@Valid @GId final String gid, final String tenpoName, final String uid, final String lat, final String lng) {
    String cookie = header.getCookie();
    String device = header.getDevice();
    return keepItemService.addItem(cookie, device, gid, tenpoName, uid, lat, lng);
  }

  @GetMapping("/v1/api/keep/remove")
  public KeepItem removeKeepItem(@Valid @GId final String gid) {
    String cookie = header.getCookie();
    return keepItemService.removeItem(cookie, gid);
  }

  @GetMapping("/v1/api/keep/get")
  public KeepItemList getKeepItemList(HttpServletRequest request) throws IOException {
    String cookie = header.getCookie();
    return keepItemService.getItemList(request, cookie);
  }
}
