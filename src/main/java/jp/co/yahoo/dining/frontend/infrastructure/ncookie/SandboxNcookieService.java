package jp.co.yahoo.dining.frontend.infrastructure.ncookie;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Slf4j
@Service
@Profile({"local", "local-prod"})
public class SandboxNcookieService implements NcookieService {

  @Override
  public String getGuid(HttpServletRequest request) throws IOException {
    throw new IllegalAccessError("ローカルにはNCookieからGUID取得ができません。");
  }

  @Override
  public String getYid(HttpServletRequest request) throws IOException {
    throw new IllegalAccessError("ローカルにはNCookieからYID取得ができません。");
  }

  @Override
  public boolean validateCookie(HttpServletRequest request) throws IOException {
    return false;
  }

}
