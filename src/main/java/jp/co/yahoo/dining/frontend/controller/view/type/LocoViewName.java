package jp.co.yahoo.dining.frontend.controller.view.type;

/**
 * 画面パス
 */
public enum LocoViewName {
  PLACE_DETAIL("place/index"),
  COUPON_LIST("place/coupon/index"),
  COURSE_LIST("place/course/index"),
  COURSE_DETAIL("place/course/detail"),
  PLAN_LIST("place/plan/index"),
  MENU_DETAIL("place/menu/index"),
  REVIEW_DETAIL("place/review/detail"),
  REVIEW_LIST("place/review/index"),
  PHOTO_LIST("place/photo/index"),
  MAP_DETAIL("place/map/index"),
  MARRIAGE_DETAIL("place/marriage/index"),
  WEDDING_DETAIL("place/wedding/index"),
  BRIDAL_DETAIL("place/bridal/index"),
  ERROR("error");

  private String path;

  private LocoViewName(String path) {
    this.path = path;
  }

  public String getPath(){
    return this.path;
  }

  public static LocoViewName getLocoViewName(String viewName){
    LocoViewName[] list = LocoViewName.values();
    for(LocoViewName type : list){
      if (type.getPath().equals(viewName)) return type;
    }
    return null;
  }
}
