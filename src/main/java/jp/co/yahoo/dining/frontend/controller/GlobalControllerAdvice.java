package jp.co.yahoo.dining.frontend.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jp.co.yahoo.dining.frontend.controller.error.TenpoNotFoundException;
import jp.co.yahoo.dining.frontend.domain.PlanList;
import jp.co.yahoo.dining.frontend.domain.Promotion;
import jp.co.yahoo.dining.frontend.domain.Tenpo;
import jp.co.yahoo.dining.frontend.domain.UserInfo;
import jp.co.yahoo.dining.frontend.domain.Wedding;
import jp.co.yahoo.dining.frontend.infrastructure.LocoResourceLoader;
import jp.co.yahoo.dining.frontend.infrastructure.eappid.EappIdService;
import jp.co.yahoo.dining.frontend.infrastructure.masthead.YahooHeadService;
import jp.co.yahoo.dining.frontend.infrastructure.masthead.model.YahooHead;
import jp.co.yahoo.dining.frontend.service.PlanService;
import jp.co.yahoo.dining.frontend.service.PromotionService;
import jp.co.yahoo.dining.frontend.service.TenpoService;
import jp.co.yahoo.dining.frontend.service.UserInfoService;
import jp.co.yahoo.dining.frontend.service.WeddingService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 店舗系の画面を表示する際に、下記の事前差し込み処理を行います。
 * <ul>
 * <li>マストヘッドHTMLをorg.springframework.ui.Modelに追加</li>
 * <li>eappIdをorg.springframework.ui.Modelに追加</li>
 * <li>店舗情報をorg.springframework.ui.Modelに追加</li>
 * <li>ULT用のページパラメータをorg.springframework.ui.Modelに追加</li>
 * <li>メタデータをorg.springframework.ui.Modelに追加</li>
 * </ul>
 */
@AllArgsConstructor
@ControllerAdvice(basePackages = "jp.co.yahoo.dining.frontend.controller.view")
public class GlobalControllerAdvice {

  private static final String PATH_VARIABLE_GID_KEY = "gid";

  private final LocoRequestHeader locoRequestHeader;

  private final EappIdService eappIdService;

  private final TenpoService tenpoService;

  private final PromotionService promotionService;

  private final LocoResourceLoader locoResourceLoader;

  private final UserInfoService userInfoService;

  private final PlanService planService;

  private final WeddingService weddingService;

  @ModelAttribute
  public void setTenpoInfo(@PathVariable Map<String, String> pathValidation, Model model) throws JsonProcessingException {
    if (pathValidation.containsKey(PATH_VARIABLE_GID_KEY)) {
      String gid = pathValidation.get(PATH_VARIABLE_GID_KEY);
      if (StringUtils.isNotEmpty(gid)) {
        Tenpo tenpoInfo = tenpoService.getTenpoInfo(gid);
        if (tenpoInfo != null && tenpoInfo.getFeature() != null && tenpoInfo.getFeature().length > 0) {
          model.addAttribute("tenpo", tenpoInfo);
          model.addAttribute("tenpoJson", locoResourceLoader.getJson(tenpoInfo));
          return;
        }
        // LSBEから店舗情報取得不可だったなあい、404処理
        throw new TenpoNotFoundException("指定したgidに対応する店舗情報が存在してないか、店舗の feature が存在しません。： " + gid);
      }
    }
    throw new TenpoNotFoundException("PathVariableでgidを指定してください");
  }

  @ModelAttribute
  public void setIsMapPage(Model model) {
    model.addAttribute("isMapPage", false);
  }

  @ModelAttribute
  public void setEappId(Model model) {
    String eappId = eappIdService.getEappId();
    if (StringUtils.isNotEmpty(eappId)) {
      model.addAttribute("eappId", eappId);
    }
  }

  @ModelAttribute
  public void setUserInfo(HttpServletRequest request, Model model) throws IOException {
    String cookie = locoRequestHeader.getCookie();
    UserInfo userInfo = userInfoService.getUserInfo(cookie, request);
    if (userInfo != null) {
      model.addAttribute("userInfo", userInfo);
      model.addAttribute("userInfoJson", locoResourceLoader.getJson(userInfo.getUserInfoView()));
    }
  }

  @ModelAttribute
  public void setPromotionInfo(Model model) throws JsonProcessingException {
    Promotion promotion = promotionService.getPromotion();
    if (promotion != null) {
      model.addAttribute("promotionJson", locoResourceLoader.getJson(promotion));
    }
  }

  @ModelAttribute
  public void setCourseList(Model model, @PathVariable Map<String, String> pathValidation, HttpServletRequest request) throws JsonProcessingException {
    // コース一覧情報を渡す(コース一覧ページではdateの絞込が行われるのでセットしない)
    if (!request.getServletPath().endsWith("/course")) {
      if (pathValidation.containsKey(PATH_VARIABLE_GID_KEY)) {
        String gid = pathValidation.get(PATH_VARIABLE_GID_KEY);
        if (StringUtils.isNotEmpty(gid)) {
          // ジャンル「グルメ」以外はダイニングAPI叩かないようにする
          PlanList planList = new PlanList();
          Tenpo tenpo = (Tenpo)model.asMap().get("tenpo");
          if (tenpo.getFeature()[0].getProperty() != null
            && tenpo.getFeature()[0].getProperty().getGenre() != null) {
            if (Arrays.stream(tenpo.getFeature()[0].getProperty().getGenre())
              .anyMatch(genre -> genre.getCode().startsWith("01"))) {
              planList = planService.getPlanList(gid);
            }
          }
          if (planList != null) {
            model.addAttribute("courseList", planList);
            model.addAttribute("courseListJson", locoResourceLoader.getJson(planList));
            return;
          }
        }
      }
    }
  }

  /**
   * 式場情報を付与する
   * @param pathValidation
   * @param model
   * @throws JsonProcessingException
   */
  @ModelAttribute
  public void setWeddingInfo(@PathVariable Map<String, String> pathValidation, Model model) throws JsonProcessingException {
    if (pathValidation.containsKey(PATH_VARIABLE_GID_KEY)) {
      String gid = pathValidation.get(PATH_VARIABLE_GID_KEY);
      if (StringUtils.isNotEmpty(gid)) {
        Tenpo tenpoInfo = tenpoService.getTenpoInfo(gid);
        if (!tenpoService.isWedding(tenpoInfo)) {
          return;
        }
        Wedding weddingInfo = weddingService.getWeddingInfo(gid);
        model.addAttribute("wedding", weddingInfo);
        model.addAttribute("weddingJson", locoResourceLoader.getJson(weddingInfo));
        return;
      }
    }
  }
}
