package jp.co.yahoo.dining.frontend.service;

import java.util.Arrays;

import jp.co.yahoo.dining.frontend.controller.form.TenpoInfo;
import java.util.Objects;

import jp.co.yahoo.dining.frontend.domain.Tenpo;
import jp.co.yahoo.dining.frontend.domain.Tenpo.Children;
import jp.co.yahoo.dining.frontend.infrastructure.rest.WeddingAPIConfig;
import jp.co.yahoo.dining.frontend.repository.TenpoTemplate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TenpoService {

  private final TenpoTemplate tenpoTemplate;

  private final WeddingAPIConfig weddingAPIConfig;

  /**
   * 店舗情報を取得する。
   *
   * @param gid
   * @return
   */
  public Tenpo getTenpoInfo(String gid) {
    return tenpoTemplate.getTenpoInfo(gid);
  }

  /**
   * 店舗情報を取得する。
   *
   * @param tenpoInfo
   * @return
   */
  public Tenpo getTenpoInfo(TenpoInfo tenpoInfo) {
    return tenpoTemplate.getTenpoInfo(tenpoInfo);
  }

  /**
   * 引数で与えられた店舗情報が式場を示すものであるかを判定します。
   * 店舗データに式場対象CPが含まれていればTRUEを返す
   * @param tenpo Tenpo
   * @return boolean
   */
  public boolean isWedding(Tenpo tenpo) {
    // throw null pointer exception if tenpo is null
    Objects.requireNonNull(tenpo, "tenpo must not be null.");

    // Featureが取れない場合は式場ではないとみなす
    Tenpo.Feature[] feature = tenpo.getFeature();
    if (feature == null || feature.length == 0) {
      log.warn("feature以下の情報が取れませんでした: " + tenpo); // TODO: ログレベルを確認する
      return false;
    }

    // Childrensが取れない場合は式場ではないとみなす
    Children[] childrens = tenpo.getFeature()[0].getChildren();
    if (childrens == null || childrens.length == 0) {
      log.warn("childrens以下の情報が取れませんでした: " + tenpo); // TODO: ログレベルを確認する
      return false;
    }

    // プロパティの式場カセット ID と JSON の値が一致している場合には、与えられた店舗が式場を示していると判断します
    for (Children children : childrens) {
      String cassetteId = children.getCassetteId();
      String[] cassetteIds = weddingAPIConfig.getCassetteIds();
      if (Arrays.asList(cassetteIds).contains(cassetteId)) {
        return true;
      }
    }

    return false;
  }
}
