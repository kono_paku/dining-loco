package jp.co.yahoo.dining.frontend.controller.api;

import java.util.List;
import javax.validation.Valid;
import jp.co.yahoo.dining.frontend.controller.form.RdsigInfo;
import jp.co.yahoo.dining.frontend.domain.Rdsig;
import jp.co.yahoo.dining.frontend.infrastructure.rdsig.RdsigService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@AllArgsConstructor
public class RdsigController {

  private final RdsigService rdsigService;

  @PostMapping("/v1/api/rdsig")
  public List<Rdsig> getRdsig(@Valid @RequestBody RdsigInfo rdsigInfo) {
    return rdsigService.getRdsidUrlList(rdsigInfo.getUrls(), rdsigInfo.getLabel());
  }
}
