package jp.co.yahoo.dining.frontend.infrastructure.ncookie;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public interface NcookieService {

  /**
   * NCookieからGuid取得
   * @param request サーブレットレクエスト
   * @return Guid
   */
  String getGuid(HttpServletRequest request) throws IOException;

  /**
   * NCookieからYid取得
   * @param request サーブレットレクエスト
   * @return Yid
   */
  String getYid(HttpServletRequest request) throws IOException;

  /**
   * NCookieのバリデーション
   * @param request サーブレットレクエスト
   * @return N Cookieのバリデーション結果
   */
  boolean validateCookie(HttpServletRequest request) throws IOException;
}
