package jp.co.yahoo.dining.frontend.infrastructure.udb.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * UDBレスポンス
 */
@Data
public class UdbResponse implements Serializable {

  @JsonProperty("demog")
  private String demog;

  @JsonProperty("prem_type")
  private Integer premiumType;

  @JsonProperty("sbmbundle_flag")
  private Integer softbankBundle;

  @JsonProperty("ymbundle_flag")
  private Integer yMobileBundle;

  @JsonProperty("jp_edit_sbm_reg")
  private String softBankCooperation;

  @JsonProperty("login")
  private String yid;

  @JsonProperty("GUID")
  private String guid;

}
