package jp.co.yahoo.dining.frontend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;
import lombok.Data;

import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LocoSecure implements ExternalEntity{
  /** 検索に要した時間 */
  private float latency;
  /** リクエスト受付日時 */
  private Date requestDate;
  /** 暗号化YUID、MaskYID */
  private Entry entry;

  @Data
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Entry {
    /** 暗号化されたYUID */
    private String encodedYuid;
    /** MaskされたYID */
    private String maskedYid;
  }
}
