package jp.co.yahoo.dining.frontend.infrastructure.ncookie;

import jp.co.yahoo.idservice.VerificationKey;
import jp.co.yahoo.idservice.YjCredential;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.zip.DataFormatException;

@Slf4j
@Service
@Profile({"staging", "prod"})
public class NcookieServiceImpl implements NcookieService {
  private static final String UPS_KEY_ID = "key-id";
  private static final String UPS_KEY_DATA = "key-data";

  private VerificationKey verificationKey;

  public NcookieServiceImpl() throws IOException, DataFormatException {
    this.verificationKey = VerificationKey.initializeByUps("ncookie-vkey");
  }

  @Override
  public String getGuid(HttpServletRequest request) throws IOException {
    YjCredential yjCredential = new YjCredential(request, verificationKey);
    return yjCredential.getGuid();
  }

  @Override
  public String getYid(HttpServletRequest request) throws IOException {
    YjCredential yjCredential = new YjCredential(request, verificationKey);
    return yjCredential.getLogin();
  }

  @Override
  public boolean validateCookie(HttpServletRequest request) throws IOException {
    return new YjCredential(request, verificationKey).validate();
  }

}
