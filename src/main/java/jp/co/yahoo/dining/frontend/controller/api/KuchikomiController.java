package jp.co.yahoo.dining.frontend.controller.api;

import jp.co.yahoo.dining.frontend.domain.Kuchikomi;
import jp.co.yahoo.dining.frontend.service.KuchikomiService;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class KuchikomiController {

  private final KuchikomiService kuchikomiService;

  /**
   * gid, guid, customexact1, sort, start, results
   * 上記リクエストパラメーターに対し、口コミを取得する
   */
  @GetMapping("/v1/api/kuchikomi")
  public Kuchikomi getKuchikomiList(
    @RequestParam String gid,
    @RequestParam Optional<String> sort,
    @RequestParam Optional<Integer> start,
    @RequestParam Optional<Integer> results,
    @RequestParam Optional<String> guid,
    @RequestParam Optional<String> customexact1
  ) {
    return kuchikomiService.getKuchikomiList(gid, sort, start, results, guid, customexact1);
  }
}
