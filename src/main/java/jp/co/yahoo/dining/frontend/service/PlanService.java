package jp.co.yahoo.dining.frontend.service;


import jp.co.yahoo.dining.frontend.domain.PlanDetail;
import jp.co.yahoo.dining.frontend.domain.PlanList;
import jp.co.yahoo.dining.frontend.domain.Tenpo;
import jp.co.yahoo.dining.frontend.repository.LifeToolTemplate;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class PlanService {

  private final LifeToolTemplate lifeToolTemplate;

  @Value("${extension.cidList.yahoo}")
  private String yahooCid;

  public PlanDetail getPlanDetail(String tenpoId, String planId) {
    return lifeToolTemplate.getPlanDetail(tenpoId, planId);
  }

  public PlanDetail getPlanDetail(Tenpo tenpo, String planId) {
    String tenpoId = getTenpoId(tenpo);
    planId = getPlanId(tenpoId, planId);
    return lifeToolTemplate.getPlanDetail(tenpoId, planId);
  }

  public PlanList getPlanList(final String gid) {
    return lifeToolTemplate.getPlanList(gid);
  }

  public PlanList getPlanList(final String gid, final Optional<String> date) {
    return lifeToolTemplate.getPlanList(gid, date);
  }

  private String getTenpoId(Tenpo tenpo) {
    for (Tenpo.Children child : tenpo.getFeature()[0].getChildren()) {
      if (yahooCid.equals(child.getCassetteId())) {
        String tenpoId = child.getId();
        if (StringUtils.isNotEmpty(tenpoId)) {
          return tenpoId;
        }
      }
    }

    throw new IllegalStateException("店舗ID取得に失敗しました。");
  }

  private String getPlanId(String tenpoId, String planId) {
    return StringUtils.isNotEmpty(planId) ? planId : tenpoId + "01";
  }

}
