package jp.co.yahoo.dining.frontend.service;

import jp.co.yahoo.dining.frontend.domain.AdInfeed;
import jp.co.yahoo.dining.frontend.repository.AdInfeedTemplate;
import lombok.RequiredArgsConstructor;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AdInfeedService {
  private final AdInfeedTemplate adInfeedTemplate;

  public AdInfeed getAdInfeedList(final String keywords) {
    return adInfeedTemplate.getAdInfeed(keywords);
  }

  public String getRefererKeyword (final String referer) throws UnsupportedEncodingException {
    if (StringUtils.isEmpty(referer)
      // || StringUtils.containsNone(referer, "loco.yahoo.co.jp/search") // STG動作確認のため一時コメントアウト(serpがないため) @todo リリース時コメントアウトを外す
      ) {
      return "";
    }

    if (referer.matches(".*?p=.*|.*&p=.*")) {
      List<String> kwRawList = Arrays.asList(referer.split("\\?p=|&p="));
      if (kwRawList.size() > 1) {
        return URLDecoder.decode(kwRawList.get(1).split("&")[0], "UTF-8");
      }
    }
    return "";
  }
}
