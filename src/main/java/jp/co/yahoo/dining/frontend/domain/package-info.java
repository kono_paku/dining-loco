/**
 * ドメイン層
 *
 * パッケージ直下には、メインとなるモデルのみ定義してください。
 * ネストしたモデルは、メインとなるモデルのstaticインナークラスとして定義してください。
 */
package jp.co.yahoo.dining.frontend.domain;
