package jp.co.yahoo.dining.frontend.infrastructure.rest;

import jp.co.yahoo.dining.frontend.controller.form.ImagesInfo;
import jp.co.yahoo.dining.frontend.domain.ResizedImage;
import jp.co.yahoo.dining.frontend.infrastructure.Dto;
import jp.co.yahoo.dining.frontend.repository.ImageWizardTemplate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.function.BiFunction;

@Component
public class ImageWizardTemplateImpl extends LocoRestTemplate implements ImageWizardTemplate {

  private final RestTemplate rt;

  public ImageWizardTemplateImpl(@Qualifier("imageWizard") RestTemplate imageWizardRestTemplate){
    rt = imageWizardRestTemplate;
  }

  @Override
  public RestTemplate rt() {
    return rt;
  }

  @Override
  protected HttpHeaders setAuthority(HttpHeaders headers) {
    return headers;
  }

  public ResizedImage resizeImages(final ImagesInfo info) {
    return postEntity(ResizedImage.class, "/Iwiz/V1/signUrl", ImageWizardParameter.of(info));
  }

  @AllArgsConstructor
  @NoArgsConstructor
  @Data
  public static class ImageWizardParameter implements Dto {
    private Source[] source;
    public static ImageWizardParameter of(ImagesInfo imagesInfo) {
      ImageWizardParameter param = new ImageWizardParameter();
      param.setSource(imagesInfo.getImages().stream()
        .map(image -> {
          if (image.getCrop()) {
            Integer cropWidthCenter = image.getCropWidthCenter() != null ? image.getCropWidthCenter() : image.getWidth();
            Integer cropHeightCenter = image.getCropHeightCenter() != null ? image.getCropHeightCenter() : image.getHeight();
            return new Source(null, image.getUrl()
              , "x=" + image.getWidth()
              +  "&y=" +  image.getHeight()
              + "&cwc=" + cropWidthCenter
              + "&chc=" + cropHeightCenter
              + "&bd=0&n=0&ifm=png"
              + "&pri=" + getPriority(image.getPriority())
              + "&q=" + getQuality(image.getQuality())
              , "olp");
          } else {
            return new Source(null, image.getUrl()
              , "x=" + image.getWidth()
              + "&y=" + image.getHeight()
              + "&bd=0&n=0&ifm=png"
              , "olp");
          }
        })
        .toArray(size -> new Source[size])
      );
      return param;
    }

    private static Integer getQuality(Integer quality){
      return quality != null ? quality : 50;
    }

    private static String getPriority(String priority){
      return priority != null ? priority : "s";
    }
  }

  @AllArgsConstructor
  @Data
  public static class Source {
    private String path;
    private String url;
    private String resize;
    private String serviceId;
  }
}
