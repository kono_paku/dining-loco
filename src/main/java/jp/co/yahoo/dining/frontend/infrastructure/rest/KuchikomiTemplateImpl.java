package jp.co.yahoo.dining.frontend.infrastructure.rest;

import jp.co.yahoo.dining.frontend.domain.Kuchikomi;
import jp.co.yahoo.dining.frontend.repository.KuchikomiTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.Optional;

@Component
public class KuchikomiTemplateImpl extends LocoRestTemplate implements KuchikomiTemplate {
  private final RestTemplate rt;
  private final KuchikomiAPIConfig kuchikomiAPIConfig;
  private final String locoKuchikomiCid;

  public KuchikomiTemplateImpl(@Qualifier("kuchikomi") RestTemplate rt, KuchikomiAPIConfig kuchikomiAPIConfig, @Value("${extension.cidList.locoKuchikomi}") String locoKuchikomiCid) {
    this.rt = rt;
    this.kuchikomiAPIConfig = kuchikomiAPIConfig;
    this.locoKuchikomiCid = locoKuchikomiCid;
  }

  @Override
  public RestTemplate rt() {
    return rt;
  }

  @Override
  protected HttpHeaders setAuthority(HttpHeaders headers) {
    return headers;
  }

  @Override
  public Kuchikomi getKuchikomiList(
    final String gid,
    final Optional<String> sort,
    final Optional<Integer> start,
    final Optional<Integer> results,
    final Optional<String> guid,
    final Optional<String> customexact1
  ) {
    UriComponentsBuilder builder = createRequest(kuchikomiAPIConfig.getKuchikomiList())
      .queryParam("appid", kuchikomiAPIConfig.getAppId())
      .queryParam("gid", gid)
      .queryParam("output", "json");
    if (sort.isPresent()) {
      builder.queryParam("sort", sort.get());
    }
    if (start.isPresent()) {
      builder.queryParam("start", start.get());
    }
    if (results.isPresent()) {
      builder.queryParam("results", results.get());
    }
    if (guid.isPresent()) {
      builder.queryParam("guid", guid.get());
    }
    if (customexact1.isPresent()) {
      builder.queryParam("customexact1", customexact1.get());
    }
    return getEntity(Kuchikomi.class, builder).convert(locoKuchikomiCid);
  }
}
