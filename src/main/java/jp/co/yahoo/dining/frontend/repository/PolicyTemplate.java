package jp.co.yahoo.dining.frontend.repository;


import jp.co.yahoo.dining.frontend.domain.PlanPolicy;

import java.util.Optional;

public interface PolicyTemplate {

  PlanPolicy getPolicyList(String tenpoId, Optional<String> planId,  Optional<String> date);

}
