package jp.co.yahoo.dining.frontend.repository;

import jp.co.yahoo.dining.frontend.controller.form.TenpoInfo;
import jp.co.yahoo.dining.frontend.domain.Tenpo;

/**
 * ローカルサーチAPIを操作するクラス
 *
 * @see <a href="https://developer.yahoo.co.jp/webapi/map/openlocalplatform/v1/localsearch.html#request-param">Yahoo!ローカルサーチAPI IF仕様書</a>
 */
public interface TenpoTemplate {

  /**
   * 単一の店舗情報を取得するAPI
   */
  Tenpo getTenpoInfo(String gid);

  Tenpo getTenpoInfo(TenpoInfo tenpoInfo);
}
