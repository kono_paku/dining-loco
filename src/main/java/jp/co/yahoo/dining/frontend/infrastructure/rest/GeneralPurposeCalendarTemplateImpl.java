package jp.co.yahoo.dining.frontend.infrastructure.rest;

import jp.co.yahoo.dining.frontend.domain.Holiday;
import jp.co.yahoo.dining.frontend.repository.GeneralPurposeCalendarTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class GeneralPurposeCalendarTemplateImpl extends LocoRestTemplate implements GeneralPurposeCalendarTemplate {

  private final RestTemplate rt;

  private final GeneralPurposeCalendarConfig generalPurposeCalendarConfig;

  GeneralPurposeCalendarTemplateImpl(@Qualifier("generalPurposeCalendar") RestTemplate rt, GeneralPurposeCalendarConfig generalPurposeCalendarConfig) {
    this.rt = rt;
    this.generalPurposeCalendarConfig = generalPurposeCalendarConfig;
  }

  @Override
  public RestTemplate rt() { return rt; }

  @Override
  protected HttpHeaders setAuthority(HttpHeaders headers) { return headers; }

  public Holiday searchHolidays(Integer year) {
    String fq = "(Year:" + year + ") OR (Year:" + (year + 1) + ")";

    UriComponentsBuilder builder = createRequest(generalPurposeCalendarConfig.getCalendarPath())
      .queryParam("sid", generalPurposeCalendarConfig.getSid())
      .queryParam("wt", generalPurposeCalendarConfig.getWt())
      .queryParam("rows", generalPurposeCalendarConfig.getRows())
      .queryParam("q", "*:*")
      .queryParam("fq", fq);

    return getEntity(Holiday.class, builder);
  }
}
