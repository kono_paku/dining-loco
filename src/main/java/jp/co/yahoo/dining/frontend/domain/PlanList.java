package jp.co.yahoo.dining.frontend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by jchung on 2017/12/08.
 * PlanAPIはCamel形なので別に@JsonPropertyをつけない。(例あり)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanList implements ExternalEntity, Serializable {

  private float latency;
  private String requestDate;
  private int totalResults;
  private PlanListEntry[] entry;

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class PlanListEntry {
    private String uid;
    private String id;
    private String name;
    private String cassette;
    private String image;
    private String description;
    private String tokutenText;
    private String discountPercent;
    private Urls urls;
    private int price;
    private int originalViewPrice;
    private boolean hasTokuten;
    private boolean premiumOnlyFlag;
    private int tokutenType;
    private int seatsMax;
    private int seatsMin;
    private int reservationType;
    private Attribute[] attributes;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Urls {
    private String original;
    private String yahoo;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Attribute {
    private String key;
    private String name;
  }
}
