package jp.co.yahoo.dining.frontend.controller.view.expression;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.IExpressionContext;
import org.thymeleaf.dialect.IExpressionObjectDialect;
import org.thymeleaf.expression.IExpressionObjectFactory;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * 使用例：${#view.credit(tenpo.Feature[0].Property)}
 */
@Component
public class ViewHelperFactory implements IExpressionObjectDialect {

  @Autowired
  ViewHelper viewHelper;
  private static final String VIEW_HEALPER_NAME = "view";
  private static final Set<String> ALL_EXPRESSION_NAMES = new HashSet<String>(){{add(VIEW_HEALPER_NAME);}};

  @Override
  public IExpressionObjectFactory getExpressionObjectFactory() {
    return new IExpressionObjectFactory() {
      @Override
      public Set<String> getAllExpressionObjectNames() {
        return ALL_EXPRESSION_NAMES;
      }

      @Override
      public Object buildObject(IExpressionContext context, String expressionObjectName) {
        // 独自Utilityのインスタンスと名前を紐付け
        if(expressionObjectName.equals(VIEW_HEALPER_NAME)){
          return viewHelper;
        }
        return null;
      }

      @Override
      public boolean isCacheable(String expressionObjectName) {
        // 必要に応じて実装
        return false;
      }
    };
  }

  @Override
  public String getName() {
    return null;
  }
}
