package jp.co.yahoo.dining.frontend.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
@RequiredArgsConstructor
public class AccessLogInterceptor implements HandlerInterceptor {

  private final LocoRequestHeader locoRequestHeader;

  @Override
  public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
    StringBuilder accessLogBuilder = new StringBuilder();
    accessLogBuilder.append("[" + locoRequestHeader.getTraceId() + "] ");
    accessLogBuilder.append("アクセス Start : ");
    accessLogBuilder.append(httpServletRequest.getRequestURI());

    log.info(accessLogBuilder.toString());
    return true;
  }

  @Override
  public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
  }

  @Override
  public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
    if (e != null) {
      return;
    }

    StringBuilder accessLogBuilder = new StringBuilder();
    accessLogBuilder.append("[" + locoRequestHeader.getTraceId() + "] ");
    accessLogBuilder.append("アクセス End : ");
    accessLogBuilder.append(httpServletRequest.getRequestURI());

    log.info(accessLogBuilder.toString());
  }

}
