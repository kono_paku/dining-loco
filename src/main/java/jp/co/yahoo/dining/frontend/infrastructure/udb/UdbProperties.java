package jp.co.yahoo.dining.frontend.infrastructure.udb;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * UDBプロパティ
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "extension.udb")
public class UdbProperties {
  private String udbApiUrl;
}
