package jp.co.yahoo.dining.frontend.controller.view;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import jp.co.yahoo.dining.frontend.domain.Tenpo;
import jp.co.yahoo.dining.frontend.service.TenpoService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Controller
public class ReviewController {

  @GetMapping("/place/g-{gid}/review")
  public String getReviewList(@PathVariable final String gid) {
    return "place/review/index";
  }

  @GetMapping("/place/g-{gid}/review/{reviewId}")
  public String getReviewDetail(Model model, @PathVariable final String gid, @PathVariable final String reviewId) {
    model.addAttribute("reviewId", reviewId);
    return "place/review/detail";
  }

}
