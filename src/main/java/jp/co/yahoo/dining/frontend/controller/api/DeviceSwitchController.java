package jp.co.yahoo.dining.frontend.controller.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeviceSwitchController {
  @GetMapping("/v1/api/yjdset/smartphone")
  public boolean setSmartPhone() {
    return true;
  }

  @GetMapping("/v1/api/yjdset/pc")
  public boolean setPc() {
    return true;
  }
}
