package jp.co.yahoo.dining.frontend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CpCoupon implements ExternalEntity, Serializable {

  @JsonProperty("Feature")
  private CpCassette[] feature;

  @Data
  @AllArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class CpCassette {
    @JsonProperty("Id")
    private String id;
    @JsonProperty("Property")
    private Property property;
  }

  @Data
  @AllArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Property {
    @JsonProperty("CassetteId")
    private String cassetteId;
    @JsonProperty("CouponFlag")
    private Boolean couponFlag;
    @JsonProperty("SmartPhoneCouponFlag")
    private Boolean smartPhoneCouponFlag;
    @JsonProperty("Detail")
    private Detail detail;
    @JsonProperty("Coupon")
    private Coupon[] coupon;
  }

  @Data
  @AllArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Coupon {
    // URLが2パタン・・・
    @JsonProperty("PcUrl")
    private String pcUrl;
    @JsonProperty("ParentPcUrl")
    private String parentPcUrl;
    @JsonProperty("SmartPhoneUrl")
    private String smartPhoneUrl;
    @JsonProperty("ParentSmartPhoneUrl")
    private String parentSmartPhoneUrl;
    @JsonProperty("ParentMobileUrl")
    private String parentMobileUrl;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("StartDay")
    private String startDay;
    @JsonProperty("EndDay")
    private String endDay;
    @JsonProperty("DisplayFlag")
    private String displayFlag;
  }

  @Data
  @AllArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Detail {
    @JsonProperty("CassetteHeader")
    private String CassetteHeader;
    @JsonProperty("CassetteOwnerUrl")
    private String cassetteOwnerUrl;
    @JsonProperty("CassetteOwnerLogoImage")
    private String cassetteOwnerLogoImage;
    @JsonProperty("PcUrl")
    private String pcUrl;
    @JsonProperty("SmartPhoneUrl")
    private String smartPhoneUrl;
    @JsonProperty("MobileUrl")
    private String mobileUrl;
  }
}
