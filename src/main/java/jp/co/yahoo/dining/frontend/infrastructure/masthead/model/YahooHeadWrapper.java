package jp.co.yahoo.dining.frontend.infrastructure.masthead.model;

import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class YahooHeadWrapper implements ExternalEntity, Serializable {
  private List<YahooHead> results;
}
