package jp.co.yahoo.dining.frontend.service;

import jp.co.yahoo.dining.frontend.domain.LatestInformation;
import jp.co.yahoo.dining.frontend.repository.LatestInformationTemplate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class LatestInformationService {

  private final LatestInformationTemplate latestInformationTemplate;

  public LatestInformation getLatestInformation(final String gid) {
    return latestInformationTemplate.getLatestInformation(gid);
  }

}
