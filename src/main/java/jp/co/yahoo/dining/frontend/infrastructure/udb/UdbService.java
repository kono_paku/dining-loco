package jp.co.yahoo.dining.frontend.infrastructure.udb;

import jp.co.yahoo.dining.frontend.infrastructure.athenz.AthenzService;
import jp.co.yahoo.dining.frontend.infrastructure.udb.model.UdbResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
public abstract class UdbService {
  private static final String REQUEST_HEADER_KEEP_ALIVE = "keep-alive";
  private static final String REQUEST_HEADER_ATHENZ_KEY = "Yahoo-App-Auth";
  private static final String REQUEST_HEADER_COOKIE = "Cookie";

  private static final String REQUEST_PARAM_KEY_UDB = "fields";
  private static final String REQUEST_PARAM_VALUE_UDB = "demog,jp_prem_user,jp_edit_sbm_reg";

  @Autowired
  private UdbProperties udbProperties;

  @Autowired
  private AthenzService athenzService;

  public abstract RestTemplate getRestTemplate();

  @Cacheable("udb")
  public UdbResponse getUdbResponse(String cookie) {
    try {
      HttpHeaders headers = buildHttpHeader(cookie);
      String url = buildRequestUrl();
      RestTemplate restTemplate = getRestTemplate();
      ResponseEntity<UdbResponse> exchange = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity(headers), UdbResponse.class);
      return exchange.getBody();
    } catch (Exception e) {
      // 暫定的にUDBエラーはログレベルwarnで出力し、StackTraceをログに出力しない
      log.warn("UDB API に異常があります。");
    }
    return null;
  }

  private HttpHeaders buildHttpHeader(String cookie) {
    String ycaCertification = athenzService.getYCACertification();
    HttpHeaders headers = new HttpHeaders();
    headers.setConnection(REQUEST_HEADER_KEEP_ALIVE);
    headers.set(REQUEST_HEADER_ATHENZ_KEY, ycaCertification);
    headers.set(REQUEST_HEADER_COOKIE, cookie);
    return headers;
  }

  private String buildRequestUrl() {
    String udbApiUrl = udbProperties.getUdbApiUrl();
    return UriComponentsBuilder.fromHttpUrl(udbApiUrl)
        .queryParam(REQUEST_PARAM_KEY_UDB, REQUEST_PARAM_VALUE_UDB)
        .build(true)
        .toString();
  }

}
