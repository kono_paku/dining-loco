package jp.co.yahoo.dining.frontend.infrastructure;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * リソース取得用クラス
 */
@Component
@RequiredArgsConstructor
public class LocoResourceLoader {

  private final ResourceLoader resourceLoader;
  private final ObjectMapper objectMapper;
  private final ObjectMapper xmlMapper = new XmlMapper();

  public <T> T getResource(Class<T> clazz, String path) throws IOException {
    return objectMapper.readValue(resourceLoader.getResource(path).getFile(), clazz);
  }

  public <T> String getJson(T model) throws JsonProcessingException {
    return objectMapper.writeValueAsString(model);
  }

  public <T> String getXml(T model) throws JsonProcessingException {
    xmlMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    return xmlMapper.writeValueAsString(model);
  }
}
