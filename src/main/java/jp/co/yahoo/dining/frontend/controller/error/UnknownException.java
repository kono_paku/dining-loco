package jp.co.yahoo.dining.frontend.controller.error;


/**
 * ロコ用のExceptionクラス
 */
public class UnknownException extends AbstractErrorResponse {
  private static final String ERROR_TYPE = "サーバの処理で問題が発生しました";

  public UnknownException(Throwable e) {
    super(ERROR_TYPE + " : " + e.getMessage(), e);
  }

}
