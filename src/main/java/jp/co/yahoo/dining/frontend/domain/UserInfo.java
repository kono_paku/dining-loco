package jp.co.yahoo.dining.frontend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInfo {
  private Boolean loginStatus;
  private UserDetail userDetail;

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class UserDetail {
    private String guid;
    private String yid;
    private String area;
    private Boolean isPremium;
    private Integer premiumType;
    private Integer softbankBundle;
    private Integer yMobileBundle;
    private String softBankCooperation;
  }

  @Data
  @AllArgsConstructor
  public static class UserInfoView {
    private String guid;
    private Boolean loginStatus;
    private Boolean isPremium;
    private Integer premiumType;
    private Integer softbankBundle;
    private Integer yMobileBundle;
    private String softBankCooperation;
  }

  /**
   * 画面で取り扱えるデータに整形する
   * @return UserInfoView
   */
  public UserInfoView getUserInfoView(){
    return new UserInfoView(
      this.getUserDetail().getGuid(),
      this.getLoginStatus(),
      this.getUserDetail().getIsPremium(),
      this.getUserDetail().getPremiumType(),
      this.getUserDetail().getSoftbankBundle(),
      this.getUserDetail().getYMobileBundle(),
      this.getUserDetail().getSoftBankCooperation()
    );
  }
}
