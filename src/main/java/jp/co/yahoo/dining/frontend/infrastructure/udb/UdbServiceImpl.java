package jp.co.yahoo.dining.frontend.infrastructure.udb;

import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * TODO : 今後UDB側のところをどう修正すればいいか確認必要。
 *
 * Created by jchung on 2017/11/08.
 */
@Slf4j
@Service
@Profile("!local")
public class UdbServiceImpl extends UdbService {

  private static final int CONNECT_TIMEOUT = 5000;
  private static final int READ_TIMEOUT = 5000;

  private RestTemplate restTemplate;

  @PostConstruct
  public void setup() {
    this.restTemplate = new RestTemplateBuilder()
      .setConnectTimeout(CONNECT_TIMEOUT)
      .setReadTimeout(READ_TIMEOUT)
      .messageConverters(new CustomMessageConverter())
      .build();
  }

  @Override
  public RestTemplate getRestTemplate() {
    return this.restTemplate;
  }
}
