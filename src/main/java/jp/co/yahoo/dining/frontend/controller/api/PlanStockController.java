package jp.co.yahoo.dining.frontend.controller.api;

import jp.co.yahoo.dining.frontend.controller.constraint.GId;
import jp.co.yahoo.dining.frontend.domain.PlanStock;
import jp.co.yahoo.dining.frontend.service.PlanStockService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@Validated
public class PlanStockController {

  private final PlanStockService planStockService;

  @GetMapping("/v1/api/planStock")
  public PlanStock getPlanStock(@Valid @GId final String gid) {
    return planStockService.getPlanStock(gid);
  }
}
