package jp.co.yahoo.dining.frontend;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SpaceId {
  private List<Device> devices;

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  public static class Device {
    private String device;
    private List<Genre> genres;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  public static class Genre {
    private String code;
    private List<Prefecture> prefectures;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  public static class Prefecture {
    private String code;
    private String spaceId;
  }
}
