package jp.co.yahoo.dining.frontend.controller.view;

import jp.co.yahoo.dining.frontend.controller.LocoRequestHeader;
import jp.co.yahoo.dining.frontend.controller.error.TenpoNotFoundException;
import jp.co.yahoo.dining.frontend.domain.UserInfo;
import jp.co.yahoo.dining.frontend.infrastructure.masthead.YahooHeadService;
import jp.co.yahoo.dining.frontend.infrastructure.masthead.model.YahooHead;
import jp.co.yahoo.dining.frontend.service.UserInfoService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.List;

/**
 * 画面を返すAPIのエラーハンドラ−
 * Throwされたエラーを横断的にキャッチします。
 */
@Slf4j
@ControllerAdvice
@AllArgsConstructor
public class ViewExceptionHandler {

  private static final String PC_PATH = "pc/html/";
  private static final String SP_PATH = "sp/html/";

  // TODO : デバイス判定いい感じ探しましょー
  private final LocoRequestHeader locoRequestHeader;

  private final YahooHeadService yahooHeadService;

  private final UserInfoService userInfoService;

  @ExceptionHandler
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ModelAndView handle(HttpServletRequest request, NoHandlerFoundException exception) {
    log.warn("間違ってるページにアクセスがあります : " + request.getRequestURL(), exception.getMessage());
    return getErrorModelAndView(request, "place/notFound/index");
  }

  // パタン1_ページが存在しません
  @ExceptionHandler
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ModelAndView handle(HttpServletRequest request, ResourceNotFoundException exception) {
    log.warn("間違ってるページにアクセスがあります : " + request.getRequestURL(), exception.getMessage());
    return getErrorModelAndView(request, "place/notFound/index");
  }

  //  IllegalStateException
  @ExceptionHandler
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ModelAndView handle(HttpServletRequest request, TenpoNotFoundException exception) {
    // bot,crawlerの場合ログレベルを下げる
    // botリスト: https://www.keycdn.com/blog/web-crawlers/
    String userAgent = request.getHeader("user-agent");
    if(userAgent != null && userAgent
      .matches("(.*)(Googlebot|Bingbot|Slurp|DuckDuckBot|Baiduspider|YandexBot|Sogou(.*)pider|Exabot|Konqueror|facebot|facebookexternalhit|ia_archiver)(.*)")) {
      log.info("存在しないGID : " + request.getRequestURL() + ", UserAgent : " + request.getHeader("user-agent"), exception.getMessage());
    } else {
      log.warn("存在しないGID : " + request.getRequestURL(), exception.getMessage());
    }
    return getErrorModelAndView(request, "place/notFound/index");
  }

  // パタン2_バリデーションエラー
  @ExceptionHandler
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ModelAndView handle(HttpServletRequest request, ConstraintViolationException exception) {
    log.warn("APIリクエストでタイムアウトが発生しています : " + request.getRequestURL(), exception.getMessage());
    return getErrorModelAndView(request, "place/systemError/index");
  }

  // パタン3_クエストパラメーターの値をセット先の型に変換できなかった
  @ExceptionHandler
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ModelAndView handle(HttpServletRequest request, MethodArgumentTypeMismatchException exception) {
    log.warn("APIリクエストでタイムアウトが発生しています : " + request.getRequestURL(), exception.getMessage());
    return getErrorModelAndView(request, "place/systemError/index");
  }

  // パタン4_Connectionタイムアウト
  @ExceptionHandler
  @ResponseStatus(HttpStatus.BAD_GATEWAY)
  public ModelAndView handle(HttpServletRequest request, SocketTimeoutException exception) {
    log.error("APIリクエストでタイムアウトが発生しています : " + request.getRequestURL(), exception.getMessage());
    return getErrorModelAndView(request, "place/systemError/index");
  }

  // パタン5_その他システムエラー
  @ExceptionHandler
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ModelAndView handle(HttpServletRequest request, Throwable exception) {
    log.error("システムエラーが発生しています : " + request.getRequestURL(), exception);
    return getErrorModelAndView(request, "place/systemError/index");
  }

  private ModelAndView getErrorModelAndView(HttpServletRequest request, String viewName) {
    ModelAndView modelAndView = new ModelAndView(getDevicePath() + viewName);

    List<YahooHead> yahooHeadList = getMastHeaderData(request);
    for (YahooHead headData : yahooHeadList) {
      modelAndView.addObject(headData.getPosition(), headData.getHtml());
    }
    try {
      modelAndView.addObject("userInfo", getUserInfo(request));
    } catch (IOException e) {
      modelAndView.addObject("userInfo", new UserInfo(false, new UserInfo.UserDetail()));
    }
    return modelAndView;
  }

  /**
   * ユーザ情報を取得する(エラー時のモデルに渡すために実行)
   * @return UserInfo 利用ユーザの情報(ログイン情報等)
   */
  private UserInfo getUserInfo(HttpServletRequest request) throws IOException {
    return userInfoService.getUserInfo(locoRequestHeader.getCookie(), request);
  }

  private List<YahooHead> getMastHeaderData(HttpServletRequest request) {
    String path = request.getServletPath();
    String cookie = locoRequestHeader.getCookie();
    String device = locoRequestHeader.getActualDevice();
    return yahooHeadService.getYahooHeadList(cookie, getErrorSpaceId(), device, path);
  }

  private String getDevicePath() {
    return StringUtils.equals(locoRequestHeader.getActualDevice(), "smartphone") ? SP_PATH : PC_PATH;
  }

  /**
   * エラーページ用のSpaceIdを取得する
   */
  private String getErrorSpaceId() {
    return locoRequestHeader.getActualDevice().equals("smartphone") ? "2080047631" : "2080042987";
  }

}
