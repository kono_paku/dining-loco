package jp.co.yahoo.dining.frontend.controller.form;

import jp.co.yahoo.dining.frontend.infrastructure.Dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TenpoInfo implements Serializable, Dto {

  @Pattern(regexp = "smartphone")
  private String device;

  private String query;

  @Min(0)
  @Digits(integer = 3, fraction = 8)
  private Float lat;

  @Min(0)
  @Digits(integer = 3, fraction = 8)
  private Float lon;

  @Min(0)
  private Integer dist;

  private String bbox;

  private String ac;

  private String gc;

  private String sc;

  private String bc;

  private String gid;

  private String cid;

  private String reservation_date;

  @Min(1)
  @Max(10001)
  private String start;

  @Min(1)
  @Max(100)
  private String results;

  private String sort;

  @Min(0)
  private Integer minprice;

  @Min(0)
  private Integer maxprice;

  @Min(0)
  private Integer lunch_minprice;

  @Min(0)
  private Integer lunch_maxprice;

  @Min(0)
  private Integer dinner_minprice;

  @Min(0)
  private Integer dinner_maxprice;

  private String reservation;

  private String flag;

  private String open;

  @Min(1)
  @Max(1)
  private Integer buzz;

  private String facet;

}
