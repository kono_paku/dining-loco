package jp.co.yahoo.dining.frontend.controller.api;


import jp.co.yahoo.dining.frontend.controller.form.TenpoInfo;
import jp.co.yahoo.dining.frontend.domain.PlanDetail;
import jp.co.yahoo.dining.frontend.domain.Tenpo;
import jp.co.yahoo.dining.frontend.service.PlanService;
import jp.co.yahoo.dining.frontend.service.TenpoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


@Slf4j
@RestController
@RequiredArgsConstructor
public class TenpoProxyController {

  private final TenpoService tenpoService;
  private final PlanService planService;

  @GetMapping("/v1/api/tenpo")
  public Tenpo test(@Valid TenpoInfo tenpoInfo) {
    return tenpoService.getTenpoInfo(tenpoInfo);
  }

  @GetMapping("/v1/api/course")
  public PlanDetail test(String tenpoId, String courseId) {
    return planService.getPlanDetail(tenpoId, courseId);
  }

}
