package jp.co.yahoo.dining.frontend.infrastructure.rest;

import jp.co.yahoo.dining.frontend.domain.PlanStock;
import jp.co.yahoo.dining.frontend.repository.PlanStockTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.text.SimpleDateFormat;
import java.util.Calendar;

@Component
public class PlanStockTempleteImpl extends LocoRestTemplate implements PlanStockTemplate {

  @Autowired
  @Qualifier("planStock")
  private RestTemplate restTemplate;

  @Autowired
  private PlanStockAPIConfig planStockAPIConfig;

  @Override
  public RestTemplate rt() {
    return restTemplate;
  }

  @Override
  protected HttpHeaders setAuthority(HttpHeaders headers) {
    return headers;
  }

  @Override
  public PlanStock getPlanStocks(final String gid) {
    // date などの指定が必要になったら、パラメータを受け付けるようにする
    UriComponentsBuilder builder = createRequest(planStockAPIConfig.getPlanStock())
      .queryParam("gid", gid)
      .queryParam("appid", planStockAPIConfig.getAppId())
      .queryParam("date", new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime()))
      .queryParam("time_from", 1800)
      .queryParam("time_to", 2200)
      .queryParam("response_group", "simple");
    return getEntity(PlanStock.class, builder);
  }
}
