package jp.co.yahoo.dining.frontend.infrastructure.rest;

import jp.co.yahoo.dining.frontend.controller.form.ImageSearchInfo;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import jp.co.yahoo.dining.frontend.domain.ImageSearch;
import jp.co.yahoo.dining.frontend.repository.ImageSearchTemplate;

@Component
public class ImageSearchTemplateImpl extends LocoRestTemplate implements ImageSearchTemplate {
  private final RestTemplate rt;
  private final ImageSearchAPIConfig imageSearchAPIConfig;

  public ImageSearchTemplateImpl(@Qualifier("imageSearch") RestTemplate rt, ImageSearchAPIConfig imageSearchAPIConfig) {
    this.rt = rt;
    this.imageSearchAPIConfig = imageSearchAPIConfig;
  }

  @Override
  public RestTemplate rt() {
    return rt;
  }

  @Override
  public ImageSearch getImageSearch(final ImageSearchInfo imageSearchInfo) {
    UriComponentsBuilder builder = createRequest(imageSearchAPIConfig.getImageList())
      .queryParam("appid", imageSearchAPIConfig.getAppId())
      .queryParam("gid", imageSearchInfo.getGid())
      .queryParam("results", imageSearchInfo.getResults())
      .queryParam("start", imageSearchInfo.getStart())
      .queryParam("output", "json");
    return getEntity(ImageSearch.class, builder);
  }

  @Override
  protected HttpHeaders setAuthority(HttpHeaders headers) {
    return headers;
  }
}
