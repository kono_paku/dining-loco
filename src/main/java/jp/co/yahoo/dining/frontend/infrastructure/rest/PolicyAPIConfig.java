package jp.co.yahoo.dining.frontend.infrastructure.rest;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "extension.api.policy")
public class PolicyAPIConfig {
  private String planPolicy;
  private String appId;
}
