package jp.co.yahoo.dining.frontend.repository;


import jp.co.yahoo.dining.frontend.domain.Promotion;

public interface PromotionTemplate {

  Promotion getPromotion();

}
