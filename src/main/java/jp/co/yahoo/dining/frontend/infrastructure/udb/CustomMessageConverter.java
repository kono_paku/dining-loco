package jp.co.yahoo.dining.frontend.infrastructure.udb;

import com.google.common.collect.Iterables;
import jp.co.yahoo.dining.frontend.infrastructure.udb.model.UdbResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * UDBレスポンス用のカスタムコンバーター
 */
public class CustomMessageConverter extends MappingJackson2HttpMessageConverter {

  private static final String RESPONSE_KEY_ADDRESS = "demog";
  private static final String RESPONSE_KEY_PREMIUM = "jp_prem_user";

  private static final String RESPONSE_ADRESS_REGEX = "PS-utf8(.*).*PS-ck";

  private final Decoder base64;

  public CustomMessageConverter() {
    super();
    this.base64 = Base64.getDecoder();
  }

  /**
   * UDBの結果をまずMapでもらって、バリューを全部デコーディングしてモデルに入れます。
   */
  @Override
  public Object read(Type type, Class<?> contextClass, HttpInputMessage inputMessage)
    throws IOException, HttpMessageNotReadableException {
    if (type.getTypeName().equals(UdbResponse.class.getTypeName())) {
      Map<String, String> response = (Map<String, String>) super.read(Map.class, contextClass, inputMessage);
      Map<String, String> decodedResponse = new HashMap<>();

      for (String key : response.keySet()) {
        String decodedValue = decodingBase64(response.get(key));
        if (key.equals(RESPONSE_KEY_ADDRESS)) {
          decodedResponse.put(key, getAddress(decodedValue));
        } else if (key.equals(RESPONSE_KEY_PREMIUM)) {
          decodedResponse.putAll(getPremiumType(decodedValue));
        } else {
          decodedResponse.put(key, decodedValue);
        }
      }
      return getObjectMapper().convertValue(decodedResponse, UdbResponse.class);
    }
    return super.read(type, contextClass, inputMessage);
  }

  private String getAddress(String decodedValue) {
    Pattern pattern = Pattern.compile(RESPONSE_ADRESS_REGEX);
    Matcher matcher = pattern.matcher(decodedValue);

    return matcher.find() ? matcher.group(1).trim() : null;
  }

  private Map<String, String> getPremiumType(String decodedValue) throws IOException {
    Map<String, Map<String, String>> stringMapMap = getObjectMapper().readValue(decodedValue, Map.class);
    return Iterables.getLast(stringMapMap.values());
  }

  private String decodingBase64(String data) {
    return StringUtils.isEmpty(data) ? null : new String(base64.decode(data));
  }

}
