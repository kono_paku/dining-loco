package jp.co.yahoo.dining.frontend.repository;

import jp.co.yahoo.dining.frontend.domain.LatestInformation;

/**
 * 最新情報を取得する
 */
public interface LatestInformationTemplate {
  /**
   * 指定した店舗の最新情報を取得する
   *
   * @param gid 最新情報を取得したい店舗のgid
   * @return 指定したgidの店舗の最新情報一覧
   */
  LatestInformation getLatestInformation(String gid);
}
