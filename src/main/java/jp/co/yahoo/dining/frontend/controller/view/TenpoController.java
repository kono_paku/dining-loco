package jp.co.yahoo.dining.frontend.controller.view;

import com.fasterxml.jackson.core.JsonProcessingException;
import jp.co.yahoo.dining.frontend.controller.LocoRequestHeader;
import jp.co.yahoo.dining.frontend.domain.PlanDetail;
import jp.co.yahoo.dining.frontend.domain.Tenpo;
import jp.co.yahoo.dining.frontend.domain.Video.Result;
import jp.co.yahoo.dining.frontend.infrastructure.LocoResourceLoader;
import jp.co.yahoo.dining.frontend.service.PlanService;
import jp.co.yahoo.dining.frontend.service.VideoService;
import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import jp.co.yahoo.dining.frontend.domain.AdInfeed;
import jp.co.yahoo.dining.frontend.service.AdInfeedService;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.utils.URLEncodedUtils;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@Controller
public class TenpoController {

  @Value("${extension.cidList.tenpoVideo}")
  private String tenpoVideoCid;

  @Value("${extension.approach.poiWeb}")
  private String approachpoiWeb;

  @Value("${extension.approach.locoDomain}")
  private String locoDomain;

  private final VideoService videoService;

  private final AdInfeedService adInfeedService;

  private final LocoResourceLoader locoResourceLoader;

  private final PlanService planService;

  private final LocoRequestHeader locoRequestHeader;

  @GetMapping("/place/g-{gid}")
  public String getPlace(Model model, HttpServletRequest request,
    @RequestParam(name = "no_redirect", required = false, defaultValue = "") String noRedirect,
    @RequestParam(name = "utm_source", required = false, defaultValue = "") String utmSource) throws IOException {
    /**
     * 動画
     * cidListにccbf79674338330c7ce5557ace5a18ecが存在する場合、
     * 全Uidで動画APIを叩き、null以外の最初contensIdを取得
     */
    Tenpo tenpo = (Tenpo)model.asMap().get("tenpo");
    // リダイレクト処理(スマホ かつ パラメータ付与確認 かつ approachからの遷移ではないことを確認)
    if(StringUtils.equals(locoRequestHeader.getActualDevice(), "smartphone")) {
      if (!(StringUtils.equals(noRedirect, "1")) && (StringUtils.equals(utmSource, "dd_spot") || StringUtils.equals(utmSource, "dd_local"))) {
        // グルメ店舗ではないかの確認
        if (tenpo.getFeature()[0].getProperty().getGenre() != null) {
          if (Arrays.stream(tenpo.getFeature()[0].getProperty().getGenre())
            .noneMatch(genre -> genre.getCode().startsWith("01"))) {
            StringBuilder urlBuilder = new StringBuilder();
            String callbackUrl = urlBuilder.append(locoDomain).append(request.getRequestURI()).append("?").append(request.getQueryString()).toString();
            // アプローチへのリダイレクトURL作成
            String redirectUrl =
              URLDecoder.decode(callbackUrl, StandardCharsets.UTF_8.name()) + "&no_redirect=1";
            return "redirect:" + approachpoiWeb + "?src=" + URLEncoder.encode(redirectUrl, StandardCharsets.UTF_8.name()).replace("+", "%20");
          }
        }
      }
    }
    if (Arrays.stream(tenpo.getFeature()[0].getInternal().getCidList()).anyMatch(cid -> cid.equals(tenpoVideoCid))) {
      for (String uid : tenpo.getFeature()[0].getInternal().getUidList()) {
        Result[] video = videoService.getVideo(uid);
        if (video != null) {
          model.addAttribute("videoHasParaMovie", video[0].getChannelId() == 833);
          model.addAttribute("videoContentUid", uid);
          model.addAttribute("videoContentId", video[0].getContentId());
          break;
        }
      }
    }

    /**
     * Adインフィード広告
     * リファラーからキーワードパラメーターの取得し、リスティングAPIを叩く
     */
    String searchKeyword = adInfeedService.getRefererKeyword(request.getHeader("referer"));
    AdInfeed adInfeed = adInfeedService.getAdInfeedList(searchKeyword);
    if (adInfeed != null) {
      model.addAttribute("adInfeed", adInfeed);
      model.addAttribute("adInfeedJson", locoResourceLoader.getJson(adInfeed));
    }
    return "place/index";
  }

  @GetMapping("/place/g-{gid}/map")
  public String getMap(Model model) {
    model.addAttribute("isMapPage", true);
    return "place/map/index";
  }

  @GetMapping("/place/g-{gid}/coupon")
  public String getCoupon() {
    return "place/coupon/index";
  }

  @GetMapping("/place/g-{gid}/photo")
  public String getPhoto() {
    return "place/photo/index";
  }

  @GetMapping("/place/g-{gid}/course/P{courseId}")
  public String getCourse(@PathVariable final String courseId, Model model) throws JsonProcessingException {
    Tenpo tenpo = (Tenpo)model.asMap().get("tenpo");
    PlanDetail planDetail = planService.getPlanDetail(tenpo, courseId);
    model.addAttribute("course", planDetail);
    model.addAttribute("courseDetailJson", locoResourceLoader.getJson(planDetail));
    return "place/course/detail";
  }

  @GetMapping("/place/g-{gid}/menu")
  public String getMenu() {
    return "place/menu/index";
  }

  @GetMapping("/place/g-{gid}/plan")
  public String getPlan() {
    return "place/plan/index";
  }

  @GetMapping("/place/g-{gid}/course")
  public String getCourse(@PathVariable final String gid, @RequestParam(name = "date", required = false) Optional<String> date, Model model) throws JsonProcessingException {
    model.addAttribute("courseListJson", locoResourceLoader.getJson(planService.getPlanList(gid, date)));
    return "place/course/index";
  }

  @GetMapping("/place/g-{gid}/marriage")
  public String getMarriage() { return "place/marriage/index"; }

  @GetMapping("/place/g-{gid}/wedding")
  public String getWedding() { return "place/wedding/index"; }

  @GetMapping("/place/g-{gid}/bridal")
  public String getBridal() { return "place/bridal/index"; }
}
