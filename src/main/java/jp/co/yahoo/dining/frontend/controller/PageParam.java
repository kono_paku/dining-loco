package jp.co.yahoo.dining.frontend.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ページパラメータ用クラス
 */

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PageParam {
  private String service;
  private String pagetype;
  private String conttype;
  private String opttype;
  private String apptype;
  private String status;
  @JsonProperty("cat_path")
  private String catPath;
  private String ctsid;
  private String prtnr;
  private String acttype;
  private String metakwd;
  @JsonProperty("auth_out")
  private String authOut;
  private String nonepv;
  private String vtestid;
  private String vtgrpid;
  @JsonProperty("v_pkjp")
  private String vpkjp;
  private String area;
  private String genre;
  private String uid;
  private String cid;
  private String locoarea;
  private String station;
  private String wwd;
  private String corsid;
  private String premium;
  private Integer isdining;
}
