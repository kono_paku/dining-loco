package jp.co.yahoo.dining.frontend.repository;

import jp.co.yahoo.dining.frontend.infrastructure.masthead.model.YahooHeadWrapper;

public interface MastHeadTemplate {

  YahooHeadWrapper getMastHead(String cookie, String spaceId, String device, String done);

}
