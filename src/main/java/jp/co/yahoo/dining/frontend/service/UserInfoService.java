package jp.co.yahoo.dining.frontend.service;

import jp.co.yahoo.dining.frontend.domain.UserInfo;
import jp.co.yahoo.dining.frontend.infrastructure.ncookie.NcookieService;
import jp.co.yahoo.dining.frontend.infrastructure.udb.UdbService;
import jp.co.yahoo.dining.frontend.infrastructure.udb.model.UdbResponse;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Service
@AllArgsConstructor
public class UserInfoService {

  private final UdbService udbService;
  private final NcookieService ncookieService;

  public UserInfo getUserInfo(String cookie, HttpServletRequest request) throws IOException {
    UserInfo userInfo = new UserInfo();
    if (StringUtils.isEmpty(cookie) || !isValidLoginCookie(cookie, request)) {
      userInfo.setLoginStatus(false);
      userInfo.setUserDetail(new UserInfo.UserDetail());
      return userInfo;
    }
    UdbResponse udbResponse = udbService.getUdbResponse(cookie);
    if (udbResponse == null || udbResponse.getYid() == null) {
      userInfo.setLoginStatus(false);
      userInfo.setUserDetail(new UserInfo.UserDetail());
      return userInfo;
    }
    Integer premiumType = udbResponse.getPremiumType();
    UserInfo.UserDetail detail = new UserInfo.UserDetail();
    detail.setGuid(udbResponse.getGuid());
    detail.setYid(udbResponse.getYid());
    detail.setArea(udbResponse.getDemog());
    detail.setIsPremium(premiumType != null);
    detail.setPremiumType(premiumType);
    detail.setSoftbankBundle(isSoftbankBundle(udbResponse));
    detail.setYMobileBundle(isYMBundle(udbResponse));
    detail.setSoftBankCooperation(udbResponse.getSoftBankCooperation());

    userInfo.setLoginStatus(true);
    userInfo.setUserDetail(detail);

    return userInfo;
  }

  private Integer isSoftbankBundle(UdbResponse udbResponse) {
    return ((udbResponse.getPremiumType() != null && udbResponse.getPremiumType() == 10) || (udbResponse.getPremiumType() != null && udbResponse.getSoftbankBundle() == 1)) ? 1 : 0;
  }

  private Integer isYMBundle(UdbResponse udbResponse) {
    return ((udbResponse.getPremiumType() != null && udbResponse.getPremiumType() == 6) || (udbResponse.getPremiumType() != null && udbResponse.getYMobileBundle() == 1)) ? 1 : 0;
  }

  /**
   * Y/T/N Cookieへのバリデーション
   * @param cookie 全てのクッキー情報
   * @param request サーブレットレクエスト
   * @return Y/T/N Cookieの確認結果
   */
  private boolean isValidLoginCookie(String cookie, HttpServletRequest request) throws IOException {
    return isContainsYTCookie(cookie) && ncookieService.validateCookie(request);
  }

  /**
   * Y/T Cookieがリクエストヘッダーに含まれているか確認
   * @param cookie 全てのクッキー情報
   * @return Y/T Cookie確認結果
   */
  private boolean isContainsYTCookie (String cookie) {
    boolean isContains = false;
    if(StringUtils.isNotEmpty(cookie)) {
      String tempCookie = ";" + cookie.replaceAll(" ", "");
      isContains = (tempCookie.contains(";Y=") && tempCookie.contains(";T="));
    }
    return isContains;
  }

}
