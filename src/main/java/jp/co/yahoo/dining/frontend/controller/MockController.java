package jp.co.yahoo.dining.frontend.controller;

import jp.co.yahoo.dining.frontend.domain.Coupon;
import jp.co.yahoo.dining.frontend.domain.CpCoupon;
import jp.co.yahoo.dining.frontend.domain.LatestInformation;
import jp.co.yahoo.dining.frontend.domain.PlanDetail;
import jp.co.yahoo.dining.frontend.domain.PlanList;
import jp.co.yahoo.dining.frontend.domain.Promotion;
import jp.co.yahoo.dining.frontend.domain.Tenpo;
import jp.co.yahoo.dining.frontend.domain.Video;

import jp.co.yahoo.dining.frontend.controller.form.ImagesInfo;
import jp.co.yahoo.dining.frontend.controller.form.TenpoInfo;
import jp.co.yahoo.dining.frontend.domain.*;
import jp.co.yahoo.dining.frontend.infrastructure.LocoResourceLoader;
import jp.co.yahoo.dining.frontend.infrastructure.masthead.model.YahooHeadWrapper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Map;

/**
 * （お知らせ）こちらのAPIは、Stubデータを呼び出すためのデータです。
 */
//@Profile({"local", "local-prod"})
@RequiredArgsConstructor
@RequestMapping("mock")
@RestController
public class MockController {

  private final LocoResourceLoader locoResourceLoader;

  @GetMapping("/tenpo")
  public Tenpo getTenpo(final TenpoInfo tenpoInfo) throws IOException {
    String gid = tenpoInfo.getGid();
    if (StringUtils.isNotEmpty(tenpoInfo.getGid())) {
      return locoResourceLoader.getResource(Tenpo.class, "classpath:json/tenpo/" + gid + ".json");
    }
    return locoResourceLoader.getResource(Tenpo.class, "classpath:json/tenpo/multitenpo/multitenpo.json");
  }

  @GetMapping("/plan/list")
  public PlanList getPlanList(@RequestParam String gid) throws IOException {
    return locoResourceLoader.getResource(PlanList.class, "classpath:json/plan/list/" + gid + ".json");
  }

  @GetMapping("/plan/detail")
  public PlanDetail getPlanDetail(@RequestParam String tenpoid, @RequestParam String planid) throws IOException {
    return locoResourceLoader.getResource(PlanDetail.class, "classpath:json/plan/detail/" + tenpoid + "_" + planid + ".json");
  }

  @GetMapping("/video")
  public Video getVideoContentsId(@RequestParam("cooperation_id") String uid) throws IOException {
    return locoResourceLoader.getResource(Video.class, "classpath:json/video/" + uid + ".json");
  }

  @GetMapping("/cpcoupon")
  public CpCoupon getCpCoupon(@RequestParam String gid) throws IOException {
    return locoResourceLoader.getResource(CpCoupon.class, "classpath:json/cpcoupon/" + gid + ".json");
  }

  @GetMapping("/coupon")
  public Coupon getMashUpCoupon(@RequestParam String gid) throws IOException {
    return locoResourceLoader.getResource(Coupon.class, "classpath:json/coupon/" + gid + ".json");
  }

  @GetMapping("/kuchikomi")
  public Kuchikomi getKuchikomiList(@RequestParam final String gid) throws IOException {
    return locoResourceLoader.getResource(Kuchikomi.class, "classpath:json/commentSearch/" + gid +  ".json");
  }

  @GetMapping("/masthead")
  public YahooHeadWrapper getMastHead() throws IOException {
    return locoResourceLoader.getResource(YahooHeadWrapper.class, "classpath:json/masthead/masthead.json");
  }

  @GetMapping("/promotion")
  public Promotion getPromotion() throws IOException {
    return locoResourceLoader.getResource(Promotion.class, "classpath:json/promotion/promotion.json");
  }

  @GetMapping("/latestInformation")
  public LatestInformation getLatestInformation(@RequestParam("gid") String gid) throws IOException {
    return locoResourceLoader.getResource(LatestInformation.class, "classpath:json/latestInformation/" + gid + ".json");
  }

  @GetMapping("/calendar")
  public Holiday getHolidays() throws IOException {
    return locoResourceLoader.getResource(Holiday.class, "classpath:json/calendar/holiday.json");
  }

  @PostMapping("/resizeImage")
  public ResizedImage getResizedImage(final ImagesInfo info) throws IOException {
    return locoResourceLoader.getResource(ResizedImage.class, "classpath:json/resizeImage/" + info.getGid() + ".json");
  }

  @GetMapping("/planStock")
  public PlanStock getPlanStock(final String gid) throws IOException {
    return locoResourceLoader.getResource(PlanStock.class, "classpath:json/planStock/PaOkPxkI4Q6.json");
  }

  @GetMapping("/imageSearch")
  public ImageSearch getImageSearch(@RequestParam final String gid) throws IOException {
    return locoResourceLoader.getResource(ImageSearch.class, "classpath:json/imageSearch/" + gid + ".json");
  }

  @GetMapping("/wedding")
  public Wedding getWedding(@RequestParam String gid) throws IOException {
    return locoResourceLoader.getResource(Wedding.class, "classpath:json/wedding/" + gid + ".json");
  }

  @GetMapping("/planPolicy")
  public PlanPolicy getPlanPolicy(@RequestParam String tenpoid) throws IOException {
    return locoResourceLoader.getResource(PlanPolicy.class, "classpath:json/planPolicy/" + tenpoid + ".json");
  }

  @GetMapping("/udb")
  public Map<String,Object> getUdbResponse() throws IOException {
    return locoResourceLoader.getResource(Map.class, "classpath:json/udb/udb.json");
  }

  @GetMapping("/adInfeed")
  public AdInfeed getAdInfeed(@RequestParam final String keywords) throws IOException {
    return locoResourceLoader.getResource(AdInfeed.class, "classpath:json/adInfeed/" + keywords + ".xml");
  }

  @RequestMapping("/keep")
  public KeepItem addKeepItem() throws IOException {
    return locoResourceLoader.getResource(KeepItem.class, "classpath:json/keep/add.json");
  }

  @RequestMapping("/keep/gid:{gid}")
  public KeepItem removeKeepItem() throws IOException {
    return locoResourceLoader.getResource(KeepItem.class, "classpath:json/keep/remove.json");
  }
}
