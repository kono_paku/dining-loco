package jp.co.yahoo.dining.frontend.infrastructure.athenz;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by jchung on 2017/11/10.
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "extension.athenz")
public class AthenzProperties {

  // Athenzのsecret
  private String athenzSecret;

  // Athenzのdomain名。
  private String domain;

  // Athenzのservice名
  private String service;

  // RightKeyが登録になってるRoleDB。yahoo.<RoleDB>
  private String appId;

  // YCA証明書を引っ張ってくるサーバ
  private String server;

  // YCA証明書のURL
  private String ycaUrl;

  // 呼び出すクライアントのIPアドレス
  private String clientIp;

}
