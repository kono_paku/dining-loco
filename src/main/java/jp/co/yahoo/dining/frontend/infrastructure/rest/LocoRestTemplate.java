package jp.co.yahoo.dining.frontend.infrastructure.rest;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import jp.co.yahoo.dining.frontend.infrastructure.Dto;
import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;

public abstract class LocoRestTemplate {

  private TypeReference parameterTypeReference;
  private ObjectMapper mapper;

  public LocoRestTemplate() {
    this.parameterTypeReference = new TypeReference<Map<String, String>>(){};
    this.mapper = new ObjectMapper();
    this.mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
  }

  public abstract RestTemplate rt();

  /**
   * 各API固有の認証情報追加
   * TODO:ヘッダ認証のことがなければ消す！
   */
  protected abstract HttpHeaders setAuthority(HttpHeaders headers);

  private <T extends Dto> HttpEntity<T> getHttpEntity(T param) {
    HttpHeaders headers = new HttpHeaders();
    headers = setAuthority(headers);
    headers.setContentType(MediaType.APPLICATION_JSON);
    return new HttpEntity<>(param, headers);
  }

  private HttpEntity<Object> getHttpEntity(HttpHeaders headers) {
    headers = setAuthority(headers);
    headers.setContentType(MediaType.APPLICATION_JSON);
    return new HttpEntity<>(headers);
  }

  private <T extends Dto> HttpEntity<T> getHttpEntity(HttpHeaders headers, T param) {
    return new HttpEntity<>(param, headers);
  }

  private HttpEntity getHttpEtity(){
    HttpHeaders headers = new HttpHeaders();
    headers = setAuthority(headers);
    headers.setContentType(MediaType.APPLICATION_JSON);
    return new HttpEntity(headers);
  }

  protected <T extends ExternalEntity> T getEntity(final Class<T> clazz, final String url, final Object... uriVariables) {
    return rt().getForObject(url, clazz, uriVariables);
  }

  protected <T extends ExternalEntity> T getEntity(final Class<T> clazz, UriComponentsBuilder builder) {
    return rt().getForObject(builder.build().toUri(), clazz);
  }

  protected <T extends ExternalEntity> T getEntity(final Class<T> clazz, HttpHeaders headers, UriComponentsBuilder builder) {
    return  rt().exchange(builder.build().toUri(), HttpMethod.GET, getHttpEntity(headers), clazz).getBody();
  }

  protected <T extends ExternalEntity> T getEntity(final Class<T> clazz, UriComponentsBuilder builder, final Dto params) {
    MultiValueMap<String, String> convertedParams = new LinkedMultiValueMap<>();
    convertedParams.setAll(mapper.convertValue(params, parameterTypeReference));
    return rt().getForObject(builder.queryParams(convertedParams).build().toUri(), clazz);
  }

  protected <T extends ExternalEntity> T postEntity(final Class<T> clazz, final String url, final Dto param) {
    HttpEntity<Dto> entity = getHttpEntity(param);
    return rt().exchange(url, HttpMethod.POST, entity, clazz).getBody();
  }

  protected <T extends ExternalEntity> T postEntity(final Class<T> clazz, HttpHeaders headers, final String url, final Dto param) {
    HttpEntity<Dto> entity = getHttpEntity(headers, param);
    return rt().exchange(url, HttpMethod.POST, entity, clazz).getBody();
  }

  protected <T extends ExternalEntity> T deleteEntity(final Class<T> clazz, HttpHeaders headers, final String url) {
    HttpEntity<Object> entity = getHttpEntity(headers);
    return rt().exchange(url, HttpMethod.DELETE, entity, clazz).getBody();
  }

  protected UriComponentsBuilder createRequest(String urlPath) {
    return UriComponentsBuilder.fromUriString(rt().getUriTemplateHandler().expand(urlPath).toString());
  }

}
