package jp.co.yahoo.dining.frontend.infrastructure.rest;

import jp.co.yahoo.dining.frontend.controller.form.TenpoInfo;
import jp.co.yahoo.dining.frontend.domain.Tenpo;
import jp.co.yahoo.dining.frontend.repository.TenpoTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class TenpoTemplateImpl extends LocoRestTemplate implements TenpoTemplate {

  /**
   * LocalSearchAPIのRestTemplate
   */
  @Qualifier("tenpo")
  @Autowired
  private RestTemplate rt;

  /**
   * LocalSearchAPIのエンドポイント一覧
   */
  @Autowired
  private TenpoAPIConfig tenpoAPIConfig;

  @Override
  public RestTemplate rt() {
    return rt;
  }

  /**
   * 店舗IDに紐づく店舗情報を取得
   *
   * @param gid 店舗ID not {@code null}
   * @throw ValidationException statusが200でNULLが帰ってきた場合、検査例外処理
   */
  @Override
  public Tenpo getTenpoInfo(String gid) {
    UriComponentsBuilder builder = createRequest(tenpoAPIConfig.getTenpoInfo())
      .queryParam("appid", tenpoAPIConfig.getAppId())
      .queryParam("output", "json")
      .queryParam("gid", gid);
    return getEntity(Tenpo.class, builder);
  }

  /**
   * LSBE情報に紐づく店舗情報を取得
   *
   * @param tenpoInfo LSBE Parms not {@code null}
   * @throw ValidationException statusが200でNULLが帰ってきた場合、検査例外処理
   */
  @Override
  public Tenpo getTenpoInfo(TenpoInfo tenpoInfo) {
    UriComponentsBuilder builder = createRequest("http://be.lsbe.yahoo.co.jp/search")
      .queryParam("appid", tenpoAPIConfig.getAppId())
      .queryParam("output", "json");
    return getEntity(Tenpo.class, builder, tenpoInfo);
  }

  @Override
  protected HttpHeaders setAuthority(HttpHeaders headers) {
    return headers;
  }
}
