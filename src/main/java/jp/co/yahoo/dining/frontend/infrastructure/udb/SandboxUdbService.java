package jp.co.yahoo.dining.frontend.infrastructure.udb;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import javax.annotation.PostConstruct;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * TODO : 今後UDB側のところをどう修正すればいいか確認必要。
 *
 * Created by jchung on 2017/11/08.
 */
@Slf4j
@Service
@Profile("local")
public class SandboxUdbService extends UdbService {

  private static final int CONNECT_TIMEOUT = 5000;
  private static final int READ_TIMEOUT = 5000;

  private RestTemplate restTemplate;

  @PostConstruct
  public void setup() {
    this.restTemplate = new RestTemplateBuilder()
        .requestFactory(new SkipSslVerificationHttpRequestFactory())
        .setConnectTimeout(CONNECT_TIMEOUT)
        .setReadTimeout(READ_TIMEOUT)
        .messageConverters(new CustomMessageConverter())
        .build();
  }

  @Override
  public RestTemplate getRestTemplate() {
    return restTemplate;
  }

  public static class SkipSslVerificationHttpRequestFactory extends SimpleClientHttpRequestFactory {

    protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
      try {
        if (connection instanceof HttpsURLConnection) {
          this.prepareHttpsConnection((HttpsURLConnection) connection);
        }
      } catch (Exception e) {
      }
      super.prepareConnection(connection, httpMethod);
    }

    private void prepareHttpsConnection(HttpsURLConnection connection) throws Exception {
      connection.setHostnameVerifier(new SkipSslVerificationHttpRequestFactory.SkipHostnameVerifier());
      connection.setSSLSocketFactory(this.createSslSocketFactory());
    }

    private SSLSocketFactory createSslSocketFactory() throws Exception {
      SSLContext context = SSLContext.getInstance("TLS");
      context.init((KeyManager[]) null, new TrustManager[]{new SkipSslVerificationHttpRequestFactory.SkipX509TrustManager()}, new SecureRandom());
      return context.getSocketFactory();
    }

    private static class SkipX509TrustManager implements X509TrustManager {

      public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
      }

      public void checkClientTrusted(X509Certificate[] chain, String authType) {
      }

      public void checkServerTrusted(X509Certificate[] chain, String authType) {
      }
    }

    private class SkipHostnameVerifier implements HostnameVerifier {

      public boolean verify(String s, SSLSession sslSession) {
        return true;
      }
    }
  }
}
