package jp.co.yahoo.dining.frontend.service;

import jp.co.yahoo.dining.frontend.domain.Video;
import jp.co.yahoo.dining.frontend.domain.Video.Result;
import jp.co.yahoo.dining.frontend.repository.VideoTemplate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;

@Slf4j
@Service
@RequiredArgsConstructor
public class VideoService {

  private final VideoTemplate videoTemplate;

  /**
   * LocalSearchから取ったUIDを利用して、その店舗に登録になってる動画オブジェクトを取得。
   * @param uid
   * @return
   */
  public Result[] getVideo(String uid) {
    // memo: この店舗に動画があるかどうかわからない状況なので、一回APIを呼び出すしかない。
    try {
      Video videoContentId = videoTemplate.getVideo(uid);
      return videoContentId.getResultSet().getResult();
    } catch (HttpStatusCodeException e) {
      log.debug("この店舗は動画データが存在しません。: " + uid);
    }
    return null;
  }

}
