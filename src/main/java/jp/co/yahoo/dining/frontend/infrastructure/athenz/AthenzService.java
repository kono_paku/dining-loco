package jp.co.yahoo.dining.frontend.infrastructure.athenz;

import com.yahoo.athenz.tenant.YCACertificateClient;
import javax.annotation.PostConstruct;
import jp.co.yahoo.dining.frontend.infrastructure.ups.UpsUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Created by jchung on 2017/11/08.
 */
@Slf4j
@Service
public class AthenzService {

  private static final String ATHENZ_SECRET_ENV_KEY = "athenz.secret";
  private static final String ATHENZ_YCA_URL_ENV_KEY = "athenz.yca.client.base.url";

  @Autowired
  private AthenzProperties athenzProperties;

  private YCACertificateClient ycaCertificateClient;

  @PostConstruct
  public void setUp() {
    // YCA API URLを登録（properties）
    String ycaUrl = athenzProperties.getYcaUrl();
    System.setProperty(ATHENZ_YCA_URL_ENV_KEY, ycaUrl);
    // AthenzSecretを環境変数に登録（UPS）
    String athenzSecret = UpsUtils.getUserProvidedValue("athenz_secret");
    if (StringUtils.isEmpty(athenzSecret)) {
      // UPSからAthenzSecretを取れなかった時に、propertiesからAthenzSecretをとってくる
      athenzSecret = athenzProperties.getAthenzSecret();
    }
    System.setProperty(ATHENZ_SECRET_ENV_KEY, athenzSecret);
    // AthenzSecretを利用して、YCACertificateClientを生成。
    this.ycaCertificateClient = buildYcaCertificateClient();
  }

  private YCACertificateClient buildYcaCertificateClient() {
    String domain = athenzProperties.getDomain();
    String service = athenzProperties.getService();
    String instanceIp = UpsUtils.getInstanceIp();
    YCACertificateClient client = new YCACertificateClient(domain, service);
    if (!StringUtils.isEmpty(instanceIp)) {
      client.setIP(instanceIp);
    }
    return client;
  }

  public String getYCACertification() {
    try {
      String appId = athenzProperties.getAppId();
      String serverHostName = athenzProperties.getServer();
      return ycaCertificateClient.getCertificateString(appId, serverHostName);
    } catch (Exception e) {
      log.error("YCA証明書取得に失敗しました。", e);
    }
    return null;
  }
}


