package jp.co.yahoo.dining.frontend.infrastructure.rest;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import jp.co.yahoo.dining.frontend.controller.LocoRequestHeader;
import jp.co.yahoo.dining.frontend.domain.AdInfeed;
import jp.co.yahoo.dining.frontend.repository.AdInfeedTemplate;

@Component
public class AdInfeedTemplateImpl extends LocoRestTemplate implements AdInfeedTemplate {
  private final RestTemplate rt;
  private final AdInfeedAPIConfig adInfeedAPIConfig;
  private final LocoRequestHeader locoRequestHeader;

  public AdInfeedTemplateImpl(@Qualifier("adInfeed") RestTemplate rt, AdInfeedAPIConfig adInfeedAPIConfig, LocoRequestHeader locoRequestHeader) {
    this.rt = rt;
    this.adInfeedAPIConfig = adInfeedAPIConfig;
    this.locoRequestHeader = locoRequestHeader;
  }

  @Override
  public RestTemplate rt() {
    return rt;
  }

  @Override
  public AdInfeed getAdInfeed(final String keywords) {
    String device = locoRequestHeader.getActualDevice();
    String partner = adInfeedAPIConfig.getPartner();
    String did = adInfeedAPIConfig.getDid();
    if (device.equals("smartphone")) {
      partner = adInfeedAPIConfig.getPartnerSp();
      did = adInfeedAPIConfig.getDidSp();
    }
    UriComponentsBuilder builder = createRequest(adInfeedAPIConfig.getAd())
      .queryParam("keywords", keywords)
      .queryParam("affilData", adInfeedAPIConfig.getAffilData())
      .queryParam("partner", partner)
      .queryParam("did", did)
      .queryParam("keywordCharEnc", adInfeedAPIConfig.getKeywordCharEnc())
      .queryParam("outputCharEnc", adInfeedAPIConfig.getOutputCharEnc())
      .queryParam("outputCharEnc", adInfeedAPIConfig.getOutputCharEnc())
      .queryParam("serverUrl", adInfeedAPIConfig.getServerUrl())
      .queryParam("type", adInfeedAPIConfig.getType())
      .queryParam("maxCount", adInfeedAPIConfig.getMaxCount());

    AdInfeed adInfeed = getEntity(AdInfeed.class, builder);
    return adInfeed;
  }

  @Override
  protected HttpHeaders setAuthority(HttpHeaders headers) {
    return headers;
  }
}
