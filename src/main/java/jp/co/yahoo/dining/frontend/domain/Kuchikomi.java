package jp.co.yahoo.dining.frontend.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Arrays;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
public class Kuchikomi implements ExternalEntity {

  private ResultInfo resultInfo;
  private Feature[] feature;

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
  public static class ResultInfo {
    private Integer total;
    private Integer count;
    private Integer start;
    private Integer status;
    private BigDecimal latency;
    private String description;
    private String copyRight;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
  public static class Feature {
    private Property property;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
  public static class Property {
    private String uid;
    private String cassetteId;
    private Comment comment;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
  public static class Comment {
    private String id;
    private String body;
    private String subject;
    private String author;
    private String guid;
    private String rating;
    private String visitDate;
    private String scene;
    private String linkUrl;
    private String image1;
    private String image2;
    private String image3;
    private String image4;
    private String image5;
    private String image6;
    private String image7;
    private String image8;
    private String image9;
    private String image10;
    private String RatingFood;
    private String RatingService;
    private String RatingAtmosphere;
    private String RatingCostperformance;
    private String RatingDrink;
    private String RatingAccess;
    private String Budget;
    private String Atmosphere;
    private String Gender;
    private String Generation;
    private String Tag;
    private String Type;
    private String ItemId;
    private String createDate;
    private String updateDate;
    private String InputSource;
    private String UsefulCount;
    private String customExact1;
  }

  /**
   * ロコの口コミのAuthorを暗号化
   */
  public Kuchikomi convert(final String locoCid){
    // featureの中身があるかの確認
    if (this.getFeature().length > 0) {
        for (Feature x : this.getFeature()) {
          if (isLocoCid(x, locoCid)) {
            setEncryptAuthor(x);
          }
        }
    }
    return this;
  }

  /**
   * CIDがロコか判定
   * @param feature
   * @return true/false
   */
  private boolean isLocoCid(final Kuchikomi.Feature feature, final String locoCid) {
    return feature.getProperty().getCassetteId().equals(locoCid);
  }

  /**
   * 暗号化されたAuthorをセット
   * @param feature
   */
  private void setEncryptAuthor(Kuchikomi.Feature feature) {
    feature.getProperty().getComment().setAuthor(getConvertedAuthor(feature));
  }

  /**
   * Authorの暗号化処理
   * @param feature
   */
  private String getConvertedAuthor(final Kuchikomi.Feature feature){
  String author = feature.getProperty().getComment().getAuthor();
    return author != null ? author.replaceAll("(?<=^.{3}).*", "*****") : null;
  }
}
