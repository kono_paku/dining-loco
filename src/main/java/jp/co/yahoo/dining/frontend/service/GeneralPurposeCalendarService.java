package jp.co.yahoo.dining.frontend.service;

import jp.co.yahoo.dining.frontend.domain.Holiday;
import jp.co.yahoo.dining.frontend.repository.GeneralPurposeCalendarTemplate;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GeneralPurposeCalendarService {

  private final GeneralPurposeCalendarTemplate generalPurposeCalendarTemplate;

  /**
   * 汎用カレンダーAPIのプロキシを提供します。
   * @param year 年
   * @return 汎用カレンダーAPI実行結果
   */
  @Cacheable(cacheNames = "holidays")
  public Holiday searchHolidays(Integer year) {
    // Cacheを確認
    Holiday result = generalPurposeCalendarTemplate.searchHolidays(year);
    // Cacheにセット
    return result;
  }
}
