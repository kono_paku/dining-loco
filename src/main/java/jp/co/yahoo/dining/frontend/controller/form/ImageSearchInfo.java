package jp.co.yahoo.dining.frontend.controller.form;

import jp.co.yahoo.dining.frontend.controller.constraint.GId;
import lombok.Data;

import javax.validation.constraints.Max;

@Data
public class ImageSearchInfo {
  @GId
  private String gid;
  @Max(100)
  private Integer results;
  @Max(9999)
  private Integer start;
}
