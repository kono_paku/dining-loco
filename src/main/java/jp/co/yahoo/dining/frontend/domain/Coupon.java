package jp.co.yahoo.dining.frontend.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Coupon implements Serializable {

  private Cassette[] cassettes;

  @Data
  @Builder
  @AllArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Cassette {
    private String cid;
    private String name;
    private String logo;
    private Boolean isDining;
    private CouponInfo[] coupons;
  }

  @Data
  @Builder
  @AllArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class CouponInfo {
    private String pcUrl;
    private String spUrl;
    private String linkText;
    private String lifetimeStart;
    private String lifetimeEnd;
    private Boolean publish;
    private Integer prior;
    private Integer userCategory;
  }
}
