package jp.co.yahoo.dining.frontend.repository;

import jp.co.yahoo.dining.frontend.controller.form.ImageSearchInfo;
import jp.co.yahoo.dining.frontend.domain.ImageSearch;

/**
 * インメージサーチのデータ取得クラス
 */
public interface ImageSearchTemplate {

  ImageSearch getImageSearch(ImageSearchInfo imageSearchInfo);

}
