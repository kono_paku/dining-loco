package jp.co.yahoo.dining.frontend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jp.co.yahoo.dining.frontend.infrastructure.ExternalEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
public class ImageSearch implements ExternalEntity {

  private ResultInfo resultInfo;
  private Feature[] feature;

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
  public static class ResultInfo {
    private String copyRight;
    private Integer count;
    private String description;
    private BigDecimal latency;
    private Integer start;
    private Integer status;
    private Integer total;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
  public static class Feature {
    private Property property;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown=true)
  @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
  public static class Property {
    private String cassetteId;
    private Image image;
    private String uid;
  }

  @Data
  @NoArgsConstructor
  @JsonIgnoreProperties(ignoreUnknown=true)
  @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
  public static class Image {
    private String caption;
    private String category;
    private String createDate;
    private String customExact1;
    private String guid;
    private String id;
    private String imageUrl;
    private String inputSource;
    private String itemId;
    private String linkUrl;
    private String tag;
    private String type;
    private String updateDate;
    private String usefulCount;
  }

}
