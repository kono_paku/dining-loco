package jp.co.yahoo.dining.frontend.service

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.domain.Wedding
import jp.co.yahoo.dining.frontend.repository.WeddingTemplate

/**
 * WeddingServiceテスト
 */
class WeddingServiceSpec extends UnitTestBase {
  def "getWeddingInfoメソッドテスト"() {
    setup:
    def sut = new WeddingService()
    def mockTemplate = Mock(WeddingTemplate)
    sut.weddingTemplate = mockTemplate

    when:
    mockTemplate.getWeddingInfo(null) >> null
    mockTemplate.getWeddingInfo(_) >> new Wedding()
    def actual = sut.getWeddingInfo(gid)

    then:
    expect == actual

    where:
    test        | gid  || expect
    "nullだった場合" | null     || null
    "からのデータ"    | ""       || new Wedding()
    "正常データ"     | "xxxx" || new Wedding()
  }
}
