package jp.co.yahoo.dining.frontend.infrastructure.rest

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.domain.Kuchikomi
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

import javax.ws.rs.core.Feature

class KuchikomiTemplateImplSpec extends UnitTestBase {

  def "getKuchikomiListメソッドテスト"() {
    setup: "テストデータ準備"
    def cid = "cid"
    def appId = "appId"
    def apiUrl = "apiUrl"
    def gid = "gid"
    def optional = Optional.empty()

    def mockKuchikomi = Mock(Kuchikomi)
    def mockApiConfig = Mock(KuchikomiAPIConfig)
    def mockRT = Mock(RestTemplate)
    def spyTemplateImpl = Spy(KuchikomiTemplateImpl, constructorArgs: [mockRT, mockApiConfig, cid])

    mockKuchikomi.convert(cid) >> new Kuchikomi()
    mockApiConfig.appId >> appId
    mockApiConfig.kuchikomiList >> apiUrl
    spyTemplateImpl.createRequest(apiUrl) >> UriComponentsBuilder.fromUriString(apiUrl)
      .queryParam("xxx", appId)
      .queryParam("gid", gid)
      .queryParam("output", "json")
    def builder = spyTemplateImpl.createRequest(apiUrl)
    spyTemplateImpl.getEntity(Kuchikomi.class, builder) >> mockKuchikomi

    when: "テスト実行"
    def actual = spyTemplateImpl.getKuchikomiList(gid, optional, optional, optional, optional, optional)
    def expect = new Kuchikomi()

    then: "結果確認"
    actual == expect
  }

}
