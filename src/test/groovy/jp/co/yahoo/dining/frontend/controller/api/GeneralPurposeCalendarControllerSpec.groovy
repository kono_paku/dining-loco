package jp.co.yahoo.dining.frontend.controller.api

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.domain.Holiday
import jp.co.yahoo.dining.frontend.service.GeneralPurposeCalendarService

class GeneralPurposeCalendarControllerSpec extends UnitTestBase {
  def "Holidayの正常レスポンステスト"() {
    def mockService = Mock(GeneralPurposeCalendarService)
    mockService.searchHolidays(2018) >> new Holiday()
    def year = 2018

    def expect = new Holiday()

    def controller = new GeneralPurposeCalendarController(mockService)

    when:
      def actual = controller.searchHolidays(year)
    then:
      expect == actual
  }
}
