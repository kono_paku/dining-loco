package jp.co.yahoo.dining.frontend.controller.api

import jp.co.yahoo.dining.frontend.domain.PlanStock
import jp.co.yahoo.dining.frontend.service.PlanStockService
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class PlanStockControllerSpec extends Specification {
  def "GetPlanStockバリデーション正常系"() {
    setup: "コントローラーのセットアップ"

    def mockPlanStockService = Mock(PlanStockService)
    mockPlanStockService.getPlanStock(*_) >> new PlanStock()

    def mockController = new PlanStockController(mockPlanStockService)

    when: "リクエスト実行"
    def actual = mockController.getPlanStock("gid")
    def expect = new PlanStock()

    then: "期待値"
    expect == actual
  }
}
