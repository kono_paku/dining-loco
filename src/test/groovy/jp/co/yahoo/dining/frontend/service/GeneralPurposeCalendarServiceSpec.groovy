package jp.co.yahoo.dining.frontend.service

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.domain.Holiday
import jp.co.yahoo.dining.frontend.repository.GeneralPurposeCalendarTemplate

class GeneralPurposeCalendarServiceSpec extends UnitTestBase{
  def "祝日情報取得＿正常" () {
    setup:
      def expect = new Holiday()
      def year = new Integer(2018)
      def calendarTemplate = Mock(GeneralPurposeCalendarTemplate)
      calendarTemplate.searchHolidays(year) >> new Holiday()
      def calendarService = new GeneralPurposeCalendarService(calendarTemplate)
    expect:
      calendarService.searchHolidays(year) == expect
  }
}
