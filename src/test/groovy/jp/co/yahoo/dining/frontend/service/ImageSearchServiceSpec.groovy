package jp.co.yahoo.dining.frontend.service

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.controller.form.ImageSearchInfo
import jp.co.yahoo.dining.frontend.domain.ImageSearch
import jp.co.yahoo.dining.frontend.repository.ImageSearchTemplate
import spock.lang.Unroll

class ImageSearchServiceSpec extends UnitTestBase {

  @Unroll
  def "getImageSearchメソッドテスト"() {
    setup:
    def mockTemplate = Mock(ImageSearchTemplate)
    def imageInfo = new ImageSearchInfo(gid: "gid", results: null, start: null)

    mockTemplate.getImageSearch(*_) >> new ImageSearch(new ImageSearch.ResultInfo(), null)

    def imageSearchService = new ImageSearchService(mockTemplate)

    when:
    def actual = imageSearchService.getImageSearch(imageInfo)

    then:
    expect == actual

    where:
    test        | gid   || expect
    "正常データ" | "gid" || new ImageSearch(new ImageSearch.ResultInfo(), null)
  }

}
