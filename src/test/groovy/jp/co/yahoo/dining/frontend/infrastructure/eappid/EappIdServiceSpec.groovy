package jp.co.yahoo.dining.frontend.infrastructure.eappid

import org.mockito.internal.util.reflection.Whitebox
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

/**
 * Created by jchung on 2017/12/06.
 */
class EappIdServiceSpec extends Specification {

  def sut

  def setup() {
    sut = new EappIdService()
    def mockRest = GroovyMock(RestTemplate)
    Whitebox.setInternalState(sut, "restTemplate", mockRest)
  }


  def "API結果がnullだった場合"() {
    setup:
    sut.restTemplate.getForObject(*_) >> null

    when:
    def actual = sut.getEappId()

    then:
    thrown(RuntimeException)
  }

  def "API結果がからだった場合"() {
    setup:
    sut.restTemplate.getForObject(*_) >> new EappIdService.EappIdWrapper()

    when:
    def actual = sut.getEappId()

    then:
    thrown(RuntimeException)
  }

  def "API結果が正常だった場合"() {
    setup:
    def eappIdWrapper = new EappIdService.EappIdWrapper()
    eappIdWrapper.eAppId = "dummy EappId"
    sut.restTemplate.getForObject(*_) >> eappIdWrapper

    when:
    def actual = sut.getEappId()

    then:
    actual == "dummy EappId"
  }
}
