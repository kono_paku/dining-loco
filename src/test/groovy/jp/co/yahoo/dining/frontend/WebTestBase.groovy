package jp.co.yahoo.dining.frontend

import jp.co.yahoo.dining.frontend.controller.LocoRequestHeader
import jp.co.yahoo.dining.frontend.controller.api.ImageResizeController
import jp.co.yahoo.dining.frontend.infrastructure.eappid.EappIdService
import jp.co.yahoo.dining.frontend.infrastructure.masthead.YahooHeadService
import jp.co.yahoo.dining.frontend.infrastructure.rdsig.RdsigService
import jp.co.yahoo.dining.frontend.infrastructure.rest.TenpoAPIConfig

import jp.co.yahoo.dining.frontend.repository.TenpoTemplate
import jp.co.yahoo.dining.frontend.service.ImageResizeService
import jp.co.yahoo.dining.frontend.service.TenpoService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.FilterType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.web.client.RestTemplate
import spock.lang.Specification
import spock.mock.DetachedMockFactory

@WebMvcTest(excludeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, value = org.springframework.web.bind.annotation.ControllerAdvice))
class WebTestBase extends Specification {

  @Autowired MockMvc mvc

  @TestConfiguration
  static class MockConfig {
    def detachedMockFactory = new DetachedMockFactory()
    @Bean
    TenpoService tenpoService() {
      return detachedMockFactory.Mock(TenpoService)
    }

    @Bean
    TenpoTemplate tenpoTemplate() {
      return detachedMockFactory.Mock(TenpoTemplate)
    }

    @Bean
    @Qualifier("tenpo")
    RestTemplate tenpoRestTemplate() {
      return detachedMockFactory.Mock(RestTemplate)
    }

    @Bean
    @Qualifier("coupon")
    RestTemplate couponRestTemplate() {
      return detachedMockFactory.Mock(RestTemplate)
    }

    @Bean
    TenpoAPIConfig tenpoAPIConfig() {
      return detachedMockFactory.Mock(TenpoAPIConfig)
    }

    @Bean
    YahooHeadService yahooHeaderService() {
      return detachedMockFactory.Mock(YahooHeadService)
    }

    @Bean
    EappIdService eappIdService() {
      return detachedMockFactory.Mock(EappIdService)
    }

    @Bean
    LocoRequestHeader LocoRequestHeader() {
      return detachedMockFactory.Mock(LocoRequestHeader)
    }

    @Bean
    @Qualifier("imageWizard")
    RestTemplate imageWizardRestTemplate() {
      return detachedMockFactory.Mock(RestTemplate)
    }

    @Bean
    ImageResizeService imageResizeService() {
      return detachedMockFactory.Mock(ImageResizeService)
    }

    @Bean
    ImageResizeController imageResizeController() {
      return detachedMockFactory.Mock(ImageResizeController)
    }

    @Bean
    RdsigService rdsigService() {
      return detachedMockFactory.Mock(RdsigService)
    }
  }
}
