package jp.co.yahoo.dining.frontend.service

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.controller.form.TenpoInfo
import jp.co.yahoo.dining.frontend.domain.CassetteInfo
import jp.co.yahoo.dining.frontend.domain.Tenpo
import jp.co.yahoo.dining.frontend.domain.Tenpo.Feature
import jp.co.yahoo.dining.frontend.domain.Tenpo.Children
import jp.co.yahoo.dining.frontend.infrastructure.rest.WeddingAPIConfig
import jp.co.yahoo.dining.frontend.repository.TenpoTemplate

/**
 * TenpoServiceテスト
 */
class TenpoServiceSpec extends UnitTestBase {

  def tenpoTemplate
  def weddingConfig
  def tenpoService

  def setup() {
    tenpoTemplate = Mock(TenpoTemplate)
    weddingConfig = Mock(WeddingAPIConfig)
    tenpoService = new TenpoService(tenpoTemplate, weddingConfig)
  }

  def "getTenpoInfo_正常系(GID指定)"() {
    setup:
    def feature = [new Feature()] as Feature[]
    tenpoTemplate.getTenpoInfo(*_) >> new Tenpo(new Tenpo.ResultInfo(), feature)

    when:
    feature[0].gid = tenpoId + "-success"
    def actual = tenpoService.getTenpoInfo(tenpoId)

    then:
    actual != null
    actual.getFeature().length == 1
    actual.getFeature()[0] != null
    actual.getFeature()[0].getGid() == expect

    where:
    test                        | tenpoId        || expect
    "GIDがdummyGidKeyだった場合"  | "dummyGidKey"  || "dummyGidKey-success"
    "GIDがPsWANeE-hisだった場合"  | "PsWANeE-his"  || "PsWANeE-his-success"
  }

  def "getTenpoInfo_正常系(TenpoInfo指定)" () {
    setup:
      def expect = new Tenpo()
      def tenpoInfo = new TenpoInfo()
      tenpoTemplate.getTenpoInfo(tenpoInfo) >> new Tenpo()
    expect:
      tenpoService.getTenpoInfo(tenpoInfo) == expect
  }


  def "getTenpoInfo_異常系"() {
    setup:
      tenpoTemplate.getTenpoInfo("") >> { throw new IllegalArgumentException() }

    when:
      tenpoService.getTenpoInfo(gid)

    then:
      thrown(expectException)

    where:
      test                                | gid             || expectException
      "GIDが空のデータだった場合"            | ""              || IllegalArgumentException
  }

  def "isWeddingメソッドテスト_正常"() {
    setup:
      weddingConfig.getCassetteIds() >> (["78ef8c916628ffb0acaafadb8a6e1842"] as String[])

    expect:
      tenpoService.isWedding(tenpo) == expect

    where:
      test              | tenpo  || expect
      "式場CPが含まれる店舗データ"  | createWeddingTenpo("78ef8c916628ffb0acaafadb8a6e1842") || true
      "式場CPが含まれない店舗データ" | createWeddingTenpo("aaa") || false
  }

  def "isWeddingメソッドテスト_Feature が null の場合"() {
    setup:
      weddingConfig.getCassetteIds() >> (["78ef8c916628ffb0acaafadb8a6e1842"] as String[])

      def tenpo = new Tenpo()

    expect:
      tenpoService.isWedding(tenpo) == false
  }

  def "isWeddingメソッドテスト_Feature が空配列の場合"() {
    setup:
      weddingConfig.getCassetteIds() >> (["78ef8c916628ffb0acaafadb8a6e1842"] as String[])

      def weddingTenpo = new Tenpo()
      def weddingFeature = new Feature()
      weddingTenpo.setFeature(weddingFeature)

    expect:
      tenpoService.isWedding(weddingTenpo) == false
  }

  def "isWeddingメソッドテスト_Feature[].Children が null の場合"() {
    setup:
      weddingConfig.getCassetteIds() >> (["78ef8c916628ffb0acaafadb8a6e1842"] as String[])

      def weddingTenpo = new Tenpo()
      def weddingFeature = new Feature()
      weddingFeature.setChildren(null)
      weddingTenpo.setFeature(weddingFeature)

    expect:
      tenpoService.isWedding(weddingTenpo) == false
  }

  def "isWeddingメソッドテスト_Feature[].Children が空配列の場合"() {
    setup:
      weddingConfig.getCassetteIds() >> (["78ef8c916628ffb0acaafadb8a6e1842"] as String[])

    expect:
      tenpoService.isWedding(tenpo) == expect

    where:
      test        | tenpo  || expect
      "式場CPが含まれない店舗データ"     | createWeddingTenpoEmptyChildren() || false
  }

  /**
   * 式場用のtenpo生成
   * @param cid
   * @return
   */
  private def createWeddingTenpo(String cid) {
    def weddingTenpo = new Tenpo()
    def weddingFeature = new Feature()
    weddingFeature.setChildren(createWeddingChildren(cid))
    weddingTenpo.setFeature(weddingFeature)
    return weddingTenpo
  }

  /**
   * 式場用のChildren生成
   * @param cid
   * @return
   */
  private def createWeddingChildren(String cid) {
    def weddingChildren = new Children()
    weddingChildren.setCassetteId(cid)
    return weddingChildren
  }

  private def createWeddingTenpoEmptyChildren() {
    def weddingTenpo = new Tenpo()
    def weddingFeature = new Feature()
    def weddingChildren = new Children()
    weddingFeature.setChildren(weddingChildren)
    weddingTenpo.setFeature(weddingFeature)
    return weddingTenpo
  }
}
