package jp.co.yahoo.dining.frontend.controller

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.controller.view.TenpoController
import jp.co.yahoo.dining.frontend.domain.PlanList
import jp.co.yahoo.dining.frontend.domain.Promotion
import jp.co.yahoo.dining.frontend.domain.Tenpo
import jp.co.yahoo.dining.frontend.domain.UserInfo
import jp.co.yahoo.dining.frontend.infrastructure.LocoResourceLoader
import jp.co.yahoo.dining.frontend.infrastructure.eappid.EappIdService
import jp.co.yahoo.dining.frontend.infrastructure.masthead.YahooHeadService
import jp.co.yahoo.dining.frontend.infrastructure.masthead.model.YahooHead
import jp.co.yahoo.dining.frontend.service.*
import org.springframework.test.web.servlet.setup.MockMvcBuilders

import static java.util.Arrays.asList
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model

/**
 * Controller Advice Test
 */
class GlobalControllerAdviceSpec extends UnitTestBase {

  def tenpoController
  def globalControllerAdvice
  def mapper
  def videoService
  def tenpoService
  def weddingService
  def adInfeedService
  def planService
  def yahooHeaderService
  def eappIdService
  def locoRequestHeader
  def locoResourceLoader
  def promotionService
  def userInfoService

  def setup () {
    mapper = Mock(LocoResourceLoader)
    videoService = Mock(VideoService)
    tenpoService = Mock(TenpoService)
    planService = Mock(PlanService)
    weddingService = Mock(WeddingService)
    yahooHeaderService = Mock(YahooHeadService)
    eappIdService = Mock(EappIdService)
    locoRequestHeader = Mock(LocoRequestHeader)
    tenpoService = Mock(TenpoService)
    promotionService = Mock(PromotionService)
    userInfoService = Mock(UserInfoService)
    userInfoService.getUserInfo(*_) >> new UserInfo(loginStatus: true, userDetail: new UserInfo.UserDetail(guid: "guid", yid: "yid", area: "area", isPremium: true, premiumType: 4, softbankBundle: 3, yMobileBundle: 4, softBankCooperation: "softBankCooperation"))
    planService = Mock(PlanService)
    adInfeedService = Mock(AdInfeedService)
    locoResourceLoader = Mock(LocoResourceLoader)
    tenpoController = new TenpoController(videoService, adInfeedService, locoResourceLoader, planService, locoRequestHeader)

    globalControllerAdvice = new GlobalControllerAdvice (locoRequestHeader, eappIdService, tenpoService, promotionService, mapper, userInfoService, planService, weddingService)
  }

  def "サンプルテスト"() {
    setup:
    def expend = new Tenpo(null, [new Tenpo.Feature()] as Tenpo.Feature[])
    yahooHeaderService.getYahooHeadList(*_) >> asList(new YahooHead("k", "v"))
    eappIdService.getEappId() >> "dummyEappId"
    tenpoService.getTenpoInfo(*_) >> expend
    userInfoService.getUserInfo(*_) >> new UserInfo()
    promotionService.getPromotion() >> new Promotion()
    planService.getPlanList(*_) >> new PlanList()
    def mockMvc = MockMvcBuilders.standaloneSetup(tenpoController)
      .setControllerAdvice(globalControllerAdvice)
      .build()

    expect: "期待値"
    mockMvc.perform(get("/place/g-test/coupon"))
        .andExpect(model().attribute("tenpo", expend))
  }

  def "ジャンルグルメの場合ダイニングAPIを叩くテスト"() {
    setup:
    def tenpoInfo = new Tenpo(null, [new Tenpo.Feature(property: new Tenpo.Property(genre: new Tenpo.Genre(code: "01")))] as Tenpo.Feature[])
    def expected = new PlanList(entry: [new PlanList.PlanListEntry()] as PlanList.PlanListEntry[])
    yahooHeaderService.getYahooHeadList(*_) >> asList(new YahooHead("k", "v"))
    eappIdService.getEappId() >> "dummyEappId"
    tenpoService.getTenpoInfo(*_) >> tenpoInfo
    userInfoService.getUserInfo(*_) >> new UserInfo()
    promotionService.getPromotion() >> new Promotion()
    planService.getPlanList("gourment-gid") >> expected
    def mockMvc = MockMvcBuilders.standaloneSetup(tenpoController)
      .setControllerAdvice(globalControllerAdvice)
      .build()

    expect: "期待値"
    mockMvc.perform(get("/place/g-gourment-gid/coupon"))
      .andExpect(model().attribute("courseList", expected))
  }

  def "ジャンルグルメ以外の場合ダイニングAPIを叩かないテスト"() {
    setup:
    def tenpoInfo = new Tenpo(null, [new Tenpo.Feature(property: new Tenpo.Property(genre: new Tenpo.Genre(code: "03")))] as Tenpo.Feature[])
    def expected = new PlanList()
    yahooHeaderService.getYahooHeadList(*_) >> asList(new YahooHead("k", "v"))
    eappIdService.getEappId() >> "dummyEappId"
    tenpoService.getTenpoInfo(*_) >> tenpoInfo
    userInfoService.getUserInfo(*_) >> new UserInfo()
    promotionService.getPromotion() >> new Promotion()
    planService.getPlanList("gourment-gid") >> expected
    def mockMvc = MockMvcBuilders.standaloneSetup(tenpoController)
      .setControllerAdvice(globalControllerAdvice)
      .build()

    expect: "期待値"
    mockMvc.perform(get("/place/g-gourment-gid/coupon"))
      .andExpect(model().attribute("courseList", expected))
  }
}
