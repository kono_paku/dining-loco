package jp.co.yahoo.dining.frontend.infrastructure.rest

import jp.co.yahoo.dining.frontend.domain.Kuchikomi
import jp.co.yahoo.dining.frontend.domain.PlanStock
import org.mockito.internal.util.reflection.Whitebox
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder
import spock.lang.Specification

import java.text.SimpleDateFormat

class PlanStockTempleteImplSpec extends Specification {
  def "test getPlanStocks"() {
    given:
    def mockRestTemplate = Mock(RestTemplate)
    def mockPlanStockAPIConfig = Mock(PlanStockAPIConfig)
    def templateImpl = Spy(PlanStockTempleteImpl)
    Whitebox.setInternalState(templateImpl, "restTemplate", mockRestTemplate)
    Whitebox.setInternalState(templateImpl, "planStockAPIConfig", mockPlanStockAPIConfig)

    templateImpl.createRequest(*_) >> UriComponentsBuilder.fromHttpUrl("http://dummy.com")
    templateImpl.getEntity(PlanStock.class, *_) >> new PlanStock()

    when:
    def actual = templateImpl.getPlanStocks("gid")
    def expect = new PlanStock()

    then:
    expect == actual
  }
}
