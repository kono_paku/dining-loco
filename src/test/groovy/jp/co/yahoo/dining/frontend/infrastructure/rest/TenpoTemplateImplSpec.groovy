package jp.co.yahoo.dining.frontend.infrastructure.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.*

/**
 * TenpoTemplateのテスト
 */
@SpringBootTest(webEnvironment = RANDOM_PORT)
class TenpoTemplateImplSpec extends Specification {

  @Autowired LocoRestTemplate tenpoTemplate

}