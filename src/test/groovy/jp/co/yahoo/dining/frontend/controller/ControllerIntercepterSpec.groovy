package jp.co.yahoo.dining.frontend.controller

import jp.co.yahoo.dining.frontend.SpaceId
import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.controller.view.TenpoController
import jp.co.yahoo.dining.frontend.domain.Tenpo
import jp.co.yahoo.dining.frontend.infrastructure.LocoResourceLoader
import jp.co.yahoo.dining.frontend.infrastructure.masthead.YahooHeadService
import jp.co.yahoo.dining.frontend.infrastructure.masthead.model.YahooHead
import jp.co.yahoo.dining.frontend.service.*
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Unroll

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view

class ControllerIntercepterSpec extends UnitTestBase {

  def tenpoController

  def videoService
  def adInfeedService
  def locoResourceLoader
  def planService
  def locoRequestHeader

  def setup () {
    locoRequestHeader = Mock(LocoRequestHeader)
    videoService = Mock(VideoService)
    adInfeedService = Mock(AdInfeedService)
    locoResourceLoader = Mock(LocoResourceLoader)
    planService = Mock(PlanService)
    tenpoController = new TenpoController(videoService, adInfeedService, locoResourceLoader, planService, locoRequestHeader)
  }

  @Unroll
  def "デバイスによってページが切り替わる"() {
    setup:
      // test data
      def tenpo = new Tenpo()
      def children = new Tenpo.Children(cassetteId: "hogehogehoge")
      def childrenList = []
      childrenList.add(children)
      def searchArea = new Tenpo.SearchArea(code: "010", name: "01")
      def searchAreaList = []
      searchAreaList.add(searchArea)
      def genre = new Tenpo.Genre(name: "genre", code: "000")
      def genreList = []
      genreList.add(genre)
      def property = new Tenpo.Property(cassetteId: "fff")
      property.setSearchArea(searchAreaList as Tenpo.SearchArea[])
      property.setGenre(genreList as Tenpo.Genre[])
      property.setStation([] as Tenpo.Station[])
      def feature = new Tenpo.Feature(name: "fff", category: ["ffff"])
      feature.setProperty(property)
      feature.setChildren(childrenList as Tenpo.Children[])
      def featureList = []
      featureList.add(feature)
      tenpo.setFeature(featureList as Tenpo.Feature[])
      // test controller
      def mockLocoRequestHeader = Mock(LocoRequestHeader)
      def mockResourceLoader = Mock(LocoResourceLoader)
      def mockYahooHeadService = Mock(YahooHeadService)
      def head = new YahooHead(position: "hoge", html: "hoge")
      mockYahooHeadService.getYahooHeadList(*_) >> [head]
      mockResourceLoader.getResource(*_) >> new SpaceId([new SpaceId.Device("pc", [new SpaceId.Genre(code: "01", prefectures: [new SpaceId.Prefecture(code: "01", spaceId: "01")])])])
      mockLocoRequestHeader.getActualDevice() >> device
      mockLocoRequestHeader.getDevice() >> device
      def sut = new ControllerIntercepter(mockLocoRequestHeader, mockResourceLoader, mockYahooHeadService)
      def mockMvc = MockMvcBuilders.standaloneSetup(tenpoController)
        .addInterceptors(sut)
        .build()
    expect: "期待値"
      mockMvc.perform(get(endpoint).flashAttr("tenpo", tenpo))
        .andExpect(view().name(path))
        .andExpect(model().attribute("tenpo", tenpo))
        .andExpect(model().attribute("spaceId", ""))
    where:
      device       | endpoint        | path
      "pc"         | "/place/g-test/coupon" | "pc/html/place/coupon/index"
      "smartphone" | "/place/g-test/coupon" | "sp/html/place/coupon/index"
  }

  @Unroll
  def "ダイニングがChildrenのCIDに含まれていたらisDiningをTrueで返す"() {
    setup: "テスト店舗データ準備"
    def tenpo = new Tenpo()
    def children = new Tenpo.Children(cassetteId: cassetteId)
    def tempChildren = new Tenpo.Children(cassetteId: "testtesttesttesttesttesttesttest")
    def childrenList = []
    childrenList.add(children)
    childrenList.add(tempChildren)
    def property = new Tenpo.Property(cassetteId: "fff")
    def feature = new Tenpo.Feature(name: "fff", category: ["ffff"])
    feature.setProperty(property)
    feature.setChildren(childrenList as Tenpo.Children[])
    def featureList = []
    featureList.add(feature)
    tenpo.setFeature(featureList as Tenpo.Feature[])
    // test controller
    def mockLocoRequestHeader = Mock(LocoRequestHeader)
    def mockResourceLoader = Mock(LocoResourceLoader)
    def mockYahooHeadService = Mock(YahooHeadService)
    def controllerIntercepter = new ControllerIntercepter(mockLocoRequestHeader, mockResourceLoader, mockYahooHeadService)
    controllerIntercepter.YAHOO_DINING_CASSETTE_ID = "ccbf79674338330c7ce5557ace5a18ec"
    when: "isDining実行"
    def result = controllerIntercepter.isDining(tenpo)
    then: "期待値"
    result == expect
    where:
    cassetteId                         || expect
    "testtesttesttesttesttesttesttest" || 0
    "ccbf79674338330c7ce5557ace5a18ec" || 1
  }
}
