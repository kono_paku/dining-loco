package jp.co.yahoo.dining.frontend.service

import jp.co.yahoo.dining.frontend.domain.PlanStock
import jp.co.yahoo.dining.frontend.repository.PlanStockTemplate
import spock.lang.Specification

class PlanStockServiceSpec extends Specification {
  def "test getPlanStock"() {
    given:
    def mockTemplate = Mock(PlanStockTemplate)
    mockTemplate.getPlanStocks(*_) >> new PlanStock()

    def planStockService = new PlanStockService(mockTemplate)

    when:
    def actual = planStockService.getPlanStock("gid")
    def expect = new PlanStock()

    then:
    expect == actual
  }
}
