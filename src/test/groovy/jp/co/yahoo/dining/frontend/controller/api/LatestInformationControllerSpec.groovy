package jp.co.yahoo.dining.frontend.controller.api

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.controller.constraint.GId

import jp.co.yahoo.dining.frontend.domain.LatestInformation
import jp.co.yahoo.dining.frontend.service.LatestInformationService
import lombok.NoArgsConstructor
import lombok.Setter
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Unroll

import javax.validation.Validation

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class LatestInformationControllerSpec extends UnitTestBase {

  def validator = Validation.buildDefaultValidatorFactory().getValidator()

  def "正常レスポンステスト"() {
    setup:
      def gid = "CGBtPVWV6EE"
      def mockService = Mock(LatestInformationService)
      mockService.getLatestInformation(gid) >> new LatestInformation()
      def latestInformationController = new LatestInformationController(mockService)
      def mockMvc = MockMvcBuilders.standaloneSetup(latestInformationController).build()
      // クラスの中身
      def expect = '{"ResultSet":null}'
    expect:
      mockMvc.perform(get("/v1/api/latestInformation").param("gid", gid).accept("application/json"))
        .andExpect(status().isOk()).andExpect(content().string(expect))
  }

  def "@GIDのバリデーションチェック_正常系"() {
    setup:
      def data = new TestForm()
      data.setGid("CGBtPVWV6EE")
      def expect = 0
    when:
      def actual = validator.validate(data).size()
    then:
      actual == expect
  }

  @Unroll
  def "@GIDのバリデーションチェック_異常系"() {
    setup:
      def data = new TestForm()
      data.setGid(gid)
    when:
      def validate = validator.validate(data)
    then:
      validate.size() == size
      validate.first().getMessage() == message
    where:
      test         | gid            || size || message
      "10文字(字数不足)" | "CGBtPVWV6E"   || 1    || "must match \"^[-_a-zA-Z0-9]{11}\$\""
      "12文字(字数過多)" | "CGBtPVWV6EEX" || 1    || "must match \"^[-_a-zA-Z0-9]{11}\$\""
      "不適切な記号"     | "CGBtPVWV6E!"  || 1    || "must match \"^[-_a-zA-Z0-9]{11}\$\""
      "空文字"        | ""             || 1    || "must match \"^[-_a-zA-Z0-9]{11}\$\""
  }

  @Setter
  @NoArgsConstructor
  static class TestForm {
    @GId
    String gid
  }
}
