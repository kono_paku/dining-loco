package jp.co.yahoo.dining.frontend.infrastructure.rest

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.domain.Holiday
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

class GeneralPurposeCalendarTemplateImplSpec extends UnitTestBase{
  def "searchHolidaysのテスト" (){
    given: "変換元データ準備"
      def mockRt = Mock(RestTemplate)
      def mockConfig = Mock(GeneralPurposeCalendarConfig)
      mockConfig.getCalendarPath() >> 'http://url.com/'
      mockConfig.getRows() >> 100
      mockConfig.sid >> 'sid'
      mockConfig.wt >> 'json'

      def year = 2018

      def calenderTemplate = Spy(GeneralPurposeCalendarTemplateImpl, constructorArgs: [mockRt, mockConfig])
      calenderTemplate.createRequest('http://url.com/') >> UriComponentsBuilder.fromUriString('http://url.com/')

      def expect = new Holiday()
      calenderTemplate.getEntity(Holiday.class, *_) >> new Holiday()
    when:
      def actual = calenderTemplate.searchHolidays(year)
    then:
      expect == actual
  }
}
