package jp.co.yahoo.dining.frontend.cache

import jp.co.yahoo.dining.frontend.UnitTestBase
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.CacheManager
import org.springframework.cache.annotation.Cacheable
import org.springframework.cache.annotation.EnableCaching
import org.springframework.cache.concurrent.ConcurrentMapCache
import org.springframework.cache.concurrent.ConcurrentMapCacheFactoryBean
import org.springframework.cache.support.SimpleCacheManager
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.test.context.ContextConfiguration

/**
 * Cache機構テスト
 */
@ContextConfiguration(classes = TestConfig.class)
class CacheSpec extends UnitTestBase {

  @Autowired TestService testService

  @Autowired CacheManager cacheManager

  def "SpringCacheテスト"() {
    when:
    testService.setData("First")
    def firstData = testService.getData()
    testService.setData("Second")
    def secondData = testService.getData()

    then:
    firstData == secondData

    when:
    cacheManager.getCache("test-cache").clear()
    secondData = testService.getData()

    then:
    firstData != secondData
  }

  @Configuration
  @EnableCaching
  static class TestConfig {

    @Bean
    TestService testService() {
      return new TestService()
    }

    @Bean
    CacheManager cacheManager() {
      SimpleCacheManager cacheManager = new SimpleCacheManager()
      List<ConcurrentMapCache> caches = new ArrayList<>()
      caches.add(cacheBean().getObject())
      cacheManager.setCaches(caches)
      return cacheManager
    }

    @Bean
    ConcurrentMapCacheFactoryBean cacheBean() {
      ConcurrentMapCacheFactoryBean cacheFactoryBean = new ConcurrentMapCacheFactoryBean()
      cacheFactoryBean.setName("test-cache")
      return cacheFactoryBean
    }
  }

  static class TestService {

    String data

    @Cacheable("test-cache")
    String getData() {
      return data
    }

    void setData(String data) {
      this.data = data
    }
  }
}