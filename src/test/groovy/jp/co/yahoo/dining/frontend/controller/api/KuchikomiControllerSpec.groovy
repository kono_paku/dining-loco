package jp.co.yahoo.dining.frontend.controller.api

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.domain.Kuchikomi
import jp.co.yahoo.dining.frontend.service.KuchikomiService

class KuchikomiControllerSpec extends UnitTestBase {

  def "getKuchikomiメソッドテスト"() {
    setup: "テスト準備"
    def gid = "gid"
    def optional = Optional.empty()

    def mockService = Mock(KuchikomiService)
    mockService.getKuchikomiList(gid, optional, optional, optional, optional, optional) >> new Kuchikomi()
    def kuchikomiController = new KuchikomiController(mockService)

    when: "テスト実行"
    def actual = kuchikomiController.getKuchikomiList(gid, optional, optional, optional, optional, optional)
    def expect = new Kuchikomi()

    then: "結果確認"
    actual == expect
  }

}
