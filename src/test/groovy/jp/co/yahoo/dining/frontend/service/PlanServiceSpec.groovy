package jp.co.yahoo.dining.frontend.service

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.domain.PlanDetail
import jp.co.yahoo.dining.frontend.domain.PlanList
import jp.co.yahoo.dining.frontend.domain.Tenpo
import jp.co.yahoo.dining.frontend.repository.LifeToolTemplate

import static jp.co.yahoo.dining.frontend.domain.Tenpo.*

class PlanServiceSpec extends UnitTestBase {
  def mockTemplate
  def planService
  def setup () {
    mockTemplate = Mock(LifeToolTemplate)
    planService = new PlanService(mockTemplate)
  }
  def "コースリスト取得_gidのみ" () {
    setup:
      def expect = new PlanList()
      def gid = "gid"
      mockTemplate.getPlanList(gid) >> new PlanList()
    expect:
      planService.getPlanList(gid) == expect
  }

  def "コースリスト取得_gidとdate指定" () {
    setup:
      def expect = new PlanList()
      def gid = "gid"
      def date = Optional.of("20181103")
      mockTemplate.getPlanList(gid, date) >> new PlanList()
    expect:
      planService.getPlanList(gid, date) == expect
  }

  def "コース詳細取得_tenpoIdとplanID指定" () {
    setup:
      def expect = new PlanDetail()
      def tenpoId = "tenpoId"
      def planId = "planId"
      mockTemplate.getPlanDetail(tenpoId, planId) >> new PlanDetail()
    expect:
      planService.getPlanDetail(tenpoId, planId) == expect
  }

  def "コース詳細取得_tenpo情報とplanId" () {
    setup:
      def expect = new PlanDetail()
      planService.yahooCid = "yahoo"
      def tenpoId = "tenpoId"
      def planId = "planId"
      def yahooInfo = new Children()
      yahooInfo.cassetteId = "yahoo"
      yahooInfo.id = tenpoId
      def feature = new Feature()
      feature.children = [yahooInfo] as Children[]
      def tenpo = new Tenpo()
      tenpo.feature = [feature] as Feature[]
      mockTemplate.getPlanDetail(tenpoId, planId) >> new PlanDetail()
    expect:
      planService.getPlanDetail(tenpo, planId) == expect
  }
}
