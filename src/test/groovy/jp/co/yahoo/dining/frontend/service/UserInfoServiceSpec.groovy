package jp.co.yahoo.dining.frontend.service

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.domain.UserInfo
import jp.co.yahoo.dining.frontend.infrastructure.ncookie.NcookieService
import jp.co.yahoo.dining.frontend.infrastructure.udb.UdbService
import jp.co.yahoo.dining.frontend.infrastructure.udb.model.UdbResponse
import spock.lang.Unroll

import javax.servlet.http.HttpServletRequest

class UserInfoServiceSpec extends UnitTestBase {
  def expect
  def udbService
  def userInfoService
  def request
  def ncookieService
  def setup() {
    expect = new UserInfo()
    udbService = Mock(UdbService)
    request = Mock(HttpServletRequest)
    ncookieService = Mock(NcookieService)
    userInfoService = new UserInfoService(udbService, ncookieService)
  }

  def "ログイン済みユーザー情報取得" () {
    setup:
      def cookie = "cookie; Y=Ycookie; T=Tcookie"
      def yid = "yid"
      def udbResponse = buildUdbResponse(premiumType as Integer)
      udbService.getUdbResponse(cookie) >> udbResponse
      expect.setUserDetail(buildUserDetail(yid, udbResponse))
      expect.setLoginStatus(true)
      ncookieService.validateCookie(request) >> true
    expect:
      userInfoService.getUserInfo(cookie, request) == expect
    where:
      premiumType | _
      new Integer(10) | _
      null | _
  }

  def "未ログイン情報取得" () {
    setup:
    expect.setLoginStatus(false)
    expect.setUserDetail(new UserInfo.UserDetail())
    expect:
    userInfoService.getUserInfo(cookie, request) == expect
    where:
    cookie = ""
  }

  @Unroll
  def "ログイン済み(YCookie・TCookie組み合わせ)ユーザー情報取得" () {
    setup:
      def udbResponse = buildUdbResponse(new Integer(10) as Integer)
      udbService.getUdbResponse(cookie) >> udbResponse
      ncookieService.validateCookie(request) >> true
    expect:
      userInfoService.getUserInfo(cookie, request) == result
    where:
      cookie || result
      "cookie; YY=Ycookie; TT=Tcookie" || new UserInfo(loginStatus: false, userDetail: new UserInfo.UserDetail())
      "cookie; Y=Ycookie; TT=Tcookie"  || new UserInfo(loginStatus: false, userDetail: new UserInfo.UserDetail())
      "cookie; YY=Ycookie; T=Tcookie"  || new UserInfo(loginStatus: false, userDetail: new UserInfo.UserDetail())
  }

  @Unroll
  def "ログイン済み（NCookie不正）ユーザー情報取得" () {
    setup:
      def udbResponse = buildUdbResponse(new Integer(10) as Integer)
      udbService.getUdbResponse(cookie) >> udbResponse
      ncookieService.validateCookie(request) >> false
    expect:
      userInfoService.getUserInfo(cookie, request) == result
    where:
      cookie || result
      "cookie; Y=Ycookie;   T=Tcookie" || new UserInfo(loginStatus: false, userDetail: new UserInfo.UserDetail())
      "cookie; YY=Ycookie; TT=Tcookie" || new UserInfo(loginStatus: false, userDetail: new UserInfo.UserDetail())
  }

  def buildUdbResponse (Integer premiumType) {
    def udbResponse = new UdbResponse()
    udbResponse.demog = "demog"
    udbResponse.premiumType = premiumType
    udbResponse.softbankBundle = premiumType == 10 ? new Integer(1) : new Integer(0)
    udbResponse.YMobileBundle = premiumType == 6 ? new Integer(1) : new Integer(0)
    udbResponse.softBankCooperation = "softBankCooperation"
    udbResponse.guid = "guid"
    udbResponse.yid = "yid"
    return udbResponse
  }

  def buildUserDetail (String yid, UdbResponse udbResponse) {
    def detail = new UserInfo.UserDetail()
    detail.setGuid("guid")
    detail.setYid(yid)
    detail.setArea(udbResponse.getDemog())
    detail.setIsPremium(udbResponse.getPremiumType() != null)
    detail.setPremiumType(udbResponse.getPremiumType())
    detail.setSoftbankBundle(udbResponse.getSoftbankBundle())
    detail.setYMobileBundle(udbResponse.getYMobileBundle())
    detail.setSoftBankCooperation(udbResponse.getSoftBankCooperation())
    return detail
  }
}
