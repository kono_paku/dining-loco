package jp.co.yahoo.dining.frontend.domain

import spock.lang.Specification
import spock.lang.Unroll

class KuchikomiSpec extends Specification {

  @Unroll
  def "CIDがロコだった場合の口コミモデルのAuthor暗号化処理テスト"() {
    given:"口コミデータ準備"
      def comment = new Kuchikomi.Comment()
      comment.author = author
      def prop = new Kuchikomi.Property()
      prop.comment = comment
      prop.cassetteId = cassetteId
      def feature = new Kuchikomi.Feature()
      feature.property = prop
      def features = []
      features[0] = feature
      def kuchikomi = new Kuchikomi()
      kuchikomi.feature = features
    when:"Author暗号化処理理実行"
      kuchikomi.convert("40f68cf163a82aeb127f0387eb314b4b")
    then:"Author確認"
      kuchikomi.feature[0].property.comment.author == result
    where: ""
      author     | cassetteId                         || result
      "hogehoge" | "40f68cf163a82aeb127f0387eb314b4b" ||  "hog*****"
      "hogehoge" | "hogehogehogehogehogehogehogehoge" ||  "hogehoge"
  }
}
