package jp.co.yahoo.dining.frontend.service

import jp.co.yahoo.dining.frontend.controller.form.ImagesInfo
import jp.co.yahoo.dining.frontend.domain.ResizedImage
import jp.co.yahoo.dining.frontend.repository.ImageWizardTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.CacheManager
import org.springframework.cache.annotation.EnableCaching
import org.springframework.cache.concurrent.ConcurrentMapCache
import org.springframework.cache.concurrent.ConcurrentMapCacheFactoryBean
import org.springframework.cache.support.SimpleCacheManager
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.test.context.ContextConfiguration
import org.springframework.web.client.RestTemplate
import spock.lang.Specification
import spock.mock.DetachedMockFactory

/**
 * 画像リサイズサービスのテスト
 */
@ContextConfiguration(classes = TestConfig.class)
class ImageResizeServiceTest extends Specification {
  @Autowired ImageResizeService service
  @Autowired SimpleCacheManager cacheManager
  @Autowired ImageWizardTemplate tmpl;

  def "キャッシュが有効か確認"() {
    setup: "パラメータセット"
      def param = new ImagesInfo("CQgD-fVDZ_M", Arrays.asList(createImageFixture()))
      tmpl.resizeImages(param) >> createResizedImageFixture()
    when: ""
      service.resizedImages(param)
    then: ""
      1 * tmpl.resizeImages(param)
    when: ""
      service.resizedImages(param)
    then: ""
      0 * tmpl.resizeImages(param)
  }

  private ResizedImage createResizedImageFixture() {
    def resultset = new ResizedImage.ResultSet()
    resultset.setFirstResultPosition("1")
    resultset.setTotalResultsAvailable("1")

    def resizedImage = new ResizedImage()
    resizedImage.setResultSet(resultset)

    return resizedImage
  }

  private ImagesInfo.Image createImageFixture() {
    def image = new ImagesInfo.Image()
    image.setUrl("http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00id.jpg")
    image.setHeight(300)
    image.setWidth(200)
    return image
  }

  @Configuration
  @EnableCaching
  static class TestConfig {
    final detachedMockFactory = new DetachedMockFactory()

    @Bean
    ImageWizardTemplate imageWizardTemplate() {
      return detachedMockFactory.Mock(ImageWizardTemplate)
    }

    @Bean
    ImageResizeService imageResizeService(ImageWizardTemplate imageWizardTemplate) {
      return new ImageResizeService(imageWizardTemplate)
    }

    @Bean
    CacheManager cacheManager() {
      SimpleCacheManager cacheManager = new SimpleCacheManager()
      List<ConcurrentMapCache> caches = new ArrayList<>()
      caches.add(cacheBean().getObject())
      cacheManager.setCaches(caches)
      return cacheManager
    }

    @Bean
    ConcurrentMapCacheFactoryBean cacheBean() {
      ConcurrentMapCacheFactoryBean cacheFactoryBean = new ConcurrentMapCacheFactoryBean()
      cacheFactoryBean.setName("images")
      return cacheFactoryBean
    }
  }
}
