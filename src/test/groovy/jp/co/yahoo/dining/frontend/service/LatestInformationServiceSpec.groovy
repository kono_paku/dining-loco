package jp.co.yahoo.dining.frontend.service

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.domain.LatestInformation
import jp.co.yahoo.dining.frontend.repository.LatestInformationTemplate

class LatestInformationServiceSpec extends UnitTestBase {
  def "最新情報取得テスト" () {
    setup:
      def mockTemplate = Mock(LatestInformationTemplate)
      mockTemplate.getLatestInformation("gid") >> new LatestInformation()
      def latestInformationService = new LatestInformationService(mockTemplate)
      def expect = new LatestInformation()
    expect:
      latestInformationService.getLatestInformation("gid") == expect
  }
}
