package jp.co.yahoo.dining.frontend.infrastructure.udb

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.infrastructure.athenz.AthenzService
import jp.co.yahoo.dining.frontend.infrastructure.udb.model.UdbResponse
import org.mockito.internal.util.reflection.Whitebox
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate

class UdbServiceSpec extends UnitTestBase {

  def "udb情報を取得"() {
    setup:
    def mockUdbProperties = Mock(UdbProperties)
    def mockAthenzService = Mock(AthenzService)
    def mockRest = Mock(RestTemplate)

    def sut = new UdbServiceImpl()

    Whitebox.setInternalState(sut, "udbProperties", mockUdbProperties)
    Whitebox.setInternalState(sut, "athenzService", mockAthenzService)
    Whitebox.setInternalState(sut, "restTemplate", mockRest)

    mockUdbProperties.getUdbApiUrl() >> "http://url.com/"
    mockAthenzService.getYCACertification() >> "ycaKey"
    mockRest.exchange(*_) >> new ResponseEntity<UdbResponse>(new UdbResponse(), HttpStatus.OK)

    when:
    def actual = sut.getUdbResponse("test")

    then:
    actual == new UdbResponse()
  }
}
