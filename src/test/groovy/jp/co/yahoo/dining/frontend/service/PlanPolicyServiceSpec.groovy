package jp.co.yahoo.dining.frontend.service

import jp.co.yahoo.dining.frontend.controller.form.PlanPolicyInfo
import jp.co.yahoo.dining.frontend.domain.PlanPolicy
import jp.co.yahoo.dining.frontend.repository.PolicyTemplate
import spock.lang.Specification

class PlanPolicyServiceSpec extends Specification {
  def "PlanPolicy取得テスト"() {
    given:
    def mockTemplate = Mock(PolicyTemplate)
    mockTemplate.getPolicyList(*_) >> new PlanPolicy()

    def planPolicyInfo = new PlanPolicyInfo("s000018792", null, null)
    def planStockService = new PlanPolicyService(mockTemplate)

    when:
    def actual = planStockService.getPlanPolicy(planPolicyInfo)
    def expect = new PlanPolicy()

    then:
    expect == actual
  }
}
