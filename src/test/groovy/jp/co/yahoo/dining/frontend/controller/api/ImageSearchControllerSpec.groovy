package jp.co.yahoo.dining.frontend.controller.api

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.controller.form.ImageSearchInfo
import jp.co.yahoo.dining.frontend.domain.ImageSearch
import jp.co.yahoo.dining.frontend.service.ImageSearchService

class ImageSearchControllerSpec extends UnitTestBase {

  def "getImageSearchメソッドテスト"() {
    setup: "テスト準備"
    def imageInfo = new ImageSearchInfo(gid: "gid", results: null, start: null)

    def mockService = Mock(ImageSearchService)
    mockService.getImageSearch(imageInfo) >> new ImageSearch()
    def imageSearchController = new ImageSearchController(mockService)

    when: "テスト実行"
    def actual = imageSearchController.getImageSearch(imageInfo)
    def expect = new ImageSearch()

    then: "結果確認"
    actual == expect
  }

}
