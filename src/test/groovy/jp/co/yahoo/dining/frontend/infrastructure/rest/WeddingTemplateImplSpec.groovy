package jp.co.yahoo.dining.frontend.infrastructure.rest

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.domain.Wedding
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

/**
 * WeddingTemplateのテスト
 */
class WeddingTemplateImplSpec extends UnitTestBase {

  def "getWeddingInfoメソッドテスト"() {
    setup: "テストデータ準備"
    def appId = "appId"
    def apiUrl = "apiUrl"
    def gid = "gid"

    def mockApiConfig = Mock(WeddingAPIConfig)
    mockApiConfig.appId >> appId
    mockApiConfig.weddingInfo >> apiUrl
    mockApiConfig.cassetteIds >> ["testCassetteId"]
    def mockRT = Mock(RestTemplate)
    def spyTemplateImpl = Spy(WeddingTemplateImpl, constructorArgs: [mockRT, mockApiConfig])

    mockApiConfig.appId >> appId
    mockApiConfig.weddingInfo >> apiUrl
    mockApiConfig.cassetteIds >> {"test"}
    spyTemplateImpl.createRequest(apiUrl) >> UriComponentsBuilder.fromUriString(apiUrl)
      .queryParam("xxx", appId)
      .queryParam("gid", gid)
      .queryParam("output", "json")
    def builder = spyTemplateImpl.createRequest(apiUrl)
    spyTemplateImpl.getEntity(Wedding.class, builder) >> new Wedding()

    when: "テスト実行"
    def actual = spyTemplateImpl.getWeddingInfo(gid)
    def expect = new Wedding()

    then: "結果確認"
    actual == expect
  }
}
