package jp.co.yahoo.dining.frontend.infrastructure.masterheader

import jp.co.yahoo.dining.frontend.infrastructure.masthead.YahooHeadService
import jp.co.yahoo.dining.frontend.infrastructure.masthead.model.YahooHeadWrapper
import jp.co.yahoo.dining.frontend.repository.MastHeadTemplate
import spock.lang.Specification

class YahooHeadServiceSpec extends Specification {

  def sut

  def setup() {
    def mockRest = GroovyMock(MastHeadTemplate)
    sut = new YahooHeadService(mockRest)
  }

  def "API結果がnullだった場合"() {
    setup:
    sut.mastHeadTemplate.getMastHead(*_) >> null

    when:
    def actual = sut.getYahooHeadList("", "", "", "")

    then:
    thrown(RuntimeException)
  }

  def "API結果がからだった場合"() {
    setup:
    sut.mastHeadTemplate.getMastHead(*_) >> new YahooHeadWrapper()

    when:
    def actual = sut.getYahooHeadList("", "", "", "")

    then:
    actual == null
  }

  def "API結果が正常だった場合"() {
    setup:
    def response = new YahooHeadWrapper()
    response.results = new ArrayList<>()
    sut.mastHeadTemplate.getMastHead(*_) >> response

    when:
    def actual = sut.getYahooHeadList("", "", "", "")

    then:
    actual == new ArrayList<>()
  }
}
