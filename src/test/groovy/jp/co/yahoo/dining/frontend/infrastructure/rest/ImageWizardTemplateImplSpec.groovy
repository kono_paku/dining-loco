package jp.co.yahoo.dining.frontend.infrastructure.rest

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.controller.form.ImagesInfo

/**
 * 画像変換APIプロキシ
 */
class ImageWizardTemplateImplSpec extends UnitTestBase {

  def "ImagesInfoをImageWizardParameter(ImageWizardのIF)に変換する処理のテスト" (){
    given: "変換元データ準備"
      def images = new ArrayList<ImagesInfo.Image>()
      def image1 = new ImagesInfo.Image()
      image1.setUrl("http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00id.jpg")
      image1.setHeight(300)
      image1.setWidth(200)
      def image2 = new ImagesInfo.Image()
      image2.setUrl("http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_0093.jpg")
      image2.setHeight(300)
      image2.setWidth(200)
      def image3 = new ImagesInfo.Image()
      image3.setUrl("http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00o2.jpg")
      image3.setHeight(300)
      image3.setWidth(200)
      def image4 = new ImagesInfo.Image()
      image4.setUrl("http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00dd.jpg")
      image4.setHeight(300)
      image4.setWidth(200)
      def image5 = new ImagesInfo.Image()
      image5.setUrl("http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_009d.jpg")
      image5.setHeight(300)
      image5.setWidth(200)
      image5.setQuality(200)
      def image6 = new ImagesInfo.Image()
      image6.setUrl("http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00hv.jpg")
      image6.setHeight(300)
      image6.setWidth(200)
      image6.setPriority("l")
      def image7 = new ImagesInfo.Image()
      image7.setUrl("http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00hv.jpg")
      image7.setHeight(300)
      image7.setWidth(200)
      image7.setCropWidthCenter(150)
      image7.setCropHeightCenter(100)
      images.add(image1)
      images.add(image2)
      images.add(image3)
      images.add(image4)
      images.add(image5)
      images.add(image6)
      images.add(image7)
      def param = new ImagesInfo("CQgD-fVDZ_M", images)
    when: "変換実行"
      def result = ImageWizardTemplateImpl.ImageWizardParameter.of(param)
    then: "リクエストパラメータが期待値通りに変換されていることを確認"
      result.getSource()[0].getUrl() == image1.getUrl()
      result.getSource()[1].getUrl() == image2.getUrl()
      result.getSource()[2].getUrl() == image3.getUrl()
      result.getSource()[3].getUrl() == image4.getUrl()
      result.getSource()[4].getUrl() == image5.getUrl()
      result.getSource()[5].getUrl() == image6.getUrl()
      result.getSource()[6].getUrl() == image7.getUrl()
      result.getSource()[0].getResize() == "x=" + image1.getWidth()+  "&y=" +  image1.getHeight()+ "&cwc=" + image1.getWidth()+ "&chc=" +  image1.getHeight()+ "&bd=0&n=0&ifm=png&pri=s&q=50"
      result.getSource()[1].getResize() == "x=" + image2.getWidth()+  "&y=" +  image2.getHeight()+ "&cwc=" + image2.getWidth()+ "&chc=" +  image2.getHeight()+ "&bd=0&n=0&ifm=png&pri=s&q=50"
      result.getSource()[2].getResize() == "x=" + image3.getWidth()+  "&y=" +  image3.getHeight()+ "&cwc=" + image3.getWidth()+ "&chc=" +  image3.getHeight()+ "&bd=0&n=0&ifm=png&pri=s&q=50"
      result.getSource()[3].getResize() == "x=" + image4.getWidth()+  "&y=" +  image4.getHeight()+ "&cwc=" + image4.getWidth()+ "&chc=" +  image4.getHeight()+ "&bd=0&n=0&ifm=png&pri=s&q=50"
      result.getSource()[4].getResize() == "x=" + image5.getWidth()+  "&y=" +  image5.getHeight()+ "&cwc=" + image5.getWidth()+ "&chc=" +  image5.getHeight()+ "&bd=0&n=0&ifm=png&pri=s&q=200"
      result.getSource()[5].getResize() == "x=" + image6.getWidth()+  "&y=" +  image6.getHeight()+ "&cwc=" + image6.getWidth()+ "&chc=" +  image6.getHeight()+ "&bd=0&n=0&ifm=png&pri=l&q=50"
      result.getSource()[6].getResize() == "x=" + image7.getWidth()+  "&y=" +  image7.getHeight()+ "&cwc=" + image7.getCropWidthCenter()+ "&chc=" +  image7.getCropHeightCenter()+ "&bd=0&n=0&ifm=png&pri=s&q=50"
  }

  def "Cropをfalseにした際の変換処理のテスト" (){
    given: "変換元データ準備"
      def images = new ArrayList<ImagesInfo.Image>()
      def image1 = new ImagesInfo.Image()
      image1.setUrl("http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00id.jpg")
      image1.setHeight(300)
      image1.setWidth(200)
      image1.setCrop(false)
      def image2 = new ImagesInfo.Image()
      image2.setUrl("http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_0093.jpg")
      image2.setHeight(300)
      image2.setWidth(200)
      image2.setCrop(false)
      def image3 = new ImagesInfo.Image()
      image3.setUrl("http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00o2.jpg")
      image3.setHeight(300)
      image3.setWidth(200)
      image3.setCrop(false)
      def image4 = new ImagesInfo.Image()
      image4.setUrl("http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00dd.jpg")
      image4.setHeight(300)
      image4.setWidth(200)
      def image5 = new ImagesInfo.Image()
      image5.setUrl("http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_009d.jpg")
      image5.setHeight(300)
      image5.setWidth(200)
      image5.setQuality(200)
      def image6 = new ImagesInfo.Image()
      image6.setUrl("http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00hv.jpg")
      image6.setHeight(300)
      image6.setWidth(200)
      image6.setPriority("l")
      images.add(image1)
      images.add(image2)
      images.add(image3)
      images.add(image4)
      images.add(image5)
      images.add(image6)
      def param = new ImagesInfo("CQgD-fVDZ_M", images)
    when: "変換実行"
      def result = ImageWizardTemplateImpl.ImageWizardParameter.of(param)
    then: "リクエストパラメータが期待値通りに変換されていることを確認"
      result.getSource()[0].getUrl() == image1.getUrl()
      result.getSource()[1].getUrl() == image2.getUrl()
      result.getSource()[2].getUrl() == image3.getUrl()
      result.getSource()[3].getUrl() == image4.getUrl()
      result.getSource()[4].getUrl() == image5.getUrl()
      result.getSource()[5].getUrl() == image6.getUrl()
      result.getSource()[0].getResize() == "x=" + image1.getWidth() + "&y=" + image1.getHeight() + "&bd=0&n=0&ifm=png"
      result.getSource()[1].getResize() == "x=" + image2.getWidth() + "&y=" + image2.getHeight() + "&bd=0&n=0&ifm=png"
      result.getSource()[2].getResize() == "x=" + image3.getWidth() + "&y=" + image3.getHeight() + "&bd=0&n=0&ifm=png"
      result.getSource()[3].getResize() == "x=" + image4.getWidth()+  "&y=" +  image4.getHeight()+ "&cwc=" + image4.getWidth()+ "&chc=" +  image4.getHeight()+ "&bd=0&n=0&ifm=png&pri=s&q=50"
      result.getSource()[4].getResize() == "x=" + image5.getWidth()+  "&y=" +  image5.getHeight()+ "&cwc=" + image5.getWidth()+ "&chc=" +  image5.getHeight()+ "&bd=0&n=0&ifm=png&pri=s&q=200"
      result.getSource()[5].getResize() == "x=" + image6.getWidth()+  "&y=" +  image6.getHeight()+ "&cwc=" + image6.getWidth()+ "&chc=" +  image6.getHeight()+ "&bd=0&n=0&ifm=png&pri=l&q=50"
  }
}
