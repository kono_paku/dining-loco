package jp.co.yahoo.dining.frontend.service

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.controller.error.ValidationException
import jp.co.yahoo.dining.frontend.domain.KeepItemList
import jp.co.yahoo.dining.frontend.infrastructure.ncookie.NcookieService
import jp.co.yahoo.dining.frontend.repository.KeepItemTemplate

import javax.servlet.http.HttpServletRequest

class KeepItemServiceSpec extends UnitTestBase {
  def expect
  def keepItemService
  def keepItemTemplate
  def request
  def ncookieService
  def setup() {
    expect = new KeepItemList()

    keepItemTemplate = Mock(KeepItemTemplate)
    ncookieService = Mock(NcookieService)
    request = Mock(HttpServletRequest)
    keepItemService = new KeepItemService(keepItemTemplate, ncookieService)
  }

  def "KeepItem情報取得" () {
    setup:
      def cookie = "Y=Ycookie; T=Tcookie"
      keepItemTemplate.getItemList(cookie) >> new KeepItemList()
      ncookieService.validateCookie(request) >> true
    expect:
      keepItemService.getItemList(request, cookie) == expect
  }

  def "KeepItem情報取得(NCookieに異常)" () {
    setup:
      def cookie = "Y=Ycookie; T=Tcookie"
      keepItemTemplate.getItemList(cookie) >> new KeepItemList()
      ncookieService.validateCookie(request) >> false
    when:
      keepItemService.getItemList(request, cookie)
    then:
      thrown(ValidationException)
  }

}
