package jp.co.yahoo.dining.frontend.controller.api

import com.fasterxml.jackson.databind.ObjectMapper
import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.controller.form.ImagesInfo
import jp.co.yahoo.dining.frontend.domain.ResizedImage
import jp.co.yahoo.dining.frontend.service.ImageResizeService
import org.springframework.http.MediaType
import spock.lang.Unroll

import javax.validation.Validation

import static org.springframework.http.HttpStatus.OK
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

class ImageResizeControllerSpec extends UnitTestBase {

  def validator = Validation.buildDefaultValidatorFactory().getValidator()
  def mapper = new ObjectMapper()

  def "画像変換APIのキャッシュテスト"() {
    setup: "テストデータ準備"
      def images = new ArrayList<ImagesInfo.Image>()
      images.add(new ImagesInfo.Image(url: "http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00id.jpg", height: 300, width: 200))
      def param = new ImagesInfo("CQgD-fVDZ_M", images)
      def expext = new ResizedImage()
      def resultset = new ResizedImage.ResultSet()
      resultset.setFirstResultPosition("1")
      resultset.setTotalResultsAvailable("1")
      expext.setResultSet(resultset)
      def service = Mock(ImageResizeService)
      service.resizedImages(param) >> expext
      def controller = new ImageResizeController(service)
      def mvc = standaloneSetup(controller).build()
    when: "画像変換API実行"
      def response = mvc.perform(post("/v1/api/resizeImage").accept("application/json")
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .content(mapper.writeValueAsString(param)))
        .andReturn().response
    then: "ImageWizardAPIで変換した結果が返ってくること"
      response.status == OK.value()
      1 * service.resizedImages(param) >> expext
      response.contentAsString == mapper.writeValueAsString(expext)
  }

  def "ImagesInfoのValidation正常系チェック"() {
    setup: "パラメータセット"
      def images = new ArrayList<ImagesInfo.Image>()
      images.add(new ImagesInfo.Image(url: "http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00id.jpg", height: 300, width: 200))
      def param = new ImagesInfo("CQgD-fVDZ_M", images)
    when: "バリデーション実行"
      def validate = validator.validate(param)
    then: "バリデータの期待値確認"
      validate.size() == 0
      validate == [] as HashSet
  }

  @Unroll
  def "ImagesInfoのValidation異常系チェック"() {
    given: "パラメータセット"
      def images = new ArrayList<ImagesInfo.Image>()
      images.add(new ImagesInfo.Image(url: url, height: height, width: width, quality: quality))
      def param = new ImagesInfo(gid, images)
    when: "バリデーション実行"
      def validate = validator.validate(param)
    then: "バリデーション結果確認"
      validate.size() == size
      validate.first().getMessage() == message
    where: "各フィールドのバリデーションチェック"
      gid           | url                                                   | height | width | quality || size || message
      null          | "http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00id.jpg" | 300    | 200   | null    || 1    || "may not be null"
      "CQgD-fVDZ_M" | null                                                  | 300    | 200   | null    || 1    || "may not be null"
      "CQgD-fVDZ_M" | "http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00id.jpg" | null   | 200   | null    || 1    || "may not be null"
      "CQgD-fVDZ_M" | "http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00id.jpg" | 300    | null  | null    || 1    || "may not be null"
      "CQgD-fVDZ_M" | "http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00id.jpg" | 300    | 200   | 300     || 1    || "must be less than or equal to 100"
      "CQgD-fVDZ_M" | "http://uds.gnst.jp/rest/img/e8s3mkxx0000/t_00id.jpg" | 300    | 200   | -1      || 1    || "must be greater than or equal to 0"
  }

}
