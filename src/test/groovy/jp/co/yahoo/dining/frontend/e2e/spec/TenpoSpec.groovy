package jp.co.yahoo.dining.frontend.e2e.spec

import geb.spock.GebReportingSpec
import jp.co.yahoo.dining.frontend.e2e.page.TenpoPage

/**
 * Created by jchung on 2017/11/21.
 */
class TenpoSpec extends GebReportingSpec {

  /**
   * テストするページの移動と事前作業に設定します。
   *
   * def setup() {
   * given:
   *   to [テストするページクラス]
   * expect:
   *   waitFor { at [テストするページクラス] }
   * }
   */
  def setup() {
    given: "/place/g-CGBtPVWV6EE ページに移動"
    to TenpoPage

    expect: "/place/g-CGBtPVWV6EE ページが表示になってるのか確認"
    waitFor { at TenpoPage }
  }

  /**
   * メソッド名はシナリオの名で定義。
   * @return
   */
  def "Mapクリック"() {
    when:
    clickTenpoMap()

    then: "クリックの結果を確認"
    waitFor { at TenpoPage }
  }

}
