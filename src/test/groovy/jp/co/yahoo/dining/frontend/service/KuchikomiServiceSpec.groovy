package jp.co.yahoo.dining.frontend.service

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.domain.Kuchikomi
import jp.co.yahoo.dining.frontend.repository.KuchikomiTemplate

class KuchikomiServiceSpec extends UnitTestBase {

  def "getKuchikomiListメソッドテスト"() {
    setup:
    def mockTemplate = Mock(KuchikomiTemplate)

    mockTemplate.getKuchikomiList(null, null, null, null, null, null) >> null
    mockTemplate.getKuchikomiList("", Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()) >> new Kuchikomi()
    mockTemplate.getKuchikomiList(*_) >> new Kuchikomi(new Kuchikomi.ResultInfo(), new Kuchikomi.Feature())

    def kuchikomiService = new KuchikomiService(mockTemplate)

    when:
    def actual = kuchikomiService.getKuchikomiList(gid, sort, start, results, guid, customexact1)

    then:
    expect == actual

    where:
    test | gid | sort | start | results | guid | customexact1 | expect
    "Nullの場合" | null | null | null | null | null | null | null
    "空の場合" | "" | Optional.empty() | Optional.empty() | Optional.empty() | Optional.empty() | Optional.empty() | new Kuchikomi()
    "正常データ" | "gid" | Optional.of("sort") | Optional.of("start") | Optional.of("results") | Optional.of("guid") | Optional.of("customexact1") | new Kuchikomi(new Kuchikomi.ResultInfo(), new Kuchikomi.Feature())
  }

}
