package jp.co.yahoo.dining.frontend.infrastructure.rest

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.controller.form.ImageSearchInfo
import jp.co.yahoo.dining.frontend.domain.ImageSearch
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

class ImageSearchTemplateImplSpec extends UnitTestBase {

  def "getImageSearchメソッドテスト"() {
    setup: "テストデータ準備"
      def appId = "appId"
      def imageUrl = "imageUrl"
      def imageInfo = new ImageSearchInfo(gid: "gid", results: 10, start: 1)
      def mockApiConfig = Mock(ImageSearchAPIConfig)
      def mockRT = Mock(RestTemplate)
      def spyTemplateImpl = Spy(ImageSearchTemplateImpl, constructorArgs: [mockRT, mockApiConfig])

      mockApiConfig.appId >> appId
      mockApiConfig.imageList >> imageUrl
      spyTemplateImpl.createRequest(imageUrl) >> UriComponentsBuilder.fromUriString(imageUrl)
        .queryParam("xxx", appId)
        .queryParam("gid", imageInfo.getGid())
        .queryParam("results", imageInfo.getResults())
        .queryParam("start", imageInfo.getStart())
        .queryParam("output", "json")
      def builder = spyTemplateImpl.createRequest(imageUrl)
      spyTemplateImpl.getEntity(ImageSearch.class, builder) >> new ImageSearch()
    when: "テスト実行"
      def actual = spyTemplateImpl.getImageSearch(imageInfo)
      def expect = new ImageSearch()
    then: "結果確認"
      actual == expect
  }

}
