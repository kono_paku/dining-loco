package jp.co.yahoo.dining.frontend.service

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.domain.Promotion
import jp.co.yahoo.dining.frontend.repository.PromotionTemplate

class PromotionServiceSpec extends UnitTestBase {
  def "プロモーション情報取得＿正常" () {
    setup:
      def expect = new Promotion()
      def promotionTemplate = Mock(PromotionTemplate)
      promotionTemplate.getPromotion() >> new Promotion()
      def promotionService = new PromotionService(promotionTemplate)
    expect:
      promotionService.getPromotion() == expect
  }
}
