package jp.co.yahoo.dining.frontend.e2e.page

import geb.Page

/**
 * Created by jchung on 2017/11/22.
 */
class TenpoPage extends Page {

  /**
   * テストコードでこのクラスを呼び出したらそのページに移動する。
   * (ex. to TenpoPage -> TenpoPageクラスの定義したstatic urlに移動)
   */
  static url = "/place/g-CGBtPVWV6EE"

  /**
   * このページが問題があるかどうかを確認する。
   * (ex. at TenpoPage -> TenpoPageクラスの定義したstatic atの内容とおり問題があるかどうかを検査する)
   */
  static at = {
    // assert title == "店舗詳細表示確認用"
    title == "マーサーブランチ(東京都港区六本木/イタリアン（イタリア料理）) - Yahoo!ロコ"
  }

  /**
   * ページのDomリソースをここで定義する。
   * 普通jQueryのSelectorみたいに呼び出せば良い。
   */
  static content = {
    tenpoTitle { $(".headerName__title") }
    tenpoMap { $("a", class: "placeDataMap__link") }
  }

  /**
   * テスト（spec）で使うActionをここで定義する。
   * メソッド名は 動詞+Content名
   */
  def "clickTenpoMap"() {
    tenpoMap.click()
  }

}
