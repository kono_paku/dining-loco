package jp.co.yahoo.dining.frontend.controller.view

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.controller.GlobalControllerAdvice
import jp.co.yahoo.dining.frontend.controller.LocoRequestHeader
import jp.co.yahoo.dining.frontend.domain.PlanList
import jp.co.yahoo.dining.frontend.domain.Promotion
import jp.co.yahoo.dining.frontend.domain.Tenpo
import jp.co.yahoo.dining.frontend.domain.UserInfo
import jp.co.yahoo.dining.frontend.infrastructure.LocoResourceLoader
import jp.co.yahoo.dining.frontend.infrastructure.eappid.EappIdService
import jp.co.yahoo.dining.frontend.service.AdInfeedService
import jp.co.yahoo.dining.frontend.service.PlanService
import jp.co.yahoo.dining.frontend.service.PromotionService
import jp.co.yahoo.dining.frontend.service.TenpoService
import jp.co.yahoo.dining.frontend.service.UserInfoService
import jp.co.yahoo.dining.frontend.service.VideoService
import jp.co.yahoo.dining.frontend.service.WeddingService
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.ui.Model
import org.springframework.validation.support.BindingAwareModelMap

import javax.servlet.http.HttpServletRequest

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view

class TenpoControllerSpec extends UnitTestBase {

  def sut
  def videoService
  def adInfeedService
  def locoResourceLoader
  def planService
  def mockMvc

  def globalControllerAdvice
  def mapper
  def tenpoService
  def weddingService
  def eappIdService
  def locoRequestHeader
  def promotionService
  def userInfoService

  def setup () {
    locoRequestHeader = Mock(LocoRequestHeader)
    eappIdService = Mock(EappIdService)
    eappIdService.getEappId() >> "dummyEappId"
    tenpoService = Mock(TenpoService)
    tenpoService.getTenpoInfo(*_) >> getTenpoModel()
    promotionService = Mock(PromotionService)
    promotionService.getPromotion() >> new Promotion()
    mapper = Mock(LocoResourceLoader)
    userInfoService = Mock(UserInfoService)
    userInfoService.getUserInfo(*_) >> new UserInfo(loginStatus: true, userDetail: new UserInfo.UserDetail(guid: "guid", yid: "yid", area: "area", isPremium: true, premiumType: 4, softbankBundle: 3, yMobileBundle: 4, softBankCooperation: "softBankCooperation"))
    planService = Mock(PlanService)
    planService.getPlanList(*_) >> new PlanList()
    weddingService = Mock(WeddingService)
    globalControllerAdvice = new GlobalControllerAdvice (locoRequestHeader, eappIdService, tenpoService, promotionService, mapper, userInfoService, planService, weddingService)

    videoService = Mock(VideoService)
    adInfeedService = Mock(AdInfeedService)
    locoResourceLoader = Mock(LocoResourceLoader)
    planService = Mock(PlanService)
    sut = new TenpoController(videoService, adInfeedService, locoResourceLoader, planService, locoRequestHeader)

    mockMvc = MockMvcBuilders.standaloneSetup(sut)
      .setControllerAdvice(globalControllerAdvice)
      .build()
  }

  def "店舗詳細"() {
    expect:
    mockMvc.perform(get("/place/g-testTenpo"))
      .andExpect(view().name("place/index"))
  }

  def "店舗詳細_redirect処理_pc"() {
    setup:
    def request = Mock(HttpServletRequest)
    def model = new BindingAwareModelMap()
    def noRedirect = ""
    def utmSource = "dd_local"

    model.addAttribute("tenpo", getTenpoModel())
    locoRequestHeader.getActualDevice() >> "pc"
    expect:
    sut.getPlace(model, request, noRedirect, utmSource) == "place/index"
  }

  def "店舗詳細_redirect処理_smartphone_リダイレクト後"() {
    setup:
    def request = Mock(HttpServletRequest)
    def model = new BindingAwareModelMap()
    def noRedirect = "1"
    def utmSource = "dd_local"

    model.addAttribute("tenpo", getTenpoModel())
    locoRequestHeader.getActualDevice() >> "smartphone"
    expect:
    sut.getPlace(model, request, noRedirect, utmSource) == "place/index"
  }

  def "店舗詳細_redirect処理_smartphone_パラメータ有るが値違い"() {
    setup:
    def request = Mock(HttpServletRequest)
    def model = new BindingAwareModelMap()
    def noRedirect = ""
    def utmSource = "dd_iplocal"
    model.addAttribute("tenpo", getTenpoModel())
    locoRequestHeader.getActualDevice() >> "smartphone"

    expect:
    sut.getPlace(model, request, noRedirect, utmSource) == "place/index"
  }

  def "店舗詳細_redirect処理_smartphone_パラメータ有り_グルメ店舗"() {
    setup:
    def request = Mock(HttpServletRequest)
    def model = new BindingAwareModelMap()
    def tenpo = getTenpoModel()
    def genre = new Tenpo.Genre()
    def property = new Tenpo.Property()
    def feature = tenpo.getFeature()[0]
    def noRedirect = ""
    def utmSource = "dd_local"

    genre.setCode("0108001")
    property.setGenre([genre] as Tenpo.Genre[])
    feature.setProperty(property)
    tenpo.setFeature([feature] as Tenpo.Feature[])
    model.addAttribute("tenpo", tenpo)

    locoRequestHeader.getActualDevice() >> "smartphone"
    expect:
    sut.getPlace(model, request, noRedirect, utmSource) == "place/index"
  }

  def "店舗詳細_redirect処理_smartphone_パラメータ有り(dd_local)_グルメじゃない店舗"() {
    setup:
    def request = Mock(HttpServletRequest)
    def model = new BindingAwareModelMap()
    def tenpo = getTenpoModel()
    def genre = new Tenpo.Genre()
    def property = new Tenpo.Property()
    def feature = tenpo.getFeature()[0]
    def noRedirect = ""
    def utmSource = "dd_local"

    genre.setCode("0308001")
    property.setGenre([genre] as Tenpo.Genre[])
    feature.setProperty(property)
    tenpo.setFeature([feature] as Tenpo.Feature[])
    model.addAttribute("tenpo", tenpo)

    locoRequestHeader.getActualDevice() >> "smartphone"
    request.getRequestURI() >> "/place/g-wYYtA4g8N_g/"
    request.getQueryString() >> "prop=search&ei=utf-8&q=%E6%96%B0%E5%AE%BF&p=%E6%9B%B8%E5%BA%97&utm_source=dd_local&sc_e=sydd_arnd_ttl&lsbe=1"
    sut.approachpoiWeb = "https://approach.yahoo.co.jp/r/zvCRtu"
    sut.locoDomain = "https://loco.yahoo.co.jp"
    expect:
    sut.getPlace(model, request, noRedirect, utmSource) == "redirect:https://approach.yahoo.co.jp/r/zvCRtu?src=https%3A%2F%2Floco.yahoo.co.jp%2Fplace%2Fg-wYYtA4g8N_g%2F%3Fprop%3Dsearch%26ei%3Dutf-8%26q%3D%E6%96%B0%E5%AE%BF%26p%3D%E6%9B%B8%E5%BA%97%26utm_source%3Ddd_local%26sc_e%3Dsydd_arnd_ttl%26lsbe%3D1%26no_redirect%3D1"
  }

  def "店舗詳細_redirect処理_smartphone_パラメータ有り(dd_spot)_グルメじゃない店舗"() {
    setup:
    def request = Mock(HttpServletRequest)
    def model = new BindingAwareModelMap()
    def tenpo = getTenpoModel()
    def genre = new Tenpo.Genre()
    def property = new Tenpo.Property()
    def feature = tenpo.getFeature()[0]
    def noRedirect = ""
    def utmSource = "dd_spot"

    genre.setCode("0308001")
    property.setGenre([genre] as Tenpo.Genre[])
    feature.setProperty(property)
    tenpo.setFeature([feature] as Tenpo.Feature[])
    model.addAttribute("tenpo", tenpo)

    locoRequestHeader.getActualDevice() >> "smartphone"
    request.getRequestURI() >> "/place/g-wYYtA4g8N_g/"
    request.getQueryString() >> "prop=search&ei=utf-8&q=%E6%96%B0%E5%AE%BF&p=%E6%9B%B8%E5%BA%97&utm_source=dd_local&sc_e=sydd_arnd_ttl&lsbe=1"
    sut.approachpoiWeb = "https://approach.yahoo.co.jp/r/zvCRtu"
    sut.locoDomain = "https://loco.yahoo.co.jp"
    expect:
    sut.getPlace(model, request, noRedirect, utmSource) == "redirect:https://approach.yahoo.co.jp/r/zvCRtu?src=https%3A%2F%2Floco.yahoo.co.jp%2Fplace%2Fg-wYYtA4g8N_g%2F%3Fprop%3Dsearch%26ei%3Dutf-8%26q%3D%E6%96%B0%E5%AE%BF%26p%3D%E6%9B%B8%E5%BA%97%26utm_source%3Ddd_local%26sc_e%3Dsydd_arnd_ttl%26lsbe%3D1%26no_redirect%3D1"
  }


  def "地図"() {
    expect:
    mockMvc.perform(get("/place/g-testTenpo/map"))
      .andExpect(view().name("place/map/index"))
  }

  def "クーポン"() {
    expect:
    mockMvc.perform(get("/place/g-testTenpo/coupon"))
      .andExpect(view().name("place/coupon/index"))
  }

  def "写真"() {
    expect:
    mockMvc.perform(get("/place/g-testTenpo/photo"))
      .andExpect(view().name("place/photo/index"))
  }

  def "コース詳細"() {
    expect:
    mockMvc.perform(get("/place/g-testTenpo/course/PtestCourse"))
      .andExpect(view().name("place/course/detail"))
  }

  def "メニュー"() {
    expect:
    mockMvc.perform(get("/place/g-testTenpo/menu"))
      .andExpect(view().name("place/menu/index"))
  }

  def "プラン"() {
    expect:
    mockMvc.perform(get("/place/g-testTenpo/plan"))
      .andExpect(view().name("place/plan/index"))
  }

  def "コース一覧"() {
    expect:
    mockMvc.perform(get("/place/g-testTenpo/course"))
      .andExpect(view().name("place/course/index"))
  }

  def "Marriage"() {
    expect:
    mockMvc.perform(get("/place/g-testTenpo/marriage"))
      .andExpect(view().name("place/marriage/index"))
  }

  def "wedding"() {
    expect:
    mockMvc.perform(get("/place/g-testTenpo/wedding"))
      .andExpect(view().name("place/wedding/index"))
  }

  def "bridal"() {
    expect:
    mockMvc.perform(get("/place/g-testTenpo/bridal"))
      .andExpect(view().name("place/bridal/index"))
  }

  private def getTenpoModel() {
    def cidList = ["cid1", "cid2"]
    def uid = ["uid1", "uid2"]

    def internal = new Tenpo.Internal() as Tenpo.Internal
    internal.cidList = cidList
    internal.uidList = uid

    def feature = new Tenpo.Feature() as Tenpo.Feature
    feature.internal = internal

    return new Tenpo(null, [feature] as Tenpo.Feature[]) as Tenpo
  }

}
