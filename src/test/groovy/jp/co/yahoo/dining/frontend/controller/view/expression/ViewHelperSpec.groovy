package jp.co.yahoo.dining.frontend.controller.view.expression;

import jp.co.yahoo.dining.frontend.UnitTestBase;
import jp.co.yahoo.dining.frontend.controller.view.expression.ViewHelper
import jp.co.yahoo.dining.frontend.domain.Tenpo
import spock.lang.Unroll;


class ViewHelperSpec extends UnitTestBase {
  def view

  def setup() {
    view = new ViewHelper();
  }

  @Unroll
  def 予算に円がない場合円をつける () {
    when: "値段の変換"
    def result = view.getBudget(original);

    then: "円がない場合円がつく"
    result == expect

    where:
    original         | expect
    "1000"           | "1000円"
    "3,800"          | "3,800円"
    "1円"            |  "1円"
    "１３００"        | "１３００円"
    "1000~2999"     | "1000~2999円"
    " 10000　"       | " 10000　円"
  }

  @Unroll
  def 円マークにへの対応 () {
    when: "値段の変換"
    def result = view.getBudget(original);

    then: "円マークへの対応"
    result == expect

    where:
    original         | expect
    "\\1000"          | "¥1000"
    "¥7000"           | "¥7000"
  }

  @Unroll
  def 予算が数字やカンマなど以外の文字の場合そのまま() {
    when: "値段の変換"
    def result = view.getBudget(original);

    then: "予算が数字やカンマ、〜、円以外の文字を含む場合そのまま返す"
    result == original

    where:
    original      | _
    "営業時間外"   | _
    "closed123"   | _
  }

  @Unroll
  def "SPパンくずリストのページタイトル表示確認" () {
    when: "コンテクストパスからタイトルを取得した時"
    def title = view.spBreadcrumbsTitle(servletPath)

    then: "正しく取得できる"
    title == expected

    where:
    servletPath                             | expected
    "/place/g-aaaaaaaaaa"                   | null // 店舗詳細ではこの項目を表示しないのでnullとします
    "/place/g-bbbbbbbbbb/photo"             | "写真"
    "/place/g-cccccccccc/menu"              | "メニュー"
    "/place/g-eeeeeeeeee/course"            | "コース"
    "/place/g-xxxxxxxxxx/course/hogehoge"   | "コース"
    "/place/g-xxxxxxxxxx/plan"              | "プラン"
    "/place/g-xxxxxxxxxx/marriage"          | "式場情報"
    "/place/g-xxxxxxxxxx/wedding"           | "挙式プラン"
    "/place/g-xxxxxxxxxx/bridal"            | "ブライダルフェア"
    "/place/g-xxxxxxxxxx/review"            | "クチコミ"
    "/place/g-xxxxxxxxxx/coupon"            | "クーポン"
    "/place/g-xxxxxxxxxx/map"               | "地図"
    "/place/g-xxxxxxxxxx/course/"           | "コース" // 末尾に/つきパターン
  }

  @Unroll
  def "nonDetailPageの確認" () {
    when: "上の階層のページを取得する"
    def page = view.nonDetailPage(servletPath)

    then: "正しく取得できる"
    page == expected

    where:
    servletPath                           | expected
    "/place/g-xxxxxxxxx/review/1258183"   | "/place/g-xxxxxxxxx/review"
    "/place/g-yyyyyyyyy/course/P111111"   | "/place/g-yyyyyyyyy/course"
  }

  @Unroll
  def "クーポンが有効かどうか確認する" () {
    when: "上の階層のページを取得する"
    def result = view.isValidCoupon(coupon)

    then: "正しく取得できる"
    result == expected

    where:
    coupon                                                     || expected
    new Tenpo.Coupon(displayFlag: false, endDay: "2018/11/11") || false
    new Tenpo.Coupon(displayFlag: true, endDay: null)          || true
    new Tenpo.Coupon(displayFlag: true, endDay: "2018/3/31")   || false
  }

  def "explodeBreakの確認" () {
    when: "<br>を分解する"
    def after = view.explodeBreak(before)

    then: "正しく取得できる"
    after == expected

    where:
    before               | expected
    "a<br>b"             | Arrays.asList("a","b")
  }

  def "unescapeHtmlの確認" () {
    when: "&gt; などを元に戻す"
    def after = view.unescapeHtml(before)

    then: "正しく取得できる"
    after == expected

    where:
    before               | expected
    "(&gt; &lt;)"        | "(> <)"
  }

  @Unroll
  def "getPriceRangeメソッドテスト" () {
    when: "PriceRangeの文字列を取得"
    def priceRange = view.getPriceRange(feature)

    then: "正しく取得できる"
    priceRange == expected

    where:
    feature                                                                                                             | expected
    new Tenpo.Feature(property: new Tenpo.Property(detail: new Tenpo.Detail(priceFrom1: "1000"  , priceTo1: "1500")))   |  "￥1,000〜￥1,500"
    new Tenpo.Feature(property: new Tenpo.Property(detail: new Tenpo.Detail(priceFrom1: "100"  , priceTo1: "150")))     |  "￥100〜￥150"
    new Tenpo.Feature(property: new Tenpo.Property(detail: new Tenpo.Detail(priceFrom1: "1000円", priceTo1: "1500円"))) |  "￥1,000〜￥1,500"
    new Tenpo.Feature(property: new Tenpo.Property(detail: new Tenpo.Detail(priceFrom1: "1000円", priceTo1: "1500")))   |  "￥1,000〜￥1,500"
    new Tenpo.Feature(property: new Tenpo.Property(detail: new Tenpo.Detail(priceFrom1: "1000"  , priceTo1: "1500円"))) |  "￥1,000〜￥1,500"
    new Tenpo.Feature(property: new Tenpo.Property(detail: new Tenpo.Detail(priceFrom1: ""      , priceTo1: "1500")))   |  ""
    new Tenpo.Feature(property: new Tenpo.Property(detail: new Tenpo.Detail(priceFrom1: "1000"  , priceTo1: "")))       |  ""
    new Tenpo.Feature(property: new Tenpo.Property(detail: new Tenpo.Detail(priceFrom1: ""      , priceTo1: "")))       |  ""
    new Tenpo.Feature(property: new Tenpo.Property(detail: new Tenpo.Detail(priceFrom1: "100.01", priceTo1: "1000")))   |  ""
  }

  @Unroll
  def "getStreetAddressメソッドテスト" () {
    when: "streetAddressの文字列を取得"
    def streetAddress = view.getStreetAddress(feature)

    then: "正しく取得できる"
    streetAddress == expected

    where:
    feature                                                                                                                                         | expected
    new Tenpo.Feature(property: new Tenpo.Property(address: "東京都中央区日本橋室町2-2-1 コレド室町1 3F",
      addressElement: [new Tenpo.AddressElement(level: "prefecture", name: "東京都"),new Tenpo.AddressElement(level: "city", name: "中央区")]))       | "日本橋室町2-2-1 コレド室町1 3F"
    new Tenpo.Feature(property: new Tenpo.Property(address: "神奈川県横浜市西区南幸1-11-1 丸藤ビルB1",
      addressElement: [new Tenpo.AddressElement(level: "prefecture", name: "神奈川県"),new Tenpo.AddressElement(level: "city", name: "横浜市")]))     | "西区南幸1-11-1 丸藤ビルB1"
    new Tenpo.Feature(property: new Tenpo.Property(address: "千葉県長生郡白子町古所3290",
      addressElement: [new Tenpo.AddressElement(level: "prefecture", name: "千葉県"),new Tenpo.AddressElement(level: "city", name: "長生郡白子町")])) | "古所3290"
  }
}
