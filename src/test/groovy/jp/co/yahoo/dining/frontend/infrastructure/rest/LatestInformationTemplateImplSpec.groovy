package jp.co.yahoo.dining.frontend.infrastructure.rest

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.domain.LatestInformation
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder


class LatestInformationTemplateImplSpec extends UnitTestBase {

  def "最新情報を取得するテスト" () {
    setup:
      def mockConfig = Mock(IpocaAPIConfig)
      mockConfig.getLatestInformation() >> "URL"
      def rt = Mock(RestTemplate)
      def spyTemplateImpl = Spy(LatestInformationTemplateImpl, constructorArgs: [rt, mockConfig])
      def gid = "gid"
      def uriComponetBuilder = UriComponentsBuilder
        .fromUriString("URL")
        .queryParam("gid", gid)
        .queryParam("output", "json")
      spyTemplateImpl.createRequest("URL") >> uriComponetBuilder
      spyTemplateImpl.getEntity(LatestInformation.class, uriComponetBuilder) >> new LatestInformation()
      def expect = new LatestInformation()
    expect:
      spyTemplateImpl.getLatestInformation(gid) == expect
  }

}
