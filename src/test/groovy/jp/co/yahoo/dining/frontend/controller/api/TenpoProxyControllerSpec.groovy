package jp.co.yahoo.dining.frontend.controller.api

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.domain.Tenpo
import jp.co.yahoo.dining.frontend.service.PlanService
import jp.co.yahoo.dining.frontend.service.TenpoService
import org.springframework.test.web.servlet.setup.MockMvcBuilders

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class TenpoProxyControllerSpec extends UnitTestBase {

  def "APIリクエスト確認_正常系"() {
    setup: "パラメータセット"
      def mockTenpoService = Mock(TenpoService)
      mockTenpoService.getTenpoInfo(*_) >> new Tenpo()
      def mockPlanService = Mock(PlanService)
      def tenpoProxyController = new TenpoProxyController(mockTenpoService, mockPlanService)
      def mockMvc = MockMvcBuilders.standaloneSetup(tenpoProxyController).build()

    expect: "正常レスポンス"
      mockMvc.perform(get("/v1/api/tenpo").param("gid", "gid"))
        .andExpect(status().isOk())
  }
}
