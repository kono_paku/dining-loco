package jp.co.yahoo.dining.frontend.controller.api

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.controller.form.UserInfo

import javax.validation.Validation

class SampleControllerSpec extends UnitTestBase {

  def validator = Validation.buildDefaultValidatorFactory().getValidator()

  def "UserInfoのValidation正常チェック"() {
    when:
    def data = new UserInfo()
    data.setName(name)
    data.setDept(dept)
    data.setId(id)

    def validate = validator.validate(data)

    then:
    validate.size() == size

    where:
    test     | name     | dept          | id      || size
    "全部入力"   | "jchung" | "travel dept" | 1000000 || 0
    "name抜き" | null     | "travel dept" | 1000000 || 0
  }

  def "UserInfoのValidation異常チェック"() {
    when:
    def data = new UserInfo()
    data.setName(name)
    data.setDept(dept)
    data.setId(id)
    def validate = validator.validate(data)

    then:
    validate.size() == size

    validate.first().getMessage() == message

    where:
    test            | name     | dept          | id      || size || message
    "dept抜き"        | "jchung" | null          | 1000000 || 1    || "may not be null"
    "dept異常データ"     | "jchung" | "travel"      | 1000000 || 1    || "must match \".+dept\""
    "id抜き"          | "jchung" | "travel dept" | 0       || 1    || "must be greater than or equal to 1000000"
    "id異常データ"       | "jchung" | "travel dept" | 3000000 || 1    || "must be less than or equal to 2000000"
  }

}
