package jp.co.yahoo.dining.frontend.service

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.domain.Video
import jp.co.yahoo.dining.frontend.repository.VideoTemplate
import org.springframework.http.HttpStatus
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.HttpServerErrorException

import static jp.co.yahoo.dining.frontend.domain.Video.*

class VideoServiceSpec extends UnitTestBase {

  def videoTemplate
  def videoService

  def setup() {
    videoTemplate = Mock(VideoTemplate)
    videoService = new VideoService(videoTemplate)
  }

  def "video Result[] 取得_正常" () {
    setup:
      def uid = "uid"
      def expect = [new Result()] as Result[]
      def result = new Result()
      result.contentId = expect[0].contentId
      def resultSet = new ResultSet()
      resultSet.result = [result] as Result[]
      def video = new Video()
      video.resultSet = resultSet
      videoTemplate.getVideo(uid) >> video
    expect:
      videoService.getVideo(uid) == expect
  }

  def "video Result[] が存在しない_HttpStatusCodeException発生時" () {
    setup:
      def uid = "uid"
      def expect = null
      videoTemplate.getVideo(uid) >> {throw exception}
    expect:
      videoService.getVideo(uid) == expect
    where:
      exception | _
      new HttpServerErrorException(HttpStatus.NOT_FOUND) | _
      new HttpClientErrorException(HttpStatus.BAD_REQUEST) | _
  }

  def "contentIdが存在しない_resultがnull" () {
    setup:
      def uid = "uid"
      def expect = null
      def resultSet = new ResultSet()
      def video = new Video()
      video.resultSet = resultSet
      videoTemplate.getVideo(uid) >> video
    expect:
      videoService.getVideo(uid) == expect
  }

}
