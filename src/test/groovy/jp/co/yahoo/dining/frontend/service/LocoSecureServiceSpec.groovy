package jp.co.yahoo.dining.frontend.service

import jp.co.yahoo.dining.frontend.UnitTestBase
import jp.co.yahoo.dining.frontend.domain.LocoSecure
import jp.co.yahoo.dining.frontend.repository.LocoSecureTemplate

class LocoSecureServiceSpec extends UnitTestBase {
  def "ロコセキュア情報取得＿正常" () {
    setup:
      def guid = "guid"
      def expect = new LocoSecure()
      def locoSecureTemplate = Mock(LocoSecureTemplate)
      locoSecureTemplate.getEncryptYUID(guid) >> new LocoSecure()
      def locoSecureService = new LocoSecureService(locoSecureTemplate)
    expect:
      locoSecureService.getEncryptYUID(guid) == expect
  }
}
