import org.apache.commons.lang3.SystemUtils
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeDriverService
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.firefox.FirefoxProfile
import org.openqa.selenium.firefox.GeckoDriverService
import org.openqa.selenium.os.ExecutableFinder

File findDriverExecutable(browserDriver) {
  def defaultExecutable = new ExecutableFinder().find(browserDriver)
  if (defaultExecutable) {
    new File(defaultExecutable)
  } else {
    new File("drivers").listFiles().findAll {
      it.name.startsWith(browserDriver)
    }.find {
      if (SystemUtils.IS_OS_LINUX) {
        it.name.contains("linux")
      } else if (SystemUtils.IS_OS_MAC) {
        it.name.contains("mac")
      } else if (SystemUtils.IS_OS_WINDOWS) {
        it.name.contains("exe")
      }
    }
  }
}

/**
 * E2Eテストに使えるブラウザを選択する。
 */
environments {
  chrome {
    driver = {
      ChromeDriverService.Builder serviceBuilder = new ChromeDriverService.Builder()
          .usingAnyFreePort()
          .usingDriverExecutable(findDriverExecutable("chromedriver"))
      new ChromeDriver(serviceBuilder.build())
    }
  }
  firefox {
    driver = {
      FirefoxProfile firefoxProfile = new FirefoxProfile()
      firefoxProfile.setPreference("reader.parse-on-load.enabled", true)

      FirefoxOptions firefoxOptions = new FirefoxOptions()
      firefoxOptions.setProfile(firefoxProfile)

      File driverFile = findDriverExecutable("geckodriver")
      System.setProperty(GeckoDriverService.GECKO_DRIVER_EXE_PROPERTY, driverFile.absolutePath)

      GeckoDriverService.Builder serviceBuilder = new GeckoDriverService.Builder()
          .usingAnyFreePort()
          .usingDriverExecutable(findDriverExecutable("geckodriver"))
      new FirefoxDriver(serviceBuilder.build(), firefoxOptions)
    }
  }
}