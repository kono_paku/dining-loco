#!/bin/bash

set -e -u -x

cp manifest-*.yml ../out
cp ../release/target/*.jar ../out