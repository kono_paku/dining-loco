#!/bin/bash

set -u -x

# リリースバージョンの中で最新のものを取得する。取得できない場合はv0とする。
release=v`git tag | sed "s/^v//" | sort -n | tail -n1`
if [ "$release" == "v" ]; then
  release=v0
fi

# 現在のリリースバージョンを元に、最新のバージョン番号を定義する。
release_number=`echo $release | sed s/v//`
new_release=v`expr $release_number + 1`
echo $new_release > ../out/name
echo $new_release > ../out/tag

# 現在のリリースバージョンを元に、リリースノートに記載する文章を作成する。
echo "## Changes" > ../out/body
if [ "$release" == "v0" ]; then
  log=`git log --oneline --merges`
  if [ "$log" == "" ]; then
    git log --oneline --no-merges --pretty=format:"%h - %s" | sed 's/^/- /' >> ../out/body
  else
    git log --oneline --merges --pretty=format:"%h - %s" | sed 's/^/- /' >> ../out/body
  fi
else
  log=`git log $release.. --oneline --merges`
  if [ "$log" == "" ]; then
    git log $release.. --oneline --no-merges --pretty=format:"%h - %s" | sed 's/^/- /' >> ../out/body
  else
    git log $release.. --oneline --merges --pretty=format:"%h - %s" | sed 's/^/- /' >> ../out/body
  fi
fi
