#!/bin/bash

set -e -u -x

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
apt-get -y install nodejs
node -v
npm -v

# stgのために事前nodeビルドしていらないファイルを削除
npm install --prefix src/main/resources/webapp/
npm run build-ci --prefix src/main/resources/webapp
mkdir tmpPublic
mv src/main/resources/webapp/public/pc tmpPublic/
mv src/main/resources/webapp/public/sp tmpPublic/
rm -rf src/main/resources/webapp/*
rm -rf doc/*
mv tmpPublic src/main/resources/webapp/public

# stgのためskipTest、npmbuildしない
mvn package -Dmaven.repo.local=.m2/repository -Dmaven.test.skip=true -Dnpmbuild=false
mv target/*.jar target/${GITHUB_REPO}.jar
cp -r . ../out

# dragon経由しない対応
mv target/${GITHUB_REPO}.jar ../out/${GITHUB_REPO}.jar
