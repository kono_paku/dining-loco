#!/bin/bash

set -e -u -x

apt-get -y update
apt-get -y install software-properties-common
apt-add-repository -y ppa:mozillateam/firefox-next
apt-get -y install xorg xvfb firefox dbus-x11 xfonts-100dpi xfonts-75dpi xfonts-cyrillic ubuntustudio-font-meta fonts-ipafont-mincho fonts-ipafont-gothic fonts-arphic-ukai fonts-arphic-uming fonts-unfonts-core

# Firefox 実行
Xvfb :10 -ac &
export DISPLAY=:10

# Geb テスト実行
{
  mvn clean clover:setup test clover:clover -Pit -Dgeb.env=firefox -Dgeb.build.baseUrl=${BASE_URL} -Dmaven.repo.local=.m2/repository
} || {
  # Firefox 終了
  pkill Xvfb;
}

# Firefox 終了
pkill Xvfb;

# Geb テストレポートスクリーンショットコピー
find target/test-reports/geb -name "*.png" -exec cp {} target/test-reports/geb \;

# レポート移動
html="<!DOCTYPE html><html lang='ja'><head><meta charset="UTF-8"></head><body>"
for i in `ls target/test-reports/geb | grep png`; do
  html+="<h1>$i</h1><br/>"
  html+="<img src='/geb/$i'>"
  html+="<br/><br/>"
done
html+="</body></html>"
echo $html > target/test-reports/geb/index.html

# Clover DashboardにGebテストレポートスクリーンショット追加
mv target/test-reports/geb target/site/clover/
mv target/site/clover ../out
mv ${MANIFEST} ../out
