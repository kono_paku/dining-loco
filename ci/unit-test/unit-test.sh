#!/bin/bash
set -e

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
apt-get -y install nodejs
node -v
npm -v

## run test and generate clover report
mvn clover:setup test clover:clover -Dmaven.repo.local=.m2/repository

## copy coverage report to output directory
mv target/site/clover ../out
mv manifest-coverage.yml ../out
