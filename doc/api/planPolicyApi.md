# プランポリシーAPI仕様書

## 概要
| 項目 | 内容 |
| :--- | :--- |
| 名前 | プランポリシーAPI |
| 概要 | ダイニングのプランのポリシーを取得するAPI |
| エントリポイント | /api/planPolicy |
| プロトコル | https |
| スタイル | REST |

## 業務フローチャート
[planPolicyApiSequence.puml](planPolicyApiSequence.puml)

## 接続API
| Name  | Component      | API Name        | Domain                         | Path                              | IF Specification                              | timeout |
| :--- | :------------ | :------------  | :---------------------------- | :------------------------------- | :---------------------------------- | :----- |
| PlanPolicyAPI | PlanPolicyAPI | プランポリシーAPI |  reservation-restaurant.yahooapis.jp | v1/plan/policy | https://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1471050282 | 5 sec    |

## 仕様
[06-02-01-17-プランポリシー Proxy API](https://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1487631474)
