# クーポンAPI仕様書

## 概要
| 項目 | 内容 |
| :--- | :--- |
| 名前 | RdsigAPI |
| 概要 | 外部のUrlをRdsigにラッピングしてくれるAPI |
| エントリポイント | /api/rdsig |
| プロトコル | https |
| スタイル | REST |

## 業務フローチャート
今後入力:

## 接続API
| Name  | Component      | API Name        | Domain                         | Path                              | IF Specification                              | timeout |
| :--- | :------------ | :------------  | :---------------------------- | :------------------------------- | :---------------------------------- | :----- |
| RdsigAPI | 全社 Rdsig API | RdsigラッピングAPI | http://rdurl.yahoo.co.jp/ | /v1/api | http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1362267556 | 5 sec    |

## POST

### リクエストパラメータ
#### Postパラメータ
| パラメータ名 | 概要 | 必須 | 型 | 指定なしの挙動 | サンプル値 | バリデーション | 説明 | 詳細 |
| :----------- | :--- | :--- | :- | :------------- | :--------- | :------------- | :--- | :--- |
| urls | ラッピングするurlリスト | O | String[] | X | https://ho.com,https://ge.com | 未定 | ラッピングしたい外部サイトURL入力。| 複数のURLをラッピングする時にサンプルみたいにリスト形で入力 |
| label | ラッピングURLの後につけるPath名 | X | String | 空欄 | loco/ho/ge | 未定 | URL Pathの形で入力 | 入力しないとRdsigの基本ドメイン |

### レスポンス（正常系）
#### HTTPステータスコード
| コード | 状態 |
| :----- | :--- |
| 200(OK) | 正常 |

#### レスポンスフィールド

| 第一階層 | 第二階層 | 第三階層 | 型 |  説明 |
| :------- | :----- | :----- | :- |  :--- |
| [] | | | List |  Listの形で格originUrl、wrappingUrlが表示 |
| | originUrl | | String | 元のURL |
| | wrappingUrl | | String | ラッピングURL |

### レスポンス（異常系）
#### HTTPステータスコード
| コード | 状態 |
| :----- | :--- |
| 400 (BAD REQUEST) | バリデーションエラー |
| 500 (INTERNAL SERVER ERROR) | システムエラー |
| 404 (NOT FOUND) | リソースが見つからない（Tomcatが返す） |
| 405 (METHOD NOT ALLOWED) | 許可されていないメソッド（Spring Bootが返す） |
| 4XX, 5XX | 下位レイヤーで出る可能性 |

#### ボディ
| フィールド | 概要 | 型 | 備考 |
| :--------- | :--- | :- | :--- |
| message | エラーメッセージ | String (300) | |


## サンプルリクエスト
実装したら書く
