# 口コミAPI仕様書

## 概要
| 項目 | 内容 |
| :--- | :--- |
| 名前 | 口コミ情報取得API |
| 概要 | コメント検索APIから口コミを取得する。一覧、個別それぞれ取得可能 |
| エントリポイント | /api/kuchikomi/ |
| プロトコル | https |
| スタイル | REST |

## 業務フローチャート
[kuchikomiApiSequence.puml](kuchikomiApiSequence.puml)

## 接続API
| Name  | Component      | API Name        | Domain                         | Path                              | IF Specification                              | timeout |
| :--- | :------------ | :------------  | :---------------------------- | :------------------------------- | :---------------------------------- | :----- |
| KuchikomiAPI | KuchikomiAPI | コメント検索API | http://internal.io.olp.yahooapis.jp | /v2/comment | https://cptl.corp.yahoo.co.jp/x/Tpa_G | 5 sec    |

https://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1466785368

TODO: id指定で1コメントだけ取得する機能があるので、要件を聞いて実装する
