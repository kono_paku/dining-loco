# クーポンAPI仕様書

## 概要
| 項目 | 内容 |
| :--- | :--- |
| 名前 | クーポン情報取得API |
| 概要 | Local Search APIのクーポンとダイニングのクーポンを突合して取得する |
| エントリポイント | /api/coupon/\<gid\> |
| プロトコル | https |
| スタイル | REST |

## 業務フローチャート
[couponApiSequence.puml](couponApiSequence.puml)

## 接続API
| Name  | Component      | API Name        | Domain                         | Path                              | IF Specification                              | timeout |
| :--- | :------------ | :------------  | :---------------------------- | :------------------------------- | :---------------------------------- | :----- |
| TenpoAPI | LocalSearchAPI | 店舗情報取得API | search.olp.yahooapis.jp | /OpenLocalPlatform/V1/localSearch | http://cptl.corp.yahoo.co.jp/x/b1YZD | 5 sec    |

## Table CRUD
| Name | table  | CRUD |
| :-- | :---- | :-- |
| [CouponKVS](http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1376918078) | tenpo_coupon_info | R |

## GET

### リクエストパラメータ
#### URLパラメータ
| パラメータ名 | 必須 | 型 | 説明 |
| :----------- | :--- | :- | :--- |
| gid          | ◯    | String (11) | 店舗のgid |

#### GETパラメータ
| パラメータ名 | 概要 | 必須 | 型 | 指定なしの挙動 | サンプル値 | バリデーション | 説明 | 詳細 |
| :----------- | :--- | :--- | :- | :------------- | :--------- | :------------- | :--- | :--- |
| publish | 掲載中フラグ |  | String (true / false) | true | true | [(true\|false\|1\|0\|yes\|no\|on\|off) のみ。case insensitive。](https://github.com/spring-projects/spring-framework/blob/master/spring-core/src/main/java/org/springframework/core/convert/support/StringToBooleanConverter.java) | 掲載中のみ探す場合true | |

##### sort
※ロコにクーポン表示順仕様確認中

| 値 | 順序 |
| :- | :--- |
| | |

### レスポンス（正常系）
#### HTTPステータスコード
| コード | 状態 |
| :----- | :--- |
| 200(OK) | 正常 |

#### レスポンスフィールド
※ カセットごとにグルーピングするかはロコのやり方に合わせたいので未定です。<br>
※ 提供元の情報（特にロゴ）がnot nullかはロコ側に確認する必要があります。<br>
※ 取得できるURLの優先順位は[このコンフル](http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=453551213)に従います。<br>
※ 表示側でダイニング以外のCPの表示順がローカルサーチの通りという仕様なので順番がそのまま出力されます。

| 第一階層 | 第二階層 | 第三階層 | 型 | null | 説明 |
| :------- | :----- | :----- | :- | :------- | :--- |
| cassettes[] | | | | 0要素のリストの場合あり | 各カセット |
| | cid | | String (50) | | カセットID |
| | name | | String (200) | | 提供元名 |
| | logo | | String (300) | | 提供元のロゴ |
| | isDining | | boolean (true / false) | | カセットがダイニングならばtrue |
| | coupons[] | | | | 各クーポン |
| | | pcUrl | String (300)| | クーポンURL (PC) |
| | | spUrl | String (300)| | クーポンURL (SP) |
| | | linkText | String (?) | | クーポンリンクのテキスト |
| | | lifetimeStart | String (10) | nullの場合あり | クーポン有効期間開始（yyyy-MM-dd） |
| | | lifetimeEnd | String (10) | nullの場合あり | クーポンの有効期間終了（yyyy-MM-dd） |
| | | publish | boolean (true / false) | | クーポン掲載中（true: クーポン掲載中） |
| | | priority | Integer | | クーポンの表示優先順位（値が小さいものを優先） |
| | | userCategory | Integer | | 特定のユーザのみ対象の場合（下の表参照） |

##### userCategory
| 値 | 種別 |
| :- | :--- |
| 0 | 全員対象 |
| 1 | プレミアム会員のみ |

### レスポンス（異常系）
#### HTTPステータスコード
| コード | 状態 |
| :----- | :--- |
| 400 (BAD REQUEST) | バリデーションエラー |
| 500 (INTERNAL SERVER ERROR) | システムエラー |
| 404 (NOT FOUND) | リソースが見つからない（Tomcatが返す） |
| 405 (METHOD NOT ALLOWED) | 許可されていないメソッド（Spring Bootが返す） |
| 4XX, 5XX | 下位レイヤーで出る可能性 |

#### ボディ
| フィールド | 概要 | 型 | 備考 |
| :--------- | :--- | :- | :--- |
| message | エラーメッセージ | String (300) | |


## サンプルリクエスト
実装したら書く
