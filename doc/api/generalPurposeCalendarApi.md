﻿# 祝日取得プロキシ API仕様書

## IF設計書
| Name     | IF                                    |
| :------ | :----------------------------------- |
| Request  | /api/generalPurposeCalendar |
| Response | [IF仕様書](https://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1472088036) |

## 業務フローチャート
[generalPurposeCalendarApiSequence.puml](generalPurposeCalendarApiSequence.puml)

## 接続API
| Name  | Component      | API Name        | Domain                         | Path                              | IF Specification                              | timeout |
| :--- | :------------ | :------------  | :---------------------------- | :------------------------------- | :---------------------------------- | :----- |
| 汎用カレンダーAPI | ABYSS2 | 汎用カレンダーAPI | http://internal.calendarv2.abyss2.yahooapis.jp:8080 | /v2/solr/1bccfd25/cal_jp_basic_v2/select | https://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=660281134 | 5 sec    |

## Cache
| Name    | Where     | Key                    | Value                             | TTL   |
| :---    | :----     | :--------------------- | :------------------------------   | :---  |
| holidays | Memcached | holidays_リクエストパラメータ(year) | 祝日情報 | 30 days |


## Error
| コード | 状態 |
| :----- | :--- |
| 400 (BAD REQUEST) | バリデーションエラー |
| 500 (INTERNAL SERVER ERROR) | システムエラー |
| 404 (NOT FOUND) | リソースが見つからない（Tomcatが返す） |
| 405 (METHOD NOT ALLOWED) | 許可されていないメソッド（Spring Bootが返す） |
| 4XX, 5XX | 下位レイヤーで出る可能性 |
