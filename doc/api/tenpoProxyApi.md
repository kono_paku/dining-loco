# LSBE プロキシ API仕様書

## IF設計書
| Name     | IF                                    |
| :------ | :----------------------------------- |
| Request  | /api/tenpo |
| Response | [IF仕様書](http://cptl.corp.yahoo.co.jp/display/reservegourmet/06-02-01-11-LSBE+Proxy+API) |

## 業務フローチャート
[tenpoProxyApiSequence.puml](tenpoProxyApiSequence.puml)

## 接続API
| Name  | Component      | API Name        | Domain                         | Path                              | IF Specification                              | timeout |
| :--- | :------------ | :------------  | :---------------------------- | :------------------------------- | :---------------------------------- | :----- |
| LSBEプロキシAPI | 店舗情報 | LSBE プロキシ API | http://be.lsbe.yahoo.co.jp/ | /search | http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=549441202 | 5 sec    |

## Error
| コード | 状態 |
| :----- | :--- |
| 400 (BAD REQUEST) | バリデーションエラー |
| 500 (INTERNAL SERVER ERROR) | システムエラー |
| 404 (NOT FOUND) | リソースが見つからない（Tomcatが返す） |
| 405 (METHOD NOT ALLOWED) | 許可されていないメソッド（Spring Bootが返す） |
| 4XX, 5XX | 下位レイヤーで出る可能性 |
