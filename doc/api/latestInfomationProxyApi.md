# 最新情報取得プロキシAPI仕様書

## 概要
| 項目 | 内容 |
| :--- | :--- |
| 名前 | 最新情報取得プロキシAPI |
| 概要 | IpocaAPIを使用して最新情報を取得する |
| エントリポイント | /api/latestInformation |
| プロトコル | https |
| スタイル | REST |

## IF設計書
| Name     | IF                                    |
| :------ | :----------------------------------- |
| Request  | /api/latestInfomation |
| Response | [IF仕様書](https://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1472087702) |

## 業務フローチャート
[latestInformationProxyApiSequence.puml](latestInformationProxyApiSequence.puml)

## 接続API
| Name  | Component      | API Name        | Domain                         | Path                              | IF Specification                              | timeout |
| :--- | :------------ | :------------  | :---------------------------- | :------------------------------- | :---------------------------------- | :----- |
| 最新情報取得API | IpocaAPI | 最新情報取得API | http://yapi.loco.yahoo.co.jp/ | /v1/stock/item | https://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1436515996 | 5 sec    |

## Error
| コード | 状態 |
| :----- | :--- |
| 400 (BAD REQUEST) | バリデーションエラー |
| 500 (INTERNAL SERVER ERROR) | システムエラー |
| 404 (NOT FOUND) | リソースが見つからない |
| 405 (METHOD NOT ALLOWED) | 許可されていないメソッド |

