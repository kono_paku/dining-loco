# 画像リサイズAPI仕様書

## 概要
| 項目 | 内容 |
| :--- | :--- |
| 名前 | 画像リサイズAPI |
| 概要 | ImageWizardを使って画像をリサイズしてURLを返す |
| エントリポイント | /api/imageReize |
| プロトコル | https |
| スタイル | REST |

## IF設計書
| Name     | IF                                    |
| :------ | :----------------------------------- |
| Request  | /api/imageResize |
| Response | [画面設計書](http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1461887591) |

## 業務フローチャート
[imageResizeApiSequence.puml](imageResizeApiSequence.puml)

## 接続API
| Name  | Component      | API Name        | Domain                         | Path                              | IF Specification                              | timeout |
| :--- | :------------ | :------------  | :---------------------------- | :------------------------------- | :---------------------------------- | :----- |
| 画像変換API | ImageWizard | 画像変換API | utilapi.iwiz.yahoo.co.jp | /Iwiz/V1/signUrl | http://cptl.corp.yahoo.co.jp/x/d7avV | 5 sec    |

## Cache
| Name    | Where     | Key                    | Value                             | TTL   |
| :---    | :----     | :--------------------- | :------------------------------   | :---  |
| ImageResizeCache | Memcached | image_\<\<Gid\>_\<URL\>_\<width\>x\<height\>のハッシュ\>  | 店舗情報(Class名があったら書く？) | 30sec |

## Error

| Name    | Status     | Error Massage                                  |
| :-----  | :-------   | :------------                                  |
| Error99 | 500        | 画像変換に失敗しました |
