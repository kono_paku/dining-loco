# プラン検索API仕様書

## 概要
| 項目 | 内容 |
| :--- | :--- |
| 名前 | プラン検索API |
| 概要 | ダイニングの時間在庫情報を取得する |
| エントリポイント | /api/planStock |
| プロトコル | https |
| スタイル | REST |

## 業務フローチャート
[planStockApiSequence.puml](planStockApiSequence.puml)

## 接続API
| Name  | Component      | API Name        | Domain                         | Path                              | IF Specification                              | timeout |
| :--- | :------------ | :------------  | :---------------------------- | :------------------------------- | :---------------------------------- | :----- |
| PlanStockAPI | PlanStockAPI | プラン検索API |  srchapi.rsv.yahooapis.jp | /v1/search/plan | http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1120622616 | 5 sec    |

## 仕様
[06-02-01-12-プラン検索 API Proxy](http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1464530782)
