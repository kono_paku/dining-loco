# 画像リサイズAPI仕様書

## 概要
| 項目 | 内容 |
| :--- | :--- |
| 名前 | 画像検索API |
| 概要 | POTARA用画像検索APIのプロクシ |
| エントリポイント | /api/imageSearch |
| プロトコル | https |
| スタイル | REST |

## IF設計書
| Name     | IF                                    |
| :------ | :----------------------------------- |
| Request  | /api/imageResize |
| Response | [IF設計書](http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1466438545) |

## 業務フローチャート
[imageSearchApiSequence.puml](imageSearchApiSequence.puml)

## 接続API
| Name  | Component      | API Name        | Domain                         | Path                              | IF Specification                              | timeout |
| :--- | :------------ | :------------  | :---------------------------- | :------------------------------- | :---------------------------------- | :----- |
| 画像検索API | InternalYahooAPI | 画像検索API | http://internal.io.olp.yahooapis.jp | /v2/image | http://cptl.corp.yahoo.co.jp/x/U60jGQ | 5 sec    |

## Cache
| Name    | Where     | Key                    | Value                             | TTL   |
| :---    | :----     | :--------------------- | :------------------------------   | :---  |
| - | - | - | - | - |

## Error

| Name    | Status     | Error Massage                                  |
| :-----  | :-------   | :------------                                  |
| Error99 | 500        | 画像検索に失敗しました |
