# 地図ページ仕様書

## 業務フローチャート
[mapSequence.puml](mapSequence.puml)

## IF設計書
| Name     | IF                                    |
| :------ | :----------------------------------- |
| Request  | http://cptl.corp.yahoo.co.jp/x/TgGiUw |
| Response | 画面設計書へのリンク                     |

## 接続API
| Name  | Component      | API Name        | Domain                         | Path                              | IF Specification                              | timeout |
| :--- | :------------ | :------------  | :---------------------------- | :------------------------------- | :---------------------------------- | :----- |

## Cache
| Name    | Where     | Key                    | Value                             | TTL   |
| :---    | :----     | :--------------------- | :------------------------------   | :---  |

## Table CRUD
    フロントエンドコンポーネントのため無し
| Name | table  | CRUD |
| :-- | :---- | :-- |
|      |        |      |

## Error
| Name    | View       | Error Massage                                  |
| :-----  | :-------   | :------------                                  |
| Error01 | 画面設計01 | システムエラー                   |