# 店舗詳細ページ仕様書

    ※あくまでコントローラーのレスポンスを書くのでajax取得部分は書きません。

## 業務フローチャート
[tenpoDetailSequence.puml](tenpoDetailSequence.puml)

## IF設計書
| Name     | IF                                    |
| :------ | :----------------------------------- |
| Request  | /detail/\<Gid\> |
| Response | [画面設計書](http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1393263339) |

## 接続API
| Name  | Component      | API Name        | Domain                         | Path                              | IF Specification                              | timeout |
| :--- | :------------ | :------------  | :---------------------------- | :------------------------------- | :---------------------------------- | :----- |
| TenpoAPI | LSBE API | 店舗情報取得API | be.lsbe.yahoo.co.jp | /search | http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1431200732 | 5 sec    |
| CourseAPI | CourseAPI | コース一覧取得API | 未定 | 未定 | http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=975820719 | ? sec |
| UserPhotoAPI | 投稿写真 API？ | 投稿写真API? | 未定（写真チームと連携必要） | 未定（写真チームと連携必要） | ？ | ?sec    |
| ImageWizard | ImageWizard | 写真サイズ変更API | utilapi.iwiz.yahoo.co.jp | /Iwiz/V1/signUrl | http://cptl.corp.yahoo.co.jp/display/devman/ImageWizard | ?sec    |

## バインド情報
| Name | Description |
| :--- | :---------- |
| tenpo | 店舗情報 |
| plans | プラン情報 |
| resizedTenpoPhotos | リサイズ後店舗写真 |
| resizedUserPhotos | リサイズ後投稿写真 |

## Cache
| Name    | Where     | Key                    | Value                             | TTL   |
| :---    | :----     | :--------------------- | :------------------------------   | :---  |
| TenpoCache | Memcached | tenpo_get_\<Gid\>  | 店舗情報(Class名があったら書く？) | 30sec |
| ReviewCache | Memcached | review_get_\<Gid\>  | 口コミ情報(Class名があったら書く？) | 30sec |
| UserPhotoCache | Memcached | user_photos_get_\<Gid\>  | 投稿写真情報(Class名があったら書く？) | 30sec |
| ResizedPhotoCache | Memcached | resized_photo_get_\<Gid\>\_\<hashedUrl\>\_\<size\>  | リサイズ後写真URL | 30sec |

## Table CRUD
    フロントエンドコンポーネントのため無し
| Name | table  | CRUD |
| :-- | :---- | :-- |
|      |        |      |

## Error
    ※エラー系の表示は決まってから書きます。一旦暫定で全て汎用エラーとします。

| Name    | View       | Error Massage                                  |
| :-----  | :-------   | :------------                                  |
| Error99 | 画面設計01 | エラーが発生しました |
