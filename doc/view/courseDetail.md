# コース詳細ページ仕様書

## 業務フローチャート
[courseDetailSequence.puml](courseDetailSequence.puml)

## IF設計書
| Name        | IF                                                                   |
|:------------|:---------------------------------------------------------------------|
| Request     | http://cptl.corp.yahoo.co.jp/x/TgGiUw                                |
| Response PC | http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1406000752 |
| Response SP | http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1397918932 |

## 接続API
| Name  | Component               | API Name          | Domain                | Path                                                   | IF Specification                     | timeout |
|:------|:------------------------|:------------------|:----------------------|:-------------------------------------------------------|:-------------------------------------|:--------|
| API01 | lifetoolAPI(planDetail) | コース情報取得API | TBD                   | /v1/plan/detail                                        | http://cptl.corp.yahoo.co.jp/x/e99ZV | 1000ms  |
| API02 | ImageWizard             | 画像リサイズAPI   | http://{EDGE-domain}/ | [Resize option]/{PREFIX}/{service name}/{path to file} | http://cptl.corp.yahoo.co.jp/x/e99ZV | 1000ms  |

※その他、店舗詳細ページの共通モジュールで参照するAPIを含む

## Cache
| Name    | Where     | Key                                | Value                               | TTL  |
|:--------|:----------|:-----------------------------------|:------------------------------------|:-----|
| Cache01 | Memcached | course\_get\_\{gid\}\_\{courseId\} | コース情報(Class名があったら書く？) | 300s |

※ コース情報取得API の タイムアウト値は暫定
※ https://jira.corp.yahoo.co.jp/browse/POTARA-861

## Table CRUD
    フロントエンドコンポーネントのため無し
| Name | table | CRUD |
|:-----|:------|:-----|
|      |       |      |

## Error
| Name    | View       | Error Massage                          |
|:--------|:-----------|:---------------------------------------|
| Error01 | 画面設計01 | システムエラーが発生しました。(要調整) |
