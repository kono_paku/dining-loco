# 式場

## 式場情報を表示
- トリガー
    - 画面表示時
    - URLパスが/place/g-\<GID\>/marriage/の場合
- フロー
    - 該当店舗の式場情報を表示する
    - CPにゼクシィがなかった場合、301リダイレクトで店舗トップへ遷移
    - 情報が不足しているなどエラーだった場合、エラー画面を表示


# 挙式プラン

## 挙式プランを表示
- トリガー
    - 画面表示時
    - URLパスが/place/g-\<GID\>/wedding/の場合
- フロー
    - 該当店舗の挙式プラン情報を表示する
    - CPにゼクシィがなかった場合、301リダイレクトで店舗トップへ遷移
    - 情報が不足しているなどエラーだった場合、エラー画面を表示

## 式場体験者レポートを表示
- トリガー
    - 画面表示時
    - URLパスが/place/g-\<GID\>/wedding/の場合
- フロー
    - 新着順6件の式場体験者レポートを表示する
    - 式場体験者レポートへのページ内リンクを設置する
    - 情報が不足しているなどエラーだった場合、ページ内リンクを削除

## 写真表示
- トリガー
    - 画面表示時
    - URLパスが/place/g-\<GID\>/wedding/の場合
- フロー
    - 挙式プランの写真を表示する
    - 情報が不足しているなどエラーだった場合、NoImageを表示（検討中）


# ブライダルフェア

## ブライダルフェア情報取得
- トリガー
    - 画面表示時
    - URLパスが/place/g-\<GID\>/bridal/(?page=\<pageNum\>)の場合
- フロー
    - 該当ブライダルフェア情報（(\<pageNum\>-1)\*10+1 〜 \<pageNum\>\*10 番目のアイテムを選別）を取得
        - 1件以上あった場合、ブライダルフェア情報を表示
        - 情報が不足しているなどエラーだった場合、エラー画面を表示

## ページング機能（PC）
- トリガー
  - 「◀︎」もしくは「▶︎」もしくは数字のリンクをクリック
- フロー
  - 店舗情報よりブライダルフェア情報を取得
    - エラーだった場合、301リダイレクトで店舗トップへ遷移
  - 該当のブライダルフェア情報を表示する
  - 表示件数を「(\<pageNum\>-1)*10+1件〜\<pageNum\>*10」に変更する
  - 総件数を「全＜総件数＞件」に変更する
  - URLのページパラメーター（?page=\<pageNum\>）の数字を増減する。ただしpage=1の場合は削除する
  
## ページング機能（SP）
- トリガー
  - もっとみるをタップする
- フロー
  - 式場情報を元にブライダルフェア情報を取得
    - エラーだった場合、301リダイレクトで店舗トップへ遷移
  - 該当店舗のブライダルフェア情報を表示する
  - 表示件数を「1件〜\<pageNum*10\>」に変更する
  - 総件数を「全＜総件数＞件」に変更する

## 写真表示
- トリガー
  - 画面表示時
- フロー
  - ブライダルフェアの写真を表示する
  - 情報が不足しているなどエラーだった場合、NoImageを表示（検討中）
