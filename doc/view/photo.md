# 写真ギャラリーページ仕様書

## 業務フローチャート
[photoSequence.puml](photoSequence.puml)

## 接続API
| Name  | Component      | API Name        | Domain                         | Path                              | IF Specification                              | timeout |
| :--- | :------------ | :------------  | :---------------------------- | :------------------------------- | :---------------------------------- | :----- |
| API01 | API | 写真取得API | http://loco.yahoo.co.jp | /api/photo/<guid> | https://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1437723814 | 5sec    |
| API02 | API | 画像リサイズAPI | http://loco.yahoo.co.jp | /api/imageResize | https://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1461887591 | 5sec |

## Cache
| Name    | Where     | Key                    | Value                             | TTL   |
| :---    | :----     | :--------------------- | :------------------------------   | :---  |
| Cache01 | Memcached(未確定) | photo_get_\<tenpoId\>(未確定)  | 写真情報 | 30sec |

## Table CRUD
| Name | table  | CRUD |
| :-- | :---- | :-- |
|      |        |      |

## Error
| Name    | View       | Error Massage                                  |
| :-----  | :-------   | :------------                                  |
| Error01 | 画面設計01 | 画像情報を取得できませんでした。               |
| Error02 | 画面設計01 | システムエラー                           |
