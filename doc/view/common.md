# 店舗詳細ページ仕様書

## 業務フローチャート
[commonSequence.puml](commonSequence.puml)

## IF設計書

### Request

#### Parameter
| キー     | 概要                                    |
| :------ | :----------------------------------- |
| gid  | 店舗一意のキー |

#### Cookie
| キー     | 概要                                    |
| :------ | :----------------------------------- |
| _n  | NCookie |

### Bind
| Name | Description |
| :--- | :---------- |
| tenpo | 店舗情報 |
| user | ユーザ情報 |
| eappid | eappid |

### Response

## 接続API
| Name  | Component      | API Name        | Domain                         | Path                              | IF Specification                              | timeout |
| :--- | :------------ | :------------  | :---------------------------- | :------------------------------- | :---------------------------------- | :----- |
| TenpoAPI | LSBE API | 店舗情報取得API | be.lsbe.yahoo.co.jp | /search | http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1431200732 | 5 sec    |

## Cache
| Name    | Where     | Key                    | Value                             | TTL   |
| :---    | :----     | :--------------------- | :------------------------------   | :---  |
| TenpoCache | Memcached | tenpo_get_\<Gid\>  | 店舗情報(Class名があったら書く？) | 30sec |

## Table CRUD
| Name | table | CRUD |
|:---|:---|---:|
| なし | | |

## Error
| Name | View | Error Massage | 対応するException |
|:---|:---|---:|:---|
| Error500 | 画面設計01 | システムエラーが発生しました。 | Throwable |
| Error404 | 画面設計02 | ページが存在しません。 | ResourceNotFoundException |
