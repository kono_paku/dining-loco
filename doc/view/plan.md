# プラン一覧ページ仕様書

## 業務フローチャート
[planSequence.puml](planSequence.puml)

## IF設計書
| Name     | IF                                    |
| :------- | :------------------------------------ |
| Request  | http://cptl.corp.yahoo.co.jp/x/hVNSVg |
| Response | -                                     |

## 接続API
| Name  | Component      | API Name | Domain                     | Path    | IF Specification                     | timeout |
| :---- | :------------- | :------- | :------------------------- | :------ | :----------------------------------- | :------ |
| API01 | LSBE API       | LSBE API | http://be.lsbe.yahoo.co.jp | /search | http://cptl.corp.yahoo.co.jp/x/ss6-I | 1sec    |

## Cache
| Name    | Where     | Key                    | Value                             | TTL (sec) |
| :------ | :-------- | :--------------------- | :-------------------------------- | :-------- |
| Cache01 | Memcached | tenpo\_[gid]           | 店舗情報                          | 600       |

## Table CRUD
    フロントエンドコンポーネントのため無し

## Error
| Name    | View       | Error Massage                                                          |
| :------ | :--------- | :--------------------------------------------------------------------- |
| Error01 | 画面設計01 | システムエラーが発生しました。時間をおいて再度お試しください。         |
