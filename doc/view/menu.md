# メニューページ仕様書

## 業務フローチャート
[menuSequence.puml](menuSequence.puml)

## IF設計書
| Name     | IF                                   |
| :------- | :----------------------------------- |
| Request  | http://cptl.corp.yahoo.co.jp/x/38XPV |
| Response | -                                    |

## 接続API
| Name  | Component      | API Name        | Domain                         | Path                              | IF Specification                     | timeout |
| :---- | :------------- | :-------------- | :----------------------------- | :-------------------------------- | :----------------------------------- | :------ |
| API01 | LocalSearchAPI | 店舗情報取得API | http://search.olp.yahooapis.jp | /OpenLocalPlatform/V1/localSearch | http://cptl.corp.yahoo.co.jp/x/b1YZD | 5sec    |

## Cache
| Name    | Where     | Key                    | Value                             | TTL (sec) |
| :------ | :-------- | :--------------------- | :-------------------------------- | :-------- |
| Cache01 | Memcached | tenpo\_[gid]           | 店舗情報                          | 600       |

## Table CRUD
    フロントエンドコンポーネントのため無し

## Error
| Name    | View       | Error Massage                                                          |
| :------ | :--------- | :--------------------------------------------------------------------- |
| Error01 | 画面設計01 | システムエラーが発生しました。時間をおいて再度お試しください。         |
