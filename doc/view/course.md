# コース一覧ページ仕様書

## 業務フローチャート
[courseSequence.puml](courseSequence.puml)

## IF設計書
| Name     | IF                                   |
|:---------|:-------------------------------------|
| Request  | http://cptl.corp.yahoo.co.jp/x/0cXPV |
| Response | HTML                                 |


## 接続API
| Name      | Component | API Name          | Domain                                      | Path            | IF Specification                     | timeout (sec) |
|:----------|:----------|:------------------|:--------------------------------------------|:----------------|:-------------------------------------|:--------------|
| TenpoAPI  | LSBE API  | 店舗情報取得API   | http://be.lsbe.yahoo.co.jp/                 | /search         | http://cptl.corp.yahoo.co.jp/x/ss6-I | 5             |
| CourceAPI | CourseAPI | コース一覧取得API | https://reservation-restaurant.yahooapis.jp | /v1/plan/detail | http://cptl.corp.yahoo.co.jp/x/e99ZV | 5             |


## Cache
| Name                             | Where     | Key               | Value    | TTL (sec) |
|:---------------------------------|:----------|:------------------|:---------|:----------|
| TenpoCache(全コンポーネント共通) | MemCached | tenpo_get_\<Gid\> | 店舗情報 | ---       |

## Table CRUD
    フロントエンドコンポーネントのため無し

## Error
| Name    | View       | Error Massage                  |
|:--------|:-----------|:-------------------------------|
| Erroe01 | 画面設計01 | 店舗情報の取得に失敗しました。 |
| Error02 | 画面設計02 | コースはまだありません。       |

## 備考

* 予約できるコースがない場合はその旨を伝える文言を表示(エラーではなく通常遷移として扱う)

| Name          | View | Massage                                  |
|:--------------|:-----|:-----------------------------------------|
| NoMatchCourse | -    | ご用意できるコースはございませんでした。 |
