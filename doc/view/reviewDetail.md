# 口コミ詳細ページ仕様書

## 業務フローチャート
[reviewDetailSequence.puml](reviewDetailSequence.puml)

## IF設計書
| Name     | IF                                    |
| :------ | :------------------------------------- |
| Request  | http://cptl.corp.yahoo.co.jp/x/TgGiUw |
| Response | 画面設計書へのリンク                     |

#### Parameter
| キー     | 概要                                    |
| :------ | :----------------------------------- |
| review_id | 口コミ一覧から、もらうreview_id |


## 接続API
| Name  | Component  | API Name  | Domain                        | Path                             | IF Specification                    | timeout |
| :---- | :--------- | :-------- | :---------------------------- | :------------------------------- | :---------------------------------- | :------ |
| なし | | | | | | |


## Cache
| Name    | Where     | Key                    | Value                             | TTL   |
| :---    | :----     | :--------------------- | :------------------------------   | :---  |
| なし | | | | |

## Table CRUD
| Name | table  | CRUD |
| :-- | :---- | :-- |
| なし | | |

## Error
| Name    | View       | Error Massage                                  |
| :-----  | :-------   | :------------                                  |
| Error01 | -    | システムエラーが発生しました                  |
