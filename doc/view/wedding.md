# 式場ページ仕様書
    ※あくまでコントローラーのレスポンスを書くのでajax取得部分は書きません。

## 業務フローチャート
[weddingSequence.puml](weddingSequence.puml)

## IF設計書
| Name     | IF                                    |
| :------ | :----------------------------------- |
| Request  | /place/g-\<Gid\>/marriage/ |
| Request  | /place/g-\<Gid\>/wedding/ |
| Request  | /place/g-\<Gid\>/bridal/?page=\<pageNum\> |
| Response | [画面設計書](http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1432271151) |

## 接続API
| Name  | Component      | API Name        | Domain                         | Path                              | IF Specification                              | timeout |
| :--- | :------------ | :------------  | :---------------------------- | :------------------------------- | :---------------------------------- | :----- |
| TenpoAPI | LSBE API | 店舗情報取得API | be.lsbe.yahoo.co.jp | /search | http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1431200732 | 5 sec    |
| LocalSearchAPI | LocalSearch API | 式場情報取得API | be.olp.yahoo.co.jp | /OpenLocalPlatform/V1/localSearch | http://cptl.corp.yahoo.co.jp/pages/viewpage.action?pageId=1431200732 | 5 sec    |
| ImageWizard | ImageWizard | 写真サイズ変更API | utilapi.iwiz.yahoo.co.jp | /Iwiz/V1/signUrl | http://cptl.corp.yahoo.co.jp/display/devman/ImageWizard | ?sec    |

## バインド情報
| Name | Description |
| :--- | :---------- |
| tenpo | 店舗情報 |
| wedding | 式場情報 |
| resizedTenpoPhotos | リサイズ後店舗写真 |
| resizedWeddingPhotos | リサイズ後式場写真 |

## Cache
| Name    | Where     | Key                    | Value                             | TTL   |
| :---    | :----     | :--------------------- | :------------------------------   | :---  |
| TenpoCache | Memcached | tenpo_get_\<Gid\>  | 店舗情報(Class名があったら書く？) | 30sec |
| WeddingCache | Memcached | wedding_get_\<Gid\>  | 式場情報(Class名があったら書く？) | 30sec |
| ResizedPhotoCache | Memcached | resized_photo_get_\<Gid\>\_\<hashedUrl\>\_\<size\>  | リサイズ後写真URL | 30sec |

## Table CRUD
    フロントエンドコンポーネントのため無し
| Name | table  | CRUD |
| :--- | :---- | :--- |
|      |        |      |

## Error
    ※エラー系の表示は決まってから書きます。一旦暫定で全て汎用エラーとします。
| Name    | View       | Error Massage                                  |
| :-----  | :-------   | :------------                                  |
| Error99 | 画面設計01 | エラーが発生しました |
